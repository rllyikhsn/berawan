@extends('backend.layouts.app')

@section('title', 'DataTables')

@push('style')
    <!-- CSS Libraries -->

    <link rel="stylesheet" href="{{ asset('backend/css/datatables/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/css/datatables/datatables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/css/datatables/select.bootstrap4.min.css') }}">
@endpush

@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Data Tags</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="{{ route('admin') }}">Dashboard</a></div>
                    <div class="breadcrumb-item">Data Tags</div>
                </div>
            </div>

            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Data Tags</h4>
                                <div class="card-header-action">
                                    <a href="{{ route('add-content-tags') }}"><button class="btn btn-icon btn-info">
                                            <i class="fas fa-plus"></i> Tambah
                                        </button></a>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table-striped table" id="table-tags">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Judul</th>
                                                <th>Created At</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('backend/js/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('backend/js/datatables/datatables.bootstrap4.min.js') }}"></script>
    <script src="{{ asset('backend/js/datatables/datatables.select.min.js') }}"></script>

    <script src="{{ asset('backend/library/jquery-ui-dist/jquery-ui.min.js') }}"></script>

    <script src="{{ asset('backend/library/sweetalert/dist/sweetalert.min.js') }}"></script>
    <!-- Page Specific JS File -->
    <script src="{{ asset('backend/js/page/tags/content-tags.js') }}"></script>
@endpush
