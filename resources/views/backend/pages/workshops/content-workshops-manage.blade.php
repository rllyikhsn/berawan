@extends('backend.layouts.app')

@section('title', 'Advanced Forms')

@push('style')
    <!-- CSS Libraries -->
    <link rel="stylesheet" href="{{ asset('backend/library/summernote/dist/summernote-bs4.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/library/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/library/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/library/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/library/selectric/public/selectric.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/library/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/library/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}">
@endpush

@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Manage Workshop</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="{{ route('admin') }}">Dashboard</a></div>
                    <div class="breadcrumb-item active"><a href="{{ route('content-workshops') }}">Data Workshop</a></div>
                    <div class="breadcrumb-item">Manage Workshop</div>
                </div>
            </div>

            <div class="section-body">
                <form action="" method="POST" id="form-workshops">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Workshop</h4>
                                    <div class="card-header-action">
                                        <button class="btn btn-icon btn-warning btn-cancel">
                                            <i class="fas fa-cancel"></i> Batal
                                        </button>
                                        <button type="submit" class="btn btn-icon btn-info">
                                            <i class="fas fa-save"></i> Simpan
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-9 col-lg-9">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Informasi Workshop</h4>
                                </div>
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>Nama Workshop</label>
                                        <input type="text" class="form-control" name="name" id="name" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Alamat Workshop</label>
                                        <textarea class="form-control" style="height: 100px;" name="alamat" id="alamat" required></textarea>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label>No Telepon Workshop</label>
                                                <input type="text" class="form-control" name="no_telepon" id="no_telepon"
                                                    required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-3 col-lg-3">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Terbitkan</h4>
                                    <div class="card-header-action">
                                        <a data-collapse="#publication-collapse" class="btn btn-icon btn-info"
                                            href="#">
                                            <i class="fas fa-minus"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="collapse show" id="publication-collapse">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label>Draft</label>
                                            <select class="form-control" name="is_draft" id="is_draft">
                                                <option value="1">Ya</option>
                                                <option value="0">Tidak</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Publikasi</label>
                                            <select class="form-control" name="is_published" id="is_published">
                                                <option value="1">Ya</option>
                                                <option value="0">Tidak</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Waktu dan Tanggal Publikasi</label>
                                            <input type="text" class="form-control datetimepicker" name="published_at"
                                                id="published_at" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('backend/library/summernote/dist/summernote-bs4.js') }}"></script>
    <script src="{{ asset('backend/library/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('backend/library/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('backend/library/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
    <script src="{{ asset('backend/library/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('backend/library/selectric/public/jquery.selectric.min.js') }}"></script>

    <script src="{{ asset('backend/library/sweetalert/dist/sweetalert.min.js') }}"></script>
    <!-- Page Specific JS File -->
    <script src="{{ asset('backend/js/page/workshops/content-workshops-manage.js') }}"></script>
@endpush
