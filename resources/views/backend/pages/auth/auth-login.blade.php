@extends('backend.layouts.auth')

@section('title', 'Login')

@push('style')
    <!-- CSS Libraries -->
    <link rel="stylesheet" href="{{ asset('backend/library/bootstrap-social/bootstrap-social.css') }}">
@endpush

@section('main')
    <div class="card card-primary">
        <div class="card-header">
            <h4>Login</h4>
        </div>

        <div class="card-body">
            <form method="POST" action="{{ route('login') }}" class="needs-validation" novalidate="">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input id="email" type="email" value="{{ Session::get('email') }}"class="form-control" name="email" tabindex="1" required
                        autofocus>
                    <div class="invalid-feedback">
                        Please fill in your email
                    </div>
                </div>

                <div class="form-group">
                    <div class="d-block">
                        <label for="password" class="control-label">Password</label>
                        <div class="float-right">
                            <a href="auth-forgot-password.html" class="text-small">
                                Forgot Password?
                            </a>
                        </div>
                    </div>
                    <input id="password" type="password" class="form-control" name="password" tabindex="2" required>
                    <div class="invalid-feedback">
                        please fill in your password
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                        Login
                    </button>
                </div>
            </form>
        </div>
    </div>
    <div class="text-muted mt-5 text-center">
        Don't have an account? <a href="auth-register.html">Create One</a>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script>
        // var urlApi = "/api/auth/login";
        // $("#form-auth-login").on("submit", function(e) {
        //     e.preventDefault(); // prevent the form from submitting normally

        //     var formData = new FormData(this);
        //     $.ajax({
        //         url: urlApi,
        //         type: "POST",
        //         data: formData,
        //         contentType: false,
        //         processData: false,
        //         success: function(result) {
        //             console.log(result);
        //             swal("Berhasil Simpan Data", {
        //                 icon: "success",
        //                 text: result.message,
        //             }).then((data) => {
        //                 window.location.href = '/admin/content-announcements';
        //                 // history.replaceState(
        //                 //     {},
        //                 //     null,
        //                 //     (window.location.href =
        //                 //         url + "/" + result.data.id)
        //                 // );
        //             });
        //         },
        //         error: function(jqXHR, textStatus, errorThrown) {

        //         },
        //     });
        // });
    </script>
    <!-- Page Specific JS File -->
    <script src="{{ asset('backend/library/sweetalert/dist/sweetalert.min.js') }}"></script>
@endpush
