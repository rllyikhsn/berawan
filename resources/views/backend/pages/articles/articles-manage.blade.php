@extends('backend.layouts.app')

@section('title', 'Advanced Forms')

@push('style')
    <!-- CSS Libraries -->
    <link rel="stylesheet" href="{{ asset('backend/library/summernote/dist/summernote-bs4.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/library/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/library/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/library/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/library/selectric/public/selectric.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/library/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/library/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}">

    <style>
        .skeleton-loading {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            height: 200px;
        }

        .skeleton-header,
        .skeleton-content,
        .skeleton-footer {
            width: 100%;
            height: 20px;
            margin-bottom: 10px;
            border-radius: 4px;
            background-color: #eee;
        }

        .skeleton-content {
            height: 80px;
        }
    </style>
@endpush

@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Manage Artikel</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="{{ route('admin') }}">Dashboard</a></div>
                    <div class="breadcrumb-item active"><a href="{{ route('articles') }}">Data Artikel</a></div>
                    <div class="breadcrumb-item">Manage Artikel</div>
                </div>
            </div>

            <div class="section-body">
                <form action="" method="POST" id="form-articles">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Artikel</h4>
                                    <div class="card-header-action">
                                        <a class="btn btn-icon btn-warning btn-cancel">
                                            <i class="fas fa-cancel"></i> Batal
                                        </a>
                                        <button type="submit" class="btn btn-icon btn-info">
                                            <i class="fas fa-save"></i> Simpan
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-9 col-lg-9">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Informasi Artikel</h4>
                                </div>
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>Judul Artikel</label>
                                        <input type="text" class="form-control" name="title" id="title" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Article Categories</label>
                                        <select class="form-control select2" multiple="" name="article_categories"
                                            id="article_categories">
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Article Tags</label>
                                        <select class="form-control select2" multiple="" name="article_tags"
                                            id="article_tags">
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label>Kontent Pendek</label>
                                        <textarea class="form-control" style="height: 100px;" name="short_content" id="short_content"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Isi Artikel</label>
                                        <textarea class="summernote" name="long_content" id="long_content"></textarea>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label>Cover Image</label>
                                                <div id="image-preview-pinned" class="image-preview">
                                                    <label for="image-upload" id="image-label">Choose File</label>
                                                    <input type="text" autocomplete="off" name="pinned_image"
                                                        id="image" data-element-form="image"
                                                        data-element-preview="image-preview-pinned" data-toggle="modal"
                                                        data-target="#modalMediaFiles" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-3 col-lg-3">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Terbitkan</h4>
                                    <div class="card-header-action">
                                        <a data-collapse="#publication-collapse" class="btn btn-icon btn-info"
                                            href="#">
                                            <i class="fas fa-minus"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="collapse show" id="publication-collapse">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label>Draft</label>
                                            <select class="form-control selectric" name="is_draft" id="is_draft">
                                                <option value="1">Ya</option>
                                                <option value="0">Tidak</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Publikasi</label>
                                            <select class="form-control selectric" name="is_published" id="is_published">
                                                <option value="1">Ya</option>
                                                <option value="0">Tidak</option>
                                            </select>
                                        </div>
                                        <div class="form-group" id="published_date" style="display: none;">
                                            <label>Waktu dan Tanggal Publikasi</label>
                                            <input type="text" class="form-control datetimepicker" name="published_at"
                                                id="published_at" disabled>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-9 col-lg-9">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Gallery Artikel</h4>
                                    <button class="btn btn-primary btn-add-gallery" type="button"
                                        id="add-gallery-item"><i class="fas fa-plus"></i></button>
                                </div>
                                <div class="card-body">
                                    <div class="row" id="listGalleryArticle">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        <!-- Media Files -->
        @include('backend.components.media-files')
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('backend/library/summernote/dist/summernote-bs4.js') }}"></script>
    <script src="{{ asset('backend/library/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('backend/library/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('backend/library/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
    <script src="{{ asset('backend/library/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('backend/library/selectric/public/jquery.selectric.min.js') }}"></script>

    <script src="{{ asset('backend/library/sweetalert/dist/sweetalert.min.js') }}"></script>
    <script src="{{ asset('backend/js/media-files/media-files.js') }}"></script>
    <script src="{{ asset('backend/js/utils/content.js') }}"></script>
    <!-- Page Specific JS File -->
    <script src="{{ asset('backend/js/page/articles/articles-manage.js') }}"></script>
@endpush
