@extends('backend.layouts.app')

@section('title', 'DataTables')

@push('style')
    <!-- CSS Libraries -->

    <link rel="stylesheet" href="{{ asset('backend/css/datatables/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/css/datatables/datatables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/css/datatables/select.bootstrap4.min.css') }}">
@endpush
@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Setting Logo Identitas Website</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="{{ route('admin') }}">Dashboard</a></div>
                    <div class="breadcrumb-item">Logo Identity </div>
                </div>
            </div>

            <div class="section-body">
                <form action="" method="POST" id="form-content-logo-identity">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Identitas Setting</h4>
                                    <div class="card-header-action">
                                        <button class="btn btn-icon btn-warning btn-cancel">
                                            <i class="fas fa-cancel"></i> Batal
                                        </button>
                                        <button type="submit" class="btn btn-icon btn-info">
                                            <i class="fas fa-save"></i> Simpan
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 col-lg-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Logo Header</h4>
                                    <div class="card-header-action">
                                        <a data-collapse="#header-collapse" class="btn btn-icon btn-info" href="#">
                                            <i class="fas fa-minus"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="collapse show" id="header-collapse">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label>FAVICON</label>
                                            <div id="image-preview-favicon" class="image-preview">
                                                <label for="image-upload" id="image-label">Choose File</label>
                                                <input type="text" autocomplete="off" name="favicon_image"
                                                    id="image_favicon" data-element-form="image_favicon"
                                                    data-element-preview="image-preview-favicon" data-toggle="modal"
                                                    data-target="#modalMediaFiles" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>LOGO</label>
                                            <div id="image-preview-logo" class="image-preview">
                                                <label for="image-upload" id="image-label">Choose File</label>
                                                <input type="text" autocomplete="off" name="logo_image" id="image_logo"
                                                    data-element-form="image_logo" data-element-preview="image-preview-logo"
                                                    data-toggle="modal" data-target="#modalMediaFiles" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>LOGO FRONT</label>
                                            <div id="image-preview-logo-front" class="image-preview">
                                                <label for="image-upload" id="image-label">Choose File</label>
                                                <input type="text" autocomplete="off" name="logo_front_image"
                                                    id="image_logo_front" data-element-form="image_logo_front"
                                                    data-element-preview="image-preview-logo-front" data-toggle="modal"
                                                    data-target="#modalMediaFiles" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-8 col-lg-8">
                            <div class="card">
                                <div class="card-header">
                                    <h4>About</h4>
                                    <div class="card-header-action">
                                        <a data-collapse="#about-collapse" class="btn btn-icon btn-info" href="#">
                                            <i class="fas fa-minus"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="collapse show" id="about-collapse">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label>About Image</label>
                                            <div id="image-preview-about" class="image-preview">
                                                <label for="image-upload" id="image-label">Choose File</label>
                                                <input type="text" autocomplete="off" name="about_image" id="image_about"
                                                    data-element-form="image_about"
                                                    data-element-preview="image-preview-about" data-toggle="modal"
                                                    data-target="#modalMediaFiles" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>About Content</label>
                                            <textarea class="form-control" style="height: 100px;" name="about_content" id="about_content"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <h4>Footer</h4>
                                    <div class="card-header-action">
                                        <a data-collapse="#footer-collapse" class="btn btn-icon btn-info" href="#">
                                            <i class="fas fa-minus"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="collapse show" id="footer-collapse">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label>Footer Image</label>
                                            <div id="image-preview-footer" class="image-preview">
                                                <label for="image-upload" id="image-label">Choose File</label>
                                                <input type="text" autocomplete="off" name="footer_image"
                                                    id="image_footer" data-element-form="image_footer"
                                                    data-element-preview="image-preview-footer" data-toggle="modal"
                                                    data-target="#modalMediaFiles" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Footer Content</label>
                                            <textarea class="form-control" style="height: 100px;" name="footer_content" id="footer_content"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <h4>Service</h4>
                                    <div class="card-header-action">
                                        <a data-collapse="#service-collapse" class="btn btn-icon btn-info"
                                            href="#">
                                            <i class="fas fa-minus"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="collapse show" id="service-collapse">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label>Service Image</label>
                                            <div id="image-preview-service" class="image-preview">
                                                <label for="image-upload" id="image-label">Choose File</label>
                                                <input type="text" autocomplete="off" name="service_image"
                                                    id="image_service" data-element-form="image_service"
                                                    data-element-preview="image-preview-service" data-toggle="modal"
                                                    data-target="#modalMediaFiles" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Service Content</label>
                                            <textarea class="form-control" style="height: 100px;" name="service" id="service"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
        <!-- Media Files -->
        @include('backend.components.media-files')
    </div>

@endsection

@push('scripts')
    <script src="{{ asset('backend/library/jquery-ui-dist/jquery-ui.min.js') }}"></script>

    <script src="{{ asset('backend/library/sweetalert/dist/sweetalert.min.js') }}"></script>

    <script src="{{ asset('backend/js/media-files/media-files.js') }}"></script>

    <script>
        var urlWeb = "/admin/content-management/logo-identity";
        $.ajax({
            url: "/api/content/detail",
            type: "GET",
            contentType: false,
            processData: false,
            success: function(result) {
                if (result.data.favicon_image) {
                    renderImage("image-preview-favicon", '/images/web/' + result.data.favicon_image,
                        "image_favicon",
                        result.data.favicon_image);
                }

                if (result.data.logo_image) {
                    renderImage("image-preview-logo", '/images/web/' + result.data.logo_image,
                        "image_logo",
                        result.data.logo_image);
                }

                if (result.data.logo_front_image) {
                    renderImage("image-preview-logo-front", '/images/web/' + result.data.logo_front_image,
                        "image_front_logo",
                        result.data.logo_front_image);
                }

                if (result.data.about_image) {
                    renderImage("image-preview-about", '/images/web/' + result.data.about_image,
                        "image_about",
                        result.data.about_image);
                }
                $("#about_content").val(result.data.about_content);

                if (result.data.footer_image) {
                    renderImage("image-preview-footer", '/images/web/' + result.data.footer_image,
                        "image_footer",
                        result.data.footer_image);
                }
                $("#footer_content").val(result.data.footer_content);

                if (result.data.service_image) {
                    renderImage("image-preview-service", '/images/web/' + result.data.service_image,
                        "image_service",
                        result.data.service_image);
                }
                $("#service").val(result.data.service);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                window.location.href = urlWeb;
            },
        });

        $(".btn-cancel").on("click", function() {
            window.history.back();
        });

        $("#form-content-logo-identity").on("submit", function(e) {
            e.preventDefault(); // prevent the form from submitting normally
            var formData = new FormData(this);
            swal({
                title: "Simpan data ini?",
                text: "Data yang diisikan sudah sesuai?",
                icon: "info",
                buttons: true,
            }).then((willSave) => {
                if (willSave) {
                    $.ajax({
                        url: "/api/content/logo-identity",
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            console.log(result.data.id);
                            if (result.status) {
                                swal("Berhasil Simpan Data", {
                                    icon: "success",
                                }).then((data) => {
                                    window.location.href = urlWeb;
                                });
                            } else {
                                swal("Simpan Data Gagal", {
                                    icon: "error",
                                });
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            swal("Simpan Data Gagal", {
                                icon: "error",
                                text: "Periksa kembali data yang diisikan.",
                            });
                        },
                    });
                }
            });
        });
    </script>
    <!-- Page Specific JS File -->
@endpush
