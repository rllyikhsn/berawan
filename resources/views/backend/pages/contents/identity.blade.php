@extends('backend.layouts.app')

@section('title', 'DataTables')

@push('style')
    <!-- CSS Libraries -->

    <link rel="stylesheet" href="{{ asset('backend/css/datatables/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/css/datatables/datatables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/css/datatables/select.bootstrap4.min.css') }}">
@endpush
@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Setting Identitas Website</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="{{ route('admin') }}">Dashboard</a></div>
                    <div class="breadcrumb-item">Identity </div>
                </div>
            </div>

            <div class="section-body">
                <form action="" method="POST" id="form-content-identity">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Identitas Setting</h4>
                                    <div class="card-header-action">
                                        <button class="btn btn-icon btn-warning btn-cancel">
                                            <i class="fas fa-cancel"></i> Batal
                                        </button>
                                        <button type="submit" class="btn btn-icon btn-info">
                                            <i class="fas fa-save"></i> Simpan
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-6">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Informasi Instansi</h4>
                                    <div class="card-header-action">
                                        <a data-collapse="#information-collapse" class="btn btn-icon btn-info"
                                            href="#">
                                            <i class="fas fa-minus"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="collapse show" id="information-collapse">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label>Nama Instansi</label>
                                            <input type="text" class="form-control" name="nama_instansi"
                                                id="nama_instansi" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Alamat Kantor</label>
                                            <textarea class="form-control" style="height: 100px;" name="alamat_office" id="alamat_office"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Alamat Warehouse</label>
                                            <textarea class="form-control" style="height: 100px;" name="alamat_warehouse" id="alamat_warehouse"></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Alamat Workshop</label>
                                            <textarea class="form-control" style="height: 100px;" name="alamat_workshop" id="alamat_workshop"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-6">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Contact</h4>
                                    <div class="card-header-action">
                                        <a data-collapse="#contact-collapse" class="btn btn-icon btn-info" href="#">
                                            <i class="fas fa-minus"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="collapse show" id="contact-collapse">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label>Nomor Telepon Kantor</label>
                                            <input type="text" class="form-control" name="no_telepon_office"
                                                id="no_telepon_office" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Nomor Telepon Warehouse</label>
                                            <input type="text" class="form-control" name="no_telepon_warehouse"
                                                id="no_telepon_warehouse" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Nomor Telepon Workshop</label>
                                            <input type="text" class="form-control" name="no_telepon_workshop"
                                                id="no_telepon_workshop" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Nomor Telepon Whatsapp</label>
                                            <input type="text" class="form-control" name="no_whatsapp" id="no_whatsapp"
                                                required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <h4>Email</h4>
                                    <div class="card-header-action">
                                        <a data-collapse="#email-collapse" class="btn btn-icon btn-info" href="#">
                                            <i class="fas fa-minus"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="collapse show" id="email-collapse">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label>Email Kantor</label>
                                            <input type="text" class="form-control" name="email_office"
                                                id="email_office" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Email Warehouse</label>
                                            <input type="text" class="form-control" name="email_warehouse"
                                                id="email_warehouse">
                                        </div>
                                        <div class="form-group">
                                            <label>Email Workshop</label>
                                            <input type="text" class="form-control" name="email_workshop"
                                                id="email_workshop">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>

@endsection

@push('scripts')
    <script src="{{ asset('backend/library/jquery-ui-dist/jquery-ui.min.js') }}"></script>

    <script src="{{ asset('backend/library/sweetalert/dist/sweetalert.min.js') }}"></script>

    <script>
        var urlWeb = "/admin/content-management/identity";
        var urlApi = "/api/content";
        $.ajax({
            url: urlApi + "/detail",
            type: "GET",
            contentType: false,
            processData: false,
            success: function(result) {
                $("#nama_instansi").val(result.data.nama_instansi);
                $("#alamat_office").val(result.data.alamat_office);
                $("#alamat_warehouse").val(result.data.alamat_warehouse);
                $("#alamat_workshop").val(result.data.alamat_workshop);
                $("#no_telepon_office").val(result.data.no_telepon_office);
                $("#no_telepon_warehouse").val(result.data.no_telepon_warehouse);
                $("#no_telepon_workshop").val(result.data.no_telepon_workshop);
                $("#no_whatsapp").val(result.data.no_whatsapp);
                $("#email_office").val(result.data.email_office);
                $("#email_warehouse").val(result.data.email_warehouse);
                $("#email_workshop").val(result.data.email_workshop);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                window.location.href = urlWeb;
            },
        });

        $(".btn-cancel").on("click", function() {
            window.history.back();
        });

        $("#form-content-identity").on("submit", function(e) {
            e.preventDefault(); // prevent the form from submitting normally
            var formData = new FormData(this);
            swal({
                title: "Simpan data ini?",
                text: "Data yang diisikan sudah sesuai?",
                icon: "info",
                buttons: true,
            }).then((willSave) => {
                if (willSave) {
                    $.ajax({
                        url: urlApi + "/identity",
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            console.log(result.data.id);
                            if (result.status) {
                                swal("Berhasil Simpan Data", {
                                    icon: "success",
                                }).then((data) => {
                                    window.location.href = urlWeb;
                                });
                            } else {
                                swal("Simpan Data Gagal", {
                                    icon: "error",
                                });
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            swal("Simpan Data Gagal", {
                                icon: "error",
                                text: "Periksa kembali data yang diisikan.",
                            });
                        },
                    });
                }
            });
        });
    </script>
    <!-- Page Specific JS File -->
@endpush
