@extends('backend.layouts.app')

@section('title', 'DataTables')

@push('style')
    <!-- CSS Libraries -->

    <link rel="stylesheet" href="{{ asset('backend/css/datatables/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/css/datatables/datatables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/css/datatables/select.bootstrap4.min.css') }}">
@endpush
@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Setting Kontak Website</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="{{ route('admin') }}">Dashboard</a></div>
                    <div class="breadcrumb-item">Kontak </div>
                </div>
            </div>

            <div class="section-body">
                <form action="" method="POST" id="form-content-contact">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Kontak Setting</h4>
                                    <div class="card-header-action">
                                        <button class="btn btn-icon btn-warning btn-cancel">
                                            <i class="fas fa-cancel"></i> Batal
                                        </button>
                                        <button type="submit" class="btn btn-icon btn-info">
                                            <i class="fas fa-save"></i> Simpan
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 col-lg-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Contact 1</h4>
                                    <div class="card-header-action">
                                        <a data-collapse="#contact1-collapse" class="btn btn-icon btn-info" href="#">
                                            <i class="fas fa-minus"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="collapse show" id="contact1-collapse">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label>Nama</label>
                                            <input type="text" class="form-control" name="contact_person_name_1"
                                                id="contact_person_name_1">
                                        </div>
                                        <div class="form-group">
                                            <label>Nomor Telepon</label>
                                            <input type="text" class="form-control" name="contact_person_phone_1"
                                                id="contact_person_phone_1">
                                        </div>
                                        <div class="form-group">
                                            <label>Nomor Telepon Whatsapp</label>
                                            <input type="text" class="form-control" name="contact_person_whatsapp_1"
                                                id="contact_person_whatsapp_1">
                                        </div>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="email" class="form-control" name="contact_person_email_1"
                                                id="contact_person_email_1">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 col-lg-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Contact 2</h4>
                                    <div class="card-header-action">
                                        <a data-collapse="#contact2-collapse" class="btn btn-icon btn-info" href="#">
                                            <i class="fas fa-minus"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="collapse show" id="contact2-collapse">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label>Nama</label>
                                            <input type="text" class="form-control" name="contact_person_name_2"
                                                id="contact_person_name_2">
                                        </div>
                                        <div class="form-group">
                                            <label>Nomor Telepon</label>
                                            <input type="text" class="form-control" name="contact_person_phone_2"
                                                id="contact_person_phone_2">
                                        </div>
                                        <div class="form-group">
                                            <label>Nomor Telepon Whatsapp</label>
                                            <input type="text" class="form-control" name="contact_person_whatsapp_2"
                                                id="contact_person_whatsapp_2">
                                        </div>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="email" class="form-control" name="contact_person_email_2"
                                                id="contact_person_email_2">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4 col-lg-4">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Contact 3</h4>
                                    <div class="card-header-action">
                                        <a data-collapse="#contact3-collapse" class="btn btn-icon btn-info" href="#">
                                            <i class="fas fa-minus"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="collapse show" id="contact3-collapse">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label>Nama</label>
                                            <input type="text" class="form-control" name="contact_person_name_3"
                                                id="contact_person_name_3">
                                        </div>
                                        <div class="form-group">
                                            <label>Nomor Telepon</label>
                                            <input type="text" class="form-control" name="contact_person_phone_3"
                                                id="contact_person_phone_3">
                                        </div>
                                        <div class="form-group">
                                            <label>Nomor Telepon Whatsapp</label>
                                            <input type="text" class="form-control" name="contact_person_whatsapp_3"
                                                id="contact_person_whatsapp_3">
                                        </div>
                                        <div class="form-group">
                                            <label>Email</label>
                                            <input type="email" class="form-control" name="contact_person_email_3"
                                                id="contact_person_email_3">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>

@endsection

@push('scripts')
    <script src="{{ asset('backend/library/jquery-ui-dist/jquery-ui.min.js') }}"></script>

    <script src="{{ asset('backend/library/sweetalert/dist/sweetalert.min.js') }}"></script>

    <script>
        var urlWeb = "/admin/content-management/contact";
        var urlApi = "/api/content";
        $.ajax({
            url: urlApi + "/detail",
            type: "GET",
            contentType: false,
            processData: false,
            success: function(result) {
                $("#contact_person_name_1").val(result.data.contact_person_name_1);
                $("#contact_person_phone_1").val(result.data.contact_person_phone_1);
                $("#contact_person_whatsapp_1").val(result.data.contact_person_whatsapp_1);
                $("#contact_person_email_1").val(result.data.contact_person_email_1);
                $("#contact_person_name_2").val(result.data.contact_person_name_2);
                $("#contact_person_phone_2").val(result.data.contact_person_phone_2);
                $("#contact_person_whatsapp_2").val(result.data.contact_person_whatsapp_2);
                $("#contact_person_email_2").val(result.data.contact_person_email_2);
                $("#contact_person_name_3").val(result.data.contact_person_name_3);
                $("#contact_person_phone_3").val(result.data.contact_person_phone_3);
                $("#contact_person_whatsapp_3").val(result.data.contact_person_whatsapp_3);
                $("#contact_person_email_3").val(result.data.contact_person_email_3);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                window.location.href = urlWeb;
            },
        });

        $(".btn-cancel").on("click", function() {
            window.history.back();
        });

        $("#form-content-contact").on("submit", function(e) {
            e.preventDefault(); // prevent the form from submitting normally
            var formData = new FormData(this);
            swal({
                title: "Simpan data ini?",
                text: "Data yang diisikan sudah sesuai?",
                icon: "info",
                buttons: true,
            }).then((willSave) => {
                if (willSave) {
                    $.ajax({
                        url: urlApi + "/identity",
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            console.log(result.data.id);
                            if (result.status) {
                                swal("Berhasil Simpan Data", {
                                    icon: "success",
                                }).then((data) => {
                                    window.location.href = urlWeb;
                                });
                            } else {
                                swal("Simpan Data Gagal", {
                                    icon: "error",
                                });
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            swal("Simpan Data Gagal", {
                                icon: "error",
                                text: "Periksa kembali data yang diisikan.",
                            });
                        },
                    });
                }
            });
        });
    </script>
    <!-- Page Specific JS File -->
@endpush
