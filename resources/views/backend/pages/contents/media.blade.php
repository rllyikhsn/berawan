@extends('backend.layouts.app')

@section('title', 'DataTables')

@push('style')
    <!-- CSS Libraries -->

    <link rel="stylesheet" href="{{ asset('backend/css/datatables/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/css/datatables/datatables.bootstrap4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/css/datatables/select.bootstrap4.min.css') }}">
@endpush
@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Setting Media Website</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="{{ route('admin') }}">Dashboard</a></div>
                    <div class="breadcrumb-item">Media </div>
                </div>
            </div>

            <div class="section-body">
                <form action="" method="POST" id="form-content-media">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Media Setting</h4>
                                    <div class="card-header-action">
                                        <button class="btn btn-icon btn-warning btn-cancel">
                                            <i class="fas fa-cancel"></i> Batal
                                        </button>
                                        <button type="submit" class="btn btn-icon btn-info">
                                            <i class="fas fa-save"></i> Simpan
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Link Media</h4>
                                    <div class="card-header-action">
                                        <a data-collapse="#media-collapse" class="btn btn-icon btn-info" href="#">
                                            <i class="fas fa-minus"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="collapse show" id="media-collapse">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-12 col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <label>Facebook</label>
                                                    <input type="text" class="form-control" name="link_facebook"
                                                        id="link_facebook">
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <label>Twitter</label>
                                                    <input type="text" class="form-control" name="link_twitter"
                                                        id="link_twitter">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <label>YouTube</label>
                                                    <input type="text" class="form-control" name="link_youtube"
                                                        id="link_youtube">
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <label>Linktr</label>
                                                    <input type="text" class="form-control" name="link_linktr"
                                                        id="link_linktr">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <label>Instagram</label>
                                                    <input type="text" class="form-control" name="link_instagram"
                                                        id="link_instagram">
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <label>LinkedIn</label>
                                                    <input type="text" class="form-control" name="link_linkedin"
                                                        id="link_linkedin">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <label>Google Maps</label>
                                                    <input type="text" class="form-control" name="link_gmaps"
                                                        id="link_gmaps">
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <label>Brochure</label>
                                                    <input type="text" class="form-control" name="link_brocure"
                                                        id="link_brocure">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <label>Playstore</label>
                                                    <input type="text" class="form-control" name="deeplink_playstore"
                                                        id="deeplink_playstore">
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <label>App Store</label>
                                                    <input type="text" class="form-control" name="deeplink_appstore"
                                                        id="deeplink_appstore">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12 col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <label>Whatsapp</label>
                                                    <input type="text" class="form-control" name="link_whatsapp"
                                                        id="link_whatsapp">
                                                </div>
                                            </div>
                                            <div class="col-12 col-md-6 col-lg-6">
                                                <div class="form-group">
                                                    <label>Whatsapp Template</label>
                                                    <input type="text" class="form-control"
                                                        name="link_whatsapp_template" id="link_whatsapp_template">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>

@endsection

@push('scripts')
    <script src="{{ asset('backend/library/jquery-ui-dist/jquery-ui.min.js') }}"></script>

    <script src="{{ asset('backend/library/sweetalert/dist/sweetalert.min.js') }}"></script>

    <script>
        var urlWeb = "/admin/content-management/media";
        var urlApi = "/api/content";
        $.ajax({
            url: urlApi + "/detail",
            type: "GET",
            contentType: false,
            processData: false,
            success: function(result) {
                $("#link_facebook").val(result.data.link_facebook);
                $("#link_whatsapp").val(result.data.link_whatsapp);
                $("#link_whatsapp_template").val(result.data.link_whatsapp_template);
                $("#link_twitter").val(result.data.link_twitter);
                $("#link_youtube").val(result.data.link_youtube);
                $("#link_linktr").val(result.data.link_linktr);
                $("#link_instagram").val(result.data.link_instagram);
                $("#link_linkedin").val(result.data.link_linkedin);
                $("#link_gmaps").val(result.data.link_gmaps);
                $("#deeplink_playstore").val(result.data.deeplink_playstore);
                $("#deeplink_appstore").val(result.data.deeplink_appstore);
                $("#link_brocure").val(result.data.link_brocure);
            },
            error: function(jqXHR, textStatus, errorThrown) {
                window.location.href = urlWeb;
            },
        });

        $(".btn-cancel").on("click", function() {
            window.history.back();
        });

        $("#form-content-media").on("submit", function(e) {
            e.preventDefault(); // prevent the form from submitting normally
            var formData = new FormData(this);
            swal({
                title: "Simpan data ini?",
                text: "Data yang diisikan sudah sesuai?",
                icon: "info",
                buttons: true,
            }).then((willSave) => {
                if (willSave) {
                    $.ajax({
                        url: urlApi + "/media",
                        type: "POST",
                        data: formData,
                        contentType: false,
                        processData: false,
                        success: function(result) {
                            console.log(result.data.id);
                            if (result.status) {
                                swal("Berhasil Simpan Data", {
                                    icon: "success",
                                }).then((data) => {
                                    window.location.href = urlWeb;
                                });
                            } else {
                                swal("Simpan Data Gagal", {
                                    icon: "error",
                                });
                            }
                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            swal("Simpan Data Gagal", {
                                icon: "error",
                                text: "Periksa kembali data yang diisikan.",
                            });
                        },
                    });
                }
            });
        });
    </script>
    <!-- Page Specific JS File -->
@endpush
