@extends('backend.layouts.app')

@section('title', 'Advanced Forms')

@push('style')
    <!-- CSS Libraries -->
    <link rel="stylesheet" href="{{ asset('backend/library/summernote/dist/summernote-bs4.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/library/bootstrap-daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/library/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/library/select2/dist/css/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/library/selectric/public/selectric.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/library/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('backend/library/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}">
@endpush

@section('main')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Manage Pengumuman</h1>
                <div class="section-header-breadcrumb">
                    <div class="breadcrumb-item active"><a href="{{ route('admin') }}">Dashboard</a></div>
                    <div class="breadcrumb-item active"><a href="{{ route('content-announcements') }}">Data Pengumuman</a>
                    </div>
                    <div class="breadcrumb-item">Manage Pengumuman</div>
                </div>
            </div>

            <div class="section-body">
                <form action="" method="POST" id="form-announcements">
                    <div class="row">
                        <div class="col-12 col-md-12 col-lg-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Pengumuman</h4>
                                    <div class="card-header-action">
                                        <button class="btn btn-icon btn-warning btn-cancel">
                                            <i class="fas fa-cancel"></i> Batal
                                        </button>
                                        <button type="submit" class="btn btn-icon btn-info">
                                            <i class="fas fa-save"></i> Simpan
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-9 col-lg-9">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Informasi Pengumuman</h4>
                                </div>
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>Judul Pengumuman</label>
                                        <input type="text" class="form-control" name="title" id="title" required>
                                    </div>
                                    <div class="form-group">
                                        <label>Isi Pengumuman</label>
                                        <textarea class="summernote" name="short_content" id="short_content"></textarea>
                                    </div>
                                    <div class="row">
                                        <div class="col-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label>Waktu Awal Pengumuman</label>
                                                <input type="text" class="form-control datetimepicker" name="start_at"
                                                    id="start_at">
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-6 col-lg-6">
                                            <div class="form-group">
                                                <label>Waktu Akhir Pengumuman</label>
                                                <input type="text" class="form-control datetimepicker" name="end_at"
                                                    id="end_at">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-3 col-lg-3">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Terbitkan</h4>
                                    <div class="card-header-action">
                                        <a data-collapse="#publication-collapse" class="btn btn-icon btn-info"
                                            href="#">
                                            <i class="fas fa-minus"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="collapse show" id="publication-collapse">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label>Draft</label>
                                            <select class="form-control" name="is_draft" id="is_draft">
                                                <option value="1">Ya</option>
                                                <option value="0">Tidak</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Publikasi</label>
                                            <select class="form-control" name="is_published" id="is_published">
                                                <option value="1">Ya</option>
                                                <option value="0">Tidak</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label>Waktu dan Tanggal Publikasi</label>
                                            <input type="text" class="form-control datetimepicker" name="published_at"
                                                id="published_at">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
@endsection

@push('scripts')
    <!-- JS Libraies -->
    <script src="{{ asset('backend/library/summernote/dist/summernote-bs4.js') }}"></script>
    <script src="{{ asset('backend/library/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('backend/library/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>
    <script src="{{ asset('backend/library/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>
    <script src="{{ asset('backend/library/select2/dist/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('backend/library/selectric/public/jquery.selectric.min.js') }}"></script>

    <script src="{{ asset('backend/library/sweetalert/dist/sweetalert.min.js') }}"></script>
    <!-- Page Specific JS File -->
    <script src="{{ asset('backend/js/page/announcements/content-announcements-manage.js') }}"></script>
@endpush
