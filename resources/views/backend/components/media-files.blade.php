@push('style')
    <link rel="stylesheet" href="{{ asset('backend/library/dropzone/dist/dropzone.css') }}">
@endpush
<div class="modal fade" tabindex="-1" role="dialog" id="modalMediaFiles">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">List All Media</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="nav nav-tabs" id="tabMedia" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="upload-media-tab" data-toggle="tab" href="#uploadMedia"
                            role="tab" aria-controls="home" aria-selected="true">Upload Media</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="media-library-tab" data-toggle="tab" href="#mediaLibrary" role="tab"
                            aria-controls="contact" aria-selected="false">Media
                            Library</a>
                    </li>
                </ul>
                <div class="tab-content tab-bordered" id="mediaUpload">
                    <div class="tab-pane fade show active" id="uploadMedia" role="tabpanel"
                        aria-labelledby="upload-media-tab">
                        <form class="dropzone" id="mydropzone" enctype="multipart/form-data" action="__URL__"
                            method="POST">
                            <div class="fallback">
                                <input name="file" type="file" multiple />
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane fade" id="mediaLibrary" role="tabpanel" aria-labelledby="media-library-tab">
                        <div class="row">
                            <div class="col-12 col-md-8 col-lg-8">
                                <div class="card">
                                    <div class="card-body" style="height: 650px; overflow-y: auto;">
                                        <div class="form-group">
                                            <div class="row gutters-sm">
                                                <div class="col-12 col-md-12 col-lg-12">
                                                    <div class="row" id="data-media-list">
                                                        <div class="col-6 col-sm-3">
                                                            <label class="imagecheck mb-4">
                                                                <input name="imagecheck" type="radio" value="6"
                                                                    data-image-id="666"
                                                                    data-image-src="https://demo.getstisla.com/assets/img/news/img06.jpg"
                                                                    data-image-created-at="20-08-2023"
                                                                    data-image-dimension="200 x 300 pixels"
                                                                    data-image-size="16.9 KB" class="imagecheck-input">
                                                                <figure class="imagecheck-figure">
                                                                    <img src="https://demo.getstisla.com/assets/img/news/img06.jpg"
                                                                        alt="}" class="imagecheck-image">
                                                                </figure>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-12 col-lg-12">
                                                    <div class="text-center">
                                                        <button class="btn btn-info btn-load-more">Load More
                                                            ...</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-4 col-lg-4" id="image-preview">
                                <div class="card">
                                    <div class="card-body">
                                        <label for="image-attach">Detail Image</label>
                                        <figure class="imagecheck-figure">
                                            <img src="https://demo.getstisla.com/assets/img/news/img03.jpg"
                                                alt="" class="imagecheck-image" style="opacity: 1" id="image-preview-src">
                                        </figure>
                                        <br>
                                        <p>Tanggal Upload</p>
                                        <p id="image-preview-created-at">17-08-2023</p>
                                        <p>File Size</p>
                                        <p id="image-preview-size">3 Mb</p>
                                        <p>Size Pixel</p>
                                        <p id="image-preview-dimension">600 x 400 piksel</p>
                                        <p>
                                            <a href="http://google.com" target="_blank"
                                                id="image-preview-link-src">http://google.com</a>
                                        </p>
                                        <button class="btn btn-icon icon-left btn-danger btn-delete-image-preview"><i
                                                class="fas fa-trash"></i>
                                            Hapus</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-primary btn-pick-image" data-dismiss="modal">Select
                    Image</button>
            </div>
        </div>
    </div>
</div>


@push('scripts')
    <script src="{{ asset('backend/library/prismjs/prism.js') }}"></script>
    <script src="{{ asset('backend/library/dropzone/dist/min/dropzone.min.js') }}"></script>
@endpush
