<div class="main-sidebar sidebar-style-2">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="{{ route('admin') }}">Stisla</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="{{ route('admin') }}">St</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li class="nav-item dropdown {{ $type_menu === 'dashboard' ? 'active' : '' }}">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-fire"></i><span>Dashboard</span></a>
                <ul class="dropdown-menu">
                    <li class='{{ Request::is('dashboard-general-dashboard') ? 'active' : '' }}'>
                        <a class="nav-link" href="{{ url('dashboard-general-dashboard') }}">General Dashboard</a>
                    </li>
                    <li class="{{ Request::is('dashboard-ecommerce-dashboard') ? 'active' : '' }}">
                        <a class="nav-link" href="{{ url('dashboard-ecommerce-dashboard') }}">Ecommerce Dashboard</a>
                    </li>
                </ul>
            </li>
            <li class="menu-header">Contents</li>
            <li class="nav-item dropdown {{ $type_menu === 'articles' ? 'active' : '' }}">
                <a href="#" class="nav-link has-dropdown"><i class="far fa-newspaper"></i>
                    <span>Artikel</span></a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('admin/articles/manage') ? 'active' : '' }}">
                        <a href="{{ route('add-articles') }}"><i class="fa fa-pencil"></i> <span>Tambah
                                Baru</span></a>
                    </li>
                    <li class="{{ Request::is('admin/articles') ? 'active' : '' }}">
                        <a href="{{ route('articles') }}"><i class="fas fa-list-ul"></i> <span>List Data</span></a>
                    </li>
                </ul>
            </li>
            <li class="nav-item dropdown {{ $type_menu === 'announcements' ? 'active' : '' }}">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-bullhorn"></i>
                    <span>Pengumuman</span></a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('admin/content-announcements/manage') ? 'active' : '' }}">
                        <a href="{{ route('add-content-announcements') }}"><i class="fa fa-pencil"></i> <span>Tambah
                                Baru</span></a>
                    </li>
                    <li class="{{ Request::is('admin/content-announcements') ? 'active' : '' }}">
                        <a href="{{ route('content-announcements') }}"><i class="fas fa-list-ul"></i> <span>List
                                Data</span></a>
                    </li>
                </ul>
            </li>
            <li class="nav-item dropdown {{ $type_menu === 'categories' ? 'active' : '' }}">
                <a href="#" class="nav-link has-dropdown"><i class="fa fa-list"></i>
                    <span>Kategori</span></a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('admin/content-categories/manage') ? 'active' : '' }}">
                        <a href="{{ route('add-content-categories') }}"><i class="fa fa-pencil"></i> <span>Tambah
                                Baru</span></a>
                    </li>
                    <li class="{{ Request::is('admin/content-categories') ? 'active' : '' }}">
                        <a href="{{ route('content-categories') }}"><i class="fas fa-list-ul"></i> <span>List
                                Data</span></a>
                    </li>
                </ul>
            </li>
            <li class="nav-item dropdown {{ $type_menu === 'tags' ? 'active' : '' }}">
                <a href="#" class="nav-link has-dropdown"><i class="fas fa-tag"></i>
                    <span>Tag</span></a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('admin/content-tags/manage') ? 'active' : '' }}">
                        <a href="{{ route('add-content-tags') }}"><i class="fa fa-pencil"></i> <span>Tambah
                                Baru</span></a>
                    </li>
                    <li class="{{ Request::is('admin/content-tags') ? 'active' : '' }}">
                        <a href="{{ route('content-tags') }}"><i class="fas fa-list-ul"></i> <span>List
                                Data</span></a>
                    </li>
                </ul>
            </li>
            <li class="nav-item dropdown {{ $type_menu === 'workshops' ? 'active' : '' }}">
                <a href="#" class="nav-link has-dropdown"><i class="fa fa-briefcase"></i>
                    <span>Workshop</span></a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('admin/content-workshops/manage') ? 'active' : '' }}">
                        <a href="{{ route('add-content-workshops') }}"><i class="fa fa-pencil"></i> <span>Tambah
                                Baru</span></a>
                    </li>
                    <li class="{{ Request::is('admin/content-workshops') ? 'active' : '' }}">
                        <a href="{{ route('content-workshops') }}"><i class="fas fa-list-ul"></i> <span>List
                                Data</span></a>
                    </li>
                </ul>
            </li>
            <li class="menu-header">Settings</li>
            <li class="nav-item dropdown {{ $type_menu === 'content-management' ? 'active' : '' }}">
                <a href="#" class="nav-link has-dropdown"><i class="far fa-user"></i> <span>Manajemen
                        Konten</span></a>
                <ul class="dropdown-menu">
                    <li class="{{ Request::is('admin/content-management/identity') ? 'active' : '' }}">
                        <a href="{{ route('content-identity') }}">Identitas</a>
                    </li>
                    <li class="{{ Request::is('admin/content-management/logo-identity') ? 'active' : '' }}">
                        <a href="{{ route('content-logo-identity') }}">Logo Identitas</a>
                    </li>
                    <li class="{{ Request::is('admin/content-management/contact') ? 'active' : '' }}">
                        <a href="{{ route('content-contact') }}">Kontak</a>
                    </li>
                    <li class="{{ Request::is('admin/content-management/media') ? 'active' : '' }}">
                        <a href="{{ route('content-media') }}">Media</a>
                    </li>
                </ul>
            </li>
        </ul>

        <div class="hide-sidebar-mini mt-4 mb-4 p-3">
            <a href="https://getstisla.com/docs" class="btn btn-primary btn-lg btn-block btn-icon-split">
                <i class="fas fa-rocket"></i> Documentation
            </a>
        </div>
    </aside>
</div>
