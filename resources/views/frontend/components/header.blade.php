<header id="header" class="navbar navbar-expand-lg navbar-end navbar-absolute-top navbar-light navbar-show-hide bg-white" data-hs-header-options='{
    "fixMoment": 1000,
    "fixEffect": "slide"
  }'>

    <div class="container">
        <nav class="js-mega-menu navbar-nav-wrap">
            <!-- Default Logo -->
            <a class="navbar-brand" href="{{ route('home') }}" aria-label="Front">
                <img class="navbar-brand-logo" src="{{ asset('frontend/img/logo-clean.png') }}" alt="Logo">
            </a>
            <!-- End Default Logo -->

            <!-- Toggler -->
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-default">
                    <i class="bi-list"></i>
                </span>
                <span class="navbar-toggler-toggled">
                    <i class="bi-x"></i>
                </span>
            </button>
            <!-- End Toggler -->

            <!-- Collapse -->
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <div class="navbar-absolute-top-scroller">
                    <ul class="navbar-nav">
                        <!-- Home -->
                        <li class="nav-item">
                            <a class="nav-link {{ Request::is('/') ? 'active' : '' }}" href="{{ route('home') }}" role="button" aria-expanded="false">Home</a>
                        </li>
                        <!-- End Home -->

                        <!-- About Us -->
                        <li class="nav-item">
                            <a class="nav-link {{ Request::is('about-us') ? 'active' : '' }}" href="{{ route('about-us') }}" role="button" aria-expanded="false">About Us</a>
                        </li>
                        <!-- End About Us -->

                        <!-- Business -->
                        <li class="hs-has-mega-menu nav-item">
                            <a id="businessMegaMenu" class="hs-mega-menu-invoker nav-link dropdown-toggle {{ Request::is('business/digital-platform-solutions') || Request::is('business/procurement-inventory-services') || Request::is('business/merchant-distributions') || Request::is('business') ? 'active' : '' }}" href="#" role="button">Business</a>

                            <!-- Mega Menu -->
                            <div class="hs-mega-menu dropdown-menu w-100" aria-labelledby="businessMegaMenu" style="min-width: 40rem;">
                                <!-- Promo -->
                                <div class="navbar-dropdown-menu-promo">
                                    <!-- Promo Item -->
                                    <div class="navbar-dropdown-menu-promo-item">
                                        <!-- Promo Link -->
                                        <a class="navbar-dropdown-menu-promo-link {{ Request::is('business/digital-platform-solutions') || Request::is('business') ? 'active' : '' }}" href="{{ route('digital') }}">
                                            <div class="d-flex">
                                                <div class="flex-shrink-0">
                                                    <span class="svg-icon svg-icon-sm text-primary">
                                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg') }}">
                                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M6.5 3C5.67157 3 5 3.67157 5 4.5V6H3.5C2.67157 6 2 6.67157 2 7.5C2 8.32843 2.67157 9 3.5 9H5V19.5C5 20.3284 5.67157 21 6.5 21C7.32843 21 8 20.3284 8 19.5V9H20.5C21.3284 9 22 8.32843 22 7.5C22 6.67157 21.3284 6 20.5 6H8V4.5C8 3.67157 7.32843 3 6.5 3Z" fill="#035A4B" />
                                                            <path opacity="0.3" fill-rule="evenodd" clip-rule="evenodd" d="M20.5 11H10V17.5C10 18.3284 10.6716 19 11.5 19H15.5C17.3546 19 19.0277 18.2233 20.2119 16.9775C20.1436 16.9922 20.0727 17 20 17C19.4477 17 19 16.5523 19 16V13C19 12.4477 19.4477 12 20 12C20.5523 12 21 12.4477 21 13V15.9657C21.6334 14.9626 22 13.7741 22 12.5C22 11.6716 21.3284 11 20.5 11Z" fill="#035A4B" />
                                                        </svg>

                                                    </span>
                                                </div>

                                                <div class="flex-grow-1 ms-3">
                                                    <span class="navbar-dropdown-menu-media-title">Digital Platform Solutions <span class="badge bg-success rounded-pill ms-1">Hot</span></span>
                                                    <p class="navbar-dropdown-menu-media-desc">Developing business with digital platforms,
                                                        and take advantage of digital marketing
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                        <!-- End Promo Link -->
                                    </div>
                                    <!-- End Promo Item -->

                                    <!-- Promo Item -->
                                    <div class="navbar-dropdown-menu-promo-item">
                                        <!-- Promo Link -->
                                        <a class="navbar-dropdown-menu-promo-link {{ Request::is('business/procurement-inventory-services') ? 'active' : '' }}" href="{{ route('procurement') }}">
                                            <div class="d-flex">
                                                <div class="flex-shrink-0">
                                                    <span class="svg-icon svg-icon-sm text-primary">
                                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg') }}">
                                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M6.5 3C5.67157 3 5 3.67157 5 4.5V6H3.5C2.67157 6 2 6.67157 2 7.5C2 8.32843 2.67157 9 3.5 9H5V19.5C5 20.3284 5.67157 21 6.5 21C7.32843 21 8 20.3284 8 19.5V9H20.5C21.3284 9 22 8.32843 22 7.5C22 6.67157 21.3284 6 20.5 6H8V4.5C8 3.67157 7.32843 3 6.5 3Z" fill="#035A4B" />
                                                            <path opacity="0.3" fill-rule="evenodd" clip-rule="evenodd" d="M20.5 11H10V17.5C10 18.3284 10.6716 19 11.5 19H15.5C17.3546 19 19.0277 18.2233 20.2119 16.9775C20.1436 16.9922 20.0727 17 20 17C19.4477 17 19 16.5523 19 16V13C19 12.4477 19.4477 12 20 12C20.5523 12 21 12.4477 21 13V15.9657C21.6334 14.9626 22 13.7741 22 12.5C22 11.6716 21.3284 11 20.5 11Z" fill="#035A4B" />
                                                        </svg>

                                                    </span>
                                                </div>

                                                <div class="flex-grow-1 ms-3">
                                                    <span class="navbar-dropdown-menu-media-title">Procurement Inventory Services</span>
                                                    <p class="navbar-dropdown-menu-media-desc">Providing company goods needs,
                                                        with a procurement system that connects several vendors.
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                        <!-- End Promo Link -->
                                    </div>
                                    <!-- End Promo Item -->

                                    <!-- Promo Item -->
                                    <div class="navbar-dropdown-menu-promo-item">
                                        <!-- Promo Link -->
                                        <a class="navbar-dropdown-menu-promo-link {{ Request::is('business/merchant-distributions') ? 'active' : '' }}" href="{{ route('merchant') }}">
                                            <div class="d-flex">
                                                <div class="flex-shrink-0">
                                                    <span class="svg-icon svg-icon-sm text-primary">
                                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg') }}">
                                                            <path opacity="0.3" d="M20 15H4C2.9 15 2 14.1 2 13V7C2 6.4 2.4 6 3 6H21C21.6 6 22 6.4 22 7V13C22 14.1 21.1 15 20 15ZM13 12H11C10.5 12 10 12.4 10 13V16C10 16.5 10.4 17 11 17H13C13.6 17 14 16.6 14 16V13C14 12.4 13.6 12 13 12Z" fill="#035A4B" />
                                                            <path d="M14 6V5H10V6H8V5C8 3.9 8.9 3 10 3H14C15.1 3 16 3.9 16 5V6H14ZM20 15H14V16C14 16.6 13.5 17 13 17H11C10.5 17 10 16.6 10 16V15H4C3.6 15 3.3 14.9 3 14.7V18C3 19.1 3.9 20 5 20H19C20.1 20 21 19.1 21 18V14.7C20.7 14.9 20.4 15 20 15Z" fill="#035A4B" />
                                                        </svg>

                                                    </span>
                                                </div>

                                                <div class="flex-grow-1 ms-3">
                                                    <span class="navbar-dropdown-menu-media-title">Merchant Distributions <span class="badge bg-success rounded-pill ms-1">Hot</span></span>
                                                    <p class="navbar-dropdown-menu-media-desc">Give you what you need with the best quality goods.</p>
                                                </div>
                                            </div>
                                        </a>
                                        <!-- End Promo Link -->
                                    </div>
                                    <!-- End Promo Item -->
                                </div>
                                <!-- End Promo -->
                            </div>
                            <!-- End Mega Menu -->
                        </li>
                        <!-- End Business -->

                        <!-- Media -->
                        <li class="hs-has-mega-menu nav-item">
                            <a id="mediaMegaMenu" class="hs-mega-menu-invoker nav-link dropdown-toggle " href="#" role="button">Media</a>

                            <!-- Mega Menu -->
                            <div class="hs-mega-menu dropdown-menu w-100" aria-labelledby="mediaMegaMenu" style="min-width: 40rem;">
                                <!-- Promo -->
                                <div class="navbar-dropdown-menu-promo">
                                    <!-- Promo Item -->
                                    <div class="navbar-dropdown-menu-promo-item">
                                        <!-- Promo Link -->
                                        <a class="navbar-dropdown-menu-promo-link " href="./demo-real-estate/index.html">
                                            <div class="d-flex">
                                                <div class="flex-shrink-0">
                                                    <span class="svg-icon svg-icon-sm text-primary">
                                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg') }}">
                                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M6.5 3C5.67157 3 5 3.67157 5 4.5V6H3.5C2.67157 6 2 6.67157 2 7.5C2 8.32843 2.67157 9 3.5 9H5V19.5C5 20.3284 5.67157 21 6.5 21C7.32843 21 8 20.3284 8 19.5V9H20.5C21.3284 9 22 8.32843 22 7.5C22 6.67157 21.3284 6 20.5 6H8V4.5C8 3.67157 7.32843 3 6.5 3Z" fill="#035A4B" />
                                                            <path opacity="0.3" fill-rule="evenodd" clip-rule="evenodd" d="M20.5 11H10V17.5C10 18.3284 10.6716 19 11.5 19H15.5C17.3546 19 19.0277 18.2233 20.2119 16.9775C20.1436 16.9922 20.0727 17 20 17C19.4477 17 19 16.5523 19 16V13C19 12.4477 19.4477 12 20 12C20.5523 12 21 12.4477 21 13V15.9657C21.6334 14.9626 22 13.7741 22 12.5C22 11.6716 21.3284 11 20.5 11Z" fill="#035A4B" />
                                                        </svg>

                                                    </span>
                                                </div>

                                                <div class="flex-grow-1 ms-3">
                                                    <span class="navbar-dropdown-menu-media-title">Gallery</span>
                                                    <p class="navbar-dropdown-menu-media-desc">Past documentation with business partners.
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                        <!-- End Promo Link -->
                                    </div>
                                    <!-- End Promo Item -->

                                    <!-- Promo Item -->
                                    <div class="navbar-dropdown-menu-promo-item">
                                        <!-- Promo Link -->
                                        <a class="navbar-dropdown-menu-promo-link " href="./demo-real-estate/index.html">
                                            <div class="d-flex">
                                                <div class="flex-shrink-0">
                                                    <span class="svg-icon svg-icon-sm text-primary">
                                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg') }}">
                                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M6.5 3C5.67157 3 5 3.67157 5 4.5V6H3.5C2.67157 6 2 6.67157 2 7.5C2 8.32843 2.67157 9 3.5 9H5V19.5C5 20.3284 5.67157 21 6.5 21C7.32843 21 8 20.3284 8 19.5V9H20.5C21.3284 9 22 8.32843 22 7.5C22 6.67157 21.3284 6 20.5 6H8V4.5C8 3.67157 7.32843 3 6.5 3Z" fill="#035A4B" />
                                                            <path opacity="0.3" fill-rule="evenodd" clip-rule="evenodd" d="M20.5 11H10V17.5C10 18.3284 10.6716 19 11.5 19H15.5C17.3546 19 19.0277 18.2233 20.2119 16.9775C20.1436 16.9922 20.0727 17 20 17C19.4477 17 19 16.5523 19 16V13C19 12.4477 19.4477 12 20 12C20.5523 12 21 12.4477 21 13V15.9657C21.6334 14.9626 22 13.7741 22 12.5C22 11.6716 21.3284 11 20.5 11Z" fill="#035A4B" />
                                                        </svg>

                                                    </span>
                                                </div>

                                                <div class="flex-grow-1 ms-3">
                                                    <span class="navbar-dropdown-menu-media-title">Videos</span>
                                                    <p class="navbar-dropdown-menu-media-desc">Documentation activity work with berawan.
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                        <!-- End Promo Link -->
                                    </div>
                                    <!-- End Promo Item -->
                                </div>
                                <!-- End Promo -->
                            </div>
                            <!-- End Mega Menu -->
                        </li>
                        <!-- End Media -->

                        <!-- Informations -->
                        <li class="hs-has-mega-menu nav-item">
                            <a id="informationsMegaMenu" class="hs-mega-menu-invoker nav-link dropdown-toggle {{ Request::is('informations/portfolios') || Request::is('informations/workshops') || Request::is('informations/blogs') || Request::is('informations') ? 'active' : '' }}" href="#" role="button">Informations</a>

                            <!-- Mega Menu -->
                            <div class="hs-mega-menu dropdown-menu w-100" aria-labelledby="informationsMegaMenu" style="min-width: 40rem;">
                                <!-- Promo -->
                                <div class="navbar-dropdown-menu-promo">
                                    <!-- Promo Item -->
                                    <div class="navbar-dropdown-menu-promo-item">
                                        <!-- Promo Link -->
                                        <a class="navbar-dropdown-menu-promo-link {{ Request::is('informations') || Request::is('informations/blogs') ? 'active' : '' }}" href="{{ route('blog') }}">
                                            <div class="d-flex">
                                                <div class="flex-shrink-0">
                                                    <span class="svg-icon svg-icon-sm text-primary">
                                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg') }}">
                                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M6.5 3C5.67157 3 5 3.67157 5 4.5V6H3.5C2.67157 6 2 6.67157 2 7.5C2 8.32843 2.67157 9 3.5 9H5V19.5C5 20.3284 5.67157 21 6.5 21C7.32843 21 8 20.3284 8 19.5V9H20.5C21.3284 9 22 8.32843 22 7.5C22 6.67157 21.3284 6 20.5 6H8V4.5C8 3.67157 7.32843 3 6.5 3Z" fill="#035A4B" />
                                                            <path opacity="0.3" fill-rule="evenodd" clip-rule="evenodd" d="M20.5 11H10V17.5C10 18.3284 10.6716 19 11.5 19H15.5C17.3546 19 19.0277 18.2233 20.2119 16.9775C20.1436 16.9922 20.0727 17 20 17C19.4477 17 19 16.5523 19 16V13C19 12.4477 19.4477 12 20 12C20.5523 12 21 12.4477 21 13V15.9657C21.6334 14.9626 22 13.7741 22 12.5C22 11.6716 21.3284 11 20.5 11Z" fill="#035A4B" />
                                                        </svg>

                                                    </span>
                                                </div>

                                                <div class="flex-grow-1 ms-3">
                                                    <span class="navbar-dropdown-menu-media-title">Blogs</span>
                                                    <p class="navbar-dropdown-menu-media-desc">Past documentation with business partners.
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                        <!-- End Promo Link -->
                                    </div>
                                    <!-- End Promo Item -->

                                    <!-- Promo Item -->
                                    <div class="navbar-dropdown-menu-promo-item">
                                        <!-- Promo Link -->
                                        <a class="navbar-dropdown-menu-promo-link {{ Request::is('informations') || Request::is('informations/portfolios') ? 'active' : '' }}" href="{{ route('portfolio') }}">
                                            <div class="d-flex">
                                                <div class="flex-shrink-0">
                                                    <span class="svg-icon svg-icon-sm text-primary">
                                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg') }}">
                                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M6.5 3C5.67157 3 5 3.67157 5 4.5V6H3.5C2.67157 6 2 6.67157 2 7.5C2 8.32843 2.67157 9 3.5 9H5V19.5C5 20.3284 5.67157 21 6.5 21C7.32843 21 8 20.3284 8 19.5V9H20.5C21.3284 9 22 8.32843 22 7.5C22 6.67157 21.3284 6 20.5 6H8V4.5C8 3.67157 7.32843 3 6.5 3Z" fill="#035A4B" />
                                                            <path opacity="0.3" fill-rule="evenodd" clip-rule="evenodd" d="M20.5 11H10V17.5C10 18.3284 10.6716 19 11.5 19H15.5C17.3546 19 19.0277 18.2233 20.2119 16.9775C20.1436 16.9922 20.0727 17 20 17C19.4477 17 19 16.5523 19 16V13C19 12.4477 19.4477 12 20 12C20.5523 12 21 12.4477 21 13V15.9657C21.6334 14.9626 22 13.7741 22 12.5C22 11.6716 21.3284 11 20.5 11Z" fill="#035A4B" />
                                                        </svg>

                                                    </span>
                                                </div>

                                                <div class="flex-grow-1 ms-3">
                                                    <span class="navbar-dropdown-menu-media-title">Portfolios</span>
                                                    <p class="navbar-dropdown-menu-media-desc">Past documentation with business partners.
                                                    </p>
                                                </div>
                                            </div>
                                        </a>
                                        <!-- End Promo Link -->
                                    </div>
                                    <!-- End Promo Item -->
                                </div>
                                <!-- End Promo -->
                            </div>
                            <!-- End Mega Menu -->
                        </li>
                        <!-- End Informations -->

                        <!-- Careers -->
                        <li class="hs-has-mega-menu nav-item">
                            <a id="careersMegaMenu" class="hs-mega-menu-invoker nav-link dropdown-toggle " href="#" role="button">Careers</a>

                            <!-- Mega Menu -->
                            <div class="hs-mega-menu dropdown-menu w-100" aria-labelledby="careersMegaMenu" style="min-width: 40rem;">
                                <!-- Promo -->
                                <div class="navbar-dropdown-menu-promo">
                                    <!-- Promo Item -->
                                    <div class="navbar-dropdown-menu-promo-item">
                                        <!-- Promo Link -->
                                        <a class="navbar-dropdown-menu-promo-link " href="./demo-jobs/index.html">
                                            <div class="d-flex">
                                                <div class="flex-shrink-0">
                                                    <span class="svg-icon svg-icon-sm text-primary">
                                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg') }}">
                                                            <path opacity="0.3" d="M20 15H4C2.9 15 2 14.1 2 13V7C2 6.4 2.4 6 3 6H21C21.6 6 22 6.4 22 7V13C22 14.1 21.1 15 20 15ZM13 12H11C10.5 12 10 12.4 10 13V16C10 16.5 10.4 17 11 17H13C13.6 17 14 16.6 14 16V13C14 12.4 13.6 12 13 12Z" fill="#035A4B" />
                                                            <path d="M14 6V5H10V6H8V5C8 3.9 8.9 3 10 3H14C15.1 3 16 3.9 16 5V6H14ZM20 15H14V16C14 16.6 13.5 17 13 17H11C10.5 17 10 16.6 10 16V15H4C3.6 15 3.3 14.9 3 14.7V18C3 19.1 3.9 20 5 20H19C20.1 20 21 19.1 21 18V14.7C20.7 14.9 20.4 15 20 15Z" fill="#035A4B" />
                                                        </svg>

                                                    </span>
                                                </div>

                                                <div class="flex-grow-1 ms-3">
                                                    <span class="navbar-dropdown-menu-media-title">Merchant Distributions <span class="badge bg-success rounded-pill ms-1">Hot</span></span>
                                                    <p class="navbar-dropdown-menu-media-desc">Give you what you need with the best quality goods.</p>
                                                </div>
                                            </div>
                                        </a>
                                        <!-- End Promo Link -->
                                    </div>
                                    <!-- End Promo Item -->
                                    <!-- Promo Item -->
                                    <div class="navbar-dropdown-menu-promo-item">
                                        <!-- Promo Link -->
                                        <a class="navbar-dropdown-menu-promo-link " href="./demo-jobs/index.html">
                                            <div class="d-flex">
                                                <div class="flex-shrink-0">
                                                    <span class="svg-icon svg-icon-sm text-primary">
                                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg') }}">
                                                            <path opacity="0.3" d="M20 15H4C2.9 15 2 14.1 2 13V7C2 6.4 2.4 6 3 6H21C21.6 6 22 6.4 22 7V13C22 14.1 21.1 15 20 15ZM13 12H11C10.5 12 10 12.4 10 13V16C10 16.5 10.4 17 11 17H13C13.6 17 14 16.6 14 16V13C14 12.4 13.6 12 13 12Z" fill="#035A4B" />
                                                            <path d="M14 6V5H10V6H8V5C8 3.9 8.9 3 10 3H14C15.1 3 16 3.9 16 5V6H14ZM20 15H14V16C14 16.6 13.5 17 13 17H11C10.5 17 10 16.6 10 16V15H4C3.6 15 3.3 14.9 3 14.7V18C3 19.1 3.9 20 5 20H19C20.1 20 21 19.1 21 18V14.7C20.7 14.9 20.4 15 20 15Z" fill="#035A4B" />
                                                        </svg>

                                                    </span>
                                                </div>

                                                <div class="flex-grow-1 ms-3">
                                                    <span class="navbar-dropdown-menu-media-title">Merchant Distributions <span class="badge bg-success rounded-pill ms-1">Hot</span></span>
                                                    <p class="navbar-dropdown-menu-media-desc">Give you what you need with the best quality goods.</p>
                                                </div>
                                            </div>
                                        </a>
                                        <!-- End Promo Link -->
                                    </div>
                                    <!-- End Promo Item -->
                                    <!-- Promo Item -->
                                    <div class="navbar-dropdown-menu-promo-item">
                                        <!-- Promo Link -->
                                        <a class="navbar-dropdown-menu-promo-link " href="./demo-jobs/index.html">
                                            <div class="d-flex">
                                                <div class="flex-shrink-0">
                                                    <span class="svg-icon svg-icon-sm text-primary">
                                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg') }}">
                                                            <path opacity="0.3" d="M20 15H4C2.9 15 2 14.1 2 13V7C2 6.4 2.4 6 3 6H21C21.6 6 22 6.4 22 7V13C22 14.1 21.1 15 20 15ZM13 12H11C10.5 12 10 12.4 10 13V16C10 16.5 10.4 17 11 17H13C13.6 17 14 16.6 14 16V13C14 12.4 13.6 12 13 12Z" fill="#035A4B" />
                                                            <path d="M14 6V5H10V6H8V5C8 3.9 8.9 3 10 3H14C15.1 3 16 3.9 16 5V6H14ZM20 15H14V16C14 16.6 13.5 17 13 17H11C10.5 17 10 16.6 10 16V15H4C3.6 15 3.3 14.9 3 14.7V18C3 19.1 3.9 20 5 20H19C20.1 20 21 19.1 21 18V14.7C20.7 14.9 20.4 15 20 15Z" fill="#035A4B" />
                                                        </svg>

                                                    </span>
                                                </div>

                                                <div class="flex-grow-1 ms-3">
                                                    <span class="navbar-dropdown-menu-media-title">Merchant Distributions <span class="badge bg-success rounded-pill ms-1">Hot</span></span>
                                                    <p class="navbar-dropdown-menu-media-desc">Give you what you need with the best quality goods.</p>
                                                </div>
                                            </div>
                                        </a>
                                        <!-- End Promo Link -->
                                    </div>
                                    <!-- End Promo Item -->
                                    <!-- Promo Item -->
                                    <div class="navbar-dropdown-menu-promo-item">
                                        <!-- Promo Link -->
                                        <a class="navbar-dropdown-menu-promo-link " href="./demo-jobs/index.html">
                                            <div class="d-flex">
                                                <div class="flex-shrink-0">
                                                    <span class="svg-icon svg-icon-sm text-primary">
                                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg') }}">
                                                            <path opacity="0.3" d="M20 15H4C2.9 15 2 14.1 2 13V7C2 6.4 2.4 6 3 6H21C21.6 6 22 6.4 22 7V13C22 14.1 21.1 15 20 15ZM13 12H11C10.5 12 10 12.4 10 13V16C10 16.5 10.4 17 11 17H13C13.6 17 14 16.6 14 16V13C14 12.4 13.6 12 13 12Z" fill="#035A4B" />
                                                            <path d="M14 6V5H10V6H8V5C8 3.9 8.9 3 10 3H14C15.1 3 16 3.9 16 5V6H14ZM20 15H14V16C14 16.6 13.5 17 13 17H11C10.5 17 10 16.6 10 16V15H4C3.6 15 3.3 14.9 3 14.7V18C3 19.1 3.9 20 5 20H19C20.1 20 21 19.1 21 18V14.7C20.7 14.9 20.4 15 20 15Z" fill="#035A4B" />
                                                        </svg>

                                                    </span>
                                                </div>

                                                <div class="flex-grow-1 ms-3">
                                                    <span class="navbar-dropdown-menu-media-title">Merchant Distributions <span class="badge bg-success rounded-pill ms-1">Hot</span></span>
                                                    <p class="navbar-dropdown-menu-media-desc">Give you what you need with the best quality goods.</p>
                                                </div>
                                            </div>
                                        </a>
                                        <!-- End Promo Link -->
                                    </div>
                                    <!-- End Promo Item -->
                                </div>
                                <!-- End Promo -->

                                <!-- Promo -->
                                <div class="navbar-dropdown-menu-promo">
                                    <!-- Promo Item -->
                                    <div class="navbar-dropdown-menu-promo-item">
                                        <!-- Promo Link -->
                                        <a class="navbar-dropdown-menu-promo-link " href="./demo-jobs/index.html">
                                            <div class="d-flex">
                                                <div class="flex-shrink-0">
                                                    <span class="svg-icon svg-icon-sm text-primary">
                                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg') }}">
                                                            <path opacity="0.3" d="M20 15H4C2.9 15 2 14.1 2 13V7C2 6.4 2.4 6 3 6H21C21.6 6 22 6.4 22 7V13C22 14.1 21.1 15 20 15ZM13 12H11C10.5 12 10 12.4 10 13V16C10 16.5 10.4 17 11 17H13C13.6 17 14 16.6 14 16V13C14 12.4 13.6 12 13 12Z" fill="#035A4B" />
                                                            <path d="M14 6V5H10V6H8V5C8 3.9 8.9 3 10 3H14C15.1 3 16 3.9 16 5V6H14ZM20 15H14V16C14 16.6 13.5 17 13 17H11C10.5 17 10 16.6 10 16V15H4C3.6 15 3.3 14.9 3 14.7V18C3 19.1 3.9 20 5 20H19C20.1 20 21 19.1 21 18V14.7C20.7 14.9 20.4 15 20 15Z" fill="#035A4B" />
                                                        </svg>

                                                    </span>
                                                </div>

                                                <div class="flex-grow-1 ms-3">
                                                    <span class="navbar-dropdown-menu-media-title">Find More Jobs <span class="badge bg-success rounded-pill ms-1">Hot</span></span>
                                                    <p class="navbar-dropdown-menu-media-desc">We're hiring.</p>
                                                </div>

                                                <div class="flex-shrink-0">
                                                    <i class="bi-chevron-right large"></i>
                                                </div>
                                            </div>
                                        </a>
                                        <!-- End Promo Link -->
                                    </div>
                                    <!-- End Promo Item -->
                                </div>
                                <!-- End Promo -->
                            </div>
                            <!-- End Mega Menu -->
                        </li>
                        <!-- End Careers -->

                        <!-- Contacts -->
                        <li class="hs-has-mega-menu nav-item">
                            <a id="pagesMegaMenu" class="hs-mega-menu-invoker nav-link dropdown-toggle {{ Request::is('contacts') || Request::is('contacts/business-and-partnerships') ? 'active' : '' }}" href="#" role="button">Contacts</a>

                            <!-- Mega Menu -->
                            <div class="hs-mega-menu dropdown-menu w-100" aria-labelledby="pagesMegaMenu" style="min-width: 40rem;">
                                <!-- Promo -->
                                <div class="navbar-dropdown-menu-promo">
                                    <!-- Promo Item -->
                                    <div class="navbar-dropdown-menu-promo-item">
                                        <!-- Promo Link -->
                                        <a class="navbar-dropdown-menu-promo-link {{ Request::is('contacts/business-and-partnerships') || Request::is('contacts') ? 'active' : '' }}" href="{{ route('business') }}">
                                            <div class="d-flex">
                                                <div class="flex-shrink-0">
                                                    <span class="svg-icon svg-icon-sm text-primary">
                                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg') }}">
                                                            <path fill-rule="evenodd" clip-rule="evenodd" d="M6.5 3C5.67157 3 5 3.67157 5 4.5V6H3.5C2.67157 6 2 6.67157 2 7.5C2 8.32843 2.67157 9 3.5 9H5V19.5C5 20.3284 5.67157 21 6.5 21C7.32843 21 8 20.3284 8 19.5V9H20.5C21.3284 9 22 8.32843 22 7.5C22 6.67157 21.3284 6 20.5 6H8V4.5C8 3.67157 7.32843 3 6.5 3Z" fill="#035A4B" />
                                                            <path opacity="0.3" fill-rule="evenodd" clip-rule="evenodd" d="M20.5 11H10V17.5C10 18.3284 10.6716 19 11.5 19H15.5C17.3546 19 19.0277 18.2233 20.2119 16.9775C20.1436 16.9922 20.0727 17 20 17C19.4477 17 19 16.5523 19 16V13C19 12.4477 19.4477 12 20 12C20.5523 12 21 12.4477 21 13V15.9657C21.6334 14.9626 22 13.7741 22 12.5C22 11.6716 21.3284 11 20.5 11Z" fill="#035A4B" />
                                                        </svg>

                                                    </span>
                                                </div>

                                                <div class="flex-grow-1 ms-3">
                                                    <span class="navbar-dropdown-menu-media-title">Business & Partnerships</span>
                                                    <p class="navbar-dropdown-menu-media-desc">Find the latest homes for
                                                        sale, real estate market data.</p>
                                                </div>
                                            </div>
                                        </a>
                                        <!-- End Promo Link -->
                                    </div>
                                    <!-- End Promo Item -->

                                    <!-- Promo Item -->
                                    <div class="navbar-dropdown-menu-promo-item">
                                        <!-- Promo Link -->
                                        <a class="navbar-dropdown-menu-promo-link " href="./demo-jobs/index.html">
                                            <div class="d-flex">
                                                <div class="flex-shrink-0">
                                                    <span class="svg-icon svg-icon-sm text-primary">
                                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg') }}">
                                                            <path opacity="0.3" d="M20 15H4C2.9 15 2 14.1 2 13V7C2 6.4 2.4 6 3 6H21C21.6 6 22 6.4 22 7V13C22 14.1 21.1 15 20 15ZM13 12H11C10.5 12 10 12.4 10 13V16C10 16.5 10.4 17 11 17H13C13.6 17 14 16.6 14 16V13C14 12.4 13.6 12 13 12Z" fill="#035A4B" />
                                                            <path d="M14 6V5H10V6H8V5C8 3.9 8.9 3 10 3H14C15.1 3 16 3.9 16 5V6H14ZM20 15H14V16C14 16.6 13.5 17 13 17H11C10.5 17 10 16.6 10 16V15H4C3.6 15 3.3 14.9 3 14.7V18C3 19.1 3.9 20 5 20H19C20.1 20 21 19.1 21 18V14.7C20.7 14.9 20.4 15 20 15Z" fill="#035A4B" />
                                                        </svg>

                                                    </span>
                                                </div>

                                                <div class="flex-grow-1 ms-3">
                                                    <span class="navbar-dropdown-menu-media-title">Jobs <span class="badge bg-success rounded-pill ms-1">Hot</span></span>
                                                    <p class="navbar-dropdown-menu-media-desc">Search millions of jobs
                                                        online to find the next step in your career.</p>
                                                </div>
                                            </div>
                                        </a>
                                        <!-- End Promo Link -->
                                    </div>
                                    <!-- End Promo Item -->
                                </div>
                                <!-- End Promo -->
                            </div>
                            <!-- End Mega Menu -->
                        </li>
                        <!-- End Contacts -->
                    </ul>
                </div>
            </div>
            <!-- End Collapse -->
        </nav>
    </div>
</header>
