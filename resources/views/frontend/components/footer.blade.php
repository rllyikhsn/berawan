<footer class="bg-light">
    <div class="container pb-1 pb-lg-5">
        <div class="row content-space-t-2">
            <div class="col-lg-3 mb-7 mb-lg-0">
                <!-- Logo -->
                <div class="mb-3">
                    <a class="navbar-brand" href="{{ route('home') }}" aria-label="Space">
                        <img class="navbar-brand-logo" src="{{ asset('frontend/img/logo-clean.png') }}" alt="Image Description">
                    </a>
                </div>
                <!-- End Logo -->

                <!-- List -->
                <ul class="list-unstyled list-py-1">
                    <li>
                        <a class="link-sm link-dark" target="_blank" href="https://www.google.com/maps/place/Berawan.id/@-6.3342528,106.8276795,15z/data=!4m2!3m1!1s0x0:0x7f5e7fb29b2c73b5?sa=X&ved=2ahUKEwjbxbPg5db_AhWH-jgGHamdDhUQ_BJ6BAhvEAk"><i class="bi-geo-alt-fill me-1"></i>Jl. Jeruk Raya No.44, RT.11/RW.1, Jagakarsa, Kec.
                            Jagakarsa, Jakarta, Daerah Khusus Ibukota Jakarta 12620
                        </a>
                    </li>
                    <li>
                        <a class="link-sm link-dark" href="tel:+6282298766855"><i class="bi-telephone-inbound-fill me-1">
                            </i>+62822 98766855
                        </a>
                    </li>
                </ul>
                <!-- End List -->
            </div>
            <!-- End Col -->

            <div class="col-lg-3 mb-7 mb-sm-0">
                <h5 class="text-black mb-3">PT. Berawan Solusi Indonesia</h5>

                <!-- List -->
                <ul class="list-unstyled list-py-1 mb-0">
                    <li><a class="link-sm link-dark">Advancing Through Technology</a></li>
                </ul>
                <!-- End List -->
            </div>
            <!-- End Col -->

            <div class="col-lg-3 mb-7 mb-sm-0">
                <h5 class="text-black mb-3">Our Services</h5>

                <!-- List -->
                <ul class="list-unstyled list-py-1 mb-0">
                    <li><a class="link-sm link-dark" href="{{ route('digital') }}">Digital Platform Solutions</a></li>
                </ul>
                <!-- End List -->
            </div>
            <!-- End Col -->

            <div class="col-lg-3 mb-7 mb-sm-0">
                <h5 class="text-black mb-3">Links</h5>

                <!-- List -->
                <ul class="list-unstyled list-py-1 mb-0">
                    <li><a class="link-sm link-dark" href="{{ route('about-us') }}">About</a></li>
                    <li><a class="link-sm link-dark" href="{{ route('blog') }}">Blogs</a></li>
                    <li><a class="link-sm link-dark" href="{{ route('business') }}">Contact Us</a></li>
                </ul>
                <!-- End List -->
            </div>
            <!-- End Col -->
        </div>
        <!-- End Row -->

        <div class="border-top border-black-10 my-3"></div>

        <div class="row mb-7">
            <div class="col-sm mb-3 mb-sm-0">
                <!-- Socials -->
                <ul class="list-inline list-separator list-separator-light mb-0">
                    <li class="list-inline-item">
                        <a class="link-sm link-dark" href="{{ route('privacy-policy') }}">Privacy &amp; Policy</a>
                    </li>
                    <li class="list-inline-item">
                        <a class="link-sm link-dark" href="{{ route('terms-and-condition') }}">Terms and Condition</a>
                    </li>
                </ul>
                <!-- End Socials -->
            </div>

            <div class="col-sm-auto">
                <!-- Socials -->
                <ul class="list-inline mb-0">
                    <li class="list-inline-item">
                        <a class="btn btn-soft-dark btn-xs btn-icon" target="_blank" href="https://www.linkedin.com/company/berawanid">
                            <i class="bi-linkedin"></i>
                        </a>
                    </li>

                    <li class="list-inline-item">
                        <a class="btn btn-soft-dark btn-xs btn-icon" target="_blank" href="https://g.co/kgs/tNfUB13">
                            <i class="bi-google"></i>
                        </a>
                    </li>

                    <li class="list-inline-item">
                        <!-- Button Group -->
                        <div class="btn-group">
                            <button type="button" class="btn btn-soft-dark btn-xs dropdown-toggle" id="footerSelectLanguage" data-bs-toggle="dropdown" aria-expanded="false" data-bs-dropdown-animation>
                                <span class="d-flex align-items-center">
                                    <span>English (US)</span>
                                </span>
                            </button>

                            <div class="dropdown-menu" aria-labelledby="footerSelectLanguage">
                                <a class="dropdown-item d-flex align-items-center active" href="#">
                                    <span>English (US)</span>
                                </a>
                                <a class="dropdown-item d-flex align-items-center" href="#">
                                    <span>Indonesia (ID)</span>
                                </a>
                            </div>
                        </div>
                        <!-- End Button Group -->
                    </li>
                </ul>
                <!-- End Socials -->
            </div>
        </div>

        <!-- Copyright -->
        <div class="w-md-85 text-lg-center mx-lg-auto">
            <p class="text-black-50 small">&copy; BerawanID. 2024. All rights reserved.</p>
        </div>
        <!-- End Copyright -->
    </div>
</footer>
