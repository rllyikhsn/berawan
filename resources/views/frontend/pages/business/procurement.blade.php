@extends('frontend.layouts.app')
@section('main')
<main id="content" role="main">
    <!-- Hero -->
    <div class="position-relative bg-img-start">
        <div class="container content-space-t-2 content-space-t-md-3 content-space-3 content-space-b-lg-5">
            <div class="w-lg-50">
                <h1 style="color: #FBA904;">You're in good company.</h1>
                <h2 class="h1" style="color: #2350AA">Join millions of businesses on Front.</h2>
            </div>
        </div>

        <!-- Shape -->
        <div class="shape shape-bottom zi-1" style="margin-bottom: -.125rem">
            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 1920 100.1">
                <path fill="#fff" d="M0,0c0,0,934.4,93.4,1920,0v100.1H0L0,0z"></path>
            </svg>
        </div>
        <!-- End Shape -->
    </div>
    <!-- End Hero -->
</main>
@endsection
