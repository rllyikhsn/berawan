@extends('frontend.layouts.app')
@section('main')
<main id="content" role="main">
    <!-- Hero -->
    <div class="position-relative bg-img-start" style="background-image: url(/frontend/img/digital-savy.jpg);">
        <div class="container position-relative content-space-t-3 content-space-t-lg-4 content-space-b-2">
            <!-- Heading -->
            <div class="w-md-80 w-lg-50 text-center mx-md-auto mb-5 mb-md-9">
                <h1 class="display-4 mb-3" style="color: #FFFFFF;">Digital Platform Solutions</h1>
                <p class="lead" style="color: #FFFFFF;">We used popular and secure tech stack for development.</p>
            </div>
            <!-- End Title & Description -->

            <!-- Swiper Slider -->
            <div class="js-swiper-software-hero swiper zi-2 swiper-initialized swiper-horizontal swiper-pointer-events swiper-backface-hidden">
                <div class="swiper-wrapper" id="swiper-wrapper-e43a1ad8cc3691a2" aria-live="polite" style="transform: translate3d(0px, 0px, 0px);">
                    <!-- Slide -->
                    <div class="swiper-slide my-2 swiper-slide-active" style="width: 199.2px; margin-right: 30px;" role="group" aria-label="1 / 5">
                        <!-- Card -->
                        <div class="card card-sm card-transition shadow-sm">
                            <img class="card-img-top" style="padding:10px; max-height: 100px; object-fit:contain" src="/frontend/img/php.png" alt="Image Description">

                            <div class="card-body">
                                <h4 class="card-title">PHP</h4>
                                <p class="card-text small">Laravel, Codeigniter</p>
                            </div>
                        </div>
                        <!-- End Card -->
                    </div>
                    <!-- End Slide -->

                    <!-- Slide -->
                    <div class="swiper-slide my-2 swiper-slide-active" style="width: 199.2px; margin-right: 30px;" role="group" aria-label="1 / 5">
                        <!-- Card -->
                        <div class="card card-sm card-transition shadow-sm">
                            <img class="card-img-top" style="padding:10px; max-height: 100px; object-fit:contain" src="/frontend/img/react.png" alt="Image Description">

                            <div class="card-body">
                                <h4 class="card-title">React</h4>
                                <p class="card-text small">React JS, React Native</p>
                            </div>
                        </div>
                        <!-- End Card -->
                    </div>
                    <!-- End Slide -->

                    <!-- Slide -->
                    <div class="swiper-slide my-2 swiper-slide-active" style="width: 199.2px; margin-right: 30px;" role="group" aria-label="1 / 5">
                        <!-- Card -->
                        <div class="card card-sm card-transition shadow-sm">
                            <img class="card-img-top" style="padding:10px; max-height: 100px; object-fit:contain" src="/frontend/img/sql.png" alt="Image Description">

                            <div class="card-body">
                                <h4 class="card-title">SQL</h4>
                                <p class="card-text small">Postgres SQL, MySQL</p>
                            </div>
                        </div>
                        <!-- End Card -->
                    </div>
                    <!-- End Slide -->

                    <!-- Slide -->
                    <div class="swiper-slide my-2 swiper-slide-active" style="width: 199.2px; margin-right: 30px;" role="group" aria-label="1 / 5">
                        <!-- Card -->
                        <div class="card card-sm card-transition shadow-sm">
                            <img class="card-img-top" style="padding:10px; max-height: 100px; object-fit:cover" src="/frontend/img/go.png" alt="Image Description">

                            <div class="card-body">
                                <h4 class="card-title">Go</h4>
                                <p class="card-text small">Gin, Echo</p>
                            </div>
                        </div>
                        <!-- End Card -->
                    </div>
                    <!-- End Slide -->

                    <!-- Slide -->
                    <div class="swiper-slide my-2 swiper-slide-active" style="width: 199.2px; margin-right: 30px;" role="group" aria-label="1 / 5">
                        <!-- Card -->
                        <div class="card card-sm card-transition shadow-sm">
                            <img class="card-img-top" style="padding:10px; max-height: 100px; object-fit:contain" src="/frontend/img/flutter.png" alt="Image Description">

                            <div class="card-body">
                                <h4 class="card-title">Flutter</h4>
                                <p class="card-text small">Multiplatform Mobile</p>
                            </div>
                        </div>
                        <!-- End Card -->
                    </div>
                    <!-- End Slide -->
                </div>
                <!-- Pagination -->
                <div class="js-swiper-software-hero-pagination swiper-pagination d-lg-none"></div>

                <!-- Preloader -->
                <div class="js-swiper-software-hero-preloader swiper-preloader">
                    <div class="spinner-border text-primary" role="status">
                        <span class="visually-hidden">Loading...</span>
                    </div>
                </div>
                <!-- End Preloader -->
            </div>
            <!-- ENd Swiper Slider -->
        </div>

        <!-- Shape -->
        <div class="shape shape-bottom zi-1">
            <svg width="3000" height="500" viewBox="0 0 3000 500" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M0 500H3000V0L0 500Z" fill="#fff"></path>
            </svg>
        </div>
        <!-- End Shape -->
    </div>
    <!-- End Hero -->

    <div class="container">
        <!-- Heading -->
        <div class="w-md-75 w-lg-50 text-center mx-md-auto mb-5 mb-md-9">
            <h2>We Use Modern Architecture to get optimal results</h2>
        </div>
        <!-- End Heading -->

        <div class="row mb-7">
            <div class="col-sm-6 col-lg-3 mb-7 mb-lg-4">
                <!-- Circle Chart -->
                <div class="circles-chart">
                    <div class="js-circle" data-hs-circles-options="{
                   &quot;value&quot;: 100,
                   &quot;maxValue&quot;: 100,
                   &quot;duration&quot;: 2000,
                   &quot;isViewportInit&quot;: true,
                   &quot;colors&quot;: [&quot;#f8fafd&quot;, &quot;#00c9a7&quot;],
                   &quot;radius&quot;: 100,
                   &quot;width&quot;: 8,
                   &quot;fgStrokeLinecap&quot;: &quot;round&quot;,
                   &quot;fgStrokeMiterlimit&quot;: &quot;10&quot;,
                   &quot;textClass&quot;: &quot;circles-chart-content&quot;,
                   &quot;textFontSize&quot;: 24,
                   &quot;textFontWeight&quot;: 500,
                   &quot;textColor&quot;: &quot;#00c9a7&quot;,
                   &quot;secondaryText&quot;: &quot;Fast-paced, detail-oriented plugins&quot;,
                   &quot;secondaryTextColor&quot;: &quot;#77838f&quot;,
                   &quot;secondaryTextFontSize&quot;: &quot;13&quot;,
                   &quot;secondaryTextFontWeight&quot;: &quot;400&quot;,
                   &quot;dividerSpace&quot;: &quot;10&quot;
                 }" id="circle-9994958179131659">
                        <div class="circles-wrap" style="position: relative; display: inline-block;"><svg xmlns="http://www.w3.org/2000/svg" width="200" height="200">
                                <path fill="transparent" stroke="#f8fafd" stroke-width="8" d="M 99.98044737244527 4.000001991173164 A 96 96 0 1 1 99.86665792594512 4.000092604777535 Z" class="circles-maxValueStroke"></path>
                                <path fill="transparent" stroke="#00c9a7" stroke-width="8" d="M 99.98044737244527 4.000001991173164 A 96 96 0 1 1 99.86665792594512 4.000092604777535 Z" class="circles-valueStroke" stroke-linecap="round" stroke-miterlimit="10"></path>
                            </svg>
                            <div class="circles-chart-content" style="position: absolute; text-align: center; width: 100%; font-size: 52px; height: auto; line-height: normal; font-weight: 500; color: rgb(0, 201, 167);">M<div style="margin-top: 5px; margin-bottom: 5px;"></div>
                                <div style="font-weight: 400; font-size: 13px; color: #77838f;">Microservices</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Circle Chart -->
            </div>
            <!-- End Col -->

            <div class="col-sm-6 col-lg-3 mb-7 mb-lg-4">
                <!-- Circle Chart -->
                <div class="circles-chart">
                    <div class="js-circle" data-hs-circles-options="{
                   &quot;value&quot;: 100,
                   &quot;maxValue&quot;: 100,
                   &quot;duration&quot;: 2000,
                   &quot;isViewportInit&quot;: true,
                   &quot;colors&quot;: [&quot;#f8fafd&quot;, &quot;#ffc107&quot;],
                   &quot;radius&quot;: 100,
                   &quot;width&quot;: 8,
                   &quot;fgStrokeLinecap&quot;: &quot;round&quot;,
                   &quot;fgStrokeMiterlimit&quot;: &quot;10&quot;,
                   &quot;isHideValue&quot;: true,
                   &quot;additionalText&quot;: &quot;4.84&quot;,
                   &quot;additionalTextType&quot;: &quot;prefix&quot;,
                   &quot;textClass&quot;: &quot;circles-chart-content&quot;,
                   &quot;textFontSize&quot;: 24,
                   &quot;textFontWeight&quot;: 500,
                   &quot;textColor&quot;: &quot;#ffc107&quot;,
                   &quot;secondaryText&quot;: &quot;rated on GetBootstrap&quot;,
                   &quot;secondaryTextColor&quot;: &quot;#77838f&quot;,
                   &quot;secondaryTextFontSize&quot;: &quot;13&quot;,
                   &quot;secondaryTextFontWeight&quot;: &quot;400&quot;,
                   &quot;dividerSpace&quot;: &quot;10&quot;
                 }" id="circle-47904412064158386">
                        <div class="circles-wrap" style="position: relative; display: inline-block;"><svg xmlns="http://www.w3.org/2000/svg" width="200" height="200">
                                <path fill="transparent" stroke="#f8fafd" stroke-width="8" d="M 99.98044737244527 4.000001991173164 A 96 96 0 1 1 99.86665792594512 4.000092604777535 Z" class="circles-maxValueStroke"></path>
                                <path fill="transparent" stroke="#ffc107" stroke-width="8" d="M 99.98044737244527 4.000001991173164 A 96 96 0 1 1 99.86665792594512 4.000092604777535 Z" class="circles-valueStroke" stroke-linecap="round" stroke-miterlimit="10"></path>
                            </svg>
                            <div class="circles-chart-content" style="position: absolute; text-align: center; width: 100%; font-size: 52px; height: auto; line-height: normal; font-weight: 500; color: rgb(255, 193, 7);">A<div style="margin-top: 5px; margin-bottom: 5px;"></div>
                                <div style="font-weight: 400; font-size: 13px; color: #77838f;">API-First</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Circle Chart -->
            </div>
            <!-- End Col -->

            <div class="col-sm-6 col-lg-3 mb-7 mb-lg-4">
                <!-- Circle Chart -->
                <div class="circles-chart">
                    <div class="js-circle" data-hs-circles-options="{
                   &quot;value&quot;: 100,
                   &quot;maxValue&quot;: 100,
                   &quot;duration&quot;: 2000,
                   &quot;isViewportInit&quot;: true,
                   &quot;colors&quot;: [&quot;#f8fafd&quot;, &quot;#de4437&quot;],
                   &quot;radius&quot;: 100,
                   &quot;width&quot;: 8,
                   &quot;fgStrokeLinecap&quot;: &quot;round&quot;,
                   &quot;fgStrokeMiterlimit&quot;: &quot;10&quot;,
                   &quot;textClass&quot;: &quot;circles-chart-content&quot;,
                   &quot;textFontSize&quot;: 24,
                   &quot;textFontWeight&quot;: 500,
                   &quot;textColor&quot;: &quot;#de4437&quot;,
                   &quot;secondaryText&quot;: &quot;months of free support to do your best work&quot;,
                   &quot;secondaryTextColor&quot;: &quot;#77838f&quot;,
                   &quot;secondaryTextFontSize&quot;: &quot;13&quot;,
                   &quot;secondaryTextFontWeight&quot;: &quot;400&quot;,
                   &quot;dividerSpace&quot;: &quot;10&quot;
                 }" id="circle-7462595973462047">
                        <div class="circles-wrap" style="position: relative; display: inline-block;"><svg xmlns="http://www.w3.org/2000/svg" width="200" height="200">
                                <path fill="transparent" stroke="#f8fafd" stroke-width="8" d="M 99.98044737244527 4.000001991173164 A 96 96 0 1 1 99.86665792594512 4.000092604777535 Z" class="circles-maxValueStroke"></path>
                                <path fill="transparent" stroke="#FB7C07" stroke-width="8" d="M 99.98044737244527 4.000001991173164 A 96 96 0 1 1 99.86665792594512 4.000092604777535 Z" class="circles-valueStroke" stroke-linecap="round" stroke-miterlimit="10"></path>
                            </svg>
                            <div class="circles-chart-content" style="position: absolute; text-align: center; width: 100%; font-size: 52px; height: auto; line-height: normal; font-weight: 500; color: rgb(222, 68, 55);">C<div style="margin-top: 5px; margin-bottom: 5px;"></div>
                                <div style="font-weight: 400; font-size: 13px; color: #77838f;">Cloud-Native</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Circle Chart -->
            </div>
            <!-- End Col -->

            <div class="col-sm-6 col-lg-3 mb-7 mb-lg-4">
                <!-- Circle Chart -->
                <div class="circles-chart">
                    <div class="js-circle" data-hs-circles-options="{
                   &quot;value&quot;: 100,
                   &quot;maxValue&quot;: 100,
                   &quot;duration&quot;: 2000,
                   &quot;isViewportInit&quot;: true,
                   &quot;colors&quot;: [&quot;#f8fafd&quot;, &quot;#377dff&quot;],
                   &quot;radius&quot;: 100,
                   &quot;width&quot;: 8,
                   &quot;fgStrokeLinecap&quot;: &quot;round&quot;,
                   &quot;fgStrokeMiterlimit&quot;: &quot;10&quot;,
                   &quot;additionalText&quot;: &quot;k+&quot;,
                   &quot;textClass&quot;: &quot;circles-chart-content&quot;,
                   &quot;textFontSize&quot;: 24,
                   &quot;textFontWeight&quot;: 500,
                   &quot;textColor&quot;: &quot;#377dff&quot;,
                   &quot;secondaryText&quot;: &quot;users - from new startups to public companies&quot;,
                   &quot;secondaryTextColor&quot;: &quot;#77838f&quot;,
                   &quot;secondaryTextFontSize&quot;: &quot;13&quot;,
                   &quot;secondaryTextFontWeight&quot;: &quot;400&quot;,
                   &quot;dividerSpace&quot;: &quot;10&quot;
                 }" id="circle-22236382834088775">
                        <div class="circles-wrap" style="position: relative; display: inline-block;">
                            <svg xmlns="http://www.w3.org/2000/svg" width="200" height="200">
                                <path fill="transparent" stroke="#f8fafd" stroke-width="8" d="M 99.98044737244527 4.000001991173164 A 96 96 0 1 1 99.86665792594512 4.000092604777535 Z" class="circles-maxValueStroke"></path>
                                <path fill="transparent" stroke="#377dff" stroke-width="8" d="M 99.98044737244527 4.000001991173164 A 96 96 0 1 1 99.86665792594512 4.000092604777535 Z" class="circles-valueStroke" stroke-linecap="round" stroke-miterlimit="10"></path>
                            </svg>
                            <div class="circles-chart-content" style="position: absolute; text-align: center; width: 100%; font-size: 52px; height: auto; line-height: normal; font-weight: 500; color: rgb(55, 125, 255);">H<div style="margin-top: 5px; margin-bottom: 5px;"></div>
                                <div style="font-weight: 400; font-size: 13px; color: #77838f;">Headless</div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Circle Chart -->
            </div>
            <!-- End Col -->
        </div>
        <!-- End Row -->
    </div>

    <!-- Icon Blocks -->
    <div class="overflow-hidden">
        <div class="container content-space-t-2 content-space-t-lg-3 content-space-b-1 content-space-b-lg-3">
            <!-- Heading -->
            <div class="w-md-75 w-lg-50 text-center mx-md-auto mb-5 mb-md-9">
                <h2>Features built for scale</h2>
                <p>Get insights to dig down into what's powering your growth the most.</p>
            </div>
            <!-- End Heading -->

            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <div class="col-sm-6 mb-5 mb-sm-7">
                            <!-- Icon Blocks -->
                            <div class="pe-lg-6">
                                <span class="svg-icon text-primary mb-4">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path opacity="0.3" d="M10.9607 12.9128H18.8607C19.4607 12.9128 19.9607 13.4128 19.8607 14.0128C19.2607 19.0128 14.4607 22.7128 9.26068 21.7128C5.66068 21.0128 2.86071 18.2128 2.16071 14.6128C1.16071 9.31284 4.96069 4.61281 9.86069 4.01281C10.4607 3.91281 10.9607 4.41281 10.9607 5.01281V12.9128V12.9128Z" fill="#035A4B"></path>
                                        <path d="M12.9607 10.9128V3.01281C12.9607 2.41281 13.4607 1.91281 14.0607 2.01281C16.0607 2.21281 17.8607 3.11284 19.2607 4.61284C20.6607 6.01284 21.5607 7.91285 21.8607 9.81285C21.9607 10.4129 21.4607 10.9128 20.8607 10.9128H12.9607V10.9128Z" fill="#035A4B"></path>
                                    </svg>

                                </span>

                                <h3 class="h4">Smart Dashboards</h3>
                                <p>Provides intelligent dashboards to improve data analysis.</p>
                            </div>
                            <!-- End Icon Blocks -->
                        </div>

                        <div class="col-sm-6 mb-3 mb-sm-7">
                            <!-- Icon Blocks -->
                            <div class="pe-lg-6">
                                <span class="svg-icon text-primary mb-4">
                                    <svg width="20" height="21" viewBox="0 0 20 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path opacity="0.3" d="M16 0.200012H4C1.8 0.200012 0 2.00001 0 4.20001V16.2C0 18.4 1.8 20.2 4 20.2H16C18.2 20.2 20 18.4 20 16.2V4.20001C20 2.00001 18.2 0.200012 16 0.200012ZM15 10.2C15 10.9 14.8 11.6 14.6 12.2H18V16.2C18 17.3 17.1 18.2 16 18.2H12V14.8C11.4 15.1 10.7 15.2 10 15.2C9.3 15.2 8.6 15 8 14.8V18.2H4C2.9 18.2 2 17.3 2 16.2V12.2H5.4C5.1 11.6 5 10.9 5 10.2C5 9.50001 5.2 8.80001 5.4 8.20001H2V4.20001C2 3.10001 2.9 2.20001 4 2.20001H8V5.60001C8.6 5.30001 9.3 5.20001 10 5.20001C10.7 5.20001 11.4 5.40001 12 5.60001V2.20001H16C17.1 2.20001 18 3.10001 18 4.20001V8.20001H14.6C14.8 8.80001 15 9.50001 15 10.2Z" fill="#035A4B"></path>
                                        <path d="M12 1.40002C15.4 2.20002 18 4.80003 18.8 8.20003H14.6C14.1 7.00003 13.2 6.10003 12 5.60003V1.40002V1.40002ZM5.40001 8.20003C5.90001 7.00003 6.80001 6.10003 8.00001 5.60003V1.40002C4.60001 2.20002 2.00001 4.80003 1.20001 8.20003H5.40001ZM14.6 12.2C14.1 13.4 13.2 14.3 12 14.8V19C15.4 18.2 18 15.6 18.8 12.2H14.6V12.2ZM8.00001 14.8C6.80001 14.3 5.90001 13.4 5.40001 12.2H1.20001C2.00001 15.6 4.60001 18.2 8.00001 19V14.8V14.8Z" fill="#035A4B"></path>
                                    </svg>

                                </span>

                                <h4>System Development Life Cycle</h4>
                                <p>We Used the System Development Life Cycle (SDLC) to help teams develop software that is high quality, efficient, consistent, and minimizes risk and increases productivity.</p>
                            </div>
                            <!-- End Icon Blocks -->
                        </div>

                        <div class="col-sm-6 mb-3 mb-sm-0">
                            <!-- Icon Blocks -->
                            <div class="pe-lg-6">
                                <span class="svg-icon text-primary mb-4">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M6 8.725C6 8.125 6.4 7.725 7 7.725H14L18 11.725V12.925L22 9.725L12.6 2.225C12.2 1.925 11.7 1.925 11.4 2.225L2 9.725L6 12.925V8.725V8.725Z" fill="#035A4B"></path>
                                        <path opacity="0.3" d="M22 9.72498V20.725C22 21.325 21.6 21.725 21 21.725H3C2.4 21.725 2 21.325 2 20.725V9.72498L11.4 17.225C11.8 17.525 12.3 17.525 12.6 17.225L22 9.72498ZM15 11.725H18L14 7.72498V10.725C14 11.325 14.4 11.725 15 11.725Z" fill="#035A4B"></path>
                                    </svg>

                                </span>

                                <h4>Mail</h4>
                                <p>We are responsible if you experience any problems after working with us.</p>
                            </div>
                            <!-- End Icon Blocks -->
                        </div>

                        <div class="col-sm-6">
                            <!-- Icon Blocks -->
                            <div class="pe-lg-6">
                                <span class="svg-icon text-primary mb-4">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M15 19.5229C15 20.265 15.9624 20.5564 16.374 19.9389L22.2227 11.166C22.5549 10.6676 22.1976 10 21.5986 10H17V4.47708C17 3.73503 16.0376 3.44363 15.626 4.06106L9.77735 12.834C9.44507 13.3324 9.80237 14 10.4014 14H15V19.5229Z" fill="#035A4B"></path>
                                        <path opacity="0.3" fill-rule="evenodd" clip-rule="evenodd" d="M3 6.5C3 5.67157 3.67157 5 4.5 5H9.5C10.3284 5 11 5.67157 11 6.5C11 7.32843 10.3284 8 9.5 8H4.5C3.67157 8 3 7.32843 3 6.5ZM3 18.5C3 17.6716 3.67157 17 4.5 17H9.5C10.3284 17 11 17.6716 11 18.5C11 19.3284 10.3284 20 9.5 20H4.5C3.67157 20 3 19.3284 3 18.5ZM2.5 11C1.67157 11 1 11.6716 1 12.5C1 13.3284 1.67157 14 2.5 14H6.5C7.32843 14 8 13.3284 8 12.5C8 11.6716 7.32843 11 6.5 11H2.5Z" fill="#035A4B"></path>
                                    </svg>

                                </span>

                                <h4>Speed</h4>
                                <p>The fastest way to build and scale your business.</p>
                            </div>
                            <!-- End Icon Blocks -->
                        </div>
                    </div>
                </div>
                <!-- End Col -->
            </div>
            <!-- End Row -->
        </div>
    </div>
    <!-- End Icon Blocks -->

    <!-- Our Solutions -->
    <div class="container">
        <div class="w-md-75 w-lg-50 text-center mx-md-auto mb-5 mb-md-9">
            <h2>Our Digital Solutions</h2>
        </div>
        <div class="row mb-5">
            <div class="col-lg-6 mb-3 mb-lg-5">
                <!-- Card -->
                <a class="card card-bordered card-transition h-100" href="../demo-help-desk/listing.html">
                    <div class="card-body">
                        <!-- Media -->
                        <div class="d-block d-sm-flex">
                            <div class="flex-shrink-0">
                                <span class="svg-icon text-primary mb-2 mb-sm-0">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path opacity="0.3" d="M4.05424 15.1982C8.34524 7.76818 13.5782 3.26318 20.9282 2.01418C21.0729 1.98837 21.2216 1.99789 21.3618 2.04193C21.502 2.08597 21.6294 2.16323 21.7333 2.26712C21.8372 2.37101 21.9144 2.49846 21.9585 2.63863C22.0025 2.7788 22.012 2.92754 21.9862 3.07218C20.7372 10.4222 16.2322 15.6552 8.80224 19.9462L4.05424 15.1982ZM3.81924 17.3372L2.63324 20.4482C2.58427 20.5765 2.5735 20.7163 2.6022 20.8507C2.63091 20.9851 2.69788 21.1082 2.79503 21.2054C2.89218 21.3025 3.01536 21.3695 3.14972 21.3982C3.28408 21.4269 3.42387 21.4161 3.55224 21.3672L6.66524 20.1802L3.81924 17.3372ZM16.5002 5.99818C16.2036 5.99818 15.9136 6.08615 15.6669 6.25097C15.4202 6.41579 15.228 6.65006 15.1144 6.92415C15.0009 7.19824 14.9712 7.49984 15.0291 7.79081C15.0869 8.08178 15.2298 8.34906 15.4396 8.55884C15.6494 8.76862 15.9166 8.91148 16.2076 8.96935C16.4986 9.02723 16.8002 8.99753 17.0743 8.884C17.3484 8.77046 17.5826 8.5782 17.7474 8.33153C17.9123 8.08486 18.0002 7.79485 18.0002 7.49818C18.0002 7.10035 17.8422 6.71882 17.5609 6.43752C17.2796 6.15621 16.8981 5.99818 16.5002 5.99818Z" fill="#035A4B"></path>
                                        <path d="M4.05423 15.1982L2.24723 13.3912C2.15505 13.299 2.08547 13.1867 2.04395 13.0632C2.00243 12.9396 1.9901 12.8081 2.00793 12.679C2.02575 12.5498 2.07325 12.4266 2.14669 12.3189C2.22013 12.2112 2.31752 12.1219 2.43123 12.0582L9.15323 8.28918C7.17353 10.3717 5.4607 12.6926 4.05423 15.1982V15.1982ZM8.80023 19.9442L10.6072 21.7512C10.6994 21.8434 10.8117 21.9129 10.9352 21.9545C11.0588 21.996 11.1903 22.0083 11.3195 21.9905C11.4486 21.9727 11.5718 21.9252 11.6795 21.8517C11.7872 21.7783 11.8765 21.6809 11.9402 21.5672L15.7092 14.8442C13.6269 16.8245 11.3061 18.5377 8.80023 19.9442V19.9442ZM7.04023 18.1832L12.5832 12.6402C12.7381 12.4759 12.8228 12.2577 12.8195 12.032C12.8161 11.8063 12.725 11.5907 12.5653 11.4311C12.4057 11.2714 12.1901 11.1803 11.9644 11.1769C11.7387 11.1736 11.5205 11.2583 11.3562 11.4132L5.81323 16.9562L7.04023 18.1832Z" fill="#035A4B"></path>
                                    </svg>

                                </span>
                            </div>

                            <div class="flex-grow-1 ms-sm-4">
                                <h3 class="card-title">Desktop Application</h3>
                                <p class="card-text text-body">Give more requirement busines</p>

                                <div class="d-flex">
                                    <div class="flex-shrink-0">
                                        <!-- Avatar Group -->
                                        <div class="avatar-group avatar-group-xs">
                                            <div class="avatar avatar-xs avatar-circle">
                                                <img class="avatar-img" src="#./assets/img/160x160/img1.jpg" alt="Image Description">
                                            </div>
                                            <div class="avatar avatar-xs avatar-circle">
                                                <img class="avatar-img" src="#./assets/img/160x160/img8.jpg" alt="Image Description">
                                            </div>
                                        </div>
                                        <!-- End Avatar Group -->
                                    </div>

                                    <div class="flex-grow-1 ms-2">
                                        <p class="card-text text-dark small mb-0">1 article in this collection</p>
                                        <p class="card-text text-dark small">
                                            <span class="text-muted">Written by</span>
                                            Luisa Woodfine
                                            <span class="text-muted">and</span>
                                            Neil Galavan
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Media -->
                    </div>
                </a>
                <!-- End Card -->
            </div>
            <!-- End Col -->

            <div class="col-lg-6 mb-3 mb-lg-5">
                <!-- Card -->
                <a class="card card-bordered card-transition h-100" href="../demo-help-desk/listing.html">
                    <div class="card-body">
                        <!-- Media -->
                        <div class="d-block d-sm-flex">
                            <div class="flex-shrink-0">
                                <span class="svg-icon text-primary mb-2 mb-sm-0">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path opacity="0.3" d="M22 12C22 17.5 17.5 22 12 22C6.5 22 2 17.5 2 12C2 6.5 6.5 2 12 2C17.5 2 22 6.5 22 12ZM12 7C10.3 7 9 8.3 9 10C9 11.7 10.3 13 12 13C13.7 13 15 11.7 15 10C15 8.3 13.7 7 12 7Z" fill="#035A4B"></path>
                                        <path d="M12 22C14.6 22 17 21 18.7 19.4C17.9 16.9 15.2 15 12 15C8.8 15 6.09999 16.9 5.29999 19.4C6.99999 21 9.4 22 12 22Z" fill="#035A4B"></path>
                                    </svg>

                                </span>
                            </div>

                            <div class="flex-grow-1 ms-sm-4">
                                <h3 class="card-title">Account</h3>
                                <p class="card-text text-body">Adjust your profile and preferences to make Front work just for you!</p>

                                <div class="d-flex">
                                    <div class="flex-shrink-0">
                                        <!-- Avatar Group -->
                                        <div class="avatar-group avatar-group-xs">
                                            <div class="avatar avatar-xs avatar-circle">
                                                <img class="avatar-img" src="#./assets/img/160x160/img9.jpg" alt="Image Description">
                                            </div>
                                            <div class="avatar avatar-xs avatar-circle">
                                                <img class="avatar-img" src="#./assets/img/160x160/img3.jpg" alt="Image Description">
                                            </div>
                                        </div>
                                        <!-- End Avatar Group -->
                                    </div>

                                    <div class="flex-grow-1 ms-2">
                                        <p class="card-text text-dark small mb-0">2 article in this collection</p>
                                        <p class="card-text text-dark small">
                                            <span class="text-muted">Written by</span>
                                            Fiona Burke, Luisa Woodfine
                                            <span class="text-muted">and</span>
                                            Neil Galavan
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Media -->
                    </div>
                </a>
                <!-- End Card -->
            </div>
            <!-- End Col -->

            <div class="col-lg-6 mb-3 mb-lg-5">
                <!-- Card -->
                <a class="card card-bordered card-transition h-100" href="../demo-help-desk/listing.html">
                    <div class="card-body">
                        <!-- Media -->
                        <div class="d-block d-sm-flex">
                            <div class="flex-shrink-0">
                                <span class="svg-icon text-primary mb-2 mb-sm-0">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <g id="com/com014">
                                            <path id="vector" opacity="0.3" fill-rule="evenodd" clip-rule="evenodd" d="M4 4L11.6314 2.56911C11.875 2.52343 12.125 2.52343 12.3686 2.56911L20 4V11.9033C20 15.696 18.0462 19.2211 14.83 21.2313L12.53 22.6687C12.2057 22.8714 11.7943 22.8714 11.47 22.6687L9.17001 21.2313C5.95382 19.2211 4 15.696 4 11.9033L4 4Z" fill="#035A4B"></path>
                                            <path id="group" fill-rule="evenodd" clip-rule="evenodd" d="M9.5 10.5C9.5 9.11929 10.6193 8 12 8C13.3807 8 14.5 9.11929 14.5 10.5V11C15.0523 11 15.5 11.4477 15.5 12V15C15.5 15.5523 15.0523 16 14.5 16H9.5C8.94772 16 8.5 15.5523 8.5 15V12C8.5 11.4477 8.94772 11 9.5 11V10.5ZM12 9C11.1716 9 10.5 9.67157 10.5 10.5V11H13.5V10.5C13.5 9.67157 12.8284 9 12 9Z" fill="#035A4B"></path>
                                        </g>
                                    </svg>

                                </span>
                            </div>

                            <div class="flex-grow-1 ms-sm-4">
                                <h3 class="card-title">Data security</h3>
                                <p class="card-text text-body">Detailed information on how our customer data is securely stored.</p>

                                <div class="d-flex">
                                    <div class="flex-shrink-0">
                                        <!-- Avatar Group -->
                                        <div class="avatar-group avatar-group-xs">
                                            <div class="avatar avatar-xs avatar-circle">
                                                <img class="avatar-img" src="#./assets/img/160x160/img9.jpg" alt="Image Description">
                                            </div>
                                            <div class="avatar avatar-xs avatar-circle">
                                                <img class="avatar-img" src="#./assets/img/160x160/img4.jpg" alt="Image Description">
                                            </div>
                                        </div>
                                        <!-- End Avatar Group -->
                                    </div>

                                    <div class="flex-grow-1 ms-2">
                                        <p class="card-text text-dark small mb-0">5 article in this collection</p>
                                        <p class="card-text text-dark small">
                                            <span class="text-muted">Written by</span>
                                            Fiona Burke, Luisa Woodfine, Neil Galavan
                                            <span class="text-muted">and</span>
                                            Monica Garcia
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Media -->
                    </div>
                </a>
                <!-- End Card -->
            </div>
            <!-- End Col -->

            <div class="col-lg-6 mb-3 mb-lg-5">
                <!-- Card -->
                <a class="card card-bordered card-transition h-100" href="../demo-help-desk/listing.html">
                    <div class="card-body">
                        <!-- Media -->
                        <div class="d-block d-sm-flex">
                            <div class="flex-shrink-0">
                                <span class="svg-icon text-primary mb-2 mb-sm-0">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path opacity="0.3" d="M18 10V20C18 20.6 18.4 21 19 21C19.6 21 20 20.6 20 20V10H18Z" fill="#035A4B"></path>
                                        <path opacity="0.3" d="M11 10V17H6V10H4V20C4 20.6 4.4 21 5 21H12C12.6 21 13 20.6 13 20V10H11Z" fill="#035A4B"></path>
                                        <path opacity="0.3" d="M10 10C10 11.1 9.1 12 8 12C6.9 12 6 11.1 6 10H10Z" fill="#035A4B"></path>
                                        <path opacity="0.3" d="M18 10C18 11.1 17.1 12 16 12C14.9 12 14 11.1 14 10H18Z" fill="#035A4B"></path>
                                        <path opacity="0.3" d="M14 4H10V10H14V4Z" fill="#035A4B"></path>
                                        <path opacity="0.3" d="M17 4H20L22 10H18L17 4Z" fill="#035A4B"></path>
                                        <path opacity="0.3" d="M7 4H4L2 10H6L7 4Z" fill="#035A4B"></path>
                                        <path d="M6 10C6 11.1 5.1 12 4 12C2.9 12 2 11.1 2 10H6ZM10 10C10 11.1 10.9 12 12 12C13.1 12 14 11.1 14 10H10ZM18 10C18 11.1 18.9 12 20 12C21.1 12 22 11.1 22 10H18ZM19 2H5C4.4 2 4 2.4 4 3V4H20V3C20 2.4 19.6 2 19 2ZM12 17C12 16.4 11.6 16 11 16H6C5.4 16 5 16.4 5 17C5 17.6 5.4 18 6 18H11C11.6 18 12 17.6 12 17Z" fill="#035A4B"></path>
                                    </svg>

                                </span>
                            </div>

                            <div class="flex-grow-1 ms-sm-4">
                                <h3 class="card-title">Market</h3>
                                <p class="card-text text-body">Some further explanation on when Front can and cannot be used.</p>

                                <div class="d-flex">
                                    <div class="flex-shrink-0">
                                        <!-- Avatar Group -->
                                        <div class="avatar-group avatar-group-xs">
                                            <div class="avatar avatar-xs avatar-circle">
                                                <img class="avatar-img" src="#./assets/img/160x160/img7.jpg" alt="Image Description">
                                            </div>
                                        </div>
                                        <!-- End Avatar Group -->
                                    </div>

                                    <div class="flex-grow-1 ms-2">
                                        <p class="card-text text-dark small mb-0">3 article in this collection</p>
                                        <p class="card-text text-dark small">
                                            <span class="text-muted">Written by</span>
                                            Luisa Woodfine
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Media -->
                    </div>
                </a>
                <!-- End Card -->
            </div>
            <!-- End Col -->

            <div class="col-lg-6 mb-3 mb-lg-5">
                <!-- Card -->
                <a class="card card-bordered card-transition h-100" href="../demo-help-desk/listing.html">
                    <div class="card-body">
                        <!-- Media -->
                        <div class="d-block d-sm-flex">
                            <div class="flex-shrink-0">
                                <span class="svg-icon text-primary mb-2 mb-sm-0">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M22 7H2V11H22V7Z" fill="#035A4B"></path>
                                        <path opacity="0.3" d="M21 19H3C2.4 19 2 18.6 2 18V6C2 5.4 2.4 5 3 5H21C21.6 5 22 5.4 22 6V18C22 18.6 21.6 19 21 19ZM14 14C14 13.4 13.6 13 13 13H5C4.4 13 4 13.4 4 14C4 14.6 4.4 15 5 15H13C13.6 15 14 14.6 14 14ZM16 15.5C16 16.3 16.7 17 17.5 17H18.5C19.3 17 20 16.3 20 15.5C20 14.7 19.3 14 18.5 14H17.5C16.7 14 16 14.7 16 15.5Z" fill="#035A4B"></path>
                                    </svg>

                                </span>
                            </div>

                            <div class="flex-grow-1 ms-sm-4">
                                <h3 class="card-title">Subscription</h3>
                                <p class="card-text text-body">Assistance on how and when you might use the subscription product.</p>

                                <div class="d-flex">
                                    <div class="flex-shrink-0">
                                        <!-- Avatar Group -->
                                        <div class="avatar-group avatar-group-xs">
                                            <div class="avatar avatar-xs avatar-circle">
                                                <img class="avatar-img" src="#./assets/img/160x160/img9.jpg" alt="Image Description">
                                            </div>
                                            <div class="avatar avatar-xs avatar-circle">
                                                <img class="avatar-img" src="#./assets/img/160x160/img3.jpg" alt="Image Description">
                                            </div>
                                        </div>
                                        <!-- End Avatar Group -->
                                    </div>

                                    <div class="flex-grow-1 ms-2">
                                        <p class="card-text text-dark small mb-0">2 article in this collection</p>
                                        <p class="card-text text-dark small">
                                            <span class="text-muted">Written by</span>
                                            Fiona Burke, Luisa Woodfine
                                            <span class="text-muted">and</span>
                                            Neil Galavan
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Media -->
                    </div>
                </a>
                <!-- End Card -->
            </div>
            <!-- End Col -->

            <div class="col-lg-6 mb-3 mb-lg-5">
                <!-- Card -->
                <a class="card card-bordered card-transition h-100" href="../demo-help-desk/listing.html">
                    <div class="card-body">
                        <!-- Media -->
                        <div class="d-block d-sm-flex">
                            <div class="flex-shrink-0">
                                <span class="svg-icon text-primary mb-2 mb-sm-0">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path opacity="0.3" fill-rule="evenodd" clip-rule="evenodd" d="M12 17C16.4183 17 20 13.4183 20 9C20 4.58172 16.4183 1 12 1C7.58172 1 4 4.58172 4 9C4 13.4183 7.58172 17 12 17Z" fill="#035A4B"></path>
                                        <path opacity="0.3" fill-rule="evenodd" clip-rule="evenodd" d="M6.53819 9L10.568 19.3624L11.976 18.8149L13.3745 19.3674L17.4703 9H6.53819ZM9.46188 11H14.5298L11.9759 17.4645L9.46188 11Z" fill="#035A4B"></path>
                                        <path opacity="0.3" d="M10 22H14V22C14 23.1046 13.1046 24 12 24V24C10.8954 24 10 23.1046 10 22V22Z" fill="#035A4B"></path>
                                        <path fill-rule="evenodd" clip-rule="evenodd" d="M8 17C8 16.4477 8.44772 16 9 16H15C15.5523 16 16 16.4477 16 17C16 17.5523 15.5523 18 15 18C15.5523 18 16 18.4477 16 19C16 19.5523 15.5523 20 15 20C15.5523 20 16 20.4477 16 21C16 21.5523 15.5523 22 15 22H9C8.44772 22 8 21.5523 8 21C8 20.4477 8.44772 20 9 20C8.44772 20 8 19.5523 8 19C8 18.4477 8.44772 18 9 18C8.44772 18 8 17.5523 8 17Z" fill="#035A4B"></path>
                                    </svg>

                                </span>
                            </div>

                            <div class="flex-grow-1 ms-sm-4">
                                <h3 class="card-title">Tips, tricks &amp; more</h3>
                                <p class="card-text text-body">Tips and tools for beginners and experts alike.</p>

                                <div class="d-flex">
                                    <div class="flex-shrink-0">
                                        <!-- Avatar Group -->
                                        <div class="avatar-group avatar-group-xs">
                                            <div class="avatar avatar-xs avatar-circle">
                                                <img class="avatar-img" src="#./assets/img/160x160/img7.jpg" alt="Image Description">
                                            </div>
                                        </div>
                                        <!-- End Avatar Group -->
                                    </div>

                                    <div class="flex-grow-1 ms-2">
                                        <p class="card-text text-dark small mb-0">2 article in this collection</p>
                                        <p class="card-text text-dark small">
                                            <span class="text-muted">Written by</span>
                                            Luisa Woodfine
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Media -->
                    </div>
                </a>
                <!-- End Card -->
            </div>
            <!-- End Col -->
        </div>
    </div>
    <!-- End Our Solutions -->

    <!-- Features -->
    <div class="container content-space-t-2 content-space-lg-3">
        <!-- Heading -->
        <div class="w-md-75 w-lg-50 text-center mx-md-auto mb-5 mb-md-9">
            <h2>Front makes designing easy and performance fast</h2>
        </div>
        <!-- End Heading -->

        <!-- Nav Scroller -->
        <div class="js-nav-scroller hs-nav-scroller-horizontal">
            <span class="hs-nav-scroller-arrow-prev" style="display: none;">
                <a class="hs-nav-scroller-arrow-link" href="javascript:;">
                    <i class="bi-chevron-left"></i>
                </a>
            </span>

            <span class="hs-nav-scroller-arrow-next" style="display: none;">
                <a class="hs-nav-scroller-arrow-link" href="javascript:;">
                    <i class="bi-chevron-right"></i>
                </a>
            </span>

            <!-- Nav -->
            <ul class="nav nav-segment nav-pills nav-fill mx-auto mb-7" id="featuresTab" role="tablist" style="max-width: 30rem;">
                <li class="nav-item" role="presentation">
                    <a class="nav-link active" href="#featuresOne" id="featuresOne-tab" data-bs-toggle="tab" data-bs-target="#featuresOne" role="tab" aria-controls="featuresOne" aria-selected="true">App Marketplace</a>
                </li>

                <li class="nav-item" role="presentation">
                    <a class="nav-link" href="#featuresTwo" id="featuresTwo-tab" data-bs-toggle="tab" data-bs-target="#featuresTwo" role="tab" aria-controls="featuresTwo" aria-selected="false" tabindex="-1">Course</a>
                </li>

                <li class="nav-item" role="presentation">
                    <a class="nav-link" href="#featuresThree" id="featuresThree-tab" data-bs-toggle="tab" data-bs-target="#featuresThree" role="tab" aria-controls="featuresThree" aria-selected="false" tabindex="-1">Account Dashboard</a>
                </li>
            </ul>
            <!-- End Nav -->
        </div>
        <!-- End Nav Scroller -->

        <!-- Tab Content -->
        <div class="tab-content" id="featuresTabContent">
            <div class="tab-pane fade active show" id="featuresOne" role="tabpanel" aria-labelledby="featuresOne-tab">
                <!-- Devices -->
                <div class="devices">
                    <!-- Mobile Device -->
                    <figure class="device-mobile rotated-3d-right">
                        <div class="device-mobile-frame">
                            <img class="device-mobile-img" src="#.assets/img/407x867/img1.png" alt="Image Description">
                        </div>
                    </figure>
                    <!-- End Mobile Device -->

                    <!-- Browser Device -->
                    <figure class="device-browser">
                        <div class="device-browser-header">
                            <div class="device-browser-header-btn-list">
                                <span class="device-browser-header-btn-list-btn"></span>
                                <span class="device-browser-header-btn-list-btn"></span>
                                <span class="device-browser-header-btn-list-btn"></span>
                            </div>
                            <div class="device-browser-header-browser-bar">www.htmlstream.com/front/</div>
                        </div>

                        <div class="device-browser-frame">
                            <img class="device-browser-img" src="#.assets/img/1618x1010/img1.png" alt="Image Description">
                        </div>
                    </figure>
                    <!-- End Browser Device -->
                </div>
                <!-- End Devices -->
            </div>

            <div class="tab-pane fade" id="featuresTwo" role="tabpanel" aria-labelledby="featuresTwo-tab">
                <!-- Devices -->
                <div class="devices">
                    <!-- Mobile Device -->
                    <figure class="device-mobile rotated-3d-right">
                        <div class="device-mobile-frame">
                            <img class="device-mobile-img" src="#.assets/img/407x867/img2.png" alt="Image Description">
                        </div>
                    </figure>
                    <!-- End Mobile Device -->

                    <!-- Browser Device -->
                    <figure class="device-browser">
                        <div class="device-browser-header">
                            <div class="device-browser-header-btn-list">
                                <span class="device-browser-header-btn-list-btn"></span>
                                <span class="device-browser-header-btn-list-btn"></span>
                                <span class="device-browser-header-btn-list-btn"></span>
                            </div>
                            <div class="device-browser-header-browser-bar">www.htmlstream.com/front/</div>
                        </div>

                        <div class="device-browser-frame">
                            <img class="device-browser-img" src="#.assets/img/1618x1010/img2.png" alt="Image Description">
                        </div>
                    </figure>
                    <!-- End Browser Device -->
                </div>
                <!-- End Devices -->
            </div>

            <div class="tab-pane fade" id="featuresThree" role="tabpanel" aria-labelledby="featuresThree-tab">
                <!-- Devices -->
                <div class="devices">
                    <!-- Mobile Device -->
                    <figure class="device-mobile rotated-3d-right">
                        <div class="device-mobile-frame">
                            <img class="device-mobile-img" src="#.assets/img/407x867/img4.png" alt="Image Description">
                        </div>
                    </figure>
                    <!-- End Mobile Device -->

                    <!-- Browser Device -->
                    <figure class="device-browser">
                        <div class="device-browser-header">
                            <div class="device-browser-header-btn-list">
                                <span class="device-browser-header-btn-list-btn"></span>
                                <span class="device-browser-header-btn-list-btn"></span>
                                <span class="device-browser-header-btn-list-btn"></span>
                            </div>
                            <div class="device-browser-header-browser-bar">www.htmlstream.com/front/</div>
                        </div>

                        <div class="device-browser-frame">
                            <img class="device-browser-img" src="#.assets/img/1618x1010/img4.png" alt="Image Description">
                        </div>
                    </figure>
                    <!-- End Browser Device -->
                </div>
                <!-- End Devices -->
            </div>
        </div>
        <!-- End Tab Content -->
    </div>
    <!-- End Features -->

    <!-- Subscribe -->
    <div class="container content-space-b-2 content-space-b-lg-3">
        <div class="w-md-75 w-lg-50 text-center mx-md-auto">
            <div class="row justify-content-lg-between">
                <!-- Heading -->
                <div class="mb-5">
                    <h2>Get started with Front</h2>
                </div>
                <!-- End Heading -->

                <form>
                    <!-- Input Card -->
                    <div class="input-card input-card-sm border mb-3">
                        <div class="input-card-form">
                            <label for="subscribeForm" class="form-label visually-hidden">Enter email</label>
                            <input type="text" class="form-control form-control-lg" id="subscribeForm" placeholder="Enter email" aria-label="Enter email">
                        </div>
                        <button type="button" class="btn btn-primary btn-lg">Get started</button>
                    </div>
                    <!-- End Input Card -->
                </form>

                <p><a href="./page-terms.html">Terms &amp; conditions</a> applied.</p>
            </div>
        </div>
    </div>
    <!-- End Subscribe -->
</main>
@endsection
@section('script-main')
<script>
    (function() {
        // INITIALIZATION OF SWIPER
        // =======================================================
        var swiper = new Swiper('.js-swiper-software-hero', {
            preloaderClass: 'custom-swiper-lazy-preloader',
            slidesPerView: 1,
            pagination: {
                el: '.js-swiper-software-hero-pagination',
                dynamicBullets: true,
                clickable: true,
            },
            breakpoints: {
                380: {
                    slidesPerView: 2,
                    spaceBetween: 15,
                },
                580: {
                    slidesPerView: 3,
                    spaceBetween: 15,
                },
                768: {
                    slidesPerView: 4,
                    spaceBetween: 15,
                },
                1024: {
                    slidesPerView: 5,
                    spaceBetween: 30,
                },
            },
            on: {
                'imagesReady': function(swiper) {
                    const preloader = swiper.el.querySelector('.js-swiper-software-hero-preloader')
                    preloader.parentNode.removeChild(preloader)
                }
            }
        });
    })()
</script>
@endsection
