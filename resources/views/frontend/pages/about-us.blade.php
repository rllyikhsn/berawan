@extends('frontend.layouts.app')
@section('main')
<main id="content" role="main">
    <!-- Hero -->
    <div class="position-relative bg-img-start" style="background-image: url(frontend/img/about-us.jpg);">
        <div class="container content-space-t-2 content-space-t-md-3 content-space-3 content-space-b-lg-5">
            <div class="w-lg-50">
                <h1 style="color: #224FA9; margin-bottom:20px">Provide Solutions until you are satisfied.</h1>
                <h2 class="h5" style="color: #224FA9">Provide convenience and peace of mind in your
                    goods procurement process with our professional team. We have experienced
                    experts who have strong relationships with procurement and distributors
                    in various regions. And enthusiastic about the development of information technology.</h2>
            </div>
        </div>

        <!-- Shape -->
        <div class="shape shape-bottom zi-1" style="margin-bottom: -.125rem">
            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 1920 100.1">
                <path fill="#fff" d="M0,0c0,0,934.4,93.4,1920,0v100.1H0L0,0z"></path>
            </svg>
        </div>
        <!-- End Shape -->
    </div>
    <!-- End Hero -->

    <!-- Info Visi & Misi -->
    <div class="container">
        <div class="row justify-content-lg-between">
            <div class="col-lg-12 mb-12" style="margin-bottom: 50px;">
                <p class="lead">Since 2007, we have helped 25 companies launch over 1k incredible products. We believe the best digital solutions are built at the intersection of business strategy, available technology, and real user's needs.</p>
            </div>
            <div class="col-lg-6 mb-6">
                <h2>Our Vision</h2>
                <p>Since 2007, we have helped 25 companies launch over 1k incredible products. We believe the best digital solutions are built at the intersection of business strategy, available technology, and real user's needs.</p>
            </div>
            <!-- End Col -->

            <div class="col-lg-6 mb-6">
                <h2>Our Mission</h2>
                <p>Since 2007, we have helped 25 companies launch over 1k incredible products. We believe the best digital solutions are built at the intersection of business strategy, available technology, and real user's needs.</p>
                <p>Things can get really complex, really quickly, and a pragmatic, synthetic and clear vision is essential to be able to create something that, after all, is meant to be used. Emotions also have a big role to play and developing clear and beautiful aesthetics is of the utmost importance to create a pleasant environment in which the user actually enjoys the time spent in it. In the end, we're all suckers for beautiful things that just work</p>
            </div>
            <!-- End Col -->
        </div>
        <!-- End Row -->
    </div>
    <!-- End Info Visi & Misi -->
</main>
@endsection
