@extends('frontend.layouts.app')
@section('main')
<main id="content" role="main">
    <!-- Hero -->
    <div class="container content-space-t-3 content-space-t-lg-5 content-space-b-1 content-space-b-md-2">
        <div class="w-md-75 w-lg-50 text-center mx-md-auto">
            <h1 class="display-4">Blogs</h1>
            <p class="lead">Latest updates and Knowledge resources.</p>
        </div>
    </div>
    <!-- End Hero -->

    <!-- Card Grid -->
    <div class="container content-space-b-2 content-space-b-lg-3">
        <div class="row justify-content-md-between align-items-md-center mb-7">
            <div class="col-md-5 mb-5 mb-md-0">
                <!-- Tags -->
                <div class="d-md-flex align-items-md-center text-center text-md-start">
                    <span class="d-block me-md-3 mb-2 mb-md-1">Tags:</span>
                    <a class="btn btn-soft-secondary btn-xs rounded-pill m-1" href="javascript:;">Business</a>
                    <a class="btn btn-soft-secondary btn-xs rounded-pill m-1" href="javascript:;">Health</a>
                    <a class="btn btn-soft-secondary btn-xs rounded-pill m-1" href="javascript:;">Environment</a>
                    <a class="btn btn-soft-secondary btn-xs rounded-pill m-1" href="javascript:;">Adventure</a>
                </div>
                <!-- End Tags -->
            </div>
            <!-- End Col -->

            <div class="col-md-5 col-lg-4">
                <form action="{{ route('blog') }}" method="GET">
                    <!-- Input Card -->
                    <div class="input-group">
                        <input type="text" class="form-control" name="search" placeholder="Search articles" aria-label="Search articles">
                        <button type="submit" class="btn btn-primary"><i class="bi-search"></i></button>
                    </div>
                    <!-- End Input Card -->
                </form>
            </div>
            <!-- End Col -->
        </div>
        <!-- End Row -->

        <!-- Card -->
        <div class="card card-stretched-vertical mb-10">
            <div class="row gx-0">
                <div class="col-lg-8">
                    <div class="shape-container overflow-hidden">
                        <img class="card-img" src="./assets/img/900x450/img2.jpg" alt="Image Description">

                        <!-- Shape -->
                        <div class="shape shape-end d-none d-lg-block zi-1">
                            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 100.1 1920" height="101%">
                                <path fill="#fff" d="M0,1920c0,0,93.4-934.4,0-1920h100.1v1920H0z" />
                            </svg>
                        </div>
                        <!-- End Shape -->

                        <!-- Shape -->
                        <div class="shape shape-bottom d-lg-none zi-1" style="margin-bottom: -.25rem">
                            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 1920 100.1">
                                <path fill="#fff" d="M0,0c0,0,934.4,93.4,1920,0v100.1H0L0,0z"></path>
                            </svg>
                        </div>
                        <!-- End Shape -->
                    </div>
                </div>
                <!-- End Col -->

                <div class="col-lg-4">
                    <!-- Card Body -->
                    <div class="card-body">
                        <h3 class="card-title">
                            <a class="text-dark" href="./blog-article.html">How Google Assistant now helps you record stories for kids</a>
                        </h3>

                        <p class="card-text">Google is constantly updating its consumer AI, Google Assistant, with new features.</p>

                        <!-- Card Footer -->
                        <div class="card-footer">
                            <div class="d-flex align-items-center">
                                <div class="flex-shrink-0 avatar-group avatar-group-xs">
                                    <a class="avatar avatar-xs avatar-circle" href="#" data-bs-toggle="tooltip" data-bs-placement="top" title="Aaron Larsson">
                                        <img class="avatar-img" src="./assets/img/160x160/img3.jpg" alt="Image Description">
                                    </a>
                                    <a class="avatar avatar-xs avatar-circle" href="#" data-bs-toggle="tooltip" data-bs-placement="top" title="John O'nolan">
                                        <img class="avatar-img" src="./assets/img/160x160/img4.jpg" alt="Image Description">
                                    </a>
                                </div>

                                <div class="flex-grow-1">
                                    <div class="d-flex justify-content-end">
                                        <p class="card-text">July 15</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Card Footer -->
                    </div>
                    <!-- End Card Body -->
                </div>
                <!-- End Col -->
            </div>
            <!-- End Row -->
        </div>
        <!-- End Card -->

        <div class="row mb-7">
            @foreach($data as $article)
            <div class="col-sm-6 col-lg-4 mb-4">
                <!-- Card -->
                <div class="card h-100">
                    <div class="shape-container">
                        <img class="card-img-top" src="/images/articles/original/{{ $article->thumbnail_image }}" alt="Image Description" style="object-fit: cover; max-height: 280px;">

                        <!-- Shape -->
                        <div class="shape shape-bottom zi-1" style="margin-bottom: -.25rem">
                            <svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 1920 100.1">
                                <path fill="#fff" d="M0,0c0,0,934.4,93.4,1920,0v100.1H0L0,0z"></path>
                            </svg>
                        </div>
                        <!-- End Shape -->
                    </div>

                    <!-- Card Body -->
                    <div class="card-body">
                        <h3 class="card-title">
                            <a class="text-dark" href="./blog-article.html">{{ $article->title }}</a>
                        </h3>

                        <p class="card-text" style="-webkit-line-clamp: 3; overflow: hidden; text-overflow: ellipsis; display: -webkit-box; -webkit-box-orient: vertical;">{{ $article->short_content }}</p>
                    </div>
                    <!-- End Card Body -->

                    <!-- Card Footer -->
                    <div class="card-footer">
                        <div class="d-flex align-items-center">
                            <div class="flex-grow-1">
                                <div class="d-flex justify-content-end">
                                    <p class="card-text">{{ formatDateIndonesia($article->published_at) }}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Card Footer -->
                </div>
                <!-- End Card -->
            </div>
            <!-- End Col -->
            @endforeach
        </div>
        <!-- End Row -->

        <!-- Pagination -->
        <nav aria-label="Page navigation">
            <ul class="pagination justify-content-end">
                @if($isPrev)
                <li class="page-item">
                    <a class="page-link" href="{{ $prevUrl }}" aria-label="Next">
                        <span aria-hidden="true">
                            <i class="bi-chevron-double-left small"></i>
                        </span>
                        Previous
                    </a>
                </li>
                @endif
                @if($isNext)
                <li class="page-item">
                    <a class="page-link" href="{{ $nextUrl }}" aria-label="Next">
                        Next
                        <span aria-hidden="true">
                            <i class="bi-chevron-double-right small"></i>
                        </span>
                    </a>
                </li>
                @endif
            </ul>
        </nav>
        <!-- End Pagination -->
    </div>
    <!-- End Card Grid -->
</main>
@endsection
