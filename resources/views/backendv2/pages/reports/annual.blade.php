@extends('backendv2.layouts.app')

@section('title', 'DataTables')

@push('style')
<link rel="stylesheet" href="{{ asset('backendv2/css/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('backendv2/css/datatables/datatables.bootstrap5.min.css') }}">
@endpush

@section('main')
<!-- Page Header -->
<div class="page-header">
    <div class="row align-items-end">
        <div class="col-sm mb-2 mb-sm-0">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-no-gutter">
                    <li class="breadcrumb-item">
                        <a class="breadcrumb-link" href="javascript:;">Pages</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a class="breadcrumb-link" href="javascript:;">Reports</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">
                        Annuals
                    </li>
                </ol>
            </nav>

            <h1 class="page-header-title">Annual Report</h1>
        </div>
        <!-- End Col -->

        <div class="col-sm-auto">
            <a class="btn btn-primary btn-sm" data-bs-toggle="modal" data-bs-target="#modalAnnualReport">
                <i class="bi-person-plus-fill me-1"></i> Add Report Annual
            </a>
        </div>
        <!-- End Col -->
    </div>
    <!-- End Row -->
</div>
<!-- End Page Header -->

<!-- Card -->
<div class="card">
    <!-- Header -->
    <div class="card-header">
        <h4 class="card-header-title">Data Annual Reports</h4>
    </div>
    <!-- End Header -->

    <!-- Table -->
    <div class="table-responsive" style="padding: 15px;">
        <table id="table-report-annuals" class="table table-lg table-borderless table-thead-bordered table-align-middle card-table">
            <thead class="thead-light">
                <tr>
                    <th width="40%">Title</th>
                    <th width="5%">File</th>
                    <th width="15%">Created At</th>
                    <th width="15%">Action</th>
                </tr>
            </thead>
        </table>
    </div>
    <!-- End Table -->
</div>
<!-- End Card -->

<!-- Modal -->
<div class="modal fade" id="modalAnnualReport" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="modalAnnualReportLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalAnnualReportLabel">Add / Update Data</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form method="POST" id="form-annual-report">
                    <div class="mb-4">
                        <label for="title" class="form-label">Title <i class="bi-question-circle text-body ms-1" data-bs-toggle="tooltip" data-bs-placement="top" title="Judul laporan tahunan."></i></label>
                        <input type="text" class="form-control" name="title" id="title" placeholder="Title Reports" aria-label="Judul Laporan" value="">
                    </div>
                    <div class="mb-4">
                        <label for="file" class="form-label">File <i class="bi-question-circle text-body ms-1" data-bs-toggle="tooltip" data-bs-placement="top" title="File laporan tahunan."></i></label>
                        <input type="file" id="file" required name="file" class="form-control">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-bs-dismiss="modal">Cancel</button>
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- End Modal -->



<!-- Modal -->
<div class="modal fade" id="modalPreviewDocument" data-bs-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="modalPreviewDocumentLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="modalPreviewDocumentLabel">Preview Document</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <embed type="application/pdf" id="embedPreview" width="100%" height="100%"></embed>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-white" data-bs-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!-- End Modal -->
@endsection

@push('scripts')
<script src="{{ asset('backendv2/js/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('backendv2/js/datatables/datatables.bootstrap5.min.js') }}"></script>
<script src="{{ asset('backend/library/sweetalert/dist/sweetalert.min.js') }}"></script>
<script src="{{ asset('backendv2/js/page/reports/annual.js') }}"></script>
@endpush
