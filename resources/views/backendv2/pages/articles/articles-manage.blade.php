@extends('backendv2.layouts.app')

@section('title', 'DataTables')

@push('style')
<!-- CSS Libraries -->
<link rel="stylesheet" href="{{ asset('backend/library/bootstrap-daterangepicker/daterangepicker.css') }}">
<link rel="stylesheet" href="{{ asset('backend/library/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('backend/library/selectric/public/selectric.css') }}">
<link rel="stylesheet" href="{{ asset('backend/library/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}">
<link rel="stylesheet" href="{{ asset('backend/library/bootstrap-tagsinput/dist/bootstrap-tagsinput.css') }}">
@endpush


@section('main')
<!-- Page Header -->
<div class="page-header">
    <div class="row align-items-end">
        <div class="col-sm mb-2 mb-sm-0">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-no-gutter">
                    <li class="breadcrumb-item">
                        <a class="breadcrumb-link" href="{{ route('admin') }}">Home</a>
                    </li>
                    <li class="breadcrumb-item">
                        <a class="breadcrumb-link" href="{{ route('articles') }}">Articles</a>
                    </li>
                    <li class="breadcrumb-item active" aria-current="page">
                        Manage
                    </li>
                </ol>
            </nav>

            <h1 class="page-header-title">Users</h1>
        </div>
        <!-- End Col -->
    </div>
    <!-- End Row -->
</div>
<!-- End Page Header -->

<form action="" method="POST" id="form-articles">
    <!-- Card -->
    <div class="card">
        <!-- Header -->
        <div class="card-header card-header-content-between">
            <h4 class="card-header-title">Product information</h4>
            <div class="row">
                <div class="col">
                    <a class="btn btn-warning btn-sm">
                        <i class="me-1"></i> Cancel
                    </a>
                    <button type="submit" class="btn btn-primary btn-sm">
                        <i class="bi-save me-1"></i> Save
                    </button>
                </div>
            </div>
        </div>
        <!-- End Header -->

        <div class="card-body">
            <!-- Form -->
            <div class="mb-4">
                <label for="titleLabel" class="form-label">Name <i class="bi-question-circle text-body ms-1" data-bs-toggle="tooltip" data-bs-placement="top" title="Judul."></i></label>

                <input type="text" class="form-control form-control-lg" name="title" id="title" placeholder="Judul" aria-label="Judul." value="" required>
            </div>
            <!-- End Form -->

            <div class="row">
                <div class="col-sm-6">
                    <!-- Form -->
                    <div class="mb-4">
                        <label for="SKULabel" class="form-label">Categories</label>

                        <div class="tom-select-custom mb-3">
                            <select class="js-select form-select" id="article_categories" autocomplete="off" data-hs-tom-select-options='{
                            "placeholder": "Categories"
                          }' multiple>
                            </select>
                        </div>
                        <!-- End Form -->
                    </div>
                    <!-- End Col -->
                </div>
                <!-- End Row -->
                <div class="col-sm-6">
                    <!-- Form -->
                    <div class="mb-4">
                        <label for="TagLabel" class="form-label">Tags</label>

                        <div class="tom-select-custom mb-3">
                            <select class="js-select form-select" id="article_tags" autocomplete="off" data-hs-tom-select-options='{
                            "placeholder": "Tags"
                          }' multiple>
                            </select>
                        </div>
                        <!-- End Form -->
                    </div>
                    <!-- End Col -->
                </div>
                <!-- End Row -->

                <!-- Form -->
                <div class="mb-4">
                    <label class="form-label" for="short_content">Konten Pendek</label>
                    <textarea name="short_content" id="short_content" class="form-control" placeholder="Konten Pendek" rows="4" required></textarea>
                </div>
                <!-- End Form -->

                <!-- Form -->
                <div class="mb-4">
                    <label class="form-label">Isi Artikel</label>
                    <!-- Quill -->
                    <div class="quill-custom">
                        <div class="js-quill" id="long_content" style="height: 15rem;" data-hs-quill-options='{
                     "placeholder": "Type your description..."
                     }'>
                        </div>
                    </div>
                    <!-- End Quill -->
                </div>
                <!-- End Form -->
            </div>
            <div class="row">
                <div class="col-12 col-md-3 col-lg-3">
                    <label for="SKULabel" class="form-label">SKU</label>
                    <div id="image-preview-pinned" class="image-preview dz-dropzone-card">
                        <input type="text" class="btn btn-white btn-sm" autocomplete="off" id="image_input" name="pinned_image" style="display: none;" />
                        <div class="d-flex justify-content-center mb-2">
                            <span class="btn btn-white btn-sm" data-bs-toggle="modal" id="image" data-element-form="image" data-element-preview="image-preview-pinned" data-bs-target="#modalMediaFiles">Browse files</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>
</div>

<!-- Media Files -->
@include('backendv2.components.media-files')
</div>
<!-- End Card -->
@endsection

@push('scripts')
<script src="{{ asset('backend/library/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('backend/library/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}"></script>
<script src="{{ asset('backend/library/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js') }}"></script>

<script src="{{ asset('backend/library/selectric/public/jquery.selectric.min.js') }}"></script>

<script src="{{ asset('backend/library/sweetalert/dist/sweetalert.min.js') }}"></script>
<script src="{{ asset('backendv2/js/media-files/media-files.js') }}"></script>
<script src="{{ asset('backendv2/js/utils/content.js') }}"></script>

<script src="{{ asset('backendv2/js/page/articles/articles-manage.js') }}"></script>

@endpush

@push('scripts-page')
<script>
    "use strict";
</script>
@endpush
