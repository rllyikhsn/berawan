@extends('backendv2.layouts.app')

@section('title', 'Articles')

@push('style')
<!-- CSS Libraries -->

<link rel="stylesheet" href="{{ asset('backendv2/css/datatables/datatables.min.css') }}">
<link rel="stylesheet" href="{{ asset('backendv2/css/datatables/datatables.bootstrap5.min.css') }}">
@endpush


@section('main')
<!-- Page Header -->
<div class="page-header">
    <div class="row align-items-end">
        <div class="col-sm mb-2 mb-sm-0">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb breadcrumb-no-gutter">
                    <li class="breadcrumb-item">
                        <a class="breadcrumb-link" href="{{ route('admin') }}">Home</a>
                    </li>
                    <li class="breadcrumb-item active">
                        Articles
                    </li>
                </ol>
            </nav>

            <h1 class="page-header-title">Articles</h1>
        </div>
        <!-- End Col -->

        <div class="col-sm-auto">
            <a class="btn btn-primary" href="{{ route('add-articles') }}">
                <i class="bi-person-plus-fill me-1"></i> Tambah Article
            </a>
        </div>
        <!-- End Col -->
    </div>
    <!-- End Row -->
</div>
<!-- End Page Header -->

<!-- Card -->
<div class="card">
    <!-- Header -->
    <div class="card-header">
        <h4 class="card-header-title">Product information</h4>
    </div>
    <!-- End Header -->

    <!-- Table -->
    <div class="table-responsive" style="padding: 15px;">
        <table id="table-articles" class="table table-lg table-borderless table-thead-bordered table-align-middle card-table">
            <thead class="thead-light">
                <tr>
                    <th>Cover</th>
                    <th>Judul</th>
                    <th>Short Content</th>
                    <th>Dibuat</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
    <!-- End Table -->
</div>
<!-- End Card -->
@endsection

@push('scripts')

<script src="{{ asset('backendv2/js/datatables/datatables.min.js') }}"></script>
<script src="{{ asset('backendv2/js/datatables/datatables.bootstrap5.min.js') }}"></script>
<script src="{{ asset('backend/library/sweetalert/dist/sweetalert.min.js') }}"></script>
<script src="{{ asset('backendv2/js/page/articles/articles.js') }}"></script>
@endpush
