@push('style')
@endpush
<div class="modal fade" tabindex="-1" role="dialog" id="modalMediaFiles">
    <div class="modal-dialog modal-xl" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">List All Media</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <ul class="nav nav-tabs" id="tabMedia" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="upload-media-tab" data-toggle="tab" href="#uploadMedia" role="tab" aria-controls="home" aria-selected="true">Upload Media</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="media-library-tab" data-toggle="tab" href="#mediaLibrary" role="tab" aria-controls="contact" aria-selected="false">Media
                            Library</a>
                    </li>
                </ul>
                <div class="tab-content tab-bordered" id="mediaUpload">
                    <div class="tab-pane fade show active" id="uploadMedia" role="tabpanel" aria-labelledby="upload-media-tab" style="padding-left: 20px; padding-top:10px">
                        <form>
                            <!-- Dropzone -->
                            <div id="modalDropzone" class="js-dropzone row dz-dropzone dz-dropzone-card">
                                <div class="dz-message">
                                    <img class="avatar avatar-xl avatar-4x3 mb-3" src="{{ asset('backendv2/svg/illustrations/oc-browse.svg') }}" alt="Image Description">

                                    <h5>Drag and drop your file here</h5>

                                    <p class="mb-2">or</p>

                                    <span class="btn btn-white btn-sm" style="margin-bottom: 20px;">Browse files</span>
                                </div>
                            </div>
                            <!-- End Dropzone -->
                        </form>
                    </div>
                    <div class="tab-pane fade" id="mediaLibrary" role="tabpanel" aria-labelledby="media-library-tab">
                        <div class="row">
                            <div class="col-12 col-md-8 col-lg-8" style="padding-top: 10px;">
                                <div class="card">
                                    <div class="card-body" style="height: 650px; overflow-y: auto;">
                                        <div id="data-media-list" class="js-fancybox row justify-content-sm gx-3">
                                            <div class="col-6 col-sm-4 col-md-3 mb-3 mb-lg-5">
                                                <!-- Card -->
                                                <div class="card card-sm">
                                                    <img class="card-img-top" src="./assets/img/400x400/img7.jpg" alt="Image Description">
                                                </div>
                                                <!-- End Card -->
                                            </div>
                                            <!-- End Col -->
                                        </div>
                                        <!-- <div class="form-group">
                                            <div class="row gutters-sm">
                                                <div class="col-12 col-md-12 col-lg-12">
                                                    <div class="row" id="data-media-list">
                                                        <div class="col-6 col-sm-3">
                                                            <label class="imagecheck mb-4">
                                                                <input name="imagecheck" type="radio" value="6" data-image-id="666" data-image-src="https://demo.getstisla.com/assets/img/news/img06.jpg" data-image-created-at="20-08-2023" data-image-dimension="200 x 300 pixels" data-image-size="16.9 KB" class="imagecheck-input">
                                                                <figure class="imagecheck-figure">
                                                                    <img src="https://demo.getstisla.com/assets/img/news/img06.jpg" alt="}" class="imagecheck-image">
                                                                </figure>
                                                            </label>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-12 col-md-12 col-lg-12">
                                                    <div class="text-center">
                                                        <button class="btn btn-info btn-load-more">Load More
                                                            ...</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-4 col-lg-4" style="padding-top: 10px;" id="image-preview">
                                <div class="card">
                                    <div class="card-body" style="height: 650px; overflow-y: auto;">
                                        <div id="data-media-list" class="js-fancybox row justify-content-sm gx-3">
                                            <div class="col-12">
                                                <!-- Card -->
                                                <div class="card card-sm">
                                                    <img class="card-img-top" src="https://demo.getstisla.com/assets/img/news/img03.jpg" alt="Image Description" class="imagecheck-image" id="image-preview-src">
                                                    <div style="padding: 10px">
                                                        <br>
                                                        <p>Tanggal Upload</p>
                                                        <p id="image-preview-created-at">17-08-2023</p>
                                                        <p>File Size</p>
                                                        <p id="image-preview-size">3 Mb</p>
                                                        <p>Size Pixel</p>
                                                        <p id="image-preview-dimension">600 x 400 piksel</p>
                                                        <p>
                                                            <a href="http://google.com" target="_blank" id="image-preview-link-src">http://google.com</a>
                                                        </p>
                                                        <button class="btn btn-danger btn-delete-image-preview"><i class="bi-trash"></i>
                                                            Hapus</button>
                                                    </div>
                                                </div>
                                                <!-- End Card -->
                                            </div>
                                            <!-- End Col -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer bg-whitesmoke br">
                <button type="button" class="btn btn-primary btn-pick-image" data-bs-dismiss="modal">Select
                    Image</button>
            </div>
        </div>
    </div>
</div>


@push('scripts')
<script>
    $(document).ready(function() {
        // When a tab link is clicked
        $('.nav-link').click(function() {
            event.preventDefault();

            // Remove "active" class from all other .nav-link elements
            $('.nav-link').not(this).removeClass('active');

            // Add "active" class to the clicked .nav-link
            $(this).addClass('active');

            // Get the href attribute of the clicked tab link
            var targetTabId = $(this).attr('href');

            // Hide all tab panes
            $('.tab-pane').removeClass('show active');

            // Show the corresponding tab pane
            $(targetTabId).addClass('show active');
        });
    });
</script>
@endpush
