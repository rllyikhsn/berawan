<?php

use Illuminate\Support\Facades\Route;


use App\Base\Controllers\Admin\AuthController;
use App\Frontend\Controllers\AboutUsController;
use App\Frontend\Controllers\BlogController;
use App\Frontend\Controllers\BusinessController;
use App\Frontend\Controllers\ContactController;
use App\Frontend\Controllers\HomeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('/about-us', [AboutUsController::class, 'index'])->name('about-us');
Route::get('/business/digital-platform-solutions', [BusinessController::class, 'digital'])->name('digital');
Route::get('/business/procurement-inventory-services', [BusinessController::class, 'procurement'])->name('procurement');
Route::get('/business/merchant-distributions', [BusinessController::class, 'merchant'])->name('merchant');
Route::get('/informations/blogs', [BlogController::class, 'index'])->name('blog');
Route::get('/informations/blogs/page/{page}', [BlogController::class, 'page']);
Route::get('/informations/portfolios', [BlogController::class, 'index'])->name('portfolio');
Route::get('/informations/portfolios/page/{page}', [BlogController::class, 'page']);
Route::get('/contacts', [ContactController::class, 'business'])->name('business');
Route::get('/contacts/business-and-partnerships', [ContactController::class, 'business'])->name('business-partnership');
Route::get('/privacy-policy', [AboutUsController::class, 'privacy'])->name('privacy-policy');
Route::get('/terms-and-condition', [AboutUsController::class, 'terms'])->name('terms-and-condition');


// Authentication
Route::get('/admin/auth-login', [AuthController::class, 'showLoginForm'])->name('login');
Route::post('/admin/auth-login', [AuthController::class, 'login']);
Route::get('/admin/logout', [AuthController::class, 'logout'])->name('logout');

// Testing
Route::get('/admin/testing', function () {
    return view('backendv2.pages.testing', ['type_menu' => 'dashboard']);
})->name('testing')->middleware('auth.web');

// Dashboard
Route::get('/admin', function () {
    return view('backend.pages.dashboard-general-dashboard', ['type_menu' => 'dashboard']);
})->name('admin')->middleware('auth.web');

// Artikel
Route::get('/admin/articles', function () {
    return view('backendv2.pages.articles.articles', ['type_menu' => 'articles']);
})->name('articles')->middleware('auth.web');

Route::get('/admin/articles/manage', function () {
    return view('backendv2.pages.articles.articles-manage', ['type_menu' => 'articles']);
})->name('add-articles')->middleware('auth.web');

Route::get('/admin/articles/manage/{id}', function () {
    return view('backendv2.pages.articles.articles-manage', ['type_menu' => 'articles']);
})->middleware('auth.web');

// Pengumuman
Route::get('/admin/content-announcements', function () {
    return view('backend.pages.announcements.content-announcements', ['type_menu' => 'announcements']);
})->name('content-announcements')->middleware('auth.web');

Route::get('/admin/content-announcements/manage', function () {
    return view('backend.pages.announcements.content-announcements-manage', ['type_menu' => 'announcements']);
})->name('add-content-announcements')->middleware('auth.web');

Route::get('/admin/content-announcements/manage/{id}', function () {
    return view('backend.pages.announcements.content-announcements-manage', ['type_menu' => 'announcements']);
})->middleware('auth.web');

// Manajemen Kontent
Route::get('/admin/content-management', function () {
    return view('backend.pages.contents.identity', ['type_menu' => 'content-management']);
})->name('content-management')->middleware('auth.web');

Route::get('/admin/content-management/identity', function () {
    return view('backend.pages.contents.identity', ['type_menu' => 'content-management']);
})->name('content-identity')->middleware('auth.web');

Route::get('/admin/content-management/logo-identity', function () {
    return view('backend.pages.contents.logo-identity', ['type_menu' => 'content-management']);
})->name('content-logo-identity')->middleware('auth.web');

Route::get('/admin/content-management/address', function () {
    return view('backend.pages.contents.address', ['type_menu' => 'content-management']);
})->name('content-address')->middleware('auth.web');

Route::get('/admin/content-management/contact', function () {
    return view('backend.pages.contents.contact', ['type_menu' => 'content-management']);
})->name('content-contact')->middleware('auth.web');

Route::get('/admin/content-management/media', function () {
    return view('backend.pages.contents.media', ['type_menu' => 'content-management']);
})->name('content-media')->middleware('auth.web');

//Manajemen Kategori
Route::get('/admin/content-categories', function () {
    return view('backendv2.pages.categories.content-categories', ['type_menu' => 'categories']);
})->name('content-categories')->middleware('auth.web');

Route::get('/admin/content-categories/manage', function () {
    return view('backend.pages.categories.content-categories-manage', ['type_menu' => 'categories']);
})->name('add-content-categories')->middleware('auth.web');

Route::get('/admin/content-categories/manage/{id}', function () {
    return view('backend.pages.categories.content-categories-manage', ['type_menu' => 'categories']);
})->middleware('auth.web');

//Manajemen Tag
Route::get('/admin/content-tags', function () {
    return view('backendv2.pages.tags.content-tags', ['type_menu' => 'tags']);
})->name('content-tags')->middleware('auth.web');

Route::get('/admin/content-tags/manage', function () {
    return view('backend.pages.tags.content-tags-manage', ['type_menu' => 'tags']);
})->name('add-content-tags')->middleware('auth.web');

Route::get('/admin/content-tags/manage/{id}', function () {
    return view('backend.pages.tags.content-tags-manage', ['type_menu' => 'tags']);
})->middleware('auth.web');

//Manajemen Workshop
Route::get('/admin/content-workshops', function () {
    return view('backend.pages.workshops.content-workshops', ['type_menu' => 'workshops']);
})->name('content-workshops')->middleware('auth.web');

Route::get('/admin/content-workshops/manage', function () {
    return view('backend.pages.workshops.content-workshops-manage', ['type_menu' => 'workshops']);
})->name('add-content-workshops')->middleware('auth.web');

Route::get('/admin/content-workshops/manage/{id}', function () {
    return view('backend.pages.workshops.content-workshops-manage', ['type_menu' => 'dashboard']);
})->middleware('auth.web');

// Sliders
Route::get('/admin/content-sliders', function () {
    return view('backend.pages.sliders.content-sliders', ['type_menu' => 'dashboard']);
})->name('content-sliders')->middleware('auth.web');

Route::get('/admin/content-sliders/manage', function () {
    return view('backend.pages.sliders.content-sliders-manage', ['type_menu' => 'dashboard']);
})->name('add-sliders')->middleware('auth.web');

Route::get('/admin/content-sliders/manage/{id}', function () {
    return view('backend.pages.sliders.content-sliders-manage', ['type_menu' => 'dashboard']);
})->middleware('auth.web');

// Reports
Route::get('/admin/reports/annual', function () {
    return view('backendv2.pages.reports.annual', ['type_menu' => 'reports']);
})->name('annual-report')->middleware('auth.web');

Route::get('/admin/reports/finance', function () {
    return view('backendv2.pages.reports.finance', ['type_menu' => 'reports']);
})->name('finance-report')->middleware('auth.web');
