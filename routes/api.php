<?php

use App\Customer\Controllers\CustomerController;
use App\User\Controllers\AuthController;
use App\User\Controllers\UserController;
use App\Content\Controllers\ArticleController;
use App\Content\Controllers\ContentAnnouncementController;
use App\Content\Controllers\ContentCategoryController;
use App\Content\Controllers\ContentClientController;
use App\Content\Controllers\ContentController;
use App\Content\Controllers\ContentGalleryController;
use App\Content\Controllers\ContentMediaFileController;
use App\Content\Controllers\ContentReportAnnualController;
use App\Content\Controllers\ContentReportFinanceController;
use App\Content\Controllers\ContentReviewClientController;
use App\Content\Controllers\ContentSliderController;
use App\Content\Controllers\ContentTagController;
use App\Content\Controllers\ContentWhyUsController;
use App\Content\Controllers\ContentWorkshopController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'middleware' => 'api'
], function ($router) {

    /**
     * Authentication Module
     */
    Route::group(['prefix' => 'auth'], function () {
        Route::post('register', [AuthController::class, 'register']);
        Route::post('login', [AuthController::class, 'login']);
        Route::post('logout', [AuthController::class, 'logout']);
        Route::post('refresh', [AuthController::class, 'refresh']);
        Route::get('me', [AuthController::class, 'me']);
    });

    /**
     * Article Module
     */
    Route::resource('articles', ArticleController::class);
    Route::get('articles/view/all', [ArticleController::class, 'indexAll']);
    Route::get('articles/view/search', [ArticleController::class, 'search']);
    Route::post('articles/{id}', [ArticleController::class, 'update']);
    Route::post('articles/view/datatables', [ArticleController::class, 'datatables']);

    /**
     * Users Module
     */
    Route::resource('users', UserController::class);
    Route::get('users/all', [UserController::class, 'indexAll']);
    Route::get('users/search', [UserController::class, 'search']);
    Route::post('users/datatables', [UserController::class, 'datatables']);

    /**
     * Content Media File Module
     */
    Route::resource('content-media-files', ContentMediaFileController::class);

    /**
     * Content
     */
    Route::resource('content', ContentController::class);
    Route::post('content/detail', [ContentController::class, 'show']);
    Route::post('content/identity', [ContentController::class, 'updateIdentity']);
    Route::post('content/logo-identity', [ContentController::class, 'updateLogoIdentity']);
    Route::post('content/contact', [ContentController::class, 'updateContact']);
    Route::post('content/media', [ContentController::class, 'updateMedia']);

    /**
     * Category Module
     */
    Route::resource('content-categories', ContentCategoryController::class);
    Route::get('content-categories/view/all', [ContentCategoryController::class, 'indexAll']);
    Route::get('content-categories/view/search', [ContentCategoryController::class, 'search']);
    Route::post('content-categories/{id}', [ContentCategoryController::class, 'update']);
    Route::post('content-categories/view/datatables', [ContentCategoryController::class, 'datatables']);

    /**
     * Tag Module
     */
    Route::resource('content-tags', ContentTagController::class);
    Route::get('content-tags/view/all', [ContentTagController::class, 'indexAll']);
    Route::get('content-tags/view/search', [ContentTagController::class, 'search']);
    Route::post('content-tags/{id}', [ContentTagController::class, 'update']);
    Route::post('content-tags/view/datatables', [ContentTagController::class, 'datatables']);

    /**
     * Announcement Module
     */
    Route::resource('content-announcements', ContentAnnouncementController::class);
    Route::get('content-announcements/view/all', [ContentAnnouncementController::class, 'indexAll']);
    Route::get('content-announcements/view/search', [ContentAnnouncementController::class, 'search']);
    Route::post('content-announcements/{id}', [ContentAnnouncementController::class, 'update']);
    Route::post('content-announcements/view/datatables', [ContentAnnouncementController::class, 'datatables']);

    /**
     * Workshop Module
     */
    Route::resource('content-workshops', ContentWorkshopController::class);
    Route::get('content-workshops/view/all', [ContentWorkshopController::class, 'indexAll']);
    Route::get('content-workshops/view/search', [ContentWorkshopController::class, 'search']);
    Route::post('content-workshops/{id}', [ContentWorkshopController::class, 'update']);
    Route::post('content-workshops/view/datatables', [ContentWorkshopController::class, 'datatables']);

    /**
     * WhyUs Module
     */
    Route::resource('content-whyus', ContentWhyUsController::class);
    Route::get('content-whyus/view/all', [ContentWhyUsController::class, 'indexAll']);
    Route::get('content-whyus/view/search', [ContentWhyUsController::class, 'search']);
    Route::post('content-whyus/{id}', [ContentWhyUsController::class, 'update']);
    Route::post('content-whyus/view/datatables', [ContentWhyUsController::class, 'datatables']);

    /**
     * Review Client Module
     */
    Route::resource('content-review-clients', ContentReviewClientController::class);
    Route::get('content-review-clients/view/all', [ContentReviewClientController::class, 'indexAll']);
    Route::get('content-review-clients/view/search', [ContentReviewClientController::class, 'search']);
    Route::post('content-review-clients/{id}', [ContentReviewClientController::class, 'update']);
    Route::post('content-review-clients/view/datatables', [ContentReviewClientController::class, 'datatables']);

    /**
     * Gallery Module
     */
    Route::resource('content-galleries', ContentGalleryController::class);
    Route::get('content-galleries/view/all', [ContentGalleryController::class, 'indexAll']);
    Route::get('content-galleries/view/search', [ContentGalleryController::class, 'search']);
    Route::post('content-galleries/{id}', [ContentGalleryController::class, 'update']);
    Route::post('content-galleries/view/datatables', [ContentGalleryController::class, 'datatables']);

    /**
     * Slider Module
     */
    Route::resource('content-sliders', ContentSliderController::class);
    Route::get('content-sliders/view/all', [ContentSliderController::class, 'indexAll']);
    Route::get('content-sliders/view/search', [ContentSliderController::class, 'search']);
    Route::post('content-sliders/{id}', [ContentSliderController::class, 'update']);
    Route::post('content-sliders/view/datatables', [ContentSliderController::class, 'datatables']);

    /**
     * Client Module
     */
    Route::resource('content-clients', ContentClientController::class);
    Route::get('content-clients/view/all', [ContentClientController::class, 'indexAll']);
    Route::get('content-clients/view/search', [ContentClientController::class, 'search']);
    Route::post('content-clients/{id}', [ContentClientController::class, 'update']);
    Route::post('content-clients/view/datatables', [ContentClientController::class, 'datatables']);

    /**
     * Reports Module
     */
    Route::resource('content-report-annuals', ContentReportAnnualController::class);
    Route::get('content-report-annuals/view/all', [ContentReportAnnualController::class, 'indexAll']);
    Route::get('content-report-annuals/view/search', [ContentReportAnnualController::class, 'search']);
    Route::post('content-report-annuals/{id}', [ContentReportAnnualController::class, 'update']);
    Route::post('content-report-annuals/view/datatables', [ContentReportAnnualController::class, 'datatables']);

    Route::resource('content-report-finances', ContentReportFinanceController::class);
    Route::get('content-report-finances/view/all', [ContentReportFinanceController::class, 'indexAll']);
    Route::get('content-report-finances/view/search', [ContentReportFinanceController::class, 'search']);
    Route::post('content-report-finances/{id}', [ContentReportFinanceController::class, 'update']);
    Route::post('content-report-finances/view/datatables', [ContentReportFinanceController::class, 'datatables']);
});
