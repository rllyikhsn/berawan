"use strict";
var urlApi = "/api/content-tags";
var id = null;

var table = $("#table-tags").DataTable({
    buttons: ["copy", "excel", "pdf", "csv"],
    // responsive: true,
    order: [[2, "desc"]],
    language: {
        searchPlaceholder: "Search...",
        sSearch: "",
        lengthMenu: "_MENU_ "
    },
    aLengthMenu: [
        [10, 25, 50, 100, 200, -1],
        [10, 25, 50, 100, 200, "Semua Data"],
    ],
    iDisplayLength: 5,
    scrollX: false,
    processing: true,
    serverSide: true,
    ajax: {
        url: urlApi + "/view/datatables",
        type: "POST",
        data: function (d) {
            $.extend(d, {
                // code: $("#fNoPPBE").val(),
            });
        },
    },
    rowCallback: function (row, data, index) {},
    columns: [
        {
            data: "title",
            name: "title",
        },
        {
            data: "created_at",
            render: function (data, type, row, meta) {
                return formattedDateTime(row.created_at);
            },
        },
        {
            data: "id",
            render: function (data, type, row, meta) {
                return (
                    `
                <button type="button" style="margin-bottom: 5px" class="btn btn-primary btn-sm btn-edit" data-id="` +
                    row.id +
                    `">
                    <i class="bi-pencil"></i>&nbsp;Edit
                </button>
                <button type="button" style="margin-bottom: 5px" class="btn btn-danger btn-sm btn-delete" data-id="` +
                    row.id +
                    `">
                    <i class="bi-trash"></i>&nbsp;Delete
                </button>
                `
                );
            },
        },
    ],
});

$("#table-tags tbody").on("click", ".btn-edit", function () {
    id = $(this).data("id");

    $.ajax({
        url: `${urlApi}/${id}`,
        type: "GET",
        contentType: false,
        processData: false,
        success: function (result) {
            $("#title").val(result.data.title);

            $("#modalTags").modal("show");
        },
        error: function (jqXHR, textStatus, errorThrown) {},
    });
});

$("#table-tags tbody").on("click", ".btn-delete", function () {
    var id = $(this).data("id");
    swal({
        title: "Hapus data ini?",
        text: "Data yang dihapus tidak dapat dikembalikan!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
                url: urlApi + "/" + id,
                type: "DELETE",
                success: function (result) {
                    swal("Berhasil Hapus Data", {
                        icon: "success",
                    });
                    table.draw();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    swal("Hapus Data Gagal", {
                        icon: "danger",
                    });
                },
            });
        }
    });
});

$("#form-tags").on("submit", function (e) {
    e.preventDefault();

    var formData = new FormData(this);

    if ($("#title").val() == "") {
        swal("Simpan Data Gagal", {
            icon: "error",
            text: "Title tidak boleh kosong",
        });

        return false;
    }

    swal({
        title: "Simpan data ini?",
        text: "Data yang diisikan sudah sesuai?",
        icon: "info",
        buttons: true,
    }).then((willSave) => {
        if (willSave) {
            $.ajax({
                url: urlApi + (id !== null ? `/${id}` : ""),
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,
                success: function (result) {
                    if (result.status) {
                        swal("Berhasil Simpan Data", {
                            icon: "success",
                        }).then((data) => {
                            $("#modalTags").modal("hide");
                            $("#form-tags")[0].reset();
                            table.ajax.reload();
                            id = null;
                        });
                    } else {
                        swal("Simpan Data Gagal", {
                            icon: "error",
                        });
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    swal("Simpan Data Gagal", {
                        icon: "error",
                        text: "Periksa kembali data yang diisikan.",
                    });
                },
            });
        }
    });
});
