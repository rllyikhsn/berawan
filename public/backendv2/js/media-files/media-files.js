"use strict";

var imageId, imageSrc, elementForm, elementPreview;

$("#image-preview").hide();
$("#modalMediaFiles .btn-pick-image").hide();
$("#modalMediaFiles .btn-load-more").hide();

$("#image").on("click", function () {
    elementForm = $(this).data("element-form");
    elementPreview = $(this).data("element-preview");
});

var urlApiMediaFiles = "/api/content-media-files";
var perPage = 4;
var page = 1;

$("#upload-media-tab").on("click", function () {});

$("#media-library-tab").on("click", function () {
    resetMediaLibrary();
});

function resetMediaLibrary() {
    $("#data-media-list").html("");
    $("#image-preview").hide();
    perPage = 20;
    page = 1;
    loadMediaLibrary();
}

$("#modalMediaFiles .btn-load-more").on("click", function () {
    loadMediaLibrary();
});

function loadMediaLibrary() {
    $.ajax({
        url: urlApiMediaFiles + `?page=${page}&perPage=${perPage}`,
        type: "GET",
        contentType: false,
        processData: false,
        success: function (result) {
            perPage = result.data.per_page;
            page = page + 1;

            if (result.data.next_page_url != null) {
                $("#modalMediaFiles .btn-load-more").show();
            } else {
                $("#modalMediaFiles .btn-load-more").hide();
                perPage = 4;
                page = 1;
            }
            result.data.data.forEach((image) => {
                var valueHtml = `
                    <div class="col-4">
                        <div class="form-check form-check-label-highlighter text-center">
                            <input name="imagecheck" type="radio" id="labelCheckImage${
                                image.id
                            }" value="${image.id}"
                                data-image-id="${image.id}"
                                data-image-src="${image.src}"
                                data-image-created-at="${formattedDateTime(
                                    image.created_at
                                )}"
                                data-image-dimension="${image.dimension}"
                                data-image-size="${
                                    image.size
                                }" class="imagecheck-input form-check-input">
                            <label class="imagecheck form-check-label mb-4" for="labelCheckImage${
                                image.id
                            }">
                                <figure class="imagecheck-figure">
                                    <img src="${image.src}"
                                        alt="${
                                            image.name
                                        }" class="imagecheck-image form-check-img">
                                </figure>
                            </label>
                        </div>
                    </div>
                `;
                $("#data-media-list").append(valueHtml);
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            // window.location.href = urlWeb;
        },
    });
}

$("#data-media-list").on("change", "input[name=imagecheck]", function () {
    imageId = $(this).data("image-id");
    imageSrc = $(this).data("image-src");
    var imageCreatedAt = $(this).data("image-created-at");
    var imageDimension = $(this).data("image-dimension");
    var imageSize = $(this).data("image-size");

    $("#image-preview").show();
    $("#modalMediaFiles .btn-pick-image").show();
    $(".btn-delete-image-preview").attr("data-image-preview-id", imageId);
    $("#image-preview-src").attr("src", imageSrc);
    $("#image-preview-created-at").html(imageCreatedAt);
    $("#image-preview-size").html(imageSize);
    $("#image-preview-dimension").html(imageDimension);
    $("#image-preview-link-src").attr("href", imageSrc);
    $("#image-preview-link-src").html(imageSrc);
});

$("#modalMediaFiles .btn-delete-image-preview").on("click", function () {
    swal({
        title: "Hapus foto ini?",
        text: "Foto yang dihapus tidak dapat dikembalikan!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
                url: urlApiMediaFiles + `/${imageId}`,
                type: "DELETE",
                contentType: false,
                processData: false,
                success: function (result) {
                    $("#" + elementPreview).removeAttr("style");
                    $("#" + elementForm).removeAttr("value");
                    $("#image-preview").hide();
                    $("#modalMediaFiles .btn-pick-image").hide();
                    $('input[name="imagecheck"]:checked').prop(
                        "checked",
                        false
                    );
                    resetMediaLibrary();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    // window.location.href = urlWeb;
                },
            });
        }
    });
});

// var dropzone = new Dropzone("#mydropzone", {
//     url: urlApiMediaFiles,
//     acceptedFiles: "image/*",
//     autoProcessQueue: true,
//     maxFilesize: 5,
//     maxFilesize: 10,
// });

var minSteps = 6,
    maxSteps = 60,
    timeBetweenSteps = 100,
    bytesPerStep = 100000,
    totalSteps = 0;

$("#modalMediaFiles .btn-pick-image").on("click", function () {
    renderImage(elementPreview, imageSrc, elementForm, imageId);
});

function renderImage(elementPreview, imageSrc, elementForm, imageId) {
    $("#" + elementPreview).attr(
        "style",
        "background-image: url(" +
            imageSrc +
            "); background-size: cover; background-position: center center;"
    );
    $("#" + elementForm).attr("value", imageId);
    $("#" + elementForm + "_input").attr("value", imageId);
}
