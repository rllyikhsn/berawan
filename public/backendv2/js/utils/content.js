"use strict";

function getContentCategories(idElement) {
    $.ajax({
        url: "/api/content-categories",
        type: "GET",
        contentType: false,
        processData: false,
        success: function (result) {
            const options = result.data.map(({ id, title }) => ({
                id,
                text: title,
            }));

            // Set the data for the select2 element
            // $(idElement).select2({
            //     data: options,
            //     multiple: true,
            // });

            result.data.forEach(({ id, title }) => {
                // Create a new option element
                var option = document.createElement("option");

                // Set the text and value of the option
                option.text = title;
                option.value = id;

                // Append the option to the select element with id "article_categories"
                $(idElement).append(option);
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            window.location.href = "/admin";
        },
    });
}

function getContentTags(idElement) {
    $.ajax({
        url: "/api/content-tags",
        type: "GET",
        contentType: false,
        processData: false,
        success: function (result) {
            const options = result.data.map(({ id, title }) => ({
                id,
                text: title,
            }));

            // Set the data for the select2 element
            // $(idElement).select2({
            //     data: options,
            //     multiple: true,
            // });
            result.data.forEach(({ id, title }) => {
                // Create a new option element
                var option = document.createElement("option");

                // Set the text and value of the option
                option.text = title;
                option.value = id;

                // Append the option to the select element with id "article_categories"
                $(idElement).append(option);
            });
        },
        error: function (jqXHR, textStatus, errorThrown) {
            window.location.href = "/admin";
        },
    });
}
