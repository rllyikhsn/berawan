function isDetail(lastSegment) {
    var isUuid = /^[a-f\d]{8}(-[a-f\d]{4}){3}-[a-f\d]{12}$/i.test(lastSegment);
    if (isUuid) {
        return true;
    } else {
        return false;
    }
}

function formattedDateTime(dateString) {
    const date = new Date(dateString);

    const year = date.getFullYear();
    const month = (date.getMonth() + 1).toString().padStart(2, "0");
    const day = date.getDate().toString().padStart(2, "0");
    const hours = date.getHours().toString().padStart(2, "0");
    const minutes = date.getMinutes().toString().padStart(2, "0");

    const formattedDate = `${year}-${month}-${day} ${hours}:${minutes}`;

    return formattedDate;
}

function cleanShortContent(text) {
    const cleanedContent = $('<div>').html(text).text();
    return cleanedContent;
}

const hostUrl = window.location.protocol + "//" + window.location.host;