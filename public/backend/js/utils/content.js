'use strict';

function getContentCategories(idElement) {
    $.ajax({
        url: '/api/content-categories',
        type: 'GET',
        contentType: false,
        processData: false,
        success: function (result) {
            const options = result.data.map(({ id, title }) => ({
                id,
                text: title,
            }))

            // Set the data for the select2 element
            $(idElement).select2({
                data: options,
                multiple: true,
            })
        },
        error: function (jqXHR, textStatus, errorThrown) {
            window.location.href = '/admin';
        },
    })
}

function getContentTags(idElement) {
    $.ajax({
        url: '/api/content-tags',
        type: 'GET',
        contentType: false,
        processData: false,
        success: function (result) {
            const options = result.data.map(({ id, title }) => ({
                id,
                text: title,
            }))

            // Set the data for the select2 element
            $(idElement).select2({
                data: options,
                multiple: true,
            })
        },
        error: function (jqXHR, textStatus, errorThrown) {
            window.location.href = '/admin';
        },
    })
}
