"use strict";

var url = window.location.href;
var urlWeb = "/admin/content-tags";
var urlApi = "/api/content-tags";

var segments = url.split("/");

var lastSegment = segments[segments.length - 1];

var urlAjax = isDetail(lastSegment) ? urlApi + "/" + lastSegment : urlApi;

if (isDetail(lastSegment) || lastSegment != "manage") {
    if (!isDetail(lastSegment)) {
        window.location.href = urlWeb;
    }

    $.ajax({
        url: urlApi + "/" + lastSegment,
        type: "GET",
        contentType: false,
        processData: false,
        success: function (result) {
            $("#title").val(result.data.title);
        },
        error: function (jqXHR, textStatus, errorThrown) {
            window.location.href = urlWeb;
        },
    });
}

$(".btn-cancel").on("click", function () {
    window.history.back();
});

$("#form-tags").on("submit", function (e) {
    e.preventDefault(); // prevent the form from submitting normally

    var formData = new FormData(this);

    swal({
        title: "Simpan data ini?",
        text: "Data yang diisikan sudah sesuai?",
        icon: "info",
        buttons: true,
    }).then((willSave) => {
        if (willSave) {
            $.ajax({
                url: urlAjax,
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,
                success: function (result) {
                    console.log(result.data.id);
                    if (result.status) {
                        swal("Berhasil Simpan Data", {
                            icon: "success",
                        }).then((data) => {
                            window.location.href = urlWeb;
                            // history.replaceState(
                            //     {},
                            //     null,
                            //     (window.location.href =
                            //         url + "/" + result.data.id)
                            // );
                        });
                    } else {
                        swal("Simpan Data Gagal", {
                            icon: "error",
                        });
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    swal("Simpan Data Gagal", {
                        icon: "error",
                        text: "Periksa kembali data yang diisikan.",
                    });
                },
            });
        }
    });
});
