"use strict";

var url = window.location.href;
var urlWeb = "/admin/content-announcements";
var urlApi = "/api/content-announcements";

var segments = url.split("/");

var lastSegment = segments[segments.length - 1];

var urlAjax = isDetail(lastSegment) ? urlApi + "/" + lastSegment : urlApi;

if (isDetail(lastSegment) || lastSegment != "manage") {
    if (!isDetail(lastSegment)) {
        window.location.href = urlWeb;
    }

    $.ajax({
        url: urlApi + "/" + lastSegment,
        type: "GET",
        contentType: false,
        processData: false,
        success: function (result) {
            $("#title").val(result.data.title);
            $("#short_content").summernote("code", result.data.short_content);
            $("#start_at").val(formattedDateTime(result.data.start_at));
            $("#end_at").val(formattedDateTime(result.data.end_at));
            var is_draft = result.data.is_draft ? "1" : "0";
            $("#is_draft").val(is_draft);
            var is_published = result.data.is_published ? "1" : "0";
            $("#is_published").val(is_published);
            $("#published_at").val(formattedDateTime(result.data.published_at));
        },
        error: function (jqXHR, textStatus, errorThrown) {
            window.location.href = urlWeb;
        },
    });
}

$(".btn-cancel").on("click", function () {
    window.history.back();
});

$("#form-announcements").on("submit", function (e) {
    e.preventDefault(); // prevent the form from submitting normally

    var formData = new FormData(this);

    if ($("#short_content").val() == "") {
        swal("Simpan Data Gagal", {
            icon: "error",
            text: "Isi pengumuman tidak boleh kosong",
        });

        return false;
    }
    if ($("#start_at").val() >= $("#end_at").val()) {
        swal("Simpan Data Gagal", {
            icon: "error",
            text: "Waktu Awal Pengumuman tidak boleh lebih besar dari Waktu Akhir Pengumuman",
        });

        return false;
    }

    swal({
        title: "Simpan data ini?",
        text: "Data yang diisikan sudah sesuai?",
        icon: "info",
        buttons: true,
    }).then((willSave) => {
        if (willSave) {
            $.ajax({
                url: urlAjax,
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,
                success: function (result) {
                    console.log(result.data.id);
                    if (result.status) {
                        swal("Berhasil Simpan Data", {
                            icon: "success",
                        }).then((data) => {
                            window.location.href = urlWeb;
                            // history.replaceState(
                            //     {},
                            //     null,
                            //     (window.location.href =
                            //         url + "/" + result.data.id)
                            // );
                        });
                    } else {
                        swal("Simpan Data Gagal", {
                            icon: "error",
                        });
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    swal("Simpan Data Gagal", {
                        icon: "error",
                        text: "Periksa kembali data yang diisikan.",
                    });
                },
            });
        }
    });
});
