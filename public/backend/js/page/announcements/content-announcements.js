"use strict";

const lazyLoad = new LazyLoad();

var table = $("#table-announcements").DataTable({
    buttons: ["copy", "excel", "pdf", "csv"],
    // responsive: true,
    order: [[0, "desc"]],
    language: {
        searchPlaceholder: "Search...",
        sSearch: "",
        lengthMenu: "_MENU_ ",
    },
    aLengthMenu: [
        [10, 25, 50, 100, 200, -1],
        [10, 25, 50, 100, 200, "Semua Data"],
    ],
    iDisplayLength: 10,
    scrollX: false,
    processing: true,
    serverSide: true,
    ajax: {
        url: "/api/content-announcements/view/datatables",
        type: "POST",
        data: function (d) {
            $.extend(d, {
                code: $("#fNoPPBE").val(),
                mcompanies_id: $("#fCompanyId").val(),
                sr_created_at: $("#fPublicationDate").val(),
            });
        },
    },
    rowCallback: function (row, data, index) {
        $("td:eq(0)", row).html(index + 1);
    },
    columns: [
        { data: "created_at", name: "created_at" },
        { data: "title", name: "title" },
        {
            data: "created_at",
            render: function (data, type, row, meta) {
                return formattedDateTime(row.created_at);
            },
        },
        {
            data: "start_at",
            render: function (data, type, row, meta) {
                return formattedDateTime(row.start_at);
            },
        },
        {
            data: "end_at",
            render: function (data, type, row, meta) {
                return formattedDateTime(row.end_at);
            },
        },
        {
            data: "is_published",
            render: function (data, type, row, meta) {
                var isPublished = "";
                if (row.is_published) {
                    isPublished = `<span class="badge badge-primary">Publish</span>`;
                } else {
                    isPublished = `<span class="badge badge-warning">Not Publish</span>`;
                }
                return isPublished;
            },
        },
        {
            data: "published_at",
            render: function (data, type, row, meta) {
                return formattedDateTime(row.published_at);
            },
        },
        { data: "action", name: "created_at" },
    ],
});

$(".image-link").magnificPopup({
    type: "image",
    gallery: {
        enabled: true,
    },
});
$("#table-announcements tbody").on("click", ".btn-delete", function () {
    var id = $(this).data("id");
    swal({
        title: "Hapus data ini?",
        text: "Data yang dihapus tidak dapat dikembalikan!",
        icon: "warning",
        buttons: true,
        dangerMode: true,
    }).then((willDelete) => {
        if (willDelete) {
            $.ajax({
                url: "/api/content-announcements/" + id,
                type: "DELETE",
                success: function (result) {
                    swal("Berhasil Hapus Data", {
                        icon: "success",
                    });
                    table.draw();
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    swal("Hapus Data Gagal", {
                        icon: "danger",
                    });
                },
            });
        }
    });
});
