"use strict";

var url = window.location.href;
var urlWeb = "/admin/articles";
var urlApi = "/api/articles";

var segments = url.split("/");

var lastSegment = segments[segments.length - 1];

var urlAjax = isDetail(lastSegment) ? urlApi + "/" + lastSegment : urlApi;

getContentCategories("#article_categories");

getContentTags("#article_tags");

$("#is_draft").selectric();
$("#is_published").selectric();

var countGalleryArticle = $("#listGalleryArticle .image-preview").length;

$("#add-gallery-item").on("click", function () {
    // Increment the count
    countGalleryArticle++;
    renderGalleryArticle(countGalleryArticle);
});

function renderGalleryArticle(countGalleryArticle) {
    // Create the HTML for the new gallery item
    const html = `
<div class="col-12 col-md-5 col-lg-5 item-list-gallery">
    <div class="form-group">
        <div id="image-preview-gallery${countGalleryArticle}" class="image-preview">
            <label for="image-upload" id="image-label">Choose File</label>
            <input type="text" autocomplete="off" multiple name="article_images[]" 
                id="image_gallery${countGalleryArticle}" data-element-form="image_gallery${countGalleryArticle}"
                data-element-preview="image-preview-gallery${countGalleryArticle}" data-toggle="modal"
                data-target="#modalMediaFiles" />
        </div>
        <div class="mt-2">
            <a class="btn btn-danger btn-delete-gallery-article">Delete</a>
        </div>
    </div>
</div>
`;

    // Add the new gallery item to the list
    $("#listGalleryArticle").append(html);
}

$(document).on("click", ".btn-delete-gallery-article", function () {
    $(this).closest(".item-list-gallery").remove();
});

if (isDetail(lastSegment) || lastSegment != "manage") {
    if (!isDetail(lastSegment)) {
        window.location.href = urlWeb;
    }

    $.ajax({
        url: urlApi + "/" + lastSegment,
        type: "GET",
        contentType: false,
        processData: false,
        success: function (result) {
            $("#title").val(result.data.title);
            $("#short_content").val(result.data.short_content);
            $("#long_content").summernote("code", result.data.long_content);

            const selectedContentCategories =
                result.data.article_categories.map(
                    (category) => category.content_category_id
                );

            $("#article_categories")
                .val(selectedContentCategories)
                .trigger("change");

            const selectedContentTags = result.data.article_tags.map(
                (tag) => tag.content_tag_id
            );

            $("#article_tags").val(selectedContentTags).trigger("change");

            if (result.data.pinned_image) {
                renderImage(
                    "image-preview-pinned",
                    "/images/articles/original/" + result.data.pinned_image,
                    "image_pinned",
                    result.data.pinned_image
                );
            }

            countGalleryArticle = result.data.article_images.length;

            for (let index = 0; index < countGalleryArticle; ) {
                renderGalleryArticle(index);
                renderImage(
                    `image-preview-gallery${index}`,
                    "/images/articles/images/" +
                        result.data.article_images[index].image,
                    `image_gallery${index}`,
                    result.data.article_images[index].image
                );
                index++;
            }

            var is_draft = result.data.is_draft ? 1 : 0;
            $("#is_draft").selectric("setValue", is_draft);

            var is_published = result.data.is_published ? 1 : 0;
            $("#is_published").selectric("setValue", is_published);

            if (result.data.is_published) {
                $("#published_date").show();
            }
            $("#published_at").val(formattedDateTime(result.data.published_at));
        },
        error: function (jqXHR, textStatus, errorThrown) {
            window.location.href = urlWeb;
        },
    });
}

$(".btn-cancel").on("click", function () {
    window.history.back();
});

$("#form-articles").on("submit", function (e) {
    e.preventDefault(); // prevent the form from submitting normally

    var formData = new FormData(this);

    if ($("#short_content").val() == "") {
        swal("Simpan Data Gagal", {
            icon: "error",
            text: "Kontent Pendek tidak boleh kosong",
        });

        return false;
    }

    if ($("#long_content").val() == "") {
        swal("Simpan Data Gagal", {
            icon: "error",
            text: "Isi Artikel tidak boleh kosong",
        });

        return false;
    }

    var articleCategories = $("#article_categories").val();
    for (let i = 0; i < articleCategories.length; i++) {
        formData.append(
            "article_categories[][content_category_id]",
            articleCategories[i]
        );
    }

    var articleTags = $("#article_tags").val();
    for (let i = 0; i < articleTags.length; i++) {
        formData.append("article_tags[][content_tag_id]", articleTags[i]);
    }

    var articleImages = $('input[name="article_images[]"]')
        .map(function () {
            return $(this).val();
        })
        .get();

    for (let i = 0; i < articleImages.length; i++) {
        formData.delete("article_images[]", articleImages[i]);
        formData.append(
            "article_images[][content_media_file_id]",
            articleImages[i]
        );
    }

    swal({
        title: "Simpan data ini?",
        text: "Data yang diisikan sudah sesuai?",
        icon: "info",
        buttons: true,
    }).then((willSave) => {
        if (willSave) {
            $.ajax({
                url: urlAjax,
                type: "POST",
                data: formData,
                contentType: false,
                processData: false,
                success: function (result) {
                    if (result.status) {
                        swal("Berhasil Simpan Data", {
                            icon: "success",
                        }).then((data) => {
                            window.location.href = urlWeb;
                        });
                    } else {
                        swal("Simpan Data Gagal", {
                            icon: "error",
                        });
                    }
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    swal("Simpan Data Gagal", {
                        icon: "error",
                        text: "Periksa kembali data yang diisikan.",
                    });
                },
            });
        }
    });
});
