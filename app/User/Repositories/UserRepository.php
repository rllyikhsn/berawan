<?php

namespace App\User\Repositories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;

use App\Base\Interfaces\CrudInterface;
use App\User\Models\User;


class UserRepository implements CrudInterface
{
    /**
     * Authenticated User Instance.
     *
     * @var User
     */
    public User | null $user;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->user = Auth::guard()->user();
    }

    /**
     * Get All Users.
     *
     * @return collections Array of User Collection
     */
    public function getAll(): Paginator
    {
        return User::orderBy('created_at', 'desc')
            ->paginate(10);
    }

    /**
     * Get Users Datatables.
     *
     * @return collections Array of Datatables User Collection
     */
    public function getDataTables()
    {
        return User::select('id', 'name', 'email', 'phone_number')
            ->orderBy('created_at', 'desc')->get();
    }

    /**
     * Get Paginated User Data.
     *
     * @param int $pageNo
     * @return collections Array of User Collection
     */
    public function getPaginatedData($perPage, $page): Paginator
    {
        $perPage = isset($perPage) ? intval($perPage) : 12;
        $page = isset($page) ? intval($perPage) : 1;
        return User::orderBy('id', 'desc')
            ->paginate($perPage, ['*'], 'page', $page);
    }

    /**
     * Get Searchable User Data with Pagination.
     *
     * @param int $pageNo
     * @return collections Array of User Collection
     */
    public function searchUser($keyword, $perPage): Paginator
    {
        $perPage = isset($perPage) ? intval($perPage) : 10;

        return User::where('name', 'like', '%' . $keyword . '%')
            // ->orWhere('description', 'like', '%' . $keyword . '%')
            // ->orWhere('price', 'like', '%' . $keyword . '%')
            // ->orderBy('id', 'desc')
            // ->with('user')
            ->paginate($perPage);
    }

    /**
     * Create New User.
     *
     * @param array $data
     * @return object User Object
     */
    public function create(array $data): User
    {
        return User::create($data);
    }

    /**
     * Delete User.
     *
     * @param string $id
     * @return boolean true if deleted otherwise false
     */
    public function delete(string $id): bool
    {
        $user = User::find($id);
        if (empty($user)) {
            return false;
        }

        $user->delete($user);
        return true;
    }

    /**
     * Get User Detail By ID.
     *
     * @param string $id
     * @return void
     */
    public function getByID(string $id): User|null
    {
        $user = new User();
        $user = User::find($id);
        return $user;
    }

    /**
     * Update User By ID.
     *
     * @param string $id
     * @param array $data
     * @return object Updated User Object
     */
    public function update(string $id, array $data): User|null
    {
        $user = User::find($id);

        if (is_null($user)) {
            return null;
        }

        // If everything is OK, then update.
        $user->update($data);

        // Finally return the updated user.
        return $this->getByID($user->id);
    }
}
