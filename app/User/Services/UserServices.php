<?php

namespace App\User\Services;

use App\User\Models\User;
use Illuminate\Database\TransactionFailedException;
use Illuminate\Support\Facades\DB;

use App\User\Repositories\UserRepository;

class UserServices
{
    /**
     * User Repository class.
     *
     * @var UserRepository
     */
    public $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function getAll()
    {
        return $this->userRepository->getAll();
    }

    public function getDataTables()
    {
        return $this->userRepository->getDataTables();
    }

    public function getPaginatedData(int $perPage)
    {
        return $this->userRepository->getPaginatedData($perPage);
    }

    public function searchUser(string $keyword, int $perPage)
    {
        return $this->userRepository->searchUser($keyword, $perPage);
    }

    public function create(array $data)
    {
        try {
            DB::beginTransaction();
            
            $user = $this->userRepository->create($data);

            // do something else with the user object
            // ...

            DB::commit();

            return $user;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    public function read(string $id)
    {
        return $this->userRepository->getByID($id);
    }

    public function delete(string $id)
    {
        // Here for bussiness Logic
        try {
            DB::beginTransaction();
            
            $user = $this->userRepository->delete($id);
            
            DB::commit();
            
            return $user;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    public function update(string $id, array $data)
    {
        // Here for bussiness Logic
        try {
            DB::beginTransaction();
            $user = $this->userRepository->update($id, $data);
            DB::commit();
            
            return $user;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }
}