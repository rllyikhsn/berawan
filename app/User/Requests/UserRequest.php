<?php

namespace App\User\Requests;

use App\Base\Requests\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name'         => 'required|max:255',
            'phone_number' => 'max:255|int',
            'username'     => 'required|max:255',
            'email'        => 'required|max:255|email',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     * Custom validation message
     */
    public function messages(): array
    {
        return [
            'name.required'   => 'Please give name user',
            'name.max'        => 'Please give name user maximum of 255 characters',
            'phone_number.max'       => 'Please give phone number user maximum of 15 characters',
            'phone_number.int'   => 'Please give a numeric phone number',
            'username.required'   => 'Please give username user',
            'username.max'        => 'Please give username user maximum of 255 characters',
            'email.required'   => 'Please give email user',
            'email.max'        => 'Please give email user maximum of 255 characters',
            'email.email'      => 'Please give format email for user',
        ];
    }
}
