<?php

namespace App\Base\Interfaces;

interface CrudInterface
{
    /**
     * Get All Data
     *
     * @return array All Data
     */
    public function getAll();

    /**
     * Get Paginated Data
     *
     * @param int   Limit Page
     * @param int   Page No
     * @return array Paginated Data
     */
    public function getPaginatedData(int $perPage, int $page);

    /**
     * Create New Item
     *
     * @param array $data
     * @return object Created Product
     */
    public function create(array $data);

    /**
     * Delete Item By Id
     *
     * @param string $id
     * @return object Deleted Item
     */
    public function delete(string $id);

    /**
     * Get Item Details By ID
     *
     * @param string $id
     * @return object Get Detail
     */
    public function getByID(string $id);

    /**
     * Update Detail By Id and Data
     *
     * @param string $id
     * @param array $data
     * @return object Updated Detail Information
     */
    public function update(string $id, array $data);
}
