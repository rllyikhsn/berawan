<?php
use Carbon\Carbon;
use Carbon\CarbonInterface;

if (!function_exists('formatDateIndonesia')) {
    function formatDateIndonesia($date) {
        // Set the Carbon locale to Indonesian
        Carbon::setLocale('id');

        // Create a Carbon instance with your date
        $date = Carbon::parse($date);

        // Format the date
        $formattedDate = $date->isoFormat('LL'); // '16 September 2023'

        return $formattedDate;
    }
}