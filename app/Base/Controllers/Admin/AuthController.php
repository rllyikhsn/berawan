<?php

namespace App\Base\Controllers\Admin;

use App\User\Controllers\AuthController as AuthControllerApi;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use App\User\Models\User;
use App\User\Requests\AuthLoginRequest;

class AuthController extends BaseController
{
    public function showLoginForm()
    {
        $user = Session::get('user');
        if ($user != null) {
            return redirect()->intended('/admin');
        }

        return view('backend.pages.auth.auth-login', ['type_menu' => 'auth']);
    }

    public function login(AuthLoginRequest $request)
    {
        $credentials = $request->only('email', 'password');

        Session::flash('email', $request->email);
        if (Auth::attempt($credentials)) {
            Session::put('user', Auth::user());
            return redirect()->intended('/admin');
        } else {
            return redirect()->back()->withInput()->withErrors(['login' => 'Invalid credentials']);
        }
    }

    public function logout()
    {
        Session::flush();
        return redirect(route('login'));
    }
}
