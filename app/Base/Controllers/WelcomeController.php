<?php

namespace App\Base\Controllers;

use Illuminate\Http\JsonResponse;

use App\Base\Traits\ResponseTrait;

class WelcomeController extends Controller
{
    /**
     * Response trait to handle return responses.
     */
    use ResponseTrait;

    public function welcome(): JsonResponse
    {
        $data = "Welcome to CRUD Clean Architecture";
        return $this->responseSuccess($data, 'Welcome Success');
    }
}