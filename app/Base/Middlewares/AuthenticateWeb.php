<?php

namespace App\Base\Middlewares;

use Closure;
use Illuminate\Support\Facades\Session;

class AuthenticateWeb
{
    public function handle($request, Closure $next)
    {
        $user = Session::get('user');
        if ($user == null) {
            return redirect(route('login'));
        }

        return $next($request);
    }
}
