<?php

namespace App\Base\Middlewares;

use Closure;
use Illuminate\Http\Request;

class Maintenance
{
    public function handle(Request $request, Closure $next)
    {
        if (env('MAINTENANCE_MODE', true)) {
            return response()->view('frontend.pages.maintenance', [], 503);
        }

        return $next($request);
    }
}
