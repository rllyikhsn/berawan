<?php

namespace App\Content\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Content extends Model
{
    use HasFactory, SoftDeletes, HasUuids;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nama_instansi',
        'alamat_office',
        'alamat_warehouse',
        'alamat_workshop',
        'no_telepon_office',
        'no_telepon_warehouse',
        'no_telepon_workshop',
        'no_whatsapp',
        'email_office',
        'email_warehouse',
        'email_workshop',
        'contact_person_name_1',
        'contact_person_phone_1',
        'contact_person_whatsapp_1',
        'contact_person_email_1',
        'contact_person_name_2',
        'contact_person_phone_2',
        'contact_person_whatsapp_2',
        'contact_person_email_2',
        'contact_person_name_3',
        'contact_person_phone_3',
        'contact_person_whatsapp_3',
        'contact_person_email_3',
        'favicon_image',
        'logo_image',
        'logo_front_image',
        'about_image',
        'about_content',
        'footer_image',
        'footer_content',
        'service',
        'service_image',
        'link_facebook',
        'link_whatsapp',
        'link_whatsapp_template',
        'link_twitter',
        'link_youtube',
        'link_linktr',
        'link_instagram',
        'link_linkedin',
        'link_gmaps',
        'deeplink_playstore',
        'deeplink_appstore',
        'link_brocure',
        'copyright',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
}
