<?php

namespace App\Content\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ContentTeam extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'division',
        'content_team_image',
    ];
}
