<?php

namespace App\Content\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ContentGallery extends Model
{
    use HasFactory, SoftDeletes, HasUuids;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'slug',
        'short_content',
        'is_draft',
        'is_published',
        'published_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    public function contentGalleryCategories()
    {
        return $this->hasMany(ContentGalleryCategory::class);
    }

    public function contentGalleryTags()
    {
        return $this->hasMany(ContentGalleryTag::class);
    }

    public function contentGalleryImages()
    {
        return $this->hasMany(ContentGalleryImage::class);
    }

    public function contentGalleryVideos()
    {
        return $this->hasMany(ContentGalleryVideo::class);
    }
}
