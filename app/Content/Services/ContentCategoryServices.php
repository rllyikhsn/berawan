<?php

namespace App\Content\Services;

use App\Content\Models\ContentCategory;
use Illuminate\Database\TransactionFailedException;
use Illuminate\Support\Facades\DB;

use App\Content\Repositories\ContentCategoryRepository;

class ContentCategoryServices
{
    /**
     * ContentCategory Repository class.
     *
     * @var ContentCategoryRepository
     */
    public $contentCategoryRepository;

    public function __construct(ContentCategoryRepository $contentCategoryRepository)
    {
        $this->contentCategoryRepository = $contentCategoryRepository;
    }

    public function getAll()
    {
        return $this->contentCategoryRepository->getAll();
    }

    public function getDataTables()
    {
        return $this->contentCategoryRepository->getDataTables();
    }

    public function getPaginatedData(int $perPage, int $page)
    {
        return $this->contentCategoryRepository->getPaginatedData($perPage, $page);
    }

    public function searchContentCategory(string $keyword, int $perPage, int $page)
    {
        return $this->contentCategoryRepository->searchContentCategorys($keyword, $perPage, $page);
    }

    public function create(array $data)
    {
    try {
            DB::beginTransaction();

            $contentCategory = $this->contentCategoryRepository->create($data);

            // do something else with the content category object
            // ...

            DB::commit();

            return $contentCategory;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    public function read(string $id)
    {
        return $this->contentCategoryRepository->getByID($id);
    }

    public function delete(string $id)
    {
        // Here for bussiness Logic
        try {
            DB::beginTransaction();

            $contentCategory = $this->contentCategoryRepository->delete($id);

            DB::commit();

            return $contentCategory;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    public function update(string $id, array $data)
    {
        // Here for bussiness Logic
        try {
            DB::beginTransaction();
            $contentCategory = $this->contentCategoryRepository->update($id, $data);
            DB::commit();

            return $contentCategory;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }
}
