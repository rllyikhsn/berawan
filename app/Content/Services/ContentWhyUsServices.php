<?php

namespace App\Content\Services;

use App\Content\Models\ContentWhyUs;
use Illuminate\Database\TransactionFailedException;
use Illuminate\Support\Facades\DB;

use App\Content\Repositories\ContentWhyUsRepository;

class ContentWhyUsServices
{
    /**
     * ContentWhyUs Repository class.
     *
     * @var ContentWhyUsRepository
     */
    public $contentWhyUsRepository;

    public function __construct(ContentWhyUsRepository $contentWhyUsRepository)
    {
        $this->contentWhyUsRepository = $contentWhyUsRepository;
    }

    public function getAll()
    {
        return $this->contentWhyUsRepository->getAll();
    }

    public function getDataTables()
    {
        return $this->contentWhyUsRepository->getDataTables();
    }

    public function getPaginatedData(int $perPage, int $page)
    {
        return $this->contentWhyUsRepository->getPaginatedData($perPage, $page);
    }

    public function searchContentWhyUs(string $keyword, int $perPage, int $page)
    {
        return $this->contentWhyUsRepository->searchContentWhyUss($keyword, $perPage, $page);
    }

    public function create(array $data)
    {
        try {
            DB::beginTransaction();

            $contentWhyUs = $this->contentWhyUsRepository->create($data);

            // do something else with the content category object
            // ...

            DB::commit();

            return $contentWhyUs;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    public function read(string $id)
    {
        return $this->contentWhyUsRepository->getByID($id);
    }

    public function delete(string $id)
    {
        // Here for bussiness Logic
        try {
            DB::beginTransaction();

            $contentWhyUs = $this->contentWhyUsRepository->delete($id);

            DB::commit();

            return $contentWhyUs;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    public function update(string $id, array $data)
    {
        // Here for bussiness Logic
        try {
            DB::beginTransaction();
            $contentWhyUs = $this->contentWhyUsRepository->update($id, $data);
            DB::commit();

            return $contentWhyUs;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }
}
