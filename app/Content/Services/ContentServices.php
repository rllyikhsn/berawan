<?php

namespace App\Content\Services;

use App\Content\Models\Content;
use Illuminate\Database\TransactionFailedException;
use Illuminate\Support\Facades\DB;

use App\Content\Repositories\ContentRepository;

class ContentServices
{
    /**
     * Content Repository class.
     *
     * @var ContentRepository
     */
    public $contentRepository;

    public function __construct(ContentRepository $contentRepository)
    {
        $this->contentRepository = $contentRepository;
    }

    public function getAll()
    {
        return $this->contentRepository->getAll();
    }

    public function getDataTables()
    {
        return $this->contentRepository->getDataTables();
    }

    public function getPaginatedData(int $perPage, int $page)
    {
        return $this->contentRepository->getPaginatedData($perPage, $page);
    }

    public function searchContent(string $keyword, int $perPage)
    {
        return $this->contentRepository->searchContents($keyword, $perPage);
    }

    public function create(array $data)
    {
        try {
            DB::beginTransaction();

            $content = $this->contentRepository->create($data);

            // do something else with the content object
            // ...

            DB::commit();

            return $content;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    public function read(string $id)
    {
        return $this->contentRepository->getByID($id);
    }

    public function delete(string $id)
    {
        // Here for bussiness Logic
        try {
            DB::beginTransaction();

            $content = $this->contentRepository->delete($id);

            DB::commit();

            return $content;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    public function update(string $id, array $data)
    {
        // Here for bussiness Logic
        try {
            DB::beginTransaction();
            $content = $this->contentRepository->update($id, $data);
            DB::commit();

            return $content;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    public function updateLogoIdentity(string $id, array $data)
    {
        // Here for bussiness Logic
        try {
            DB::beginTransaction();
            $content = $this->contentRepository->updateLogoIdentity($id, $data);
            DB::commit();

            return $content;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }
}
