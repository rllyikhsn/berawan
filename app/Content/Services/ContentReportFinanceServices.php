<?php

namespace App\Content\Services;

use App\Content\Models\ContentReportFinance;
use Illuminate\Database\TransactionFailedException;
use Illuminate\Support\Facades\DB;

use App\Content\Repositories\ContentReportFinanceRepository;

class ContentReportFinanceServices
{
    /**
     * ContentReportFinance Repository class.
     *
     * @var ContentReportFinanceRepository
     */
    public $contentReportFinanceRepository;

    public function __construct(ContentReportFinanceRepository $contentReportFinanceRepository)
    {
        $this->contentReportFinanceRepository = $contentReportFinanceRepository;
    }

    public function getAll()
    {
        return $this->contentReportFinanceRepository->getAll();
    }

    public function getDataTables()
    {
        return $this->contentReportFinanceRepository->getDataTables();
    }

    public function getPaginatedData(int $perPage, int $page)
    {
        return $this->contentReportFinanceRepository->getPaginatedData($perPage, $page);
    }

    public function searchContentReportFinance(string $keyword, int $perPage)
    {
        return $this->contentReportFinanceRepository->searchContentReportFinances($keyword, $perPage);
    }

    public function create(array $data)
    {
        try {
            DB::beginTransaction();

            $contentReportFinance = $this->contentReportFinanceRepository->create($data);

            // do something else with the content report finance object
            // ...

            DB::commit();

            return $contentReportFinance;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    public function read(string $id)
    {
        return $this->contentReportFinanceRepository->getByID($id);
    }

    public function delete(string $id)
    {
        // Here for bussiness Logic
        try {
            DB::beginTransaction();

            $contentReportFinance = $this->contentReportFinanceRepository->delete($id);

            DB::commit();

            return $contentReportFinance;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    public function update(string $id, array $data)
    {
        // Here for bussiness Logic
        try {
            DB::beginTransaction();
            $contentReportFinance = $this->contentReportFinanceRepository->update($id, $data);
            DB::commit();

            return $contentReportFinance;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }
}
