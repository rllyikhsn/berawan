<?php

namespace App\Content\Services;

use Illuminate\Database\TransactionFailedException;
use Illuminate\Support\Facades\DB;

use App\Content\Repositories\ContentClientRepository;

class ContentClientServices
{
    /**
     * Content Client Repository class.
     *
     * @var ContentClientRepository
     */
    public $contentClientRepository;

    public function __construct(ContentClientRepository $contentClientRepository)
    {
        $this->contentClientRepository = $contentClientRepository;
    }

    public function getAll()
    {
        return $this->contentClientRepository->getAll();
    }

    public function getDataTables()
    {
        return $this->contentClientRepository->getDataTables();
    }

    public function getPaginatedData(int $perPage, int $page)
    {
        return $this->contentClientRepository->getPaginatedData($perPage, $page);
    }

    public function searchContentClients(string $keyword, int $perPage, int $page)
    {
        return $this->contentClientRepository->searchContentClients($keyword, $perPage, $page);
    }

    public function create(array $data)
    {
        try {
            DB::beginTransaction();

            $contentClient = $this->contentClientRepository->create($data);

            // do something else with the content client object
            // ...

            DB::commit();

            return $contentClient;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    public function read(string $id)
    {
        return $this->contentClientRepository->getByID($id);
    }

    public function delete(string $id)
    {
        // Here for business Logic
        try {
            DB::beginTransaction();

            $contentClient = $this->contentClientRepository->delete($id);

            DB::commit();

            return $contentClient;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    public function update(string $id, array $data)
    {
        // Here for business Logic
        try {
            DB::beginTransaction();
            $contentClient = $this->contentClientRepository->update($id, $data);
            DB::commit();

            return $contentClient;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }
}
