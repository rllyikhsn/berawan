<?php

namespace App\Content\Services;

use App\Content\Models\ContentReportAnnual;
use Illuminate\Database\TransactionFailedException;
use Illuminate\Support\Facades\DB;

use App\Content\Repositories\ContentReportAnnualRepository;

class ContentReportAnnualServices
{
    /**
     * ContentReportAnnual Repository class.
     *
     * @var ContentReportAnnualRepository
     */
    public $contentReportAnnualRepository;

    public function __construct(ContentReportAnnualRepository $contentReportAnnualRepository)
    {
        $this->contentReportAnnualRepository = $contentReportAnnualRepository;
    }

    public function getAll()
    {
        return $this->contentReportAnnualRepository->getAll();
    }

    public function getDataTables()
    {
        return $this->contentReportAnnualRepository->getDataTables();
    }

    public function getPaginatedData(int $perPage, int $page)
    {
        return $this->contentReportAnnualRepository->getPaginatedData($perPage, $page);
    }

    public function searchContentReportAnnual(string $keyword, int $perPage)
    {
        return $this->contentReportAnnualRepository->searchContentReportAnnuals($keyword, $perPage);
    }

    public function create(array $data)
    {
        try {
            DB::beginTransaction();

            $contentReportAnnual = $this->contentReportAnnualRepository->create($data);

            // do something else with the content report annual object
            // ...

            DB::commit();

            return $contentReportAnnual;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    public function read(string $id)
    {
        return $this->contentReportAnnualRepository->getByID($id);
    }

    public function delete(string $id)
    {
        // Here for bussiness Logic
        try {
            DB::beginTransaction();

            $contentReportAnnual = $this->contentReportAnnualRepository->delete($id);

            DB::commit();

            return $contentReportAnnual;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    public function update(string $id, array $data)
    {
        // Here for bussiness Logic
        try {
            DB::beginTransaction();
            $contentReportAnnual = $this->contentReportAnnualRepository->update($id, $data);
            DB::commit();

            return $contentReportAnnual;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }
}
