<?php

namespace App\Content\Services;

use App\Content\Models\ContentAnnouncement;
use Illuminate\Database\TransactionFailedException;
use Illuminate\Support\Facades\DB;

use App\Content\Repositories\ContentAnnouncementRepository;

class ContentAnnouncementServices
{
    /**
     * ContentAnnouncement Repository class.
     *
     * @var ContentAnnouncementRepository
     */
    public $contentAnnouncementRepository;

    public function __construct(ContentAnnouncementRepository $contentAnnouncementRepository)
    {
        $this->contentAnnouncementRepository = $contentAnnouncementRepository;
    }

    public function getAll()
    {
        return $this->contentAnnouncementRepository->getAll();
    }

    public function getDataTables()
    {
        return $this->contentAnnouncementRepository->getDataTables();
    }

    public function getPaginatedData(int $perPage, int $page)
    {
        return $this->contentAnnouncementRepository->getPaginatedData($perPage, $page);
    }

    public function searchContentAnnouncement(string $keyword, int $perPage)
    {
        return $this->contentAnnouncementRepository->searchContentAnnouncements($keyword, $perPage);
    }

    public function create(array $data)
    {
        try {
            DB::beginTransaction();

            $contentAnnouncement = $this->contentAnnouncementRepository->create($data);

            // do something else with the content announcement object
            // ...

            DB::commit();

            return $contentAnnouncement;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    public function read(string $id)
    {
        return $this->contentAnnouncementRepository->getByID($id);
    }

    public function delete(string $id)
    {
        // Here for bussiness Logic
        try {
            DB::beginTransaction();

            $contentAnnouncement = $this->contentAnnouncementRepository->delete($id);

            DB::commit();

            return $contentAnnouncement;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    public function update(string $id, array $data)
    {
        // Here for bussiness Logic
        try {
            DB::beginTransaction();
            $contentAnnouncement = $this->contentAnnouncementRepository->update($id, $data);
            DB::commit();

            return $contentAnnouncement;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }
}
