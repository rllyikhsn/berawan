<?php

namespace App\Content\Services;

use App\Content\Models\ContentSlider;
use Illuminate\Database\TransactionFailedException;
use Illuminate\Support\Facades\DB;

use App\Content\Repositories\ContentSliderRepository;

class ContentSliderServices
{
    /**
     * ContentSlider Repository class.
     *
     * @var ContentSliderRepository
     */
    public $contentSliderRepository;

    public function __construct(ContentSliderRepository $contentSliderRepository)
    {
        $this->contentSliderRepository = $contentSliderRepository;
    }

    public function getAll()
    {
        return $this->contentSliderRepository->getAll();
    }

    public function getDataTables()
    {
        return $this->contentSliderRepository->getDataTables();
    }

    public function getPaginatedData(int $perPage, int $page)
    {
        return $this->contentSliderRepository->getPaginatedData($perPage, $page);
    }

    public function searchContentSlider(string $keyword, int $perPage, int $page)
    {
        return $this->contentSliderRepository->searchContentSliders($keyword, $perPage, $page);
    }

    public function create(array $data)
    {
    try {
            DB::beginTransaction();

            $contentSlider = $this->contentSliderRepository->create($data);

            // do something else with the content category object
            // ...

            DB::commit();

            return $contentSlider;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    public function read(string $id)
    {
        return $this->contentSliderRepository->getByID($id);
    }

    public function delete(string $id)
    {
        // Here for bussiness Logic
        try {
            DB::beginTransaction();

            $contentSlider = $this->contentSliderRepository->delete($id);

            DB::commit();

            return $contentSlider;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    public function update(string $id, array $data)
    {
        // Here for bussiness Logic
        try {
            DB::beginTransaction();
            $contentSlider = $this->contentSliderRepository->update($id, $data);
            DB::commit();

            return $contentSlider;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }
}
