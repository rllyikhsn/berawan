<?php

namespace App\Content\Services;

use App\Content\Models\ContentGallery;
use Illuminate\Database\TransactionFailedException;
use Illuminate\Support\Facades\DB;

use App\Content\Repositories\ContentGalleryRepository;

class ContentGalleryServices
{
    /**
     * ContentGallery Repository class.
     *
     * @var ContentGalleryRepository
     */
    public $contentGalleryRepository;

    public function __construct(ContentGalleryRepository $contentGalleryRepository)
    {
        $this->contentGalleryRepository = $contentGalleryRepository;
    }

    public function getAll()
    {
        return $this->contentGalleryRepository->getAll();
    }

    public function getDataTables()
    {
        return $this->contentGalleryRepository->getDataTables();
    }

    public function getPaginatedData(int $perPage, int $page)
    {
        return $this->contentGalleryRepository->getPaginatedData($perPage, $page);
    }

    public function searchContentGallery(string $keyword, int $perPage, int $page)
    {
        return $this->contentGalleryRepository->searchContentGallerys($keyword, $perPage, $page);
    }

    public function create(array $data)
    {
        try {
            DB::beginTransaction();

            $contentGallery = $this->contentGalleryRepository->create($data);

            // do something else with the content category object
            // ...

            DB::commit();

            return $contentGallery;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    public function read(string $id)
    {
        return $this->contentGalleryRepository->getByID($id);
    }

    public function delete(string $id)
    {
        // Here for bussiness Logic
        try {
            DB::beginTransaction();

            $contentGallery = $this->contentGalleryRepository->delete($id);

            DB::commit();

            return $contentGallery;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    public function update(string $id, array $data)
    {
        // Here for bussiness Logic
        try {
            DB::beginTransaction();
            $contentGallery = $this->contentGalleryRepository->update($id, $data);
            DB::commit();

            return $contentGallery;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }
}
