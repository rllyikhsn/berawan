<?php

namespace App\Content\Services;

use App\Content\Models\Article;
use Illuminate\Database\TransactionFailedException;
use Illuminate\Support\Facades\DB;

use App\Content\Repositories\ArticleRepository;

class ArticleServices
{
    /**
     * Article Repository class.
     *
     * @var ArticleRepository
     */
    public $articleRepository;

    public function __construct(ArticleRepository $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }

    public function getAll()
    {
        return $this->articleRepository->getAll();
    }

    public function getDataTables()
    {
        return $this->articleRepository->getDataTables();
    }

    public function getPaginatedData(int $perPage, int $page)
    {
        return $this->articleRepository->getPaginatedData($perPage, $page);
    }

    public function searchArticle(string $keyword, int $perPage, int $page)
    {
        return $this->articleRepository->searchArticles($keyword, $perPage, $page);
    }

    public function create(array $data)
    {
        try {
            DB::beginTransaction();

            $article = $this->articleRepository->create($data);

            // do something else with the content category object
            // ...

            DB::commit();

            return $article;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    public function read(string $id)
    {
        return $this->articleRepository->getByID($id);
    }

    public function delete(string $id)
    {
        // Here for bussiness Logic
        try {
            DB::beginTransaction();

            $article = $this->articleRepository->delete($id);

            DB::commit();

            return $article;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    public function update(string $id, array $data)
    {
        // Here for bussiness Logic
        try {
            DB::beginTransaction();
            $article = $this->articleRepository->update($id, $data);
            DB::commit();

            return $article;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }
}
