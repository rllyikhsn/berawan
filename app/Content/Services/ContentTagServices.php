<?php

namespace App\Content\Services;

use App\Content\Models\ContentTag;
use Illuminate\Database\TransactionFailedException;
use Illuminate\Support\Facades\DB;

use App\Content\Repositories\ContentTagRepository;

class ContentTagServices
{
    /**
     * ContentTag Repository class.
     *
     * @var ContentTagRepository
     */
    public $contentTagRepository;

    public function __construct(ContentTagRepository $contentTagRepository)
    {
        $this->contentTagRepository = $contentTagRepository;
    }

    public function getAll()
    {
        return $this->contentTagRepository->getAll();
    }

    public function getDataTables()
    {
        return $this->contentTagRepository->getDataTables();
    }

    public function getPaginatedData(int $perPage, int $page)
    {
        return $this->contentTagRepository->getPaginatedData($perPage, $page);
    }

    public function searchContentTag(string $keyword, int $perPage)
    {
        return $this->contentTagRepository->searchContentTags($keyword, $perPage);
    }

    public function create(array $data)
    {
        try {
            DB::beginTransaction();

            $contentTag = $this->contentTagRepository->create($data);

            // do something else with the content tag object
            // ...

            DB::commit();

            return $contentTag;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    public function read(string $id)
    {
        return $this->contentTagRepository->getByID($id);
    }

    public function delete(string $id)
    {
        // Here for bussiness Logic
        try {
            DB::beginTransaction();

            $contentTag = $this->contentTagRepository->delete($id);

            DB::commit();

            return $contentTag;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    public function update(string $id, array $data)
    {
        // Here for bussiness Logic
        try {
            DB::beginTransaction();
            $contentTag = $this->contentTagRepository->update($id, $data);
            DB::commit();

            return $contentTag;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }
}
