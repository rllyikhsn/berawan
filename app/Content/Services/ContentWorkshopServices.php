<?php

namespace App\Content\Services;

use App\Content\Models\ContentWorkshop;
use Illuminate\Database\TransactionFailedException;
use Illuminate\Support\Facades\DB;

use App\Content\Repositories\ContentWorkshopRepository;

class ContentWorkshopServices
{
    /**
     * ContentWorkshop Repository class.
     *
     * @var ContentWorkshopRepository
     */
    public $contentWorkshopRepository;

    public function __construct(ContentWorkshopRepository $contentWorkshopRepository)
    {
        $this->contentWorkshopRepository = $contentWorkshopRepository;
    }

    public function getAll()
    {
        return $this->contentWorkshopRepository->getAll();
    }

    public function getDataTables()
    {
        return $this->contentWorkshopRepository->getDataTables();
    }

    public function getPaginatedData(int $perPage, int $page)
    {
        return $this->contentWorkshopRepository->getPaginatedData($perPage, $page);
    }

    public function searchContentWorkshop(string $keyword, int $perPage)
    {
        return $this->contentWorkshopRepository->searchContentWorkshops($keyword, $perPage);
    }

    public function create(array $data)
    {
        try {
            DB::beginTransaction();

            $contentWorkshop = $this->contentWorkshopRepository->create($data);

            // do something else with the content workshop object
            // ...

            DB::commit();

            return $contentWorkshop;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    public function read(string $id)
    {
        return $this->contentWorkshopRepository->getByID($id);
    }

    public function delete(string $id)
    {
        // Here for bussiness Logic
        try {
            DB::beginTransaction();

            $contentWorkshop = $this->contentWorkshopRepository->delete($id);

            DB::commit();

            return $contentWorkshop;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    public function update(string $id, array $data)
    {
        // Here for bussiness Logic
        try {
            DB::beginTransaction();
            $contentWorkshop = $this->contentWorkshopRepository->update($id, $data);
            DB::commit();

            return $contentWorkshop;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }
}