<?php

namespace App\Content\Services;

use App\Content\Models\ContentMediaFile;
use Illuminate\Database\TransactionFailedException;
use Illuminate\Support\Facades\DB;

use App\Content\Repositories\ContentMediaFileRepository;

class ContentMediaFileServices
{
    /**
     * ContentMediaFile Repository class.
     *
     * @var ContentMediaFileRepository
     */
    public $contentMediaFileRepository;

    public function __construct(ContentMediaFileRepository $contentMediaFileRepository)
    {
        $this->contentMediaFileRepository = $contentMediaFileRepository;
    }

    public function getAll()
    {
        return $this->contentMediaFileRepository->getAll();
    }

    public function getDataTables()
    {
        return $this->contentMediaFileRepository->getDataTables();
    }

    public function getPaginatedData(int $perPage, int $page)
    {
        return $this->contentMediaFileRepository->getPaginatedData($perPage, $page);
    }

    public function searchContent(string $keyword, int $perPage)
    {
        return $this->contentMediaFileRepository->searchContents($keyword, $perPage);
    }

    public function create(array $data)
    {
        try {
            DB::beginTransaction();

            $content = $this->contentMediaFileRepository->create($data);

            // do something else with the content object
            // ...

            DB::commit();

            return $content;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    public function read(string $id)
    {
        return $this->contentMediaFileRepository->getByID($id);
    }

    public function delete(string $id)
    {
        // Here for bussiness Logic
        try {
            DB::beginTransaction();

            $content = $this->contentMediaFileRepository->delete($id);

            DB::commit();

            return $content;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    public function update(string $id, array $data)
    {
        // Here for bussiness Logic
        try {
            DB::beginTransaction();
            $content = $this->contentMediaFileRepository->update($id, $data);
            DB::commit();

            return $content;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }
}
