<?php

namespace App\Content\Services;

use App\Content\Models\ContentReviewClient;
use Illuminate\Database\TransactionFailedException;
use Illuminate\Support\Facades\DB;

use App\Content\Repositories\ContentReviewClientRepository;

class ContentReviewClientServices
{
    /**
     * ContentReviewClient Repository class.
     *
     * @var ContentReviewClientRepository
     */
    public $contentReviewClientRepository;

    public function __construct(ContentReviewClientRepository $contentReviewClientRepository)
    {
        $this->contentReviewClientRepository = $contentReviewClientRepository;
    }

    public function getAll()
    {
        return $this->contentReviewClientRepository->getAll();
    }

    public function getDataTables()
    {
        return $this->contentReviewClientRepository->getDataTables();
    }

    public function getPaginatedData(int $perPage, int $page)
    {
        return $this->contentReviewClientRepository->getPaginatedData($perPage, $page);
    }

    public function searchContentReviewClient(string $keyword, int $perPage, int $page)
    {
        return $this->contentReviewClientRepository->searchContentReviewClients($keyword, $perPage, $page);
    }

    public function create(array $data)
    {
        try {
            DB::beginTransaction();

            $contentReviewClient = $this->contentReviewClientRepository->create($data);

            // do something else with the content category object
            // ...

            DB::commit();

            return $contentReviewClient;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    public function read(string $id)
    {
        return $this->contentReviewClientRepository->getByID($id);
    }

    public function delete(string $id)
    {
        // Here for bussiness Logic
        try {
            DB::beginTransaction();

            $contentReviewClient = $this->contentReviewClientRepository->delete($id);

            DB::commit();

            return $contentReviewClient;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }

    public function update(string $id, array $data)
    {
        // Here for bussiness Logic
        try {
            DB::beginTransaction();
            $contentReviewClient = $this->contentReviewClientRepository->update($id, $data);
            DB::commit();

            return $contentReviewClient;
        } catch (\Exception $e) {
            DB::rollback();
            return $e;
        }
    }
}
