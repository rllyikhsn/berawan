<?php

namespace App\Content\Providers;

use Illuminate\Support\ServiceProvider;

class ContentServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // Register the ContentRepository class
        $this->app->bind(
            \App\Content\Repositories\ContentRepository::class
        );
        
        // Register the ContentService class
        $this->app->bind(
            \App\Content\Services\ContentService::class
        );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
