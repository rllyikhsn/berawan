<?php

namespace App\Content\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

use App\Base\Controllers\Controller;
use App\Base\Traits\ResponseTrait;
use App\Content\Requests\ContentLogoIdentityRequest;
use App\Content\Requests\ContentIdentityRequest;
use App\Content\Requests\ContentContactRequest;
use App\Content\Services\ContentServices;

class ContentController extends Controller
{
    /**
     * Response trait to handle return responses.
     */
    use ResponseTrait;

    /**
     * Content Repository class.
     *
     * @var ContentServices
     */
    public $contentServices;

    public function __construct(ContentServices $contentServices)
    {
        // $this->middleware('auth:api', ['except' => ['indexAll']]);
        $this->contentServices = $contentServices;
    }

    /**
     * @OA\GET(
     *     path="/api/content/detail",
     *     tags={"Content"},
     *     summary="Show Content by ID",
     *     description="Show Content by ID",
     *     security={{"bearer":{}}},
     *     @OA\Response(response=200, description="Show Content by ID"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function show(): JsonResponse
    {
        try {
            $id = env('UUID_CONTENT');
            $data = $this->contentServices->read($id);
            if (is_null($data)) {
                return $this->responseError(null, 'Content Not Found', Response::HTTP_NOT_FOUND);
            }

            return $this->responseSuccess($data, 'Content Details Fetch Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content/identity",
     *     tags={"Content"},
     *     summary="Update Content by ID",
     *     description="Update Content by ID",
     *     security={{"bearer":{}}},
     *     @OA\RequestBody(
     *          @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                  @OA\Property(property="nama_instansi", type="string", example="New Content"),
     *                  @OA\Property(property="alamat_office", type="string", example="New Content"),
     *                  @OA\Property(property="alamat_warehouse", type="string", example="New Content"),
     *                  @OA\Property(property="alamat_workshop", type="string", example="New Content"),
     *                  @OA\Property(property="no_telepon_office", type="string", example="New Content"),
     *                  @OA\Property(property="no_telepon_warehouse", type="string", example="New Content"),
     *                  @OA\Property(property="no_telepon_workshop", type="string", example="New Content"),
     *                  @OA\Property(property="no_whatsapp", type="string", example="New Content"),
     *                  @OA\Property(property="email_office", type="string", example="New Content"),
     *                  @OA\Property(property="email_warehouse", type="string", example="New Content"),
     *                  @OA\Property(property="email_workshop", type="string", example="New Content"),
     *             ),
     *          ),
     *     ),
     *     @OA\Response(response=200, description="Content Updated Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function updateIdentity(ContentIdentityRequest $request): JsonResponse
    {
        try {
            $id = env('UUID_CONTENT');
            $data = $this->contentServices->update($id, $request->all());
            if (is_null($data))
                return $this->responseError(null, 'Content Not Found', Response::HTTP_NOT_FOUND);

            return $this->responseSuccess($data, 'Content Updated Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content/logo-identity",
     *     tags={"Content"},
     *     summary="Update Logo Content by ID",
     *     description="Update Logo Content by ID",
     *     security={{"bearer":{}}},
     *     @OA\RequestBody(
     *          @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                  @OA\Property(property="favicon_image", type="string"),
     *                  @OA\Property(property="logo_image", type="string"),
     *                  @OA\Property(property="logo_front_image", type="string"),
     *                  @OA\Property(property="about_image", type="string"),
     *                  @OA\Property(property="about_content", type="string"),
     *                  @OA\Property(property="footer_image", type="string"),
     *                  @OA\Property(property="footer_content", type="string"),
     *                  @OA\Property(property="service", type="string"),
     *                  @OA\Property(property="service_image", type="string"),
     *             ),
     *          ),
     *     ),
     *     @OA\Response(response=200, description="Content Updated Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function updateLogoIdentity(ContentLogoIdentityRequest $request): JsonResponse
    {
        try {
            $id = env('UUID_CONTENT');
            $data = $this->contentServices->updateLogoIdentity($id, $request->all());
            if (is_null($data))
                return $this->responseError(null, 'Content Not Found', Response::HTTP_NOT_FOUND);

            return $this->responseSuccess($data, 'Content Updated Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content/contact",
     *     tags={"Content"},
     *     summary="Update Content by ID",
     *     description="Update Content by ID",
     *     security={{"bearer":{}}},
     *     @OA\RequestBody(
     *          @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                  @OA\Property(property="contact_person_name_1", type="string"),
     *                  @OA\Property(property="contact_person_phone_1", type="string"),
     *                  @OA\Property(property="contact_person_whatsapp_1", type="string"),
     *                  @OA\Property(property="contact_person_email_1", type="string"),
     *                  @OA\Property(property="contact_person_name_2", type="string"),
     *                  @OA\Property(property="contact_person_phone_2", type="string"),
     *                  @OA\Property(property="contact_person_whatsapp_2", type="string"),
     *                  @OA\Property(property="contact_person_email_2", type="string"),
     *                  @OA\Property(property="contact_person_name_3", type="string"),
     *                  @OA\Property(property="contact_person_phone_3", type="string"),
     *                  @OA\Property(property="contact_person_whatsapp_3", type="string"),
     *                  @OA\Property(property="contact_person_email_3", type="string"),
     *             ),
     *          ),
     *     ),
     *     @OA\Response(response=200, description="Content Updated Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function updateContact(ContentContactRequest $request): JsonResponse
    {
        try {
            $id = env('UUID_CONTENT');
            $data = $this->contentServices->update($id, $request->all());
            if (is_null($data))
                return $this->responseError(null, 'Content Not Found', Response::HTTP_NOT_FOUND);

            return $this->responseSuccess($data, 'Content Updated Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content/media",
     *     tags={"Content"},
     *     summary="Update Content by ID",
     *     description="Update Content by ID",
     *     security={{"bearer":{}}},
     *     @OA\RequestBody(
     *          @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                  @OA\Property(property="link_facebook", type="string"),
     *                  @OA\Property(property="link_whatsapp", type="string"),
     *                  @OA\Property(property="link_whatsapp_template", type="string"),
     *                  @OA\Property(property="link_twitter", type="string"),
     *                  @OA\Property(property="link_youtube", type="string"),
     *                  @OA\Property(property="link_linktr", type="string"),
     *                  @OA\Property(property="link_instagram", type="string"),
     *                  @OA\Property(property="link_linkedin", type="string"),
     *                  @OA\Property(property="link_gmaps", type="string"),
     *                  @OA\Property(property="deeplink_playstore", type="string"),
     *                  @OA\Property(property="deeplink_appstore", type="string"),
     *                  @OA\Property(property="link_brocure", type="string"),
     *             ),
     *          ),
     *     ),
     *     @OA\Response(response=200, description="Content Updated Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function updateMedia(ContentIdentityRequest $request): JsonResponse
    {
        try {
            $id = env('UUID_CONTENT');
            $data = $this->contentServices->update($id, $request->all());
            if (is_null($data))
                return $this->responseError(null, 'Content Not Found', Response::HTTP_NOT_FOUND);

            return $this->responseSuccess($data, 'Content Updated Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
