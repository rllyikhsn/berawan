<?php

namespace App\Content\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

use App\Base\Controllers\Controller;
use App\Base\Traits\ResponseTrait;

use App\Content\Requests\ContentWorkshopRequest;
use App\Content\Services\ContentWorkshopServices;

use Yajra\DataTables\DataTables;

class ContentWorkshopController extends Controller
{
    /**
     * Response trait to handle return responses.
     */
    use ResponseTrait;

    /**
     * ContentWorkshop Repository class.
     *
     * @var ContentWorkshopServices
     */
    public $contentWorkshopServices;

    public function __construct(ContentWorkshopServices $contentWorkshopServices)
    {
        // $this->middleware('auth:api', ['except' => ['indexAll']]);
        $this->contentWorkshopServices = $contentWorkshopServices;
    }

    /**
     * @OA\GET(
     *     path="/api/content-workshops",
     *     tags={"Content Workshops"},
     *     summary="Get Content Workshop List",
     *     description="Get Content Workshop List as Array",
     *     operationId="indexWorkshop",
     *     security={{"bearer":{}}},
     *     @OA\Response(response=200,description="Get Content Workshop List as Array"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function index(): JsonResponse
    {
        try {
            $data = $this->contentWorkshopServices->getAll();
            return $this->responseSuccess($data, 'Content Workshop List Fetch Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/content-workshops/view/all",
     *     tags={"Content Workshops"},
     *     summary="All Content Workshops - Publicly Accessible",
     *     description="All Content Workshops - Publicly Accessible",
     *     @OA\Parameter(name="perPage", description="perPage, eg; 20", example=20, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="page", description="page, eg; 1", example=1, in="query", @OA\Schema(type="integer")),
     *     @OA\Response(response=200, description="All Content Workshops - Publicly Accessible" ),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function indexAll(Request $request): JsonResponse
    {
        try {
            $perPage = isset($request->perPage) ? intval($request->perPage) : 5;
            $page = isset($request->page) ? intval($request->page) : 1;
            $data = $this->contentWorkshopServices->getPaginatedData($perPage, $page);
            return $this->responseSuccess($data, 'Content Workshops List Fetched Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content-workshops/view/datatables",
     *     tags={"Content Workshops"},
     *     summary="Get Content Workshop List for DataTables",
     *     description="Get Content Workshop List for DataTables",
     *     operationId="datatablesWorkshop",
     *     security={{"bearer":{}}},
     *     @OA\Response(response=200,description="Get Content Workshop List for DataTables"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function datatables(Request $request): JsonResponse
    {
        try {
            $data = $this->contentWorkshopServices->getDataTables();

            return DataTables::of($data)
                ->addColumn('action', function ($row) {
                    $editUrl = '/admin/content-workshops/manage/' . $row->id;
                    $deleteUrl = route('content-workshops.destroy', $row->id);
                    $csrfToken = csrf_token();

                    $editButton = "<a href='{$editUrl}' class='btn btn-primary btn-sm mr-1'>
                        <i class='fas fa-edit'></i>
                    </a>";

                    $deleteButton = "<button type='button' class='btn btn-danger btn-sm btn-delete' data-id='{$row->id}'\">
                        <i class='fas fa-trash'></i>
                    </button>";

                    return "{$editButton}$deleteButton";
                })
                ->make(true);
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/content-workshops/view/search",
     *     tags={"Content Workshops"},
     *     summary="Search Content Workshops",
     *     description="Search Content Workshops",
     *     operationId="searchWorkshop",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="perPage", description="perPage, eg; 20", example=20, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="page", description="page, eg; 1", example=1, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="search", description="search, eg; Test", example="Test", in="query", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Search Content Workshops as Array"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function search(Request $request): JsonResponse
    {
        try {
            $perPage = isset($request->perPage) ? intval($request->perPage) : 5;
            $page = isset($request->page) ? intval($request->page) : 1;
            $data = $this->contentWorkshopServices->searchContentWorkshop($request->search, $perPage, $page);
            return $this->responseSuccess($data, 'Content Workshops Search Results Found !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content-workshops",
     *     tags={"Content Workshops"},
     *     summary="Create New Content Workshop",
     *     description="Create New Content Workshop",
     *     operationId="storeWorkshop",
     *     @OA\RequestBody(
     *         required=true,
     *          @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                  @OA\Property(property="name", type="string", example="New Content Workshop"),
     *                  @OA\Property(property="alamat", type="string", example="jl keman"),
     *                  @OA\Property(property="no_telepon", type="string", example="08xxxxxxxxxx"),
     *                  @OA\Property(property="is_draft", type="boolean", example=true),
     *                  @OA\Property(property="is_published", type="boolean", example=false),
     *             ),
     *          ),
     *     ),
     *     @OA\Response(response=201, description="Content Workshop Created Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function store(ContentWorkshopRequest $request): JsonResponse
    {
        try {
            $data = $this->contentWorkshopServices->create($request->validated());
            return $this->responseSuccess($data, 'Content Workshop Created Successfully !', Response::HTTP_CREATED);
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/content-workshops/{id}",
     *     tags={"Content Workshops"},
     *     summary="Show Content Workshop by ID",
     *     description="Show Content Workshop by ID",
     *     operationId="searchWorkshops",
     *     security={{"bearer":{}}},
     *  @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      description="UUID parameter",
     *      @OA\Schema(
     *          type="string",
     *          format="uuid",
     *          example="ec98669e-bcee-4c27-8c97-0712b1d83431"
     *      )
     *  ),
     *     @OA\Response(response=200, description="Show Content Workshop by ID"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function show($id): JsonResponse
    {
        try {
            $data = $this->contentWorkshopServices->read($id);
            if (is_null($data)) {
                return $this->responseError(null, 'Content Workshop Not Found', Response::HTTP_NOT_FOUND);
            }

            return $this->responseSuccess($data, 'Content Workshop Details Fetch Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content-workshops/{id}",
     *     tags={"Content Workshops"},
     *     summary="Update Content Workshop by ID",
     *     description="Update Content Workshop by ID",
     *     operationId="updateWorkshops",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\RequestBody(
     *          @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                  @OA\Property(property="name", type="string", example="New Content Workshop"),
     *                  @OA\Property(property="alamat", type="string", example="jl keman"),
     *                  @OA\Property(property="no_telepon", type="string", example="08xxxxxxxxxx"),
     *                  @OA\Property(property="is_draft", type="boolean", example=true),
     *                  @OA\Property(property="is_published", type="boolean", example=false),
     *             ),
     *          ),
     *     ),
     *     @OA\Response(response=200, description="Content Workshop Updated Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function update(ContentWorkshopRequest $request, $id): JsonResponse
    {
        try {
            $data = $this->contentWorkshopServices->update($id, $request->all());
            if (is_null($data))
                return $this->responseError(null, 'Content Workshop Not Found', Response::HTTP_NOT_FOUND);

            return $this->responseSuccess($data, 'Content Workshop Updated Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\DELETE(
     *     path="/api/content-workshops/{id}",
     *     tags={"Content Workshops"},
     *     summary="Delete Content Workshop by ID",
     *     description="Delete Content Workshop by ID",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Content Workshop Deleted Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function destroy($id): JsonResponse
    {
        try {
            $this->contentWorkshopServices->delete($id);
            return $this->responseSuccess(null, 'Content Workshop Deleted Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
