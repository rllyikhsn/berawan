<?php

namespace App\Content\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

use App\Base\Controllers\Controller;
use App\Base\Traits\ResponseTrait;

use App\Content\Requests\ContentReviewClientRequest;
use App\Content\Services\ContentReviewClientServices;

use Yajra\DataTables\DataTables;

class ContentReviewClientController extends Controller
{
    /**
     * Response trait to handle return responses.
     */
    use ResponseTrait;

    /**
     * ContentReviewClient Repository class.
     *
     * @var ContentReviewClientServices
     */
    public $contentContentReviewClientServices;

    public function __construct(ContentReviewClientServices $contentContentReviewClientServices)
    {
        // $this->middleware('auth:api', ['except' => ['indexAll']]);
        $this->contentContentReviewClientServices = $contentContentReviewClientServices;
    }

    /**
     * @OA\GET(
     *     path="/api/content-review-clients",
     *     tags={"ContentReviewClient"},
     *     summary="Get Content ContentReviewClient List",
     *     description="Get Content ContentReviewClient List as Array",
     *     operationId="indexContentReviewClient",
     *     security={{"bearer":{}}},
     *     @OA\Response(response=200,description="Get Content ContentReviewClient List as Array"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function index(): JsonResponse
    {
        try {
            $data = $this->contentContentReviewClientServices->getAll();
            return $this->responseSuccess($data, 'Content ContentReviewClient List Fetch Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/content-review-clients/view/all",
     *     tags={"ContentReviewClient"},
     *     summary="All ContentReviewClient - Publicly Accessible",
     *     description="All ContentReviewClient - Publicly Accessible",
     *     @OA\Parameter(name="perPage", description="perPage, eg; 20", example=20, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="page", description="page, eg; 1", example=1, in="query", @OA\Schema(type="integer")),
     *     @OA\Response(response=200, description="All ContentReviewClient - Publicly Accessible" ),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function indexAll(Request $request): JsonResponse
    {
        try {
            $perPage = isset($request->perPage) ? intval($request->perPage) : 5;
            $page = isset($request->page) ? intval($request->page) : 1;
            $data = $this->contentContentReviewClientServices->getPaginatedData($perPage, $page);
            return $this->responseSuccess($data, 'ContentReviewClient List Fetched Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content-review-clients/view/datatables",
     *     tags={"ContentReviewClient"},
     *     summary="Get Content ContentReviewClient List for DataTables",
     *     description="Get Content ContentReviewClient List for DataTables",
     *     operationId="datatablesContentReviewClient",
     *     security={{"bearer":{}}},
     *     @OA\Response(response=200,description="Get Content ContentReviewClient List for DataTables"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function datatables(Request $request): JsonResponse
    {
        try {
            $data = $this->contentContentReviewClientServices->getDataTables();

            return DataTables::of($data)
                ->addColumn('action', function ($row) {
                    $editUrl = '/admin/content-review-clients/manage/' . $row->id;
                    $deleteUrl = route('content-review-clients.destroy', $row->id);
                    $csrfToken = csrf_token();

                    $editButton = "<a href='{$editUrl}' class='btn btn-primary btn-sm mr-1'>
                        <i class='fas fa-edit'></i>
                    </a>";

                    $deleteButton = "<button type='button' class='btn btn-danger btn-sm btn-delete' data-id='{$row->id}'\">
                        <i class='fas fa-trash'></i>
                    </button>";

                    return "{$editButton}$deleteButton";
                })
                ->make(true);
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/content-review-clients/view/search",
     *     tags={"ContentReviewClient"},
     *     summary="Search ContentReviewClient",
     *     description="Search ContentReviewClient",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="perPage", description="perPage, eg; 20", example=20, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="page", description="page, eg; 1", example=1, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="search", description="search, eg; Test", example="Test", in="query", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Search ContentReviewClient as Array"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function search(Request $request): JsonResponse
    {
        try {
            $perPage = isset($request->perPage) ? intval($request->perPage) : 5;
            $page = isset($request->page) ? intval($request->page) : 1;
            $data = $this->contentContentReviewClientServices->searchContentReviewClient($request->search, $perPage, $page);
            return $this->responseSuccess($data, 'ContentReviewClient Search Results Found !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content-review-clients",
     *     tags={"ContentReviewClient"},
     *     summary="Create New Content ContentReviewClient",
     *     description="Create New Content ContentReviewClient",
     *     operationId="storeContentReviewClient",
     *     @OA\RequestBody(
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="title", type="string", example="Roman Beer"),
     *              @OA\Property(property="short_content", type="string", example="Short Content"),
     *              @OA\Property(property="stars", type="int", example="1"),
     *              @OA\Property(property="job", type="string", example="Long Content ContentReviewClient"),
     *              @OA\Property(property="avatar_image", example="98a1ba2d-66ec-4a92-943e-9c61829a4c58", type="string"),
     *              @OA\Property(property="is_spam", type="boolean", example=false),
     *          ),
     *      ),
     *     @OA\Response(response=201, description="Content ContentReviewClient Created Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function store(ContentReviewClientRequest $request): JsonResponse
    {
        try {
            $data = $this->contentContentReviewClientServices->create($request->validated());
            return $this->responseSuccess($data, 'Content ContentReviewClient Created Successfully !', Response::HTTP_CREATED);
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/content-review-clients/{id}",
     *     tags={"ContentReviewClient"},
     *     summary="Show Content ContentReviewClient by ID",
     *     description="Show Content ContentReviewClient by ID",
     *     operationId="searchContentReviewClient",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Show Content ContentReviewClient by ID"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function show($id): JsonResponse
    {
        try {
            $data = $this->contentContentReviewClientServices->read($id);
            if (is_null($data)) {
                return $this->responseError(null, 'Content ContentReviewClient Not Found', Response::HTTP_NOT_FOUND);
            }

            return $this->responseSuccess($data, 'Content ContentReviewClient Details Fetch Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content-review-clients/{id}",
     *     tags={"ContentReviewClient"},
     *     summary="Update Content ContentReviewClient by ID",
     *     description="Update Content ContentReviewClient by ID",
     *     operationId="updateContentReviewClient",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\RequestBody(
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="title", type="string", example="Roman Beer"),
     *              @OA\Property(property="short_content", type="string", example="Short Content"),
     *              @OA\Property(property="stars", type="int", example="1"),
     *              @OA\Property(property="job", type="string", example="Long Content ContentReviewClient"),
     *              @OA\Property(property="avatar_image", example="98a1ba2d-66ec-4a92-943e-9c61829a4c58", type="string"),
     *              @OA\Property(property="is_spam", type="boolean", example=false),
     *          ),
     *      ),
     *     @OA\Response(response=200, description="Content ContentReviewClient Updated Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function update(ContentReviewClientRequest $request, $id): JsonResponse
    {
        try {
            $data = $this->contentContentReviewClientServices->update($id, $request->all());
            if (is_null($data))
                return $this->responseError(null, 'Content ContentReviewClient Not Found', Response::HTTP_NOT_FOUND);

            return $this->responseSuccess($data, 'Content ContentReviewClient Updated Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\DELETE(
     *     path="/api/content-review-clients/{id}",
     *     tags={"ContentReviewClient"},
     *     summary="Delete Content ContentReviewClient by ID",
     *     description="Delete Content ContentReviewClient by ID",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Content ContentReviewClient Deleted Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function destroy($id): JsonResponse
    {
        try {
            $this->contentContentReviewClientServices->delete($id);
            return $this->responseSuccess(null, 'Content ContentReviewClient Deleted Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
