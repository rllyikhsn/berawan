<?php

namespace App\Content\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

use App\Base\Controllers\Controller;
use App\Base\Traits\ResponseTrait;
use App\Content\Requests\ContentIdentityRequest;
use App\Content\Requests\ContentMediaFileRequest;
use App\Content\Requests\ContentRequest;
use App\Content\Services\ContentMediaFileServices;

use Yajra\DataTables\DataTables;

class ContentMediaFileController extends Controller
{
    /**
     * Response trait to handle return responses.
     */
    use ResponseTrait;

    /**
     * ContentMediaFile Repository class.
     *
     * @var ContentMediaFileServices
     */
    public $contentMediaFileServices;

    public function __construct(ContentMediaFileServices $contentMediaFileServices)
    {
        // $this->middleware('auth:api', ['except' => ['indexAll']]);
        $this->contentMediaFileServices = $contentMediaFileServices;
    }

    /**
     * @OA\GET(
     *     path="/api/content-media-files",
     *     tags={"Content Media Files"},
     *     summary="Get Content Media File List",
     *     description="Get Content Media File List as Array",
     *     @OA\Parameter(name="perPage", description="perPage, eg; 20", example=20, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="page", description="page, eg; 1", example=1, in="query", @OA\Schema(type="integer")),
     *     security={{"bearer":{}}},
     *     @OA\Response(response=200,description="Get Content Media File List as Array"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function index(Request $request): JsonResponse
    {
        try {
            $perPage = isset($request->perPage) ? intval($request->perPage) : 5;
            $page = isset($request->page) ? intval($request->page) : 1;
            $data = $this->contentMediaFileServices->getPaginatedData($perPage, $page);

            foreach ($data->items() as $index => $dataImage) {

                $srcUrl = sprintf(
                    "/images/media-files/%s",
                    $dataImage->file
                );

                $data->items()[$index]->src = $srcUrl;

                $dimension = sprintf(
                    "%s x %s pixels",
                    $dataImage->width,
                    $dataImage->height
                );

                $data->items()[$index]->dimension = $dimension;

                $sizeInKB = round($dataImage->size / 1024, 2); // kilobytes

                $size = sprintf(
                    "%s KB",
                    $sizeInKB,
                );

                $data->items()[$index]->size = $size;
            }

            return $this->responseSuccess($data, 'Content Media File List Fetch Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content-media-files",
     *     tags={"Content Media Files"},
     *     summary="Create New Content Media File",
     *     description="Create New Content Media File",
     *     @OA\RequestBody(
     *         required=true,
     *          @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *              required={"file", "description"},
     *                  @OA\Property(property="image", type="file"),
     *             ),
     *          ),
     *     ),
     *     @OA\Response(response=201, description="Content Media File Created Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function store(ContentMediaFileRequest $request): JsonResponse
    {
        try {
            $data = $this->contentMediaFileServices->create($request->validated());
            return $this->responseSuccess($data, 'Content Media File Created Successfully !', Response::HTTP_CREATED);
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\DELETE(
     *     path="/api/content-media-files/{id}",
     *     tags={"Content Media Files"},
     *     summary="Delete Content Media File by ID",
     *     description="Delete Content Media File by ID",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Content Announcement Deleted Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function destroy($id): JsonResponse
    {
        try {
            $this->contentMediaFileServices->delete($id);
            return $this->responseSuccess(null, 'Content Media File Deleted Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
