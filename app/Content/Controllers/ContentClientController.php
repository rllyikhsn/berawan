<?php

namespace App\Content\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

use App\Base\Controllers\Controller;
use App\Base\Traits\ResponseTrait;

use App\Content\Requests\ContentClientRequest;
use App\Content\Services\ContentClientServices;

use Yajra\DataTables\DataTables;

class ContentClientController extends Controller
{
    /**
     * Response trait to handle return responses.
     */
    use ResponseTrait;

    /**
     * ContentClient Repository class.
     *
     * @var ContentClientServices
     */
    public $contentClientServices;

    public function __construct(ContentClientServices $contentClientServices)
    {
        $this->contentClientServices = $contentClientServices;
    }

    /**
     * @OA\GET(
     *     path="/api/content-clients",
     *     tags={"ContentClients"},
     *     summary="Get Content Client List",
     *     description="Get Content Client List as Array",
     *     operationId="indexContentClient",
     *     security={{"bearer":{}}},
     *     @OA\Response(response=200,description="Get Content Client List as Array"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function index(): JsonResponse
    {
        try {
            $data = $this->contentClientServices->getAll();
            return $this->responseSuccess($data, 'Content Client List Fetch Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/content-clients/view/all",
     *     tags={"ContentClients"},
     *     summary="All ContentClients - Publicly Accessible",
     *     description="All ContentClients - Publicly Accessible",
     *     @OA\Parameter(name="perPage", description="perPage, eg; 20", example=20, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="page", description="page, eg; 1", example=1, in="query", @OA\Schema(type="integer")),
     *     @OA\Response(response=200, description="All ContentClients - Publicly Accessible" ),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function indexAll(Request $request): JsonResponse
    {
        try {
            $perPage = isset($request->perPage) ? intval($request->perPage) : 5;
            $page = isset($request->page) ? intval($request->page) : 1;
            $data = $this->contentClientServices->getPaginatedData($perPage, $page);
            return $this->responseSuccess($data, 'ContentClients List Fetched Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content-clients/view/datatables",
     *     tags={"ContentClients"},
     *     summary="Get Content Client List for DataTables",
     *     description="Get Content Client List for DataTables",
     *     operationId="datatablesContentClient",
     *     security={{"bearer":{}}},
     *     @OA\Response(response=200,description="Get Content Client List for DataTables"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function datatables(Request $request): JsonResponse
    {
        try {
            $data = $this->contentClientServices->getDataTables();

            return DataTables::of($data)
                ->editColumn('thumbnail_image', function ($row) {
                    if ($row->thumbnail_image) {
                        $thumbnailImage = "/images/content-clients/thumbnail/{$row->thumbnail_image}";
                    } else {
                        $thumbnailImage = "";
                    }
                    return $thumbnailImage;
                })
                ->addColumn('action', function ($row) {
                    $editUrl = '/admin/content-clients/manage/' . $row->id;
                    $deleteUrl = route('content-clients.destroy', $row->id);
                    $csrfToken = csrf_token();

                    $editButton = "<a href='{$editUrl}' class='btn btn-primary btn-sm mr-1'>
                        <i class='fas fa-edit'></i>
                    </a>";

                    $deleteButton = "<button type='button' class='btn btn-danger btn-sm btn-delete' data-id='{$row->id}'\">
                        <i class='fas fa-trash'></i>
                    </button>";

                    return "{$editButton}$deleteButton";
                })
                ->make(true);
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/content-clients/view/search",
     *     tags={"ContentClients"},
     *     summary="Search ContentClients",
     *     description="Search ContentClients",
     *     operationId="searchContentClient",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="perPage", description="perPage, eg; 20", example=20, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="page", description="page, eg; 1", example=1, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="search", description="search, eg; Test", example="Test", in="query", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Search ContentClients as Array"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function search(Request $request): JsonResponse
    {
        try {
            $perPage = isset($request->perPage) ? intval($request->perPage) : 5;
            $page = isset($request->page) ? intval($request->page) : 1;
            $data = $this->contentClientServices->searchContentClients($request->search, $perPage, $page);
            return $this->responseSuccess($data, 'ContentClients Search Results Found !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content-clients",
     *     tags={"ContentClients"},
     *     summary="Create New Content Client",
     *     description="Create New Content Client",
     *     operationId="storeContentClient",
     *     @OA\RequestBody(
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="name", type="string", example="Roman Beer"),
     *              @OA\Property(property="pinned_image", example="98a1ba2d-66ec-4a92-943e-9c61829a4c58", type="string"),
     *          ),
     *      ),
     *     @OA\Response(response=201, description="Content Client Created Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function store(ContentClientRequest $request): JsonResponse
    {
        try {
            $data = $this->contentClientServices->create($request->validated());
            return $this->responseSuccess($data, 'Content Client Created Successfully !', Response::HTTP_CREATED);
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/content-clients/{id}",
     *     tags={"ContentClients"},
     *     summary="Show Content Client by ID",
     *     description="Show Content Client by ID",
     *     operationId="searchContentClients",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Show Content Client by ID"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function show($id): JsonResponse
    {
        try {
            $data = $this->contentClientServices->read($id);
            if (is_null($data)) {
                return $this->responseError(null, 'Content Client Not Found', Response::HTTP_NOT_FOUND);
            }

            return $this->responseSuccess($data, 'Content Client Details Fetch Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content-clients/{id}",
     *     tags={"ContentClients"},
     *     summary="Update Content Client by ID",
     *     description="Update Content Client by ID",
     *     operationId="updateContentClients",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\RequestBody(
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="name", type="string", example="Roman Beer"),
     *              @OA\Property(property="pinned_image", example="98a1ba2d-66ec-4a92-943e-9c61829a4c58", type="string"),
     *          ),
     *      ),
     *     @OA\Response(response=200, description="Content Client Updated Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function update(ContentClientRequest $request, $id): JsonResponse
    {
        try {
            $data = $this->contentClientServices->update($id, $request->all());
            if (is_null($data))
                return $this->responseError(null, 'Content Client Not Found', Response::HTTP_NOT_FOUND);

            return $this->responseSuccess($data, 'Content Client Updated Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\DELETE(
     *     path="/api/content-clients/{id}",
     *     tags={"ContentClients"},
     *     summary="Delete Content Client by ID",
     *     description="Delete Content Client by ID",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Content Client Deleted Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function destroy($id): JsonResponse
    {
        try {
            $this->contentClientServices->delete($id);
            return $this->responseSuccess(null, 'Content Client Deleted Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
