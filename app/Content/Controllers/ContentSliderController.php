<?php

namespace App\Content\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

use App\Base\Controllers\Controller;
use App\Base\Traits\ResponseTrait;

use App\Content\Requests\ContentSliderRequest;
use App\Content\Services\ContentSliderServices;

use Yajra\DataTables\DataTables;

class ContentSliderController extends Controller
{
    /**
     * Response trait to handle return responses.
     */
    use ResponseTrait;

    /**
     * ContentSlider Repository class.
     *
     * @var ContentSliderServices
     */
    public $contentContentSliderServices;

    public function __construct(ContentSliderServices $contentContentSliderServices)
    {
        // $this->middleware('auth:api', ['except' => ['indexAll']]);
        $this->contentContentSliderServices = $contentContentSliderServices;
    }

    /**
     * @OA\GET(
     *     path="/api/content-sliders",
     *     tags={"ContentSliders"},
     *     summary="Get Content Slider List",
     *     description="Get Content Slider List as Array",
     *     operationId="indexContentSlider",
     *     security={{"bearer":{}}},
     *     @OA\Response(response=200,description="Get Content Slider List as Array"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function index(): JsonResponse
    {
        try {
            $data = $this->contentContentSliderServices->getAll();
            return $this->responseSuccess($data, 'Content Slider List Fetch Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/content-sliders/view/all",
     *     tags={"ContentSliders"},
     *     summary="All ContentSliders - Publicly Accessible",
     *     description="All ContentSliders - Publicly Accessible",
     *     @OA\Parameter(name="perPage", description="perPage, eg; 20", example=20, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="page", description="page, eg; 1", example=1, in="query", @OA\Schema(type="integer")),
     *     @OA\Response(response=200, description="All ContentSliders - Publicly Accessible" ),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function indexAll(Request $request): JsonResponse
    {
        try {
            $perPage = isset($request->perPage) ? intval($request->perPage) : 5;
            $page = isset($request->page) ? intval($request->page) : 1;
            $data = $this->contentContentSliderServices->getPaginatedData($perPage, $page);
            return $this->responseSuccess($data, 'ContentSliders List Fetched Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content-sliders/view/datatables",
     *     tags={"ContentSliders"},
     *     summary="Get Content Slider List for DataTables",
     *     description="Get Content Slider List for DataTables",
     *     operationId="datatablesContentSlider",
     *     security={{"bearer":{}}},
     *     @OA\Response(response=200,description="Get Content Slider List for DataTables"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function datatables(Request $request): JsonResponse
    {
        try {
            $data = $this->contentContentSliderServices->getDataTables();

            return DataTables::of($data)
                ->editColumn('thumbnail_image', function ($row) {
                    if ($row->thumbnail_image) {
                        $thumbnailImage = "/images/content-sliders/thumbnail/{$row->thumbnail_image}";
                    } else {
                        $thumbnailImage = "";
                    }
                    return $thumbnailImage;
                })
                ->addColumn('action', function ($row) {
                    $editUrl = '/admin/content-sliders/manage/' . $row->id;
                    $deleteUrl = route('content-sliders.destroy', $row->id);
                    $csrfToken = csrf_token();

                    $editButton = "<a href='{$editUrl}' class='btn btn-primary btn-sm mr-1'>
                        <i class='fas fa-edit'></i>
                    </a>";

                    $deleteButton = "<button type='button' class='btn btn-danger btn-sm btn-delete' data-id='{$row->id}'\">
                        <i class='fas fa-trash'></i>
                    </button>";

                    return "{$editButton}$deleteButton";
                })
                ->make(true);
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/content-sliders/view/search",
     *     tags={"ContentSliders"},
     *     summary="Search ContentSliders",
     *     description="Search ContentSliders",
     *     operationId="searchContentSlider",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="perPage", description="perPage, eg; 20", example=20, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="page", description="page, eg; 1", example=1, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="search", description="search, eg; Test", example="Test", in="query", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Search ContentSliders as Array"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function search(Request $request): JsonResponse
    {
        try {
            $perPage = isset($request->perPage) ? intval($request->perPage) : 5;
            $page = isset($request->page) ? intval($request->page) : 1;
            $data = $this->contentContentSliderServices->searchContentSlider($request->search, $perPage, $page);
            return $this->responseSuccess($data, 'ContentSliders Search Results Found !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content-sliders",
     *     tags={"ContentSliders"},
     *     summary="Create New Content Slider",
     *     description="Create New Content Slider",
     *     operationId="storeContentSlider",
     *     @OA\RequestBody(
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="title", type="string", example="Roman Beer"),
     *              @OA\Property(property="short_content", type="string", example="Short Content Slider"),
     *              @OA\Property(property="image", example="98a1ba2d-66ec-4a92-943e-9c61829a4c58", type="string"),
     *              @OA\Property(property="is_draft", type="boolean", example=true),
     *              @OA\Property(property="is_published", type="boolean", example=false),
     *          ),
     *      ),
     *     @OA\Response(response=201, description="Content Slider Created Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function store(ContentSliderRequest $request): JsonResponse
    {
        try {
            $data = $this->contentContentSliderServices->create($request->validated());
            return $this->responseSuccess($data, 'Content Slider Created Successfully !', Response::HTTP_CREATED);
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/content-sliders/{id}",
     *     tags={"ContentSliders"},
     *     summary="Show Content Slider by ID",
     *     description="Show Content Slider by ID",
     *     operationId="searchContentSliders",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Show Content Slider by ID"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function show($id): JsonResponse
    {
        try {
            $data = $this->contentContentSliderServices->read($id);
            if (is_null($data)) {
                return $this->responseError(null, 'Content Slider Not Found', Response::HTTP_NOT_FOUND);
            }

            return $this->responseSuccess($data, 'Content Slider Details Fetch Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content-sliders/{id}",
     *     tags={"ContentSliders"},
     *     summary="Update Content Slider by ID",
     *     description="Update Content Slider by ID",
     *     operationId="updateContentSliders",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\RequestBody(
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="title", type="string", example="Roman Beer"),
     *              @OA\Property(property="short_content", type="string", example="Short Content Slider"),
     *              @OA\Property(property="image", example="98a1ba2d-66ec-4a92-943e-9c61829a4c58", type="string"),
     *              @OA\Property(property="is_draft", type="boolean", example=true),
     *              @OA\Property(property="is_published", type="boolean", example=false),
     *          ),
     *      ),
     *     @OA\Response(response=200, description="Content Slider Updated Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function update(ContentSliderRequest $request, $id): JsonResponse
    {
        try {
            $data = $this->contentContentSliderServices->update($id, $request->all());
            if (is_null($data))
                return $this->responseError(null, 'Content Slider Not Found', Response::HTTP_NOT_FOUND);

            return $this->responseSuccess($data, 'Content Slider Updated Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\DELETE(
     *     path="/api/content-sliders/{id}",
     *     tags={"ContentSliders"},
     *     summary="Delete Content Slider by ID",
     *     description="Delete Content Slider by ID",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Content Slider Deleted Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function destroy($id): JsonResponse
    {
        try {
            $this->contentContentSliderServices->delete($id);
            return $this->responseSuccess(null, 'Content Slider Deleted Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
