<?php

namespace App\Content\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

use App\Base\Controllers\Controller;
use App\Base\Traits\ResponseTrait;

use App\Content\Requests\ArticleRequest;
use App\Content\Services\ArticleServices;

use Yajra\DataTables\DataTables;

class ArticleController extends Controller
{
    /**
     * Response trait to handle return responses.
     */
    use ResponseTrait;

    /**
     * Article Repository class.
     *
     * @var ArticleServices
     */
    public $contentArticleServices;

    public function __construct(ArticleServices $contentArticleServices)
    {
        // $this->middleware('auth:api', ['except' => ['indexAll']]);
        $this->contentArticleServices = $contentArticleServices;
    }

    /**
     * @OA\GET(
     *     path="/api/articles",
     *     tags={"Articles"},
     *     summary="Get Content Article List",
     *     description="Get Content Article List as Array",
     *     operationId="indexArticle",
     *     security={{"bearer":{}}},
     *     @OA\Response(response=200,description="Get Content Article List as Array"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function index(): JsonResponse
    {
        try {
            $data = $this->contentArticleServices->getAll();
            return $this->responseSuccess($data, 'Content Article List Fetch Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/articles/view/all",
     *     tags={"Articles"},
     *     summary="All Articles - Publicly Accessible",
     *     description="All Articles - Publicly Accessible",
     *     @OA\Parameter(name="perPage", description="perPage, eg; 20", example=20, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="page", description="page, eg; 1", example=1, in="query", @OA\Schema(type="integer")),
     *     @OA\Response(response=200, description="All Articles - Publicly Accessible" ),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function indexAll(Request $request): JsonResponse
    {
        try {
            $perPage = isset($request->perPage) ? intval($request->perPage) : 5;
            $page = isset($request->page) ? intval($request->page) : 1;
            $data = $this->contentArticleServices->getPaginatedData($perPage, $page);
            return $this->responseSuccess($data, 'Articles List Fetched Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/articles/view/datatables",
     *     tags={"Articles"},
     *     summary="Get Content Article List for DataTables",
     *     description="Get Content Article List for DataTables",
     *     operationId="datatablesArticle",
     *     security={{"bearer":{}}},
     *     @OA\Response(response=200,description="Get Content Article List for DataTables"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function datatables(Request $request): JsonResponse
    {
        try {
            $data = $this->contentArticleServices->getDataTables();

            return DataTables::of($data)
                ->editColumn('thumbnail_image', function ($row) {
                    if ($row->thumbnail_image) {
                        $thumbnailImage = "/images/articles/thumbnail/{$row->thumbnail_image}";
                    } else {
                        $thumbnailImage = "";
                    }
                    return $thumbnailImage;
                })
                ->addColumn('action', function ($row) {
                    $editUrl = '/admin/articles/manage/' . $row->id;
                    $deleteUrl = route('articles.destroy', $row->id);
                    $csrfToken = csrf_token();

                    $editButton = "<a href='{$editUrl}' class='btn btn-primary btn-sm mr-1'>
                        <i class='fas fa-edit'></i>
                    </a>";

                    $deleteButton = "<button type='button' class='btn btn-danger btn-sm btn-delete' data-id='{$row->id}'\">
                        <i class='fas fa-trash'></i>
                    </button>";

                    return "{$editButton}$deleteButton";
                })
                ->make(true);
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/articles/view/search",
     *     tags={"Articles"},
     *     summary="Search Articles",
     *     description="Search Articles",
     *     operationId="searchArticle",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="perPage", description="perPage, eg; 20", example=20, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="page", description="page, eg; 1", example=1, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="search", description="search, eg; Test", example="Test", in="query", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Search Articles as Array"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function search(Request $request): JsonResponse
    {
        try {
            $perPage = isset($request->perPage) ? intval($request->perPage) : 5;
            $page = isset($request->page) ? intval($request->page) : 1;
            $data = $this->contentArticleServices->searchArticle($request->search, $perPage, $page);
            return $this->responseSuccess($data, 'Articles Search Results Found !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/articles",
     *     tags={"Articles"},
     *     summary="Create New Content Article",
     *     description="Create New Content Article",
     *     operationId="storeArticle",
     *     @OA\RequestBody(
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="title", type="string", example="Roman Beer"),
     *              @OA\Property(property="slug", type="string", example="roman-beer"),
     *              @OA\Property(property="short_content", type="string", example="Short Content Article"),
     *              @OA\Property(property="long_content", type="string", example="Long Content Article"),
     *              @OA\Property(property="pinned_image", example="98a1ba2d-66ec-4a92-943e-9c61829a4c58", type="string"),
     *              @OA\Property(property="thumbnail_image", example="98a1ba2d-66ec-4a92-943e-9c61829a4c58", type="string"),
     *              @OA\Property(property="is_draft", type="boolean", example=true),
     *              @OA\Property(property="is_published", type="boolean", example=false),
     *              @OA\Property(property="article_categories", type="array",
     *                  @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="content_category_id" ,type="string", example="98a1ba2d-66ec-4a92-943e-9c61829a4c58")
     *                  )
     *              ),
     *              @OA\Property(property="article_tags", type="array",
     *                  @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="content_tag_id" ,type="string", example="98a1ba2d-66ec-4a92-943e-9c61829a4c58")
     *                  )
     *              ),
     *              @OA\Property(property="article_images", type="array",
     *                  @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="content_media_file_id" ,type="string", example="98a1ba2d-66ec-4a92-943e-9c61829a4c58")
     *                  )
     *              ),
     *          ),
     *      ),
     *     @OA\Response(response=201, description="Content Article Created Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function store(ArticleRequest $request): JsonResponse
    {
        try {
            $data = $this->contentArticleServices->create($request->validated());
            return $this->responseSuccess($data, 'Content Article Created Successfully !', Response::HTTP_CREATED);
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/articles/{id}",
     *     tags={"Articles"},
     *     summary="Show Content Article by ID",
     *     description="Show Content Article by ID",
     *     operationId="searchArticles",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Show Content Article by ID"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function show($id): JsonResponse
    {
        try {
            $data = $this->contentArticleServices->read($id);
            if (is_null($data)) {
                return $this->responseError(null, 'Content Article Not Found', Response::HTTP_NOT_FOUND);
            }

            return $this->responseSuccess($data, 'Content Article Details Fetch Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/articles/{id}",
     *     tags={"Articles"},
     *     summary="Update Content Article by ID",
     *     description="Update Content Article by ID",
     *     operationId="updateArticles",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\RequestBody(
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="title", type="string", example="Roman Beer"),
     *              @OA\Property(property="slug", type="string", example="roman-beer"),
     *              @OA\Property(property="short_content", type="string", example="Short Content Article"),
     *              @OA\Property(property="long_content", type="string", example="Long Content Article"),
     *              @OA\Property(property="pinned_image", example="98a1ba2d-66ec-4a92-943e-9c61829a4c58", type="string"),
     *              @OA\Property(property="thumbnail_image", example="98a1ba2d-66ec-4a92-943e-9c61829a4c58", type="string"),
     *              @OA\Property(property="is_draft", type="boolean", example=true),
     *              @OA\Property(property="is_published", type="boolean", example=false),
     *              @OA\Property(property="article_categories", type="array",
     *                  @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="content_category_id" ,type="string", example="98a1ba2d-66ec-4a92-943e-9c61829a4c58")
     *                  )
     *              ),
     *              @OA\Property(property="article_tags", type="array",
     *                  @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="content_tag_id" ,type="string", example="98a1ba2d-66ec-4a92-943e-9c61829a4c58")
     *                  )
     *              ),
     *              @OA\Property(property="article_images", type="array",
     *                  @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="content_media_file_id" ,type="string", example="98a1ba2d-66ec-4a92-943e-9c61829a4c58")
     *                  )
     *              ),
     *          ),
     *      ),
     *     @OA\Response(response=200, description="Content Article Updated Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function update(ArticleRequest $request, $id): JsonResponse
    {
        try {
            $data = $this->contentArticleServices->update($id, $request->all());
            if (is_null($data))
                return $this->responseError(null, 'Content Article Not Found', Response::HTTP_NOT_FOUND);

            return $this->responseSuccess($data, 'Content Article Updated Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\DELETE(
     *     path="/api/articles/{id}",
     *     tags={"Articles"},
     *     summary="Delete Content Article by ID",
     *     description="Delete Content Article by ID",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Content Article Deleted Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function destroy($id): JsonResponse
    {
        try {
            $this->contentArticleServices->delete($id);
            return $this->responseSuccess(null, 'Content Article Deleted Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
