<?php

namespace App\Content\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

use App\Base\Controllers\Controller;
use App\Base\Traits\ResponseTrait;

use App\Content\Requests\ContentReportAnnualRequest;
use App\Content\Services\ContentReportAnnualServices;

use Yajra\DataTables\DataTables;

class ContentReportAnnualController extends Controller
{
    /**
     * Response trait to handle return responses.
     */
    use ResponseTrait;

    /**
     * ContentReportAnnual Repository class.
     *
     * @var ContentReportAnnualServices
     */
    public $contentReportAnnualServices;

    public function __construct(ContentReportAnnualServices $contentReportAnnualServices)
    {
        // $this->middleware('auth:api', ['except' => ['indexAll']]);
        $this->contentReportAnnualServices = $contentReportAnnualServices;
    }

    /**
     * @OA\GET(
     *     path="/api/content-report-annuals",
     *     tags={"Content Report Annuals"},
     *     summary="Get Content Report Annual List",
     *     description="Get Content Report Annual List as Array",
     *     operationId="indexReportAnnual",
     *     security={{"bearer":{}}},
     *     @OA\Response(response=200,description="Get Content Report Annual List as Array"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function index(): JsonResponse
    {
        try {
            $data = $this->contentReportAnnualServices->getAll();
            return $this->responseSuccess($data, 'Content Report Annual List Fetch Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/content-report-annuals/view/all",
     *     tags={"Content Report Annuals"},
     *     summary="All Content Report Annuals - Publicly Accessible",
     *     description="All Content Report Annuals - Publicly Accessible",
     *     @OA\Parameter(name="perPage", description="perPage, eg; 20", example=20, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="page", description="page, eg; 1", example=1, in="query", @OA\Schema(type="integer")),
     *     @OA\Response(response=200, description="All Content Report Annuals - Publicly Accessible" ),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function indexAll(Request $request): JsonResponse
    {
        try {
            $perPage = isset($request->perPage) ? intval($request->perPage) : 5;
            $page = isset($request->page) ? intval($request->page) : 1;
            $data = $this->contentReportAnnualServices->getPaginatedData($perPage, $page);
            return $this->responseSuccess($data, 'Content Report Annuals List Fetched Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content-report-annuals/view/datatables",
     *     tags={"Content Report Annuals"},
     *     summary="Get Content Report Annual List for DataTables",
     *     description="Get Content Report Annual List for DataTables",
     *     operationId="datatablesReportAnnual",
     *     security={{"bearer":{}}},
     *     @OA\Response(response=200,description="Get Content Report Annual List for DataTables"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function datatables(Request $request): JsonResponse
    {
        try {
            $data = $this->contentReportAnnualServices->getDataTables();

            return DataTables::of($data)
                ->addColumn('action', function ($row) {
                    $editUrl = '/admin/content-report-annuals/manage/' . $row->id;
                    $deleteUrl = route('content-report-annuals.destroy', $row->id);
                    $csrfToken = csrf_token();

                    $editButton = "<a href='{$editUrl}' class='btn btn-primary btn-sm mr-1'>
                        <i class='fas fa-edit'></i>
                    </a>";

                    $deleteButton = "<button type='button' class='btn btn-danger btn-sm btn-delete' data-id='{$row->id}'\">
                        <i class='fas fa-trash'></i>
                    </button>";

                    return "{$editButton}$deleteButton";
                })
                ->make(true);
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/content-report-annuals/view/search",
     *     tags={"Content Report Annuals"},
     *     summary="Search Content Report Annuals",
     *     description="Search Content Report Annuals",
     *     operationId="searchReportAnnual",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="perPage", description="perPage, eg; 20", example=20, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="page", description="page, eg; 1", example=1, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="search", description="search, eg; Test", example="Test", in="query", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Search Content Report Annuals as Array"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function search(Request $request): JsonResponse
    {
        try {
            $perPage = isset($request->perPage) ? intval($request->perPage) : 5;
            $page = isset($request->page) ? intval($request->page) : 1;
            $data = $this->contentReportAnnualServices->searchContentReportAnnual($request->search, $perPage, $page);
            return $this->responseSuccess($data, 'Content Report Annuals Search Results Found !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content-report-annuals",
     *     tags={"Content Report Annuals"},
     *     summary="Create New Content Report Annual",
     *     description="Create New Content Report Annual",
     *     operationId="storeReportAnnual",
     *     @OA\RequestBody(
     *          @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *              required={"title", "file"},
     *                  @OA\Property(property="title", type="string"),
     *                  @OA\Property(property="file", type="file"),
     *             ),
     *          ),
     *     ),
     *     @OA\Response(response=201, description="Content Report Annual Created Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function store(ContentReportAnnualRequest $request): JsonResponse
    {
        try {
            $data = $this->contentReportAnnualServices->create($request->validated());
            return $this->responseSuccess($data, 'Content Report Annual Created Successfully !', Response::HTTP_CREATED);
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/content-report-annuals/{id}",
     *     tags={"Content Report Annuals"},
     *     summary="Show Content Report Annual by ID",
     *     description="Show Content Report Annual by ID",
     *     operationId="searchReportAnnuals",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Show Content Report Annual by ID"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function show($id): JsonResponse
    {
        try {
            $data = $this->contentReportAnnualServices->read($id);
            if (is_null($data)) {
                return $this->responseError(null, 'Content Report Annual Not Found', Response::HTTP_NOT_FOUND);
            }

            return $this->responseSuccess($data, 'Content Report Annual Details Fetch Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content-report-annuals/{id}",
     *     tags={"Content Report Annuals"},
     *     summary="Update Content Report Annual by ID",
     *     description="Update Content Report Annual by ID",
     *     operationId="updateReportAnnuals",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\RequestBody(
     *          @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *              required={"title", "file"},
     *                  @OA\Property(property="title", type="string"),
     *                  @OA\Property(property="file", type="file"),
     *             ),
     *          ),
     *     ),
     *     @OA\Response(response=200, description="Content Report Annual Updated Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function update(ContentReportAnnualRequest $request, $id): JsonResponse
    {
        try {
            $data = $this->contentReportAnnualServices->update($id, $request->all());
            if (is_null($data))
                return $this->responseError(null, 'Content Report Annual Not Found', Response::HTTP_NOT_FOUND);

            return $this->responseSuccess($data, 'Content Report Annual Updated Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\DELETE(
     *     path="/api/content-report-annuals/{id}",
     *     tags={"Content Report Annuals"},
     *     summary="Delete Content Report Annual by ID",
     *     description="Delete Content Report Annual by ID",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Content Report Annual Deleted Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function destroy($id): JsonResponse
    {
        try {
            $this->contentReportAnnualServices->delete($id);
            return $this->responseSuccess(null, 'Content Report Annual Deleted Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
