<?php

namespace App\Content\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

use App\Base\Controllers\Controller;
use App\Base\Traits\ResponseTrait;

use App\Content\Requests\ContentReportFinanceRequest;
use App\Content\Services\ContentReportFinanceServices;

use Yajra\DataTables\DataTables;

class ContentReportFinanceController extends Controller
{
    /**
     * Response trait to handle return responses.
     */
    use ResponseTrait;

    /**
     * ContentReportFinance Repository class.
     *
     * @var ContentReportFinanceServices
     */
    public $contentReportFinanceServices;

    public function __construct(ContentReportFinanceServices $contentReportFinanceServices)
    {
        // $this->middleware('auth:api', ['except' => ['indexAll']]);
        $this->contentReportFinanceServices = $contentReportFinanceServices;
    }

    /**
     * @OA\GET(
     *     path="/api/content-report-finances",
     *     tags={"Content Report Finances"},
     *     summary="Get Content Report Finance List",
     *     description="Get Content Report Finance List as Array",
     *     operationId="indexReportFinance",
     *     security={{"bearer":{}}},
     *     @OA\Response(response=200,description="Get Content Report Finance List as Array"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function index(): JsonResponse
    {
        try {
            $data = $this->contentReportFinanceServices->getAll();
            return $this->responseSuccess($data, 'Content Report Finance List Fetch Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/content-report-finances/view/all",
     *     tags={"Content Report Finances"},
     *     summary="All Content Report Finances - Publicly Accessible",
     *     description="All Content Report Finances - Publicly Accessible",
     *     @OA\Parameter(name="perPage", description="perPage, eg; 20", example=20, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="page", description="page, eg; 1", example=1, in="query", @OA\Schema(type="integer")),
     *     @OA\Response(response=200, description="All Content Report Finances - Publicly Accessible" ),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function indexAll(Request $request): JsonResponse
    {
        try {
            $perPage = isset($request->perPage) ? intval($request->perPage) : 5;
            $page = isset($request->page) ? intval($request->page) : 1;
            $data = $this->contentReportFinanceServices->getPaginatedData($perPage, $page);
            return $this->responseSuccess($data, 'Content Report Finances List Fetched Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content-report-finances/view/datatables",
     *     tags={"Content Report Finances"},
     *     summary="Get Content Report Finance List for DataTables",
     *     description="Get Content Report Finance List for DataTables",
     *     operationId="datatablesReportFinance",
     *     security={{"bearer":{}}},
     *     @OA\Response(response=200,description="Get Content Report Finance List for DataTables"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function datatables(Request $request): JsonResponse
    {
        try {
            $data = $this->contentReportFinanceServices->getDataTables();

            return DataTables::of($data)
                ->addColumn('action', function ($row) {
                    $editUrl = '/admin/content-report-finances/manage/' . $row->id;
                    $deleteUrl = route('content-report-finances.destroy', $row->id);
                    $csrfToken = csrf_token();

                    $editButton = "<a href='{$editUrl}' class='btn btn-primary btn-sm mr-1'>
                        <i class='fas fa-edit'></i>
                    </a>";

                    $deleteButton = "<button type='button' class='btn btn-danger btn-sm btn-delete' data-id='{$row->id}'\">
                        <i class='fas fa-trash'></i>
                    </button>";

                    return "{$editButton}$deleteButton";
                })
                ->make(true);
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/content-report-finances/view/search",
     *     tags={"Content Report Finances"},
     *     summary="Search Content Report Finances",
     *     description="Search Content Report Finances",
     *     operationId="searchReportFinance",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="perPage", description="perPage, eg; 20", example=20, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="page", description="page, eg; 1", example=1, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="search", description="search, eg; Test", example="Test", in="query", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Search Content Report Finances as Array"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function search(Request $request): JsonResponse
    {
        try {
            $perPage = isset($request->perPage) ? intval($request->perPage) : 5;
            $page = isset($request->page) ? intval($request->page) : 1;
            $data = $this->contentReportFinanceServices->searchContentReportFinance($request->search, $perPage, $page);
            return $this->responseSuccess($data, 'Content Report Finances Search Results Found !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content-report-finances",
     *     tags={"Content Report Finances"},
     *     summary="Create New Content Report Finance",
     *     description="Create New Content Report Finance",
     *     operationId="storeReportFinance",
     *     @OA\RequestBody(
     *          @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *              required={"title", "file"},
     *                  @OA\Property(property="title", type="string"),
     *                  @OA\Property(property="file", type="file"),
     *             ),
     *          ),
     *     ),
     *     @OA\Response(response=201, description="Content Report Finance Created Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function store(ContentReportFinanceRequest $request): JsonResponse
    {
        try {
            $data = $this->contentReportFinanceServices->create($request->validated());
            return $this->responseSuccess($data, 'Content Report Finance Created Successfully !', Response::HTTP_CREATED);
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/content-report-finances/{id}",
     *     tags={"Content Report Finances"},
     *     summary="Show Content Report Finance by ID",
     *     description="Show Content Report Finance by ID",
     *     operationId="searchReportFinances",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Show Content Report Finance by ID"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function show($id): JsonResponse
    {
        try {
            $data = $this->contentReportFinanceServices->read($id);
            if (is_null($data)) {
                return $this->responseError(null, 'Content Report Finance Not Found', Response::HTTP_NOT_FOUND);
            }

            return $this->responseSuccess($data, 'Content Report Finance Details Fetch Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content-report-finances/{id}",
     *     tags={"Content Report Finances"},
     *     summary="Update Content Report Finance by ID",
     *     description="Update Content Report Finance by ID",
     *     operationId="updateReportFinances",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\RequestBody(
     *          @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *              required={"title", "file"},
     *                  @OA\Property(property="title", type="string"),
     *                  @OA\Property(property="file", type="file"),
     *             ),
     *          ),
     *     ),
     *     @OA\Response(response=200, description="Content Report Finance Updated Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function update(ContentReportFinanceRequest $request, $id): JsonResponse
    {
        try {
            $data = $this->contentReportFinanceServices->update($id, $request->all());
            if (is_null($data))
                return $this->responseError(null, 'Content Report Finance Not Found', Response::HTTP_NOT_FOUND);

            return $this->responseSuccess($data, 'Content Report Finance Updated Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\DELETE(
     *     path="/api/content-report-finances/{id}",
     *     tags={"Content Report Finances"},
     *     summary="Delete Content Report Finance by ID",
     *     description="Delete Content Report Finance by ID",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Content Report Finance Deleted Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function destroy($id): JsonResponse
    {
        try {
            $this->contentReportFinanceServices->delete($id);
            return $this->responseSuccess(null, 'Content Report Finance Deleted Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
