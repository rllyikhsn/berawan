<?php

namespace App\Content\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

use App\Base\Controllers\Controller;
use App\Base\Traits\ResponseTrait;

use App\Content\Requests\ContentAnnouncementRequest;
use App\Content\Services\ContentAnnouncementServices;

use Yajra\DataTables\DataTables;

class ContentAnnouncementController extends Controller
{
    /**
     * Response trait to handle return responses.
     */
    use ResponseTrait;

    /**
     * ContentAnnouncement Repository class.
     *
     * @var ContentAnnouncementServices
     */
    public $contentAnnouncementServices;

    public function __construct(ContentAnnouncementServices $contentAnnouncementServices)
    {
        // $this->middleware('auth:api', ['except' => ['indexAll']]);
        $this->contentAnnouncementServices = $contentAnnouncementServices;
    }

    /**
     * @OA\GET(
     *     path="/api/content-announcements",
     *     tags={"Content Announcements"},
     *     summary="Get Content Announcement List",
     *     description="Get Content Announcement List as Array",
     *     operationId="indexAnnouncement",
     *     security={{"bearer":{}}},
     *     @OA\Response(response=200,description="Get Content Announcement List as Array"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function index(): JsonResponse
    {
        try {
            $data = $this->contentAnnouncementServices->getAll();
            return $this->responseSuccess($data, 'Content Announcement List Fetch Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/content-announcements/view/all",
     *     tags={"Content Announcements"},
     *     summary="All Content Announcements - Publicly Accessible",
     *     description="All Content Announcements - Publicly Accessible",
     *     @OA\Parameter(name="perPage", description="perPage, eg; 20", example=20, in="query", @OA\Schema(type="integer")),
     *     @OA\Response(response=200, description="All Content Announcements - Publicly Accessible" ),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function indexAll(Request $request): JsonResponse
    {
        try {
            $perPage = isset($request->perPage) ? intval($request->perPage) : 5;
            $data = $this->contentAnnouncementServices->getPaginatedData($perPage, $request->page);
            return $this->responseSuccess($data, 'Content Announcements List Fetched Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content-announcements/view/datatables",
     *     tags={"Content Announcements"},
     *     summary="Get Content Announcement List for DataTables",
     *     description="Get Content Announcement List for DataTables",
     *     operationId="datatablesAnnouncement",
     *     security={{"bearer":{}}},
     *     @OA\Response(response=200,description="Get Content Announcement List for DataTables"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function datatables(Request $request): JsonResponse
    {
        try {
            $data = $this->contentAnnouncementServices->getDataTables();

            return DataTables::of($data)
                ->addColumn('action', function ($row) {
                    $editUrl = '/admin/content-announcements/manage/' . $row->id;
                    $deleteUrl = route('content-announcements.destroy', $row->id);
                    $csrfToken = csrf_token();

                    $editButton = "<a href='{$editUrl}' class='btn btn-primary btn-sm mr-1'>
                        <i class='fas fa-edit'></i>
                    </a>";

                    $deleteButton = "<button type='button' class='btn btn-danger btn-sm btn-delete' data-id='{$row->id}'\">
                        <i class='fas fa-trash'></i>
                    </button>";

                    return "{$editButton}$deleteButton";
                })
                ->make(true);
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/content-announcements/view/search",
     *     tags={"Content Announcements"},
     *     summary="Search Content Announcements",
     *     description="Search Content Announcements",
     *     operationId="searchAnnouncement",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="perPage", description="perPage, eg; 20", example=20, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="search", description="search, eg; Test", example="Test", in="query", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Search Content Announcements as Array"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function search(Request $request): JsonResponse
    {
        try {
            $data = $this->contentAnnouncementServices->searchContentAnnouncement($request->search, $request->perPage);
            return $this->responseSuccess($data, 'Content Announcements Search Results Found !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content-announcements",
     *     tags={"Content Announcements"},
     *     summary="Create New Content Announcement",
     *     description="Create New Content Announcement",
     *     operationId="storeAnnouncement",
     *     @OA\RequestBody(
     *         required=true,
     *          @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *              required={"file", "description"},
     *                  @OA\Property(property="title", type="string", example="New Content Announcement"),
     *                  @OA\Property(property="description", type="string", example="This is a new content announcement"),
     *                  @OA\Property(property="short_content", type="string", example="This is a new content announcement"),
     *                  @OA\Property(property="start_at", type="string", format="date-time", example="2023-04-18 08:00:00"),
     *                  @OA\Property(property="end_at", type="string", format="date-time", example="2023-04-20 23:59:59"),
     *                  @OA\Property(property="is_draft", type="boolean", example=true),
     *                  @OA\Property(property="is_published", type="boolean", example=false),
     *                  @OA\Property(property="image", type="file"),
     *             ),
     *          ),
     *     ),
     *     @OA\Response(response=201, description="Content Announcement Created Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function store(ContentAnnouncementRequest $request): JsonResponse
    {
        try {
            $data = $this->contentAnnouncementServices->create($request->validated());
            return $this->responseSuccess($data, 'Content Announcement Created Successfully !', Response::HTTP_CREATED);
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/content-announcements/{id}",
     *     tags={"Content Announcements"},
     *     summary="Show Content Announcement by ID",
     *     description="Show Content Announcement by ID",
     *     operationId="searchAnnouncements",
     *     security={{"bearer":{}}},
     *  @OA\Parameter(
     *      name="id",
     *      in="path",
     *      required=true,
     *      description="UUID parameter",
     *      @OA\Schema(
     *          type="string",
     *          format="uuid",
     *          example="ec98669e-bcee-4c27-8c97-0712b1d83431"
     *      )
     *  ),
     *     @OA\Response(response=200, description="Show Content Announcement by ID"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function show($id): JsonResponse
    {
        try {
            $data = $this->contentAnnouncementServices->read($id);
            if (is_null($data)) {
                return $this->responseError(null, 'Content Announcement Not Found', Response::HTTP_NOT_FOUND);
            }

            return $this->responseSuccess($data, 'Content Announcement Details Fetch Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content-announcements/{id}",
     *     tags={"Content Announcements"},
     *     summary="Update Content Announcement by ID",
     *     description="Update Content Announcement by ID",
     *     operationId="updateAnnouncements",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\RequestBody(
     *          @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                  @OA\Property(property="title", type="string", example="New Content Announcement"),
     *                  @OA\Property(property="description", type="string", example="This is a new content announcement"),
     *                  @OA\Property(property="short_content", type="string", example="This is a new content announcement"),
     *                  @OA\Property(property="start_at", type="string", format="date-time", example="2023-04-18 08:00:00"),
     *                  @OA\Property(property="end_at", type="string", format="date-time", example="2023-04-20 23:59:59"),
     *                  @OA\Property(property="is_draft", type="boolean", example=true),
     *                  @OA\Property(property="is_published", type="boolean", example=false),
     *                  @OA\Property(property="image", type="file"),
     *             ),
     *          ),
     *     ),
     *     @OA\Response(response=200, description="Content Announcement Updated Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function update(ContentAnnouncementRequest $request, $id): JsonResponse
    {
        try {
            $data = $this->contentAnnouncementServices->update($id, $request->all());
            if (is_null($data))
                return $this->responseError(null, 'Content Announcement Not Found', Response::HTTP_NOT_FOUND);

            return $this->responseSuccess($data, 'Content Announcement Updated Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\DELETE(
     *     path="/api/content-announcements/{id}",
     *     tags={"Content Announcements"},
     *     summary="Delete Content Announcement by ID",
     *     description="Delete Content Announcement by ID",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Content Announcement Deleted Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function destroy($id): JsonResponse
    {
        try {
            $this->contentAnnouncementServices->delete($id);
            return $this->responseSuccess(null, 'Content Announcement Deleted Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
