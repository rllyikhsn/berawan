<?php

namespace App\Content\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

use App\Base\Controllers\Controller;
use App\Base\Traits\ResponseTrait;

use App\Content\Requests\ContentGalleryRequest;
use App\Content\Services\ContentGalleryServices;

use Yajra\DataTables\DataTables;

class ContentGalleryController extends Controller
{
    /**
     * Response trait to handle return responses.
     */
    use ResponseTrait;

    /**
     * ContentGallery Repository class.
     *
     * @var ContentGalleryServices
     */
    public $contentContentGalleryServices;

    public function __construct(ContentGalleryServices $contentContentGalleryServices)
    {
        // $this->middleware('auth:api', ['except' => ['indexAll']]);
        $this->contentContentGalleryServices = $contentContentGalleryServices;
    }

    /**
     * @OA\GET(
     *     path="/api/content-galleries",
     *     tags={"ContentGalleries"},
     *     summary="Get Content Gallery List",
     *     description="Get Content Gallery List as Array",
     *     operationId="indexContentGallery",
     *     security={{"bearer":{}}},
     *     @OA\Response(response=200,description="Get Content Gallery List as Array"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function index(): JsonResponse
    {
        try {
            $data = $this->contentContentGalleryServices->getAll();
            return $this->responseSuccess($data, 'Content Gallery List Fetch Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/content-galleries/view/all",
     *     tags={"ContentGalleries"},
     *     summary="All ContentGalleries - Publicly Accessible",
     *     description="All ContentGalleries - Publicly Accessible",
     *     @OA\Parameter(name="perPage", description="perPage, eg; 20", example=20, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="page", description="page, eg; 1", example=1, in="query", @OA\Schema(type="integer")),
     *     @OA\Response(response=200, description="All ContentGalleries - Publicly Accessible" ),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function indexAll(Request $request): JsonResponse
    {
        try {
            $perPage = isset($request->perPage) ? intval($request->perPage) : 5;
            $page = isset($request->page) ? intval($request->page) : 1;
            $data = $this->contentContentGalleryServices->getPaginatedData($perPage, $page);
            return $this->responseSuccess($data, 'ContentGalleries List Fetched Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content-galleries/view/datatables",
     *     tags={"ContentGalleries"},
     *     summary="Get Content Gallery List for DataTables",
     *     description="Get Content Gallery List for DataTables",
     *     operationId="datatablesContentGallery",
     *     security={{"bearer":{}}},
     *     @OA\Response(response=200,description="Get Content Gallery List for DataTables"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function datatables(Request $request): JsonResponse
    {
        try {
            $data = $this->contentContentGalleryServices->getDataTables();

            return DataTables::of($data)
                ->editColumn('thumbnail_image', function ($row) {
                    if ($row->thumbnail_image) {
                        $thumbnailImage = "/images/content-galleries/thumbnail/{$row->thumbnail_image}";
                    } else {
                        $thumbnailImage = "";
                    }
                    return $thumbnailImage;
                })
                ->addColumn('action', function ($row) {
                    $editUrl = '/admin/content-galleries/manage/' . $row->id;
                    $deleteUrl = route('content-galleries.destroy', $row->id);
                    $csrfToken = csrf_token();

                    $editButton = "<a href='{$editUrl}' class='btn btn-primary btn-sm mr-1'>
                        <i class='fas fa-edit'></i>
                    </a>";

                    $deleteButton = "<button type='button' class='btn btn-danger btn-sm btn-delete' data-id='{$row->id}'\">
                        <i class='fas fa-trash'></i>
                    </button>";

                    return "{$editButton}$deleteButton";
                })
                ->make(true);
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/content-galleries/view/search",
     *     tags={"ContentGalleries"},
     *     summary="Search ContentGalleries",
     *     description="Search ContentGalleries",
     *     operationId="searchContentGallery",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="perPage", description="perPage, eg; 20", example=20, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="page", description="page, eg; 1", example=1, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="search", description="search, eg; Test", example="Test", in="query", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Search ContentGalleries as Array"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function search(Request $request): JsonResponse
    {
        try {
            $perPage = isset($request->perPage) ? intval($request->perPage) : 5;
            $page = isset($request->page) ? intval($request->page) : 1;
            $data = $this->contentContentGalleryServices->searchContentGallery($request->search, $perPage, $page);
            return $this->responseSuccess($data, 'ContentGalleries Search Results Found !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content-galleries",
     *     tags={"ContentGalleries"},
     *     summary="Create New Content Gallery",
     *     description="Create New Content Gallery",
     *     operationId="storeContentGallery",
     *     @OA\RequestBody(
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="title", type="string", example="Roman Beer"),
     *              @OA\Property(property="slug", type="string", example="roman-beer"),
     *              @OA\Property(property="short_content", type="string", example="Short Content Gallery"),
     *              @OA\Property(property="is_draft", type="boolean", example=true),
     *              @OA\Property(property="is_published", type="boolean", example=false),
     *              @OA\Property(property="content_gallery_categories", type="array",
     *                  @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="content_category_id" ,type="string", example="98a1ba2d-66ec-4a92-943e-9c61829a4c58")
     *                  )
     *              ),
     *              @OA\Property(property="content_gallery_tags", type="array",
     *                  @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="content_tag_id" ,type="string", example="98a1ba2d-66ec-4a92-943e-9c61829a4c58")
     *                  )
     *              ),
     *              @OA\Property(property="content_gallery_images", type="array",
     *                  @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="content_media_file_id" ,type="string", example="98a1ba2d-66ec-4a92-943e-9c61829a4c58")
     *                  )
     *              ),
     *          ),
     *      ),
     *     @OA\Response(response=201, description="Content Gallery Created Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function store(ContentGalleryRequest $request): JsonResponse
    {
        try {
            $data = $this->contentContentGalleryServices->create($request->validated());
            return $this->responseSuccess($data, 'Content Gallery Created Successfully !', Response::HTTP_CREATED);
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/content-galleries/{id}",
     *     tags={"ContentGalleries"},
     *     summary="Show Content Gallery by ID",
     *     description="Show Content Gallery by ID",
     *     operationId="searchContentGalleries",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Show Content Gallery by ID"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function show($id): JsonResponse
    {
        try {
            $data = $this->contentContentGalleryServices->read($id);
            if (is_null($data)) {
                return $this->responseError(null, 'Content Gallery Not Found', Response::HTTP_NOT_FOUND);
            }

            return $this->responseSuccess($data, 'Content Gallery Details Fetch Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content-galleries/{id}",
     *     tags={"ContentGalleries"},
     *     summary="Update Content Gallery by ID",
     *     description="Update Content Gallery by ID",
     *     operationId="updateContentGalleries",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\RequestBody(
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="title", type="string", example="Roman Beer"),
     *              @OA\Property(property="slug", type="string", example="roman-beer"),
     *              @OA\Property(property="short_content", type="string", example="Short Content Gallery"),
     *              @OA\Property(property="is_draft", type="boolean", example=true),
     *              @OA\Property(property="is_published", type="boolean", example=false),
     *              @OA\Property(property="content_gallery_categories", type="array",
     *                  @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="content_category_id" ,type="string", example="98a1ba2d-66ec-4a92-943e-9c61829a4c58")
     *                  )
     *              ),
     *              @OA\Property(property="content_gallery_tags", type="array",
     *                  @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="content_tag_id" ,type="string", example="98a1ba2d-66ec-4a92-943e-9c61829a4c58")
     *                  )
     *              ),
     *              @OA\Property(property="content_gallery_images", type="array",
     *                  @OA\Items(
     *                      type="object",
     *                      @OA\Property(property="content_media_file_id" ,type="string", example="98a1ba2d-66ec-4a92-943e-9c61829a4c58")
     *                  )
     *              ),
     *          ),
     *      ),
     *     @OA\Response(response=200, description="Content Gallery Updated Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function update(ContentGalleryRequest $request, $id): JsonResponse
    {
        try {
            $data = $this->contentContentGalleryServices->update($id, $request->all());
            if (is_null($data))
                return $this->responseError(null, 'Content Gallery Not Found', Response::HTTP_NOT_FOUND);

            return $this->responseSuccess($data, 'Content Gallery Updated Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\DELETE(
     *     path="/api/content-galleries/{id}",
     *     tags={"ContentGalleries"},
     *     summary="Delete Content Gallery by ID",
     *     description="Delete Content Gallery by ID",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Content Gallery Deleted Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function destroy($id): JsonResponse
    {
        try {
            $this->contentContentGalleryServices->delete($id);
            return $this->responseSuccess(null, 'Content Gallery Deleted Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
