<?php

namespace App\Content\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

use App\Base\Controllers\Controller;
use App\Base\Traits\ResponseTrait;

use App\Content\Requests\ContentCategoryRequest;
use App\Content\Services\ContentCategoryServices;

use Yajra\DataTables\DataTables;

class ContentCategoryController extends Controller
{
    /**
     * Response trait to handle return responses.
     */
    use ResponseTrait;

    /**
     * ContentCategory Repository class.
     *
     * @var ContentCategoryServices
     */
    public $contentCategoryServices;

    public function __construct(ContentCategoryServices $contentCategoryServices)
    {
        // $this->middleware('auth:api', ['except' => ['indexAll']]);
        $this->contentCategoryServices = $contentCategoryServices;
    }

    /**
     * @OA\GET(
     *     path="/api/content-categories",
     *     tags={"Content Categories"},
     *     summary="Get Content Category List",
     *     description="Get Content Category List as Array",
     *     operationId="indexCategory",
     *     security={{"bearer":{}}},
     *     @OA\Response(response=200,description="Get Content Category List as Array"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function index(): JsonResponse
    {
        try {
            $data = $this->contentCategoryServices->getAll();
            return $this->responseSuccess($data, 'Content Category List Fetch Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/content-categories/view/all",
     *     tags={"Content Categories"},
     *     summary="All Content Categories - Publicly Accessible",
     *     description="All Content Categories - Publicly Accessible",
     *     @OA\Parameter(name="perPage", description="perPage, eg; 20", example=20, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="page", description="page, eg; 1", example=1, in="query", @OA\Schema(type="integer")),
     *     @OA\Response(response=200, description="All Content Categories - Publicly Accessible" ),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function indexAll(Request $request): JsonResponse
    {
        try {
            $perPage = isset($request->perPage) ? intval($request->perPage) : 5;
            $page = isset($request->page) ? intval($request->page) : 1;
            $data = $this->contentCategoryServices->getPaginatedData($perPage, $page);
            return $this->responseSuccess($data, 'Content Categories List Fetched Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content-categories/view/datatables",
     *     tags={"Content Categories"},
     *     summary="Get Content Category List for DataTables",
     *     description="Get Content Category List for DataTables",
     *     operationId="datatablesCategory",
     *     security={{"bearer":{}}},
     *     @OA\Response(response=200,description="Get Content Category List for DataTables"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function datatables(Request $request): JsonResponse
    {
        try {
            $data = $this->contentCategoryServices->getDataTables();

            return DataTables::of($data)
                ->addColumn('action', function ($row) {
                    $editUrl = '/admin/content-categories/manage/' . $row->id;
                    $deleteUrl = route('content-categories.destroy', $row->id);
                    $csrfToken = csrf_token();

                    $editButton = "<a href='{$editUrl}' class='btn btn-primary btn-sm mr-1'>
                        <i class='fas fa-edit'></i>
                    </a>";

                    $deleteButton = "<button type='button' class='btn btn-danger btn-sm btn-delete' data-id='{$row->id}'\">
                        <i class='fas fa-trash'></i>
                    </button>";

                    return "{$editButton}$deleteButton";
                })
                ->make(true);
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/content-categories/view/search",
     *     tags={"Content Categories"},
     *     summary="Search Content Categories",
     *     description="Search Content Categories",
     *     operationId="searchCategory",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="perPage", description="perPage, eg; 20", example=20, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="page", description="page, eg; 1", example=1, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="search", description="search, eg; Test", example="Test", in="query", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Search Content Categories as Array"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function search(Request $request): JsonResponse
    {
        try {
            $perPage = isset($request->perPage) ? intval($request->perPage) : 5;
            $page = isset($request->page) ? intval($request->page) : 1;
            $data = $this->contentCategoryServices->searchContentCategory($request->search, $perPage, $page);
            return $this->responseSuccess($data, 'Content Categories Search Results Found !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content-categories",
     *     tags={"Content Categories"},
     *     summary="Create New Content Category",
     *     description="Create New Content Category",
     *     operationId="storeCategory",
     *     @OA\RequestBody(
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="title", type="string", example="Roman Beer"),
     *          ),
     *      ),
     *     @OA\Response(response=201, description="Content Category Created Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function store(ContentCategoryRequest $request): JsonResponse
    {
        try {
            $data = $this->contentCategoryServices->create($request->validated());
            return $this->responseSuccess($data, 'Content Category Created Successfully !', Response::HTTP_CREATED);
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/content-categories/{id}",
     *     tags={"Content Categories"},
     *     summary="Show Content Category by ID",
     *     description="Show Content Category by ID",
     *     operationId="searchCategorys",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Show Content Category by ID"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function show($id): JsonResponse
    {
        try {
            $data = $this->contentCategoryServices->read($id);
            if (is_null($data)) {
                return $this->responseError(null, 'Content Category Not Found', Response::HTTP_NOT_FOUND);
            }

            return $this->responseSuccess($data, 'Content Category Details Fetch Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content-categories/{id}",
     *     tags={"Content Categories"},
     *     summary="Update Content Category by ID",
     *     description="Update Content Category by ID",
     *     operationId="updateCategorys",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\RequestBody(
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="title", type="string", example="Roman Beer"),
     *          ),
     *      ),
     *     @OA\Response(response=200, description="Content Category Updated Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function update(ContentCategoryRequest $request, $id): JsonResponse
    {
        try {
            $data = $this->contentCategoryServices->update($id, $request->all());
            if (is_null($data))
                return $this->responseError(null, 'Content Category Not Found', Response::HTTP_NOT_FOUND);

            return $this->responseSuccess($data, 'Content Category Updated Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\DELETE(
     *     path="/api/content-categories/{id}",
     *     tags={"Content Categories"},
     *     summary="Delete Content Category by ID",
     *     description="Delete Content Category by ID",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Content Category Deleted Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function destroy($id): JsonResponse
    {
        try {
            $this->contentCategoryServices->delete($id);
            return $this->responseSuccess(null, 'Content Category Deleted Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
