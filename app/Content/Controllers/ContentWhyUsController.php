<?php

namespace App\Content\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

use App\Base\Controllers\Controller;
use App\Base\Traits\ResponseTrait;

use App\Content\Requests\ContentWhyUsRequest;
use App\Content\Services\ContentWhyUsServices;

use Yajra\DataTables\DataTables;

class ContentWhyUsController extends Controller
{
    /**
     * Response trait to handle return responses.
     */
    use ResponseTrait;

    /**
     * ContentWhyUs Repository class.
     *
     * @var ContentWhyUsServices
     */
    public $contentContentWhyUsServices;

    public function __construct(ContentWhyUsServices $contentContentWhyUsServices)
    {
        // $this->middleware('auth:api', ['except' => ['indexAll']]);
        $this->contentContentWhyUsServices = $contentContentWhyUsServices;
    }

    /**
     * @OA\GET(
     *     path="/api/content-whyus",
     *     tags={"ContentWhyUs"},
     *     summary="Get Content ContentWhyUs List",
     *     description="Get Content ContentWhyUs List as Array",
     *     operationId="indexContentWhyUs",
     *     security={{"bearer":{}}},
     *     @OA\Response(response=200,description="Get Content ContentWhyUs List as Array"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function index(): JsonResponse
    {
        try {
            $data = $this->contentContentWhyUsServices->getAll();
            return $this->responseSuccess($data, 'Content ContentWhyUs List Fetch Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/content-whyus/view/all",
     *     tags={"ContentWhyUs"},
     *     summary="All ContentWhyUs - Publicly Accessible",
     *     description="All ContentWhyUs - Publicly Accessible",
     *     @OA\Parameter(name="perPage", description="perPage, eg; 20", example=20, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="page", description="page, eg; 1", example=1, in="query", @OA\Schema(type="integer")),
     *     @OA\Response(response=200, description="All ContentWhyUs - Publicly Accessible" ),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function indexAll(Request $request): JsonResponse
    {
        try {
            $perPage = isset($request->perPage) ? intval($request->perPage) : 5;
            $page = isset($request->page) ? intval($request->page) : 1;
            $data = $this->contentContentWhyUsServices->getPaginatedData($perPage, $page);
            return $this->responseSuccess($data, 'ContentWhyUs List Fetched Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content-whyus/view/datatables",
     *     tags={"ContentWhyUs"},
     *     summary="Get Content ContentWhyUs List for DataTables",
     *     description="Get Content ContentWhyUs List for DataTables",
     *     operationId="datatablesContentWhyUs",
     *     security={{"bearer":{}}},
     *     @OA\Response(response=200,description="Get Content ContentWhyUs List for DataTables"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function datatables(Request $request): JsonResponse
    {
        try {
            $data = $this->contentContentWhyUsServices->getDataTables();

            return DataTables::of($data)
                ->addColumn('action', function ($row) {
                    $editUrl = '/admin/content-whyus/manage/' . $row->id;
                    $deleteUrl = route('content-whyus.destroy', $row->id);
                    $csrfToken = csrf_token();

                    $editButton = "<a href='{$editUrl}' class='btn btn-primary btn-sm mr-1'>
                        <i class='fas fa-edit'></i>
                    </a>";

                    $deleteButton = "<button type='button' class='btn btn-danger btn-sm btn-delete' data-id='{$row->id}'\">
                        <i class='fas fa-trash'></i>
                    </button>";

                    return "{$editButton}$deleteButton";
                })
                ->make(true);
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/content-whyus/view/search",
     *     tags={"ContentWhyUs"},
     *     summary="Search ContentWhyUs",
     *     description="Search ContentWhyUs",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="perPage", description="perPage, eg; 20", example=20, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="page", description="page, eg; 1", example=1, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="search", description="search, eg; Test", example="Test", in="query", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Search ContentWhyUs as Array"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function search(Request $request): JsonResponse
    {
        try {
            $perPage = isset($request->perPage) ? intval($request->perPage) : 5;
            $page = isset($request->page) ? intval($request->page) : 1;
            $data = $this->contentContentWhyUsServices->searchContentWhyUs($request->search, $perPage, $page);
            return $this->responseSuccess($data, 'ContentWhyUs Search Results Found !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content-whyus",
     *     tags={"ContentWhyUs"},
     *     summary="Create New Content ContentWhyUs",
     *     description="Create New Content ContentWhyUs",
     *     operationId="storeContentWhyUs",
     *     @OA\RequestBody(
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="name_reviewer", type="string", example="Roman Beer"),
     *              @OA\Property(property="title", type="string", example="Roman Beer"),
     *              @OA\Property(property="short_content", type="string", example="Short Content"),
     *              @OA\Property(property="stars", type="int", example="1"),
     *              @OA\Property(property="job", type="string", example="Long Content ContentWhyUs"),
     *              @OA\Property(property="avatar_image", example="98a1ba2d-66ec-4a92-943e-9c61829a4c58", type="string"),
     *          ),
     *      ),
     *     @OA\Response(response=201, description="Content ContentWhyUs Created Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function store(ContentWhyUsRequest $request): JsonResponse
    {
        try {
            $data = $this->contentContentWhyUsServices->create($request->validated());
            return $this->responseSuccess($data, 'Content ContentWhyUs Created Successfully !', Response::HTTP_CREATED);
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/content-whyus/{id}",
     *     tags={"ContentWhyUs"},
     *     summary="Show Content ContentWhyUs by ID",
     *     description="Show Content ContentWhyUs by ID",
     *     operationId="searchContentWhyUs",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Show Content ContentWhyUs by ID"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function show($id): JsonResponse
    {
        try {
            $data = $this->contentContentWhyUsServices->read($id);
            if (is_null($data)) {
                return $this->responseError(null, 'Content ContentWhyUs Not Found', Response::HTTP_NOT_FOUND);
            }

            return $this->responseSuccess($data, 'Content ContentWhyUs Details Fetch Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content-whyus/{id}",
     *     tags={"ContentWhyUs"},
     *     summary="Update Content ContentWhyUs by ID",
     *     description="Update Content ContentWhyUs by ID",
     *     operationId="updateContentWhyUs",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\RequestBody(
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="name_reviewer", type="string", example="Roman Beer"),
     *              @OA\Property(property="title", type="string", example="Roman Beer"),
     *              @OA\Property(property="short_content", type="string", example="Short Content"),
     *              @OA\Property(property="stars", type="int", example="1"),
     *              @OA\Property(property="job", type="string", example="Long Content ContentWhyUs"),
     *              @OA\Property(property="avatar_image", example="98a1ba2d-66ec-4a92-943e-9c61829a4c58", type="string"),
     *          ),
     *      ),
     *     @OA\Response(response=200, description="Content ContentWhyUs Updated Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function update(ContentWhyUsRequest $request, $id): JsonResponse
    {
        try {
            $data = $this->contentContentWhyUsServices->update($id, $request->all());
            if (is_null($data))
                return $this->responseError(null, 'Content ContentWhyUs Not Found', Response::HTTP_NOT_FOUND);

            return $this->responseSuccess($data, 'Content ContentWhyUs Updated Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\DELETE(
     *     path="/api/content-whyus/{id}",
     *     tags={"ContentWhyUs"},
     *     summary="Delete Content ContentWhyUs by ID",
     *     description="Delete Content ContentWhyUs by ID",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Content ContentWhyUs Deleted Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function destroy($id): JsonResponse
    {
        try {
            $this->contentContentWhyUsServices->delete($id);
            return $this->responseSuccess(null, 'Content ContentWhyUs Deleted Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
