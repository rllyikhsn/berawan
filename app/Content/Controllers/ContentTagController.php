<?php

namespace App\Content\Controllers;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Http\Request;

use App\Base\Controllers\Controller;
use App\Base\Traits\ResponseTrait;

use App\Content\Requests\ContentTagRequest;
use App\Content\Services\ContentTagServices;

use Yajra\DataTables\DataTables;

class ContentTagController extends Controller
{
    /**
     * Response trait to handle return responses.
     */
    use ResponseTrait;

    /**
     * ContentTag Repository class.
     *
     * @var ContentTagServices
     */
    public $contentTagServices;

    public function __construct(ContentTagServices $contentTagServices)
    {
        // $this->middleware('auth:api', ['except' => ['indexAll']]);
        $this->contentTagServices = $contentTagServices;
    }

    /**
     * @OA\GET(
     *     path="/api/content-tags",
     *     tags={"Content Tags"},
     *     summary="Get Content Tag List",
     *     description="Get Content Tag List as Array",
     *     operationId="indexTag",
     *     security={{"bearer":{}}},
     *     @OA\Response(response=200,description="Get Content Tag List as Array"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function index(): JsonResponse
    {
        try {
            $data = $this->contentTagServices->getAll();
            return $this->responseSuccess($data, 'Content Tag List Fetch Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/content-tags/view/all",
     *     tags={"Content Tags"},
     *     summary="All Content Tags - Publicly Accessible",
     *     description="All Content Tags - Publicly Accessible",
     *     @OA\Parameter(name="perPage", description="perPage, eg; 20", example=20, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="page", description="page, eg; 1", example=1, in="query", @OA\Schema(type="integer")),
     *     @OA\Response(response=200, description="All Content Tags - Publicly Accessible" ),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function indexAll(Request $request): JsonResponse
    {
        try {
            $perPage = isset($request->perPage) ? intval($request->perPage) : 5;
            $page = isset($request->page) ? intval($request->page) : 1;
            $data = $this->contentTagServices->getPaginatedData($perPage, $page);
            return $this->responseSuccess($data, 'Content Tags List Fetched Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content-tags/view/datatables",
     *     tags={"Content Tags"},
     *     summary="Get Content Tag List for DataTables",
     *     description="Get Content Tag List for DataTables",
     *     operationId="datatablesTag",
     *     security={{"bearer":{}}},
     *     @OA\Response(response=200,description="Get Content Tag List for DataTables"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function datatables(Request $request): JsonResponse
    {
        try {
            $data = $this->contentTagServices->getDataTables();

            return DataTables::of($data)
                ->addColumn('action', function ($row) {
                    $editUrl = '/admin/content-tags/manage/' . $row->id;
                    $deleteUrl = route('content-tags.destroy', $row->id);
                    $csrfToken = csrf_token();

                    $editButton = "<a href='{$editUrl}' class='btn btn-primary btn-sm mr-1'>
                        <i class='fas fa-edit'></i>
                    </a>";

                    $deleteButton = "<button type='button' class='btn btn-danger btn-sm btn-delete' data-id='{$row->id}'\">
                        <i class='fas fa-trash'></i>
                    </button>";

                    return "{$editButton}$deleteButton";
                })
                ->make(true);
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/content-tags/view/search",
     *     tags={"Content Tags"},
     *     summary="Search Content Tags",
     *     description="Search Content Tags",
     *     operationId="searchTag",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="perPage", description="perPage, eg; 20", example=20, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="page", description="page, eg; 1", example=1, in="query", @OA\Schema(type="integer")),
     *     @OA\Parameter(name="search", description="search, eg; Test", example="Test", in="query", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Search Content Tags as Array"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function search(Request $request): JsonResponse
    {
        try {
            $perPage = isset($request->perPage) ? intval($request->perPage) : 5;
            $page = isset($request->page) ? intval($request->page) : 1;
            $data = $this->contentTagServices->searchContentTag($request->search, $perPage, $page);
            return $this->responseSuccess($data, 'Content Tags Search Results Found !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content-tags",
     *     tags={"Content Tags"},
     *     summary="Create New Content Tag",
     *     description="Create New Content Tag",
     *     operationId="storeTag",
     *     @OA\RequestBody(
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="title", type="string", example="Roman Beer"),
     *          ),
     *     ),
     *     @OA\Response(response=201, description="Content Tag Created Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function store(ContentTagRequest $request): JsonResponse
    {
        try {
            $data = $this->contentTagServices->create($request->validated());
            return $this->responseSuccess($data, 'Content Tag Created Successfully !', Response::HTTP_CREATED);
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\GET(
     *     path="/api/content-tags/{id}",
     *     tags={"Content Tags"},
     *     summary="Show Content Tag by ID",
     *     description="Show Content Tag by ID",
     *     operationId="searchTags",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Show Content Tag by ID"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function show($id): JsonResponse
    {
        try {
            $data = $this->contentTagServices->read($id);
            if (is_null($data)) {
                return $this->responseError(null, 'Content Tag Not Found', Response::HTTP_NOT_FOUND);
            }

            return $this->responseSuccess($data, 'Content Tag Details Fetch Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\POST(
     *     path="/api/content-tags/{id}",
     *     tags={"Content Tags"},
     *     summary="Update Content Tag by ID",
     *     description="Update Content Tag by ID",
     *     operationId="updateTags",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\RequestBody(
     *          @OA\JsonContent(
     *              type="object",
     *              @OA\Property(property="title", type="string", example="Roman Beer"),
     *          ),
     *     ),
     *     @OA\Response(response=200, description="Content Tag Updated Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function update(ContentTagRequest $request, $id): JsonResponse
    {
        try {
            $data = $this->contentTagServices->update($id, $request->all());
            if (is_null($data))
                return $this->responseError(null, 'Content Tag Not Found', Response::HTTP_NOT_FOUND);

            return $this->responseSuccess($data, 'Content Tag Updated Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * @OA\DELETE(
     *     path="/api/content-tags/{id}",
     *     tags={"Content Tags"},
     *     summary="Delete Content Tag by ID",
     *     description="Delete Content Tag by ID",
     *     security={{"bearer":{}}},
     *     @OA\Parameter(name="id", description="id, eg; 98a1ba2d-66ec-4a92-943e-9c61829a4c58", required=true, in="path", @OA\Schema(type="string")),
     *     @OA\Response(response=200, description="Content Tag Deleted Successfully !"),
     *     @OA\Response(response=400, description="Bad request"),
     *     @OA\Response(response=404, description="Resource Not Found"),
     * )
     */
    public function destroy($id): JsonResponse
    {
        try {
            $this->contentTagServices->delete($id);
            return $this->responseSuccess(null, 'Content Tag Deleted Successfully !');
        } catch (\Exception $e) {
            return $this->responseError(null, $e->getMessage(), Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
