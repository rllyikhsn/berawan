<?php

namespace App\Content\Repositories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

use App\Base\Helpers\UploadHelper;
use App\Base\Interfaces\CrudInterface;
use App\User\Models\User;

use App\Content\Models\ContentWhyUs;
use App\Content\Models\ContentMediaFile;

use App\Content\Repositories\ContentMediaFileRepository;
use Carbon\Carbon;

class ContentWhyUsRepository implements CrudInterface
{
    /**
     * Authenticated User Instance.
     *
     * @var User
     */
    public User|null $user;

    /**
     * Content Media File Repository class.
     *
     * @var ContentMediaFileRepository
     */
    public $contentMediaFileRepository;

    /**
     * Constructor.
     */
    public function __construct(ContentMediaFileRepository $contentMediaFileRepository)
    {
        $this->user = Auth::guard()->user();
        $this->contentMediaFileRepository = $contentMediaFileRepository;
    }

    /**
     * Get All Content ContentWhyUs.
     *
     * @return Paginator Array of ContentWhyUs Collection
     */
    public function getAll(): Paginator
    {
        return ContentWhyUs::orderBy('created_at', 'desc')
            ->paginate(10);
    }

    /**
     * Get Content ContentWhyUs Datatables.
     *
     * @return collections Array of Datatables ContentWhyUs Collection
     */
    public function getDataTables()
    {
        return ContentWhyUs::select('id', 'created_at', 'name_reviewer', 'title', 'short_content', 'stars', 'job', 'avatar_image')
            ->orderBy('created_at', 'desc')->get();
    }

    /**
     * Get Paginated Content ContentWhyUs Data.
     *
     * @param int $perPage
     * @param int $page
     * @return Paginator Array of ContentWhyUs Collection
     */
    public function getPaginatedData($perPage, $page): Paginator
    {
        $perPage = isset($perPage) ? intval($perPage) : 5;
        $page = isset($page) ? intval($page) : 1;
        return ContentWhyUs::orderBy('id', 'desc')
            ->paginate($perPage, ['*'], 'page', $page)
            ->appends(['perPage' => $perPage]);
    }

    /**
     * Get Searchable Content ContentWhyUs Data with Pagination.
     *
     * @param string $keyword
     * @param int $perPage
     * @param int $page
     * @return Paginator Array of ContentWhyUs Collection
     */
    public function searchContentWhyUss($keyword, $perPage, $page): Paginator
    {
        $perPage = isset($perPage) ? intval($perPage) : 10;
        $page = isset($page) ? intval($page) : 1;
        return ContentWhyUs::where('title', 'like', '%' . $keyword . '%')
            ->orderBy('created_at', 'desc')
            ->paginate($perPage, ['*'], 'page', $page);
    }

    /**
     * Create New Content ContentWhyUs.
     *
     * @param array $data
     * @return ContentWhyUs
     */
    public function create(array $data): ContentWhyUs
    {
        if (!empty($data['avatar_image'])) {
            $image = $this->contentMediaFileRepository->getByID($data['avatar_image']);
            if ($image) {
                $srcImage = 'images/media-files/' . $image->file;
                $fileImage = new File($srcImage);
                if ($fileImage) {
                    $originalImage = file_get_contents($fileImage);
                    file_put_contents('images/whyus/' . $image->file, $originalImage);
                    $data['avatar_image'] = $image->file;
                }
            } else {
                $data['avatar_image'] = null;
            }
        }

        $contentWhyUs = ContentWhyUs::create($data);

        return $contentWhyUs;
    }

    /**
     * Delete Content ContentWhyUs.
     *
     * @param string $id
     * @return boolean true if deleted otherwise false
     */
    public function delete(string $id): bool
    {
        $contentWhyUs = ContentWhyUs::find($id);

        if (empty($contentWhyUs)) {
            return false;
        }

        $contentWhyUs->delete($contentWhyUs);
        return true;
    }

    /**
     * Get Content ContentWhyUs Detail By ID.
     *
     * @param string $id
     * @return ContentWhyUs|null
     */
    public function getByID(string $id): ContentWhyUs|null
    {
        $contentWhyUs = ContentWhyUs::find($id);
        if (is_null($contentWhyUs)) {
            return null;
        }

        return $contentWhyUs;
    }

    /**
     * Update Content ContentWhyUs By ID.
     *
     * @param string $id
     * @param array $data
     * @return ContentWhyUs|null
     */
    public function update(string $id, array $data): ContentWhyUs|null
    {
        $contentWhyUs = ContentWhyUs::find($id);

        if (is_null($contentWhyUs)) {
            return null;
        }
        if (!empty($data['avatar_image'])) {
            $image = $this->contentMediaFileRepository->getByID($data['avatar_image']);
            if ($image) {
                $srcImage = 'images/media-files/' . $image->file;
                $fileImage = new File($srcImage);
                if ($fileImage) {
                    $originalImage = file_get_contents($fileImage);
                    file_put_contents('images/whyus/' . $image->file, $originalImage);
                    $data['avatar_image'] = $image->file;
                }
            } else {
                $data['avatar_image'] = null;
            }
        }

        // If everything is OK, then update.
        $contentWhyUs->update($data);

        // Finally return the updated user.
        return $this->getByID($contentWhyUs->id);
    }
}
