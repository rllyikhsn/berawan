<?php

namespace App\Content\Repositories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

use App\Base\Helpers\UploadHelper;
use App\Base\Interfaces\CrudInterface;
use App\User\Models\User;

use App\Content\Models\ContentReviewClient;
use App\Content\Models\ContentMediaFile;

use App\Content\Repositories\ContentMediaFileRepository;
use Carbon\Carbon;

class ContentReviewClientRepository implements CrudInterface
{
    /**
     * Authenticated User Instance.
     *
     * @var User
     */
    public User|null $user;

    /**
     * Content Media File Repository class.
     *
     * @var ContentMediaFileRepository
     */
    public $contentMediaFileRepository;

    /**
     * Constructor.
     */
    public function __construct(ContentMediaFileRepository $contentMediaFileRepository)
    {
        $this->user = Auth::guard()->user();
        $this->contentMediaFileRepository = $contentMediaFileRepository;
    }

    /**
     * Get All Content ContentReviewClient.
     *
     * @return Paginator Array of ContentReviewClient Collection
     */
    public function getAll(): Paginator
    {
        return ContentReviewClient::orderBy('created_at', 'desc')
            ->paginate(10);
    }

    /**
     * Get Content ContentReviewClient Datatables.
     *
     * @return collections Array of Datatables ContentReviewClient Collection
     */
    public function getDataTables()
    {
        return ContentReviewClient::select('id', 'created_at', 'title', 'short_content', 'stars', 'job', 'avatar_image', 'is_spam')
            ->orderBy('created_at', 'desc')->get();
    }

    /**
     * Get Paginated Content ContentReviewClient Data.
     *
     * @param int $perPage
     * @param int $page
     * @return Paginator Array of ContentReviewClient Collection
     */
    public function getPaginatedData($perPage, $page): Paginator
    {
        $perPage = isset($perPage) ? intval($perPage) : 5;
        $page = isset($page) ? intval($page) : 1;
        return ContentReviewClient::orderBy('id', 'desc')
            ->paginate($perPage, ['*'], 'page', $page)
            ->appends(['perPage' => $perPage]);
    }

    /**
     * Get Searchable Content ContentReviewClient Data with Pagination.
     *
     * @param string $keyword
     * @param int $perPage
     * @param int $page
     * @return Paginator Array of ContentReviewClient Collection
     */
    public function searchContentReviewClients($keyword, $perPage, $page): Paginator
    {
        $perPage = isset($perPage) ? intval($perPage) : 10;
        $page = isset($page) ? intval($page) : 1;
        return ContentReviewClient::where('title', 'like', '%' . $keyword . '%')
            ->orderBy('created_at', 'desc')
            ->paginate($perPage, ['*'], 'page', $page);
    }

    /**
     * Create New Content ContentReviewClient.
     *
     * @param array $data
     * @return ContentReviewClient
     */
    public function create(array $data): ContentReviewClient
    {
        if (!empty($data['avatar_image'])) {
            $image = $this->contentMediaFileRepository->getByID($data['avatar_image']);
            if ($image) {
                $srcImage = 'images/media-files/' . $image->file;
                $fileImage = new File($srcImage);
                if ($fileImage) {
                    $originalImage = file_get_contents($fileImage);
                    file_put_contents('images/review-clients/' . $image->file, $originalImage);
                    $data['avatar_image'] = $image->file;
                }
            } else {
                $data['avatar_image'] = null;
            }
        }

        $contentReviewClient = ContentReviewClient::create($data);

        return $contentReviewClient;
    }

    /**
     * Delete Content ContentReviewClient.
     *
     * @param string $id
     * @return boolean true if deleted otherwise false
     */
    public function delete(string $id): bool
    {
        $contentReviewClient = ContentReviewClient::find($id);

        if (empty($contentReviewClient)) {
            return false;
        }

        $contentReviewClient->delete($contentReviewClient);
        return true;
    }

    /**
     * Get Content ContentReviewClient Detail By ID.
     *
     * @param string $id
     * @return ContentReviewClient|null
     */
    public function getByID(string $id): ContentReviewClient|null
    {
        $contentReviewClient = ContentReviewClient::find($id);
        if (is_null($contentReviewClient)) {
            return null;
        }

        return $contentReviewClient;
    }

    /**
     * Update Content ContentReviewClient By ID.
     *
     * @param string $id
     * @param array $data
     * @return ContentReviewClient|null
     */
    public function update(string $id, array $data): ContentReviewClient|null
    {
        $contentReviewClient = ContentReviewClient::find($id);

        if (is_null($contentReviewClient)) {
            return null;
        }
        if (!empty($data['avatar_image'])) {
            $image = $this->contentMediaFileRepository->getByID($data['avatar_image']);
            if ($image) {
                $srcImage = 'images/media-files/' . $image->file;
                $fileImage = new File($srcImage);
                if ($fileImage) {
                    $originalImage = file_get_contents($fileImage);
                    file_put_contents('images/review-clients/' . $image->file, $originalImage);
                    $data['avatar_image'] = $image->file;
                }
            } else {
                $data['avatar_image'] = null;
            }
        }

        // If everything is OK, then update.
        $contentReviewClient->update($data);

        // Finally return the updated user.
        return $this->getByID($contentReviewClient->id);
    }
}
