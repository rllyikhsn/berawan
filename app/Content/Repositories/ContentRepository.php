<?php

namespace App\Content\Repositories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use App\Base\Helpers\UploadHelper;
use App\Base\Interfaces\CrudInterface;
use App\Content\Models\Content;
use App\User\Models\User;
use App\Content\Repositories\ContentMediaFileRepository;


use Illuminate\Http\File;


class ContentRepository implements CrudInterface
{
    /**
     * Authenticated User Instance.
     *
     * @var User
     */
    public User|null $user;

    /**
     * Content Media File Repository class.
     *
     * @var ContentMediaFileRepository
     */
    public $contentMediaFileRepository;

    /**
     * Constructor.
     */
    public function __construct(ContentMediaFileRepository $contentMediaFileRepository)
    {
        $this->user = Auth::guard()->user();
        $this->contentMediaFileRepository = $contentMediaFileRepository;
    }

    /**
     * Get All Content.
     *
     * @return Paginator Array of Content Collection
     */
    public function getAll(): Paginator
    {
        return Content::orderBy('created_at', 'desc')
            ->paginate(10);
    }

    /**
     * Get Content Datatables.
     *
     * @return collections Array of Datatables Content Collection
     */
    public function getDataTables()
    {
        return Content::select('id', 'created_at')
            ->orderBy('created_at', 'desc')->get();
    }

    /**
     * Get Paginated Content Data.
     *
     * @param int $perPage
     * @return Paginator Array of Content Collection
     */
    public function getPaginatedData($perPage, $page): Paginator
    {
        $perPage = isset($perPage) ? intval($perPage) : 5;
        $page = isset($page) ? intval($page) : 1;
        return Content::orderBy('id', 'desc')
            ->paginate($perPage, ['*'], 'page', $page)
            ->appends(['perPage' => $perPage]);
    }

    /**
     * Get Searchable Content Data with Pagination.
     *
     * @param string $keyword
     * @param int $perPage
     * @return Paginator Array of Content Collection
     */
    public function searchContents($keyword, $perPage): Paginator
    {
        $perPage = isset($perPage) ? intval($perPage) : 10;
        return Content::where('title', 'like', '%' . $keyword . '%')
            ->orWhere('short_content', 'like', '%' . $keyword . '%')
            ->orderBy('created_at', 'desc')
            ->paginate($perPage);
    }

    /**
     * Create New Content .
     *
     * @param array $data
     * @return Content
     */
    public function create(array $data): Content
    {
        return Content::create($data);
    }

    /**
     * Delete Content .
     *
     * @param string $id
     * @return boolean true if deleted otherwise false
     */
    public function delete(string $id): bool
    {
        $content = Content::find($id);
        if (empty($content)) {
            return false;
        }

        $content->delete($content);
        return true;
    }

    /**
     * Get Content Detail By ID.
     *
     * @param string $id
     * @return Content|null
     */
    public function getByID(string $id): Content|null
    {
        return Content::find($id);
    }

    /**
     * Update Content By ID.
     *
     * @param string $id
     * @param array $data
     * @return Content|null
     */
    public function update(string $id, array $data): Content|null
    {
        $content = Content::find($id);

        if (is_null($content)) {
            return null;
        }

        // If everything is OK, then update.
        $content->update($data);

        // Finally return the updated user.
        return $this->getByID($content->id);
    }

    /**
     * Update Content By ID.
     *
     * @param string $id
     * @param array $data
     * @return Content|null
     */
    public function updateLogoIdentity(string $id, array $data): Content|null
    {
        $content = Content::find($id);

        if (is_null($content)) {
            return null;
        }

        if (!empty($data['favicon_image'])) {
            if ($data['favicon_image'] != $content->favicon_image) {
                UploadHelper::deleteFile('images/web/' . $content->favicon_image);
                $image = $this->contentMediaFileRepository->getByID($data['favicon_image']);
                if ($image) {
                    $srcImage = 'images/media-files/' . $image->file;
                    $fileImage = new File($srcImage);
                    if ($fileImage) {
                        $originalImage = file_get_contents($fileImage);
                        file_put_contents('images/web/' . $image->file, $originalImage);
                        $data['favicon_image'] = $image->file;
                    }
                }
            }
        } else {
            $data['favicon_image'] = $content->favicon_image;
        }

        if (!empty($data['logo_image'])) {
            if ($data['logo_image'] != $content->logo_image) {
                UploadHelper::deleteFile('images/web/' . $content->logo_image);
                $image = $this->contentMediaFileRepository->getByID($data['logo_image']);
                if ($image) {
                    $srcImage = 'images/media-files/' . $image->file;
                    $fileImage = new File($srcImage);
                    if ($fileImage) {
                        $originalImage = file_get_contents($fileImage);
                        file_put_contents('images/web/' . $image->file, $originalImage);
                        $data['logo_image'] = $image->file;
                    }
                }
            }
        } else {
            $data['logo_image'] = $content->logo_image;
        }

        if (!empty($data['logo_front_image'])) {
            if ($data['logo_front_image'] != $content->logo_front_image) {
                UploadHelper::deleteFile('images/web/' . $content->logo_front_image);
                $image = $this->contentMediaFileRepository->getByID($data['logo_front_image']);
                if ($image) {
                    $srcImage = 'images/media-files/' . $image->file;
                    $fileImage = new File($srcImage);
                    if ($fileImage) {
                        $originalImage = file_get_contents($fileImage);
                        file_put_contents('images/web/' . $image->file, $originalImage);
                        $data['logo_front_image'] = $image->file;
                    }
                }
            }
        } else {
            $data['logo_front_image'] = $content->logo_front_image;
        }

        if (!empty($data['about_image'])) {
            if ($data['about_image'] != $content->about_image) {
                UploadHelper::deleteFile('images/web/' . $content->about_image);
                $image = $this->contentMediaFileRepository->getByID($data['about_image']);
                if ($image) {
                    $srcImage = 'images/media-files/' . $image->file;
                    $fileImage = new File($srcImage);
                    if ($fileImage) {
                        $originalImage = file_get_contents($fileImage);
                        file_put_contents('images/web/' . $image->file, $originalImage);
                        $data['about_image'] = $image->file;
                    }
                }
            }
        } else {
            $data['about_image'] = $content->about_image;
        }

        if (!empty($data['footer_image'])) {
            if ($data['footer_image'] != $content->footer_image) {
                UploadHelper::deleteFile('images/web/' . $content->footer_image);
                $image = $this->contentMediaFileRepository->getByID($data['footer_image']);
                if ($image) {
                    $srcImage = 'images/media-files/' . $image->file;
                    $fileImage = new File($srcImage);
                    if ($fileImage) {
                        $originalImage = file_get_contents($fileImage);
                        file_put_contents('images/web/' . $image->file, $originalImage);
                        $data['footer_image'] = $image->file;
                    }
                }
            }
        } else {
            $data['footer_image'] = $content->footer_image;
        }

        if (!empty($data['service_image'])) {
            if ($data['service_image'] != $content->service_image) {
                UploadHelper::deleteFile('images/web/' . $content->service_image);
                $image = $this->contentMediaFileRepository->getByID($data['service_image']);
                if ($image) {
                    $srcImage = 'images/media-files/' . $image->file;
                    $fileImage = new File($srcImage);
                    if ($fileImage) {
                        $originalImage = file_get_contents($fileImage);
                        file_put_contents('images/web/' . $image->file, $originalImage);
                        $data['service_image'] = $image->file;
                    }
                }
            }
        } else {
            $data['service_image'] = $content->service_image;
        }

        // If everything is OK, then update.
        $content->update($data);

        // Finally return the updated user.
        return $this->getByID($content->id);
    }
}
