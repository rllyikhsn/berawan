<?php

namespace App\Content\Repositories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

use App\Base\Helpers\UploadHelper;
use App\Base\Interfaces\CrudInterface;
use App\User\Models\User;

use App\Content\Models\Article;
use App\Content\Models\ArticleCategory;
use App\Content\Models\ArticleImage;
use App\Content\Models\ArticleTag;
use App\Content\Models\ContentCategory;
use App\Content\Models\ContentMediaFile;
use App\Content\Models\ContentTag;

use App\Content\Repositories\ContentMediaFileRepository;
use Carbon\Carbon;

class ArticleRepository implements CrudInterface
{
    /**
     * Authenticated User Instance.
     *
     * @var User
     */
    public User|null $user;

    /**
     * Content Media File Repository class.
     *
     * @var ContentMediaFileRepository
     */
    public $contentMediaFileRepository;

    /**
     * Constructor.
     */
    public function __construct(ContentMediaFileRepository $contentMediaFileRepository)
    {
        $this->user = Auth::guard()->user();
        $this->contentMediaFileRepository = $contentMediaFileRepository;
    }

    /**
     * Get All Content Article.
     *
     * @return Paginator Array of Article Collection
     */
    public function getAll(): Paginator
    {
        return Article::orderBy('created_at', 'desc')
            ->paginate(10);
    }

    /**
     * Get Content Article Datatables.
     *
     * @return collections Array of Datatables Article Collection
     */
    public function getDataTables()
    {
        return Article::select('id', 'created_at', 'title', 'short_content', 'is_published', 'published_at', 'is_draft', 'thumbnail_image')
            ->orderBy('created_at', 'desc')->get();
    }

    /**
     * Get Paginated Content Article Data.
     *
     * @param int $perPage
     * @param int $page
     * @return Paginator Array of Article Collection
     */
    public function getPaginatedData($perPage, $page): Paginator
    {
        $perPage = isset($perPage) ? intval($perPage) : 5;
        $page = isset($page) ? intval($page) : 1;
        return Article::orderBy('id', 'desc')
            ->paginate($perPage, ['*'], 'page', $page)
            ->appends(['perPage' => $perPage]);
    }

    /**
     * Get Searchable Content Article Data with Pagination.
     *
     * @param string $keyword
     * @param int $perPage
     * @param int $page
     * @return Paginator Array of Article Collection
     */
    public function searchArticles($keyword, $perPage, $page): Paginator
    {
        $perPage = isset($perPage) ? intval($perPage) : 10;
        $page = isset($page) ? intval($page) : 1;
        return Article::where('title', 'like', '%' . $keyword . '%')
            ->orderBy('created_at', 'desc')
            ->paginate($perPage, ['*'], 'page', $page);
    }

    public function createSlug($title, $id = 0)
    {
        $slug = Str::slug($title);
        $allSlugs = $this->getRelatedSlugs($slug, $id);
        if (!$allSlugs->contains('slug', $slug)) {
            return $slug;
        }
        $suffix = 2;
        while ($allSlugs->contains('slug', $slug . '-' . $suffix)) {
            $suffix++;
        }

        return $slug . '-' . $suffix;
    }

    protected function getRelatedSlugs($slug, $id = 0)
    {
        return Article::select('slug')->where('slug', 'like', $slug . '%')
            ->where('id', '<>', $id)
            ->get();
    }

    /**
     * Create New Content Article.
     *
     * @param array $data
     * @return Article
     */
    public function create(array $data): Article
    {
        $data['slug'] = $this->createSlug($data['title']);

        if (!empty($data['is_published'])) {
            $data['published_at'] = Carbon::now()->format('Y-m-d H:i');
        }

        $articleCategories = array();

        // Add
        if (!empty($data['article_categories'])) {
            $contentCategoryIds = array_column($data['article_categories'], 'content_category_id');
            $contentCategories = ContentCategory::whereIn('id', $contentCategoryIds)->get()->keyBy('id');

            foreach ($data['article_categories'] as $articleCategoryData) {
                $addArticleCategory = new ArticleCategory();
                $contentCategoryId = $articleCategoryData['content_category_id'];
                $contentCategory = $contentCategories->get($contentCategoryId);
                if ($contentCategory) {
                    $addArticleCategory->contentCategory()->associate($contentCategory);
                    array_push($articleCategories, $addArticleCategory);
                }
            }
        }

        $articleTags = array();

        // Add
        if (!empty($data['article_tags'])) {
            $contentTagIds = array_column($data['article_tags'], 'content_tag_id');
            $contentTags = ContentTag::whereIn('id', $contentTagIds)->get()->keyBy('id');

            foreach ($data['article_tags'] as $articleTagData) {
                $addArticleTag = new ArticleTag();
                $contentTagId = $articleTagData['content_tag_id'];
                $contentTag = $contentTags->get($contentTagId);
                if ($contentTag) {
                    $addArticleTag->contentTag()->associate($contentTag);
                    array_push($articleTags, $addArticleTag);
                }
            }
        }

        $articleImages = array();

        // Add
        if (!empty($data['article_images'])) {
            $contentMediaFileIds = array_column($data['article_images'], 'content_media_file_id');
            $contentMediaFiles = ContentMediaFile::whereIn('id', $contentMediaFileIds)->get()->keyBy('id');

            foreach ($data['article_images'] as $articleImageData) {
                if ($articleImageData) {
                    $addArticleImage = new ArticleImage();
                    $contentMediaFileId = $articleImageData['content_media_file_id'];
                    $contentMediaFile = $contentMediaFiles->get($contentMediaFileId);
                    if ($contentMediaFile) {
                        $srcImage = 'images/media-files/' . $contentMediaFile->file;
                        $fileImage = new File($srcImage);
                        if ($fileImage) {
                            $originalImage = file_get_contents($fileImage);
                            file_put_contents('images/articles/images/' . $contentMediaFile->file, $originalImage);
                        }
                        $addArticleImage->image = $contentMediaFile->file;
                        array_push($articleImages, $addArticleImage);
                    }
                }
            }
        }

        if (!empty($data['pinned_image'])) {
            $image = $this->contentMediaFileRepository->getByID($data['pinned_image']);
            if ($image) {
                $srcImage = 'images/media-files/' . $image->file;
                $fileImage = new File($srcImage);
                if ($fileImage) {
                    $originalImage = file_get_contents($fileImage);
                    file_put_contents('images/articles/original/' . $image->file, $originalImage);
                    $data['pinned_image'] = $image->file;

                    $thumbnailImage = Image::make($originalImage);
                    $thumbnail = $thumbnailImage->resize(100, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $thumbnailPath = public_path('images/articles/thumbnail/' . $image->file);
                    $thumbnail->save($thumbnailPath);
                    $data['thumbnail_image'] = $image->file;
                }
            } else {
                $data['pinned_image'] = null;
                $data['thumbnail_image'] = null;
            }
        }

        $article = Article::create($data);

        if (count($articleCategories) > 0) {
            $article->articleCategories()->saveMany($articleCategories);
        }

        if (count($articleTags) > 0) {
            $article->articleTags()->saveMany($articleTags);
        }

        if (count($articleImages) > 0) {
            $article->articleImages()->saveMany($articleImages);
        }

        $article->load(['articleCategories', 'articleTags', 'articleImages']);

        return $article;
    }

    /**
     * Delete Content Article.
     *
     * @param string $id
     * @return boolean true if deleted otherwise false
     */
    public function delete(string $id): bool
    {
        $article = Article::find($id);

        if (empty($article)) {
            return false;
        }

        $article->articleCategories()->delete();
        $article->articleTags()->delete();
        $article->articleImages()->delete();
        $article->delete($article);
        return true;
    }

    /**
     * Get Content Article Detail By ID.
     *
     * @param string $id
     * @return Article|null
     */
    public function getByID(string $id): Article|null
    {
        $article = Article::find($id);
        if (is_null($article)) {
            return null;
        }

        $article->load(['articleCategories', 'articleTags', 'articleImages']);

        return $article;
    }

    /**
     * Update Content Article By ID.
     *
     * @param string $id
     * @param array $data
     * @return Article|null
     */
    public function update(string $id, array $data): Article|null
    {
        $article = Article::find($id);

        if (is_null($article)) {
            return null;
        }

        // Remove existing
        $article->articleCategories()->delete();
        $article->articleTags()->delete();
        $article->articleImages()->delete();

        $data['slug'] = $this->createSlug($data['title']);

        if (!empty($data['is_published'])) {
            $data['published_at'] = Carbon::now()->format('Y-m-d H:i');
        } else {
            $data['published_at'] = null;
        }

        $articleCategories = array();

        // Add
        if (!empty($data['article_categories'])) {
            $contentCategoryIds = array_column($data['article_categories'], 'content_category_id');
            $contentCategories = ContentCategory::whereIn('id', $contentCategoryIds)->get()->keyBy('id');

            foreach ($data['article_categories'] as $articleCategoryData) {
                $addArticleCategory = new ArticleCategory();
                $contentCategoryId = $articleCategoryData['content_category_id'];
                $contentCategory = $contentCategories->get($contentCategoryId);
                if ($contentCategory) {
                    $addArticleCategory->contentCategory()->associate($contentCategory);
                    array_push($articleCategories, $addArticleCategory);
                }
            }
        }

        $articleTags = array();

        // Add
        if (!empty($data['article_tags'])) {
            $contentTagIds = array_column($data['article_tags'], 'content_tag_id');
            $contentTags = ContentTag::whereIn('id', $contentTagIds)->get()->keyBy('id');

            foreach ($data['article_tags'] as $articleTagData) {
                $addArticleTag = new ArticleTag();
                $contentTagId = $articleTagData['content_tag_id'];
                $contentTag = $contentTags->get($contentTagId);
                if ($contentTag) {
                    $addArticleTag->contentTag()->associate($contentTag);
                    array_push($articleTags, $addArticleTag);
                }
            }
        }

        $articleImages = array();

        // Add
        if (!empty($data['article_images'])) {
            $contentMediaFileIds = array_column($data['article_images'], 'content_media_file_id');
            $contentMediaFiles = ContentMediaFile::whereIn('id', $contentMediaFileIds)->get()->keyBy('id');
            $contentMediaFilesByFile = ContentMediaFile::whereIn('file', $contentMediaFileIds)
                ->get()
                ->keyBy('file');

            foreach ($data['article_images'] as $articleImageData) {
                if ($articleImageData) {
                    $addArticleImage = new ArticleImage();
                    $contentMediaFileId = $articleImageData['content_media_file_id'];
                    $contentMediaFile = $contentMediaFiles->get($contentMediaFileId);

                    if (!$contentMediaFile) {
                        $contentMediaFile = $contentMediaFilesByFile->get($contentMediaFileId);
                    }

                    if ($contentMediaFile) {
                        $srcImage = 'images/media-files/' . $contentMediaFile->file;
                        $fileImage = new File($srcImage);
                        if ($fileImage) {
                            $originalImage = file_get_contents($fileImage);
                            file_put_contents('images/articles/images/' . $contentMediaFile->file, $originalImage);
                        }
                        $addArticleImage->image = $contentMediaFile->file;
                        array_push($articleImages, $addArticleImage);
                    }
                }
            }
        }

        if (!empty($data['pinned_image'])) {
            $image = $this->contentMediaFileRepository->getByID($data['pinned_image']);
            if ($image) {
                $srcImage = 'images/media-files/' . $image->file;
                $fileImage = new File($srcImage);
                if ($fileImage) {
                    $originalImage = file_get_contents($fileImage);
                    file_put_contents('images/articles/original/' . $image->file, $originalImage);
                    $data['pinned_image'] = $image->file;

                    $thumbnailImage = Image::make($originalImage);
                    $thumbnail = $thumbnailImage->resize(100, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $thumbnailPath = public_path('images/articles/thumbnail/' . $image->file);
                    $thumbnail->save($thumbnailPath);
                    $data['thumbnail_image'] = $image->file;
                }
            }
        }

        // If everything is OK, then update.
        $article->update($data);

        if (count($articleCategories) > 0) {
            $article->articleCategories()->saveMany($articleCategories);
        }

        if (count($articleTags) > 0) {
            $article->articleTags()->saveMany($articleTags);
        }

        if (count($articleImages) > 0) {
            $article->articleImages()->saveMany($articleImages);
        }

        // Finally return the updated user.
        return $this->getByID($article->id);
    }
}
