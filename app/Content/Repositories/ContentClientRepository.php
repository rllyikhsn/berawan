<?php

namespace App\Content\Repositories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

use App\Base\Interfaces\CrudInterface;
use App\User\Models\User;

use App\Content\Models\ContentClient;
use App\Content\Models\ContentMediaFile;

use App\Content\Repositories\ContentMediaFileRepository;
use Carbon\Carbon;

class ContentClientRepository implements CrudInterface
{
    /**
     * Authenticated User Instance.
     *
     * @var User|null
     */
    public User|null $user;

    /**
     * Content Media File Repository class.
     *
     * @var ContentMediaFileRepository
     */
    public $contentMediaFileRepository;

    /**
     * Constructor.
     */
    public function __construct(ContentMediaFileRepository $contentMediaFileRepository)
    {
        $this->user = Auth::guard()->user();
        $this->contentMediaFileRepository = $contentMediaFileRepository;
    }

    /**
     * Get All Content Clients.
     *
     * @return Paginator Array of ContentClient Collection
     */
    public function getAll(): Paginator
    {
        return ContentClient::orderBy('created_at', 'desc')
            ->paginate(10);
    }

    /**
     * Get Content Clients Datatables.
     *
     * @return collections Array of Datatables ContentClient Collection
     */
    public function getDataTables()
    {
        return ContentClient::select('id', 'created_at', 'name', 'thumbnail_image', 'pinned_image')
            ->orderBy('created_at', 'desc')->get();
    }

    /**
     * Get Paginated Content Clients Data.
     *
     * @param int $perPage
     * @param int $page
     * @return Paginator Array of ContentClient Collection
     */
    public function getPaginatedData($perPage, $page): Paginator
    {
        $perPage = isset($perPage) ? intval($perPage) : 5;
        $page = isset($page) ? intval($page) : 1;
        return ContentClient::orderBy('id', 'desc')
            ->paginate($perPage, ['*'], 'page', $page)
            ->appends(['perPage' => $perPage]);
    }

    /**
     * Get Searchable Content Clients Data with Pagination.
     *
     * @param string $keyword
     * @param int $perPage
     * @param int $page
     * @return Paginator Array of ContentClient Collection
     */
    public function searchContentClients($keyword, $perPage, $page): Paginator
    {
        $perPage = isset($perPage) ? intval($perPage) : 10;
        $page = isset($page) ? intval($page) : 1;
        return ContentClient::where('name', 'like', '%' . $keyword . '%')
            ->orderBy('created_at', 'desc')
            ->paginate($perPage, ['*'], 'page', $page);
    }

    /**
     * Create New Content Client.
     *
     * @param array $data
     * @return ContentClient
     */
    public function create(array $data): ContentClient
    {
        if (!empty($data['pinned_image'])) {
            $image = $this->contentMediaFileRepository->getByID($data['pinned_image']);
            if ($image) {
                $srcImage = 'images/media-files/' . $image->file;
                $fileImage = new File($srcImage);
                if ($fileImage) {
                    $originalImage = file_get_contents($fileImage);
                    file_put_contents('images/clients/original/' . $image->file, $originalImage);
                    $data['pinned_image'] = $image->file;

                    $thumbnailImage = Image::make($originalImage);
                    $thumbnail = $thumbnailImage->resize(100, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $thumbnailPath = public_path('images/clients/thumbnail/' . $image->file);
                    $thumbnail->save($thumbnailPath);
                    $data['thumbnail_image'] = $image->file;
                }
            } else {
                $data['pinned_image'] = null;
                $data['thumbnail_image'] = null;
            }
        }

        $contentClient = ContentClient::create($data);

        return $contentClient;
    }

    /**
     * Delete Content Client.
     *
     * @param string $id
     * @return boolean true if deleted otherwise false
     */
    public function delete(string $id): bool
    {
        $contentClient = ContentClient::find($id);

        if (empty($contentClient)) {
            return false;
        }

        $contentClient->delete($contentClient);
        return true;
    }

    /**
     * Get Content Client Detail By ID.
     *
     * @param string $id
     * @return ContentClient|null
     */
    public function getByID(string $id): ContentClient|null
    {
        $contentClient = ContentClient::find($id);
        if (is_null($contentClient)) {
            return null;
        }

        return $contentClient;
    }

    /**
     * Update Content Client By ID.
     *
     * @param string $id
     * @param array $data
     * @return ContentClient|null
     */
    public function update(string $id, array $data): ContentClient|null
    {
        $contentClient = ContentClient::find($id);

        if (is_null($contentClient)) {
            return null;
        }

        if (!empty($data['pinned_image'])) {
            $image = $this->contentMediaFileRepository->getByID($data['pinned_image']);
            if ($image) {
                $srcImage = 'images/media-files/' . $image->file;
                $fileImage = new File($srcImage);
                if ($fileImage) {
                    $originalImage = file_get_contents($fileImage);
                    file_put_contents('images/clients/original/' . $image->file, $originalImage);
                    $data['pinned_image'] = $image->file;

                    $thumbnailImage = Image::make($originalImage);
                    $thumbnail = $thumbnailImage->resize(100, null, function ($constraint) {
                        $constraint->aspectRatio();
                    });
                    $thumbnailPath = public_path('images/clients/thumbnail/' . $image->file);
                    $thumbnail->save($thumbnailPath);
                    $data['thumbnail_image'] = $image->file;
                }
            } else {
                $data['pinned_image'] = null;
                $data['thumbnail_image'] = null;
            }
        }

        // If everything is OK, then update.
        $contentClient->update($data);

        // Finally return the updated content client.
        return $this->getByID($contentClient->id);
    }
}
