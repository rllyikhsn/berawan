<?php

namespace App\Content\Repositories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;

use App\Base\Helpers\UploadHelper;
use App\Base\Interfaces\CrudInterface;
use App\User\Models\User;
use App\Content\Models\ContentCategory;

class ContentCategoryRepository implements CrudInterface
{
    /**
     * Authenticated User Instance.
     *
     * @var User
     */
    public User|null $user;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->user = Auth::guard()->user();
    }

    /**
     * Get All Content Category.
     *
     * @return Paginator Array of ContentCategory Collection
     */
    public function getAll()
    {
        return ContentCategory::orderBy('title', 'asc')->get();
    }

    /**
     * Get Content Category Datatables.
     *
     * @return collections Array of Datatables ContentCategory Collection
     */
    public function getDataTables()
    {
        return ContentCategory::select('id', 'created_at', 'title')
            ->orderBy('created_at', 'desc')->get();
    }

    /**
     * Get Paginated Content Category Data.
     *
     * @param int $perPage
     * @param int $page
     * @return Paginator Array of ContentCategory Collection
     */
    public function getPaginatedData($perPage, $page): Paginator
    {
        $perPage = isset($perPage) ? intval($perPage) : 5;
        $page = isset($page) ? intval($page) : 1;
        return ContentCategory::orderBy('id', 'desc')
            ->paginate($perPage, ['*'], 'page', $page)
            ->appends(['perPage' => $perPage]);
    }

    /**
     * Get Searchable Content Category Data with Pagination.
     *
     * @param string $keyword
     * @param int $perPage
     * @param int $page
     * @return Paginator Array of ContentCategory Collection
     */
    public function searchContentCategorys($keyword, $perPage, $page): Paginator
    {
        $perPage = isset($perPage) ? intval($perPage) : 10;
        $page = isset($page) ? intval($page) : 1;
        return ContentCategory::where('title', 'like', '%' . $keyword . '%')
            ->orderBy('created_at', 'desc')
            ->paginate($perPage, ['*'], 'page', $page);
    }

    /**
     * Create New Content Category.
     *
     * @param array $data
     * @return ContentCategory
     */
    public function create(array $data): ContentCategory
    {
        return ContentCategory::create($data);
    }

    /**
     * Delete Content Category.
     *
     * @param string $id
     * @return boolean true if deleted otherwise false
     */
    public function delete(string $id): bool
    {
        $category = ContentCategory::find($id);
        if (empty($category)) {
            return false;
        }

        $category->delete($category);
        return true;
    }

    /**
     * Get Content Category Detail By ID.
     *
     * @param string $id
     * @return ContentCategory|null
     */
    public function getByID(string $id): ContentCategory|null
    {
        return ContentCategory::find($id);
    }

    /**
     * Update Content Category By ID.
     *
     * @param string $id
     * @param array $data
     * @return ContentCategory|null
     */
    public function update(string $id, array $data): ContentCategory|null
    {
        $category = ContentCategory::find($id);

        if (is_null($category)) {
            return null;
        }

        // If everything is OK, then update.
        $category->update($data);

        // Finally return the updated user.
        return $this->getByID($category->id);
    }
}
