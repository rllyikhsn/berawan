<?php

namespace App\Content\Repositories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

use App\Base\Helpers\UploadHelper;
use App\Base\Interfaces\CrudInterface;
use App\User\Models\User;
use App\Content\Models\ContentWorkshop;
use Carbon\Carbon;

class ContentWorkshopRepository implements CrudInterface
{
    /**
     * Authenticated User Instance.
     *
     * @var User
     */
    public User|null $user;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->user = Auth::guard()->user();
    }

    /**
     * Get All Content Workshops.
     *
     * @return Paginator Array of ContentWorkshop Collection
     */
    public function getAll(): Paginator
    {
        return ContentWorkshop::orderBy('created_at', 'desc')
            ->paginate(10);
    }

    /**
     * Get Content Workshops Datatables.
     *
     * @return collections Array of Datatables ContentWorkshop Collection
     */
    public function getDataTables()
    {
        return ContentWorkshop::select('id', 'created_at', 'name', 'alamat', 'no_telepon', 'published_at', 'is_published')
            ->orderBy('created_at', 'desc')->get();
    }

    /**
     * Get Paginated Content Workshops Data.
     *
     * @param int $perPage
     * @param int $page
     * @return Paginator Array of ContentWorkshop Collection
     */
    public function getPaginatedData($perPage, $page): Paginator
    {
        $perPage = isset($perPage) ? intval($perPage) : 5;
        $page = isset($page) ? intval($page) : 1;
        return ContentWorkshop::orderBy('id', 'desc')
            ->paginate($perPage, ['*'], 'page', $page)
            ->appends(['perPage' => $perPage]);
    }

    /**
     * Get Searchable Content Workshops Data with Pagination.
     *
     * @param string $keyword
     * @param int $perPage
     * @return Paginator Array of ContentWorkshop Collection
     */
    public function searchContentWorkshops($keyword, $perPage): Paginator
    {
        $perPage = isset($perPage) ? intval($perPage) : 10;
        return ContentWorkshop::where('name', 'like', '%' . $keyword . '%')
            ->orWhere('alamat', 'like', '%' . $keyword . '%')
            ->orderBy('created_at', 'desc')
            ->paginate($perPage);
    }

    public function createSlug($title, $id = 0)
    {
        $slug = Str::slug($title);
        $allSlugs = $this->getRelatedSlugs($slug, $id);
        if (!$allSlugs->contains('slug', $slug)) {
            return $slug;
        }
        $suffix = 2;
        while ($allSlugs->contains('slug', $slug . '-' . $suffix)) {
            $suffix++;
        }

        return $slug . '-' . $suffix;
    }

    protected function getRelatedSlugs($slug, $id = 0)
    {
        return ContentWorkshop::select('slug')->where('slug', 'like', $slug . '%')
            ->where('id', '<>', $id)
            ->get();
    }

    /**
     * Create New Content Workshop.
     *
     * @param array $data
     * @return ContentWorkshop
     */
    public function create(array $data): ContentWorkshop
    {
        $data['slug'] = $this->createSlug($data['name']);
        if (!empty($data['is_published'])) {
            $data['published_at'] = Carbon::now()->format('Y-m-d H:i');
        }
        return ContentWorkshop::create($data);
    }

    /**
     * Delete Content Workshop.
     *
     * @param string $id
     * @return boolean true if deleted otherwise false
     */
    public function delete(string $id): bool
    {
        $workshop = ContentWorkshop::find($id);
        if (empty($workshop)) {
            return false;
        }

        $workshop->delete($workshop);
        return true;
    }

    /**
     * Get Content Workshop Detail By ID.
     *
     * @param string $id
     * @return ContentWorkshop|null
     */
    public function getByID(string $id): ContentWorkshop|null
    {
        return ContentWorkshop::find($id);
    }

    /**
     * Update Content Workshop By ID.
     *
     * @param string $id
     * @param array $data
     * @return ContentWorkshop|null
     */
    public function update(string $id, array $data): ContentWorkshop|null
    {
        $workshop = ContentWorkshop::find($id);

        if (is_null($workshop)) {
            return null;
        }

        $data['slug'] = $this->createSlug($data['name']);
        if (!empty($data['is_published'])) {
            $data['published_at'] = Carbon::now()->format('Y-m-d H:i');
        }
        // If everything is OK, then update.
        $workshop->update($data);

        // Finally return the updated user.
        return $this->getByID($workshop->id);
    }
}
