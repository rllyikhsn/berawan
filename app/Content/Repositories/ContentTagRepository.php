<?php

namespace App\Content\Repositories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;

use App\Base\Helpers\UploadHelper;
use App\Base\Interfaces\CrudInterface;
use App\User\Models\User;
use App\Content\Models\ContentTag;

class ContentTagRepository implements CrudInterface
{
    /**
     * Authenticated User Instance.
     *
     * @var User
     */
    public User|null $user;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->user = Auth::guard()->user();
    }

    /**
     * Get All Content Tag.
     *
     * @return Paginator Array of ContentTag Collection
     */
    public function getAll()
    {
        return ContentTag::orderBy('title', 'asc')->get();
    }

    /**
     * Get Content Tag Datatables.
     *
     * @return collections Array of Datatables ContentTag Collection
     */
    public function getDataTables()
    {
        return ContentTag::select('id', 'created_at', 'title')
            ->orderBy('created_at', 'desc')->get();
    }

    /**
     * Get Paginated Content Tag Data.
     *
     * @param int $perPage
     * @param int $page
     * @return Paginator Array of ContentTag Collection
     */
    public function getPaginatedData($perPage, $page): Paginator
    {
        $perPage = isset($perPage) ? intval($perPage) : 5;
        $page = isset($page) ? intval($page) : 1;
        return ContentTag::orderBy('id', 'desc')
            ->paginate($perPage, ['*'], 'page', $page)
            ->appends(['perPage' => $perPage]);
    }

    /**
     * Get Searchable Content Tag Data with Pagination.
     *
     * @param string $keyword
     * @param int $perPage
     * @return Paginator Array of ContentTag Collection
     */
    public function searchContentTags($keyword, $perPage): Paginator
    {
        $perPage = isset($perPage) ? intval($perPage) : 10;
        return ContentTag::where('title', 'like', '%' . $keyword . '%')
            ->orderBy('created_at', 'desc')
            ->paginate($perPage);
    }

    /**
     * Create New Content Tag.
     *
     * @param array $data
     * @return ContentTag
     */
    public function create(array $data): ContentTag
    {
        return ContentTag::create($data);
    }

    /**
     * Delete Content Tag.
     *
     * @param string $id
     * @return boolean true if deleted otherwise false
     */
    public function delete(string $id): bool
    {
        $tag = ContentTag::find($id);
        if (empty($tag)) {
            return false;
        }

        $tag->delete($tag);
        return true;
    }

    /**
     * Get Content Tag Detail By ID.
     *
     * @param string $id
     * @return ContentTag|null
     */
    public function getByID(string $id): ContentTag|null
    {
        return ContentTag::find($id);
    }

    /**
     * Update Content Tag By ID.
     *
     * @param string $id
     * @param array $data
     * @return ContentTag|null
     */
    public function update(string $id, array $data): ContentTag|null
    {
        $tag = ContentTag::find($id);

        if (is_null($tag)) {
            return null;
        }

        // If everything is OK, then update.
        $tag->update($data);

        // Finally return the updated user.
        return $this->getByID($tag->id);
    }
}
