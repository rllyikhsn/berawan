<?php

namespace App\Content\Repositories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

use App\Base\Helpers\UploadHelper;
use App\Base\Interfaces\CrudInterface;
use App\User\Models\User;

use App\Content\Models\ContentSlider;
use App\Content\Models\Article;
use App\Content\Models\ContentMediaFile;

use App\Content\Repositories\ContentMediaFileRepository;
use Carbon\Carbon;

class ContentSliderRepository implements CrudInterface
{
    /**
     * Authenticated User Instance.
     *
     * @var User
     */
    public User|null $user;

    /**
     * Content Media File Repository class.
     *
     * @var ContentMediaFileRepository
     */
    public $contentMediaFileRepository;

    /**
     * Constructor.
     */
    public function __construct(ContentMediaFileRepository $contentMediaFileRepository)
    {
        $this->user = Auth::guard()->user();
        $this->contentMediaFileRepository = $contentMediaFileRepository;
    }

    /**
     * Get All Content ContentSlider.
     *
     * @return Paginator Array of ContentSlider Collection
     */
    public function getAll(): Paginator
    {
        return ContentSlider::orderBy('created_at', 'desc')
            ->paginate(10);
    }

    /**
     * Get Content ContentSlider Datatables.
     *
     * @return collections Array of Datatables ContentSlider Collection
     */
    public function getDataTables()
    {
        return ContentSlider::select('id', 'created_at', 'title', 'short_content', 'is_published', 'published_at', 'is_draft', 'image')
            ->orderBy('created_at', 'desc')->get();
    }

    /**
     * Get Paginated Content ContentSlider Data.
     *
     * @param int $perPage
     * @param int $page
     * @return Paginator Array of ContentSlider Collection
     */
    public function getPaginatedData($perPage, $page): Paginator
    {
        $perPage = isset($perPage) ? intval($perPage) : 5;
        $page = isset($page) ? intval($page) : 1;
        return ContentSlider::orderBy('id', 'desc')
            ->paginate($perPage, ['*'], 'page', $page)
            ->appends(['perPage' => $perPage]);
    }

    /**
     * Get Searchable Content ContentSlider Data with Pagination.
     *
     * @param string $keyword
     * @param int $perPage
     * @param int $page
     * @return Paginator Array of ContentSlider Collection
     */
    public function searchContentSliders($keyword, $perPage, $page): Paginator
    {
        $perPage = isset($perPage) ? intval($perPage) : 10;
        $page = isset($page) ? intval($page) : 1;
        return ContentSlider::where('title', 'like', '%' . $keyword . '%')
            ->orderBy('created_at', 'desc')
            ->paginate($perPage, ['*'], 'page', $page);
    }

    /**
     * Create New Content ContentSlider.
     *
     * @param array $data
     * @return ContentSlider
     */
    public function create(array $data): ContentSlider
    {
        if (!empty($data['is_published'])) {
            $data['published_at'] = Carbon::now()->format('Y-m-d H:i');
        }

        if (!empty($data['image'])) {
            $image = $this->contentMediaFileRepository->getByID($data['image']);
            if ($image) {
                $srcImage = 'images/media-files/' . $image->file;
                $fileImage = new File($srcImage);
                if ($fileImage) {
                    $originalImage = file_get_contents($fileImage);
                    file_put_contents('images/sliders/' . $image->file, $originalImage);
                    $data['image'] = $image->file;
                }
            } else {
                $data['image'] = null;
            }
        }

        $contentSlider = ContentSlider::create($data);

        $contentSlider->load(['article']);

        return $contentSlider;
    }

    /**
     * Delete Content ContentSlider.
     *
     * @param string $id
     * @return boolean true if deleted otherwise false
     */
    public function delete(string $id): bool
    {
        $contentSlider = ContentSlider::find($id);

        if (empty($contentSlider)) {
            return false;
        }

        $contentSlider->delete($contentSlider);
        return true;
    }

    /**
     * Get Content ContentSlider Detail By ID.
     *
     * @param string $id
     * @return ContentSlider|null
     */
    public function getByID(string $id): ContentSlider|null
    {
        $contentSlider = ContentSlider::find($id);
        if (is_null($contentSlider)) {
            return null;
        }

        $contentSlider->load(['article']);

        return $contentSlider;
    }

    /**
     * Update Content ContentSlider By ID.
     *
     * @param string $id
     * @param array $data
     * @return ContentSlider|null
     */
    public function update(string $id, array $data): ContentSlider|null
    {
        $contentSlider = ContentSlider::find($id);

        if (is_null($contentSlider)) {
            return null;
        }

        if (!empty($data['is_published'])) {
            $data['published_at'] = Carbon::now()->format('Y-m-d H:i');
        } else {
            $data['published_at'] = null;
        }

        if (!empty($data['image'])) {
            $image = $this->contentMediaFileRepository->getByID($data['image']);
            if ($image) {
                $srcImage = 'images/media-files/' . $image->file;
                $fileImage = new File($srcImage);
                if ($fileImage) {
                    $originalImage = file_get_contents($fileImage);
                    file_put_contents('images/sliders/' . $image->file, $originalImage);
                    $data['image'] = $image->file;
                }
            } else {
                $data['image'] = null;
            }
        }

        // If everything is OK, then update.
        $contentSlider->update($data);

        // Finally return the updated user.
        return $this->getByID($contentSlider->id);
    }
}
