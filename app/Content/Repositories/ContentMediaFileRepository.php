<?php

namespace App\Content\Repositories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\Facades\Image;

use App\Base\Helpers\UploadHelper;
use App\Base\Interfaces\CrudInterface;
use App\Content\Models\ContentMediaFile;
use App\User\Models\User;


class ContentMediaFileRepository implements CrudInterface
{
    /**
     * Authenticated User Instance.
     *
     * @var User
     */
    public User|null $user;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->user = Auth::guard()->user();
    }

    /**
     * Get All ContentMediaFile.
     *
     * @return Paginator Array of ContentMediaFile Collection
     */
    public function getAll(): Paginator
    {
        return ContentMediaFile::orderBy('created_at', 'desc')
            ->paginate(10);
    }

    /**
     * Get ContentMediaFile Datatables.
     *
     * @return collections Array of Datatables ContentMediaFile Collection
     */
    public function getDataTables()
    {
        return ContentMediaFile::select('id', 'created_at')
            ->orderBy('created_at', 'desc')->get();
    }

    /**
     * Get Paginated ContentMediaFile Data.
     *
     * @param int $perPage
     * @param int $page
     * @return Paginator Array of ContentMediaFile Collection
     */
    public function getPaginatedData($perPage, $page): Paginator
    {
        $perPage = isset($perPage) ? intval($perPage) : 5;
        $page = isset($page) ? intval($page) : 1;
        return ContentMediaFile::orderBy('id', 'desc')
            ->paginate($perPage, ['*'], 'page', $page)
            ->appends(['perPage' => $perPage]);
    }

    /**
     * Get Searchable ContentMediaFile Data with Pagination.
     *
     * @param string $keyword
     * @param int $perPage
     * @return Paginator Array of ContentMediaFile Collection
     */
    public function searchContents($keyword, $perPage): Paginator
    {
        $perPage = isset($perPage) ? intval($perPage) : 10;
        return ContentMediaFile::where('title', 'like', '%' . $keyword . '%')
            ->orWhere('short_content', 'like', '%' . $keyword . '%')
            ->orderBy('created_at', 'desc')
            ->paginate($perPage);
    }

    /**
     * Create New ContentMediaFile .
     *
     * @param array $data
     * @return ContentMediaFile
     */
    public function create(array $data): ContentMediaFile
    {
        if (!empty($data['file'])) {
            $file = $data['file'];
            $fileSize = $file->getSize();

            // Get the pixel dimensions using Intervention Image
            $image = Image::make($file);
            $width = $image->getWidth();
            $height = $image->getHeight();

            $data['file'] = UploadHelper::upload('file', $data['file'], substr(md5(uniqid(mt_rand(), false), false), 0, 20) . time(), 'images/media-files');
            // Get the file size in bytes

            $data['size'] = $fileSize;
            $data['width'] = $width;
            $data['height'] = $height;
        }
        return ContentMediaFile::create($data);
    }

    /**
     * Delete ContentMediaFile .
     *
     * @param string $id
     * @return boolean true if deleted otherwise false
     */
    public function delete(string $id): bool
    {
        $mediaFile = ContentMediaFile::find($id);
        UploadHelper::deleteFile('images/media-files/' . $mediaFile->file);
        if (empty($mediaFile)) {
            return false;
        }

        $mediaFile->forceDelete($mediaFile);
        return true;
    }

    /**
     * Get ContentMediaFile Detail By ID.
     *
     * @param string $id
     * @return ContentMediaFile|null
     */
    public function getByID(string $id): ContentMediaFile|null
    {
        return ContentMediaFile::find($id);
    }

    /**
     * Update ContentMediaFile By ID.
     *
     * @param string $id
     * @param array $data
     * @return ContentMediaFile|null
     */
    public function update(string $id, array $data): ContentMediaFile|null
    {
        $mediaFile = ContentMediaFile::find($id);

        if (is_null($mediaFile)) {
            return null;
        }

        // If everything is OK, then update.
        $mediaFile->update($data);

        // Finally return the updated user.
        return $this->getByID($mediaFile->id);
    }
}
