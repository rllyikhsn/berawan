<?php

namespace App\Content\Repositories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;

use App\Base\Helpers\UploadHelper;
use App\Base\Interfaces\CrudInterface;
use App\User\Models\User;
use App\Content\Models\ContentReportAnnual;

class ContentReportAnnualRepository implements CrudInterface
{
    /**
     * Authenticated User Instance.
     *
     * @var User
     */
    public User|null $user;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->user = Auth::guard()->user();
    }

    /**
     * Get All Content Report Annual.
     *
     * @return Paginator Array of ContentReportAnnual Collection
     */
    public function getAll()
    {
        return ContentReportAnnual::orderBy('title', 'asc')->get();
    }

    /**
     * Get Content Report Annual Datatables.
     *
     * @return collections Array of Datatables ContentReportAnnual Collection
     */
    public function getDataTables()
    {
        return ContentReportAnnual::select('id', 'created_at', 'title', 'file')
            ->orderBy('created_at', 'desc')->get();
    }

    /**
     * Get Paginated Content Report Annual Data.
     *
     * @param int $perPage
     * @param int $page
     * @return Paginator Array of ContentReportAnnual Collection
     */
    public function getPaginatedData($perPage, $page): Paginator
    {
        $perPage = isset($perPage) ? intval($perPage) : 5;
        $page = isset($page) ? intval($page) : 1;
        return ContentReportAnnual::orderBy('id', 'desc')
            ->paginate($perPage, ['*'], 'page', $page)
            ->appends(['perPage' => $perPage]);
    }

    /**
     * Get Searchable Content Report Annual Data with Pagination.
     *
     * @param string $keyword
     * @param int $perPage
     * @return Paginator Array of ContentReportAnnual Collection
     */
    public function searchContentReportAnnuals($keyword, $perPage): Paginator
    {
        $perPage = isset($perPage) ? intval($perPage) : 10;
        return ContentReportAnnual::where('title', 'like', '%' . $keyword . '%')
            ->orderBy('created_at', 'desc')
            ->paginate($perPage);
    }

    /**
     * Create New Content Report Annual.
     *
     * @param array $data
     * @return ContentReportAnnual
     */
    public function create(array $data): ContentReportAnnual
    {
        if (!empty($data['file'])) {
            $data['file'] = UploadHelper::upload('file', $data['file'], substr(md5(uniqid(mt_rand(), false), false), 0, 20) . time(), 'documents/report/annual');
        }
        return ContentReportAnnual::create($data);
    }

    /**
     * Delete Content Report Annual.
     *
     * @param string $id
     * @return boolean true if deleted otherwise false
     */
    public function delete(string $id): bool
    {
        $reportAnnual = ContentReportAnnual::find($id);
        if (empty($reportAnnual)) {
            return false;
        }

        $reportAnnual->delete($reportAnnual);
        return true;
    }

    /**
     * Get Content Report Annual Detail By ID.
     *
     * @param string $id
     * @return ContentReportAnnual|null
     */
    public function getByID(string $id): ContentReportAnnual|null
    {
        return ContentReportAnnual::find($id);
    }

    /**
     * Update Content Report Annual By ID.
     *
     * @param string $id
     * @param array $data
     * @return ContentReportAnnual|null
     */
    public function update(string $id, array $data): ContentReportAnnual|null
    {
        $reportAnnual = ContentReportAnnual::find($id);

        if (is_null($reportAnnual)) {
            return null;
        }

        if (!empty($data['file'])) {
            if (!is_null($reportAnnual->file)) {
                UploadHelper::deleteFile('documents/report/annual/' . $reportAnnual->file);
            }

            $data['file'] = UploadHelper::upload('file', $data['file'], substr(md5(uniqid(mt_rand(), false), false), 0, 20) . time(), 'documents/report/annual');
        }

        // If everything is OK, then update.
        $reportAnnual->update($data);

        // Finally return the updated user.
        return $this->getByID($reportAnnual->id);
    }
}
