<?php

namespace App\Content\Repositories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;

use App\Base\Helpers\UploadHelper;
use App\Base\Interfaces\CrudInterface;
use App\User\Models\User;
use App\Content\Models\ContentAnnouncement;

class ContentAnnouncementRepository implements CrudInterface
{
    /**
     * Authenticated User Instance.
     *
     * @var User
     */
    public User|null $user;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->user = Auth::guard()->user();
    }

    /**
     * Get All Content Announcements.
     *
     * @return Paginator Array of ContentAnnouncement Collection
     */
    public function getAll(): Paginator
    {
        return ContentAnnouncement::orderBy('created_at', 'desc')
            ->paginate(10);
    }

    /**
     * Get Content Announcements Datatables.
     *
     * @return collections Array of Datatables ContentAnnouncement Collection
     */
    public function getDataTables()
    {
        return ContentAnnouncement::select('id', 'created_at', 'title', 'short_content', 'image', 'published_at', 'is_published', 'start_at', 'end_at')
            ->orderBy('created_at', 'desc')->get();
    }

    /**
     * Get Paginated Content Announcements Data.
     *
     * @param int $perPage
     * @param int $page
     * @return Paginator Array of ContentAnnouncement Collection
     */
    public function getPaginatedData($perPage, $page): Paginator
    {
        $perPage = isset($perPage) ? intval($perPage) : 5;
        $page = isset($page) ? intval($page) : 1;
        return ContentAnnouncement::orderBy('id', 'desc')
            ->paginate($perPage, ['*'], 'page', $page)
            ->appends(['perPage' => $perPage]);
    }

    /**
     * Get Searchable Content Announcements Data with Pagination.
     *
     * @param string $keyword
     * @param int $perPage
     * @return Paginator Array of ContentAnnouncement Collection
     */
    public function searchContentAnnouncements($keyword, $perPage): Paginator
    {
        $perPage = isset($perPage) ? intval($perPage) : 10;
        return ContentAnnouncement::where('title', 'like', '%' . $keyword . '%')
            ->orWhere('short_content', 'like', '%' . $keyword . '%')
            ->orderBy('created_at', 'desc')
            ->paginate($perPage);
    }

    /**
     * Create New Content Announcement.
     *
     * @param array $data
     * @return ContentAnnouncement
     */
    public function create(array $data): ContentAnnouncement
    {
        if (!empty($data['image'])) {
            $data['image'] = UploadHelper::upload('image', $data['image'], substr(md5(uniqid(mt_rand(), false), false), 0, 20) . time(), 'images/announcements');
        }
        return ContentAnnouncement::create($data);
    }

    /**
     * Delete Content Announcement.
     *
     * @param string $id
     * @return boolean true if deleted otherwise false
     */
    public function delete(string $id): bool
    {
        $announcement = ContentAnnouncement::find($id);
        if (empty($announcement)) {
            return false;
        }

        $announcement->delete($announcement);
        return true;
    }

    /**
     * Get Content Announcement Detail By ID.
     *
     * @param string $id
     * @return ContentAnnouncement|null
     */
    public function getByID(string $id): ContentAnnouncement|null
    {
        return ContentAnnouncement::find($id);
    }

    /**
     * Update Content Announcement By ID.
     *
     * @param string $id
     * @param array $data
     * @return ContentAnnouncement|null
     */
    public function update(string $id, array $data): ContentAnnouncement|null
    {
        $announcement = ContentAnnouncement::find($id);
        if (!empty($data['image'])) {
            UploadHelper::deleteFile('images/announcements/' . $announcement->image);
            $data['image'] = UploadHelper::upload('image', $data['image'], substr(md5(uniqid(mt_rand(), false), false), 0, 20) . time(), 'images/announcements', $announcement->image);
        } else {
            $data['image'] = $announcement->image;
        }

        if (is_null($announcement)) {
            return null;
        }

        // If everything is OK, then update.
        $announcement->update($data);

        // Finally return the updated user.
        return $this->getByID($announcement->id);
    }
}
