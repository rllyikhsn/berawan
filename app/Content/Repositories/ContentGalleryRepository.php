<?php

namespace App\Content\Repositories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

use App\Base\Helpers\UploadHelper;
use App\Base\Interfaces\CrudInterface;
use App\User\Models\User;

use App\Content\Models\ContentGallery;
use App\Content\Models\ContentGalleryCategory;
use App\Content\Models\ContentGalleryImage;
use App\Content\Models\ContentGalleryTag;
use App\Content\Models\ContentCategory;
use App\Content\Models\ContentMediaFile;
use App\Content\Models\ContentTag;

use App\Content\Repositories\ContentMediaFileRepository;
use Carbon\Carbon;

class ContentGalleryRepository implements CrudInterface
{
    /**
     * Authenticated User Instance.
     *
     * @var User
     */
    public User|null $user;

    /**
     * Content Media File Repository class.
     *
     * @var ContentMediaFileRepository
     */
    public $contentMediaFileRepository;

    /**
     * Constructor.
     */
    public function __construct(ContentMediaFileRepository $contentMediaFileRepository)
    {
        $this->user = Auth::guard()->user();
        $this->contentMediaFileRepository = $contentMediaFileRepository;
    }

    /**
     * Get All Content Gallery.
     *
     * @return Paginator Array of ContentGallery Collection
     */
    public function getAll(): Paginator
    {
        return ContentGallery::orderBy('created_at', 'desc')
            ->paginate(10);
    }

    /**
     * Get Content Gallery Datatables.
     *
     * @return collections Array of Datatables ContentGallery Collection
     */
    public function getDataTables()
    {
        return ContentGallery::select('id', 'created_at', 'title', 'short_content', 'is_published', 'published_at', 'is_draft')
            ->orderBy('created_at', 'desc')->get();
    }

    /**
     * Get Paginated Content Gallery Data.
     *
     * @param int $perPage
     * @param int $page
     * @return Paginator Array of ContentGallery Collection
     */
    public function getPaginatedData($perPage, $page): Paginator
    {
        $perPage = isset($perPage) ? intval($perPage) : 5;
        $page = isset($page) ? intval($page) : 1;
        return ContentGallery::orderBy('id', 'desc')
            ->paginate($perPage, ['*'], 'page', $page)
            ->appends(['perPage' => $perPage]);
    }

    /**
     * Get Searchable Content Gallery Data with Pagination.
     *
     * @param string $keyword
     * @param int $perPage
     * @param int $page
     * @return Paginator Array of ContentGallery Collection
     */
    public function searchContentGallerys($keyword, $perPage, $page): Paginator
    {
        $perPage = isset($perPage) ? intval($perPage) : 10;
        $page = isset($page) ? intval($page) : 1;
        return ContentGallery::where('title', 'like', '%' . $keyword . '%')
            ->orderBy('created_at', 'desc')
            ->paginate($perPage, ['*'], 'page', $page);
    }

    public function createSlug($title, $id = 0)
    {
        $slug = Str::slug($title);
        $allSlugs = $this->getRelatedSlugs($slug, $id);
        if (!$allSlugs->contains('slug', $slug)) {
            return $slug;
        }
        $suffix = 2;
        while ($allSlugs->contains('slug', $slug . '-' . $suffix)) {
            $suffix++;
        }

        return $slug . '-' . $suffix;
    }

    protected function getRelatedSlugs($slug, $id = 0)
    {
        return ContentGallery::select('slug')->where('slug', 'like', $slug . '%')
            ->where('id', '<>', $id)
            ->get();
    }

    /**
     * Create New Content Gallery.
     *
     * @param array $data
     * @return ContentGallery
     */
    public function create(array $data): ContentGallery
    {
        $data['slug'] = $this->createSlug($data['title']);

        if (!empty($data['is_published'])) {
            $data['published_at'] = Carbon::now()->format('Y-m-d H:i');
        }

        $contentGalleryCategories = array();

        // Add
        if (!empty($data['content_gallery_categories'])) {
            $contentCategoryIds = array_column($data['content_gallery_categories'], 'content_category_id');
            $contentCategories = ContentCategory::whereIn('id', $contentCategoryIds)->get()->keyBy('id');

            foreach ($data['content_gallery_categories'] as $contentGalleryCategoryData) {
                $addContentGalleryCategory = new ContentGalleryCategory();
                $contentCategoryId = $contentGalleryCategoryData['content_category_id'];
                $contentCategory = $contentCategories->get($contentCategoryId);
                if ($contentCategory) {
                    $addContentGalleryCategory->contentCategory()->associate($contentCategory);
                    array_push($contentGalleryCategories, $addContentGalleryCategory);
                }
            }
        }

        $contentGalleryTags = array();

        // Add
        if (!empty($data['content_gallery_tags'])) {
            $contentTagIds = array_column($data['content_gallery_tags'], 'content_tag_id');
            $contentTags = ContentTag::whereIn('id', $contentTagIds)->get()->keyBy('id');

            foreach ($data['content_gallery_tags'] as $contentGalleryTagData) {
                $addContentGalleryTag = new ContentGalleryTag();
                $contentTagId = $contentGalleryTagData['content_tag_id'];
                $contentTag = $contentTags->get($contentTagId);
                if ($contentTag) {
                    $addContentGalleryTag->contentTag()->associate($contentTag);
                    array_push($contentGalleryTags, $addContentGalleryTag);
                }
            }
        }

        $contentGalleryImages = array();

        // Add
        if (!empty($data['content_gallery_images'])) {
            $contentMediaFileIds = array_column($data['content_gallery_images'], 'content_media_file_id');
            $contentMediaFiles = ContentMediaFile::whereIn('id', $contentMediaFileIds)->get()->keyBy('id');

            foreach ($data['content_gallery_images'] as $contentGalleryImageData) {
                if ($contentGalleryImageData) {
                    $addContentGalleryImage = new ContentGalleryImage();
                    $contentMediaFileId = $contentGalleryImageData['content_media_file_id'];
                    $contentMediaFile = $contentMediaFiles->get($contentMediaFileId);
                    if ($contentMediaFile) {
                        $srcImage = 'images/media-files/' . $contentMediaFile->file;
                        $fileImage = new File($srcImage);
                        if ($fileImage) {
                            $originalImage = file_get_contents($fileImage);
                            file_put_contents('images/content-galleries/images/' . $contentMediaFile->file, $originalImage);
                        }
                        $addContentGalleryImage->image = $contentMediaFile->file;
                        array_push($contentGalleryImages, $addContentGalleryImage);
                    }
                }
            }
        }

        $contentGallery = ContentGallery::create($data);

        if (count($contentGalleryCategories) > 0) {
            $contentGallery->contentGalleryCategories()->saveMany($contentGalleryCategories);
        }

        if (count($contentGalleryTags) > 0) {
            $contentGallery->contentGalleryTags()->saveMany($contentGalleryTags);
        }

        if (count($contentGalleryImages) > 0) {
            $contentGallery->contentGalleryImages()->saveMany($contentGalleryImages);
        }

        $contentGallery->load(['contentGalleryCategories', 'contentGalleryTags', 'contentGalleryImages', 'contentGalleryVideos']);

        return $contentGallery;
    }

    /**
     * Delete Content Gallery.
     *
     * @param string $id
     * @return boolean true if deleted otherwise false
     */
    public function delete(string $id): bool
    {
        $contentGallery = ContentGallery::find($id);

        if (empty($contentGallery)) {
            return false;
        }

        $contentGallery->contentGalleryCategories()->delete();
        $contentGallery->contentGalleryTags()->delete();
        $contentGallery->contentGalleryImages()->delete();
        $contentGallery->contentGalleryVideos()->delete();
        $contentGallery->delete($contentGallery);
        return true;
    }

    /**
     * Get Content Gallery Detail By ID.
     *
     * @param string $id
     * @return ContentGallery|null
     */
    public function getByID(string $id): ContentGallery|null
    {
        $contentGallery = ContentGallery::find($id);
        if (is_null($contentGallery)) {
            return null;
        }

        $contentGallery->load(['contentGalleryCategories', 'contentGalleryTags', 'contentGalleryImages', 'contentGalleryVideos']);

        return $contentGallery;
    }

    /**
     * Update Content Gallery By ID.
     *
     * @param string $id
     * @param array $data
     * @return ContentGallery|null
     */
    public function update(string $id, array $data): ContentGallery|null
    {
        $contentGallery = ContentGallery::find($id);

        if (is_null($contentGallery)) {
            return null;
        }

        // Remove existing
        $contentGallery->contentGalleryCategories()->delete();
        $contentGallery->contentGalleryTags()->delete();
        $contentGallery->contentGalleryImages()->delete();
        $contentGallery->contentGalleryVideos()->delete();

        $data['slug'] = $this->createSlug($data['title']);

        if (!empty($data['is_published'])) {
            $data['published_at'] = Carbon::now()->format('Y-m-d H:i');
        } else {
            $data['published_at'] = null;
        }

        $contentGalleryCategories = array();

        // Add
        if (!empty($data['content_gallery_categories'])) {
            $contentCategoryIds = array_column($data['content_gallery_categories'], 'content_category_id');
            $contentCategories = ContentCategory::whereIn('id', $contentCategoryIds)->get()->keyBy('id');

            foreach ($data['content_gallery_categories'] as $contentGalleryCategoryData) {
                $addContentGalleryCategory = new ContentGalleryCategory();
                $contentCategoryId = $contentGalleryCategoryData['content_category_id'];
                $contentCategory = $contentCategories->get($contentCategoryId);
                if ($contentCategory) {
                    $addContentGalleryCategory->contentCategory()->associate($contentCategory);
                    array_push($contentGalleryCategories, $addContentGalleryCategory);
                }
            }
        }

        $contentGalleryTags = array();

        // Add
        if (!empty($data['content_gallery_tags'])) {
            $contentTagIds = array_column($data['content_gallery_tags'], 'content_tag_id');
            $contentTags = ContentTag::whereIn('id', $contentTagIds)->get()->keyBy('id');

            foreach ($data['content_gallery_tags'] as $contentGalleryTagData) {
                $addContentGalleryTag = new ContentGalleryTag();
                $contentTagId = $contentGalleryTagData['content_tag_id'];
                $contentTag = $contentTags->get($contentTagId);
                if ($contentTag) {
                    $addContentGalleryTag->contentTag()->associate($contentTag);
                    array_push($contentGalleryTags, $addContentGalleryTag);
                }
            }
        }

        $contentGalleryImages = array();

        // Add
        if (!empty($data['content_gallery_images'])) {
            $contentMediaFileIds = array_column($data['content_gallery_images'], 'content_media_file_id');
            $contentMediaFiles = ContentMediaFile::whereIn('id', $contentMediaFileIds)->get()->keyBy('id');
            $contentMediaFilesByFile = ContentMediaFile::whereIn('file', $contentMediaFileIds)
                ->get()
                ->keyBy('file');

            foreach ($data['content_gallery_images'] as $contentGalleryImageData) {
                if ($contentGalleryImageData) {
                    $addContentGalleryImage = new ContentGalleryImage();
                    $contentMediaFileId = $contentGalleryImageData['content_media_file_id'];
                    $contentMediaFile = $contentMediaFiles->get($contentMediaFileId);

                    if (!$contentMediaFile) {
                        $contentMediaFile = $contentMediaFilesByFile->get($contentMediaFileId);
                    }

                    if ($contentMediaFile) {
                        $srcImage = 'images/media-files/' . $contentMediaFile->file;
                        $fileImage = new File($srcImage);
                        if ($fileImage) {
                            $originalImage = file_get_contents($fileImage);
                            file_put_contents('images/content-galleries/images/' . $contentMediaFile->file, $originalImage);
                        }
                        $addContentGalleryImage->image = $contentMediaFile->file;
                        array_push($contentGalleryImages, $addContentGalleryImage);
                    }
                }
            }
        }

        // If everything is OK, then update.
        $contentGallery->update($data);

        if (count($contentGalleryCategories) > 0) {
            $contentGallery->contentGalleryCategories()->saveMany($contentGalleryCategories);
        }

        if (count($contentGalleryTags) > 0) {
            $contentGallery->contentGalleryTags()->saveMany($contentGalleryTags);
        }

        if (count($contentGalleryImages) > 0) {
            $contentGallery->contentGalleryImages()->saveMany($contentGalleryImages);
        }

        // Finally return the updated user.
        return $this->getByID($contentGallery->id);
    }
}
