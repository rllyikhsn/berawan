<?php

namespace App\Content\Repositories;

use Illuminate\Contracts\Pagination\Paginator;
use Illuminate\Support\Facades\Auth;

use App\Base\Helpers\UploadHelper;
use App\Base\Interfaces\CrudInterface;
use App\User\Models\User;
use App\Content\Models\ContentReportFinance;

class ContentReportFinanceRepository implements CrudInterface
{
    /**
     * Authenticated User Instance.
     *
     * @var User
     */
    public User|null $user;

    /**
     * Constructor.
     */
    public function __construct()
    {
        $this->user = Auth::guard()->user();
    }

    /**
     * Get All Content Report Finance.
     *
     * @return Paginator Array of ContentReportFinance Collection
     */
    public function getAll()
    {
        return ContentReportFinance::orderBy('title', 'asc')->get();
    }

    /**
     * Get Content Report Finance Datatables.
     *
     * @return collections Array of Datatables ContentReportFinance Collection
     */
    public function getDataTables()
    {
        return ContentReportFinance::select('id', 'created_at', 'title')
            ->orderBy('created_at', 'desc')->get();
    }

    /**
     * Get Paginated Content Report Finance Data.
     *
     * @param int $perPage
     * @param int $page
     * @return Paginator Array of ContentReportFinance Collection
     */
    public function getPaginatedData($perPage, $page): Paginator
    {
        $perPage = isset($perPage) ? intval($perPage) : 5;
        $page = isset($page) ? intval($page) : 1;
        return ContentReportFinance::orderBy('id', 'desc')
            ->paginate($perPage, ['*'], 'page', $page)
            ->appends(['perPage' => $perPage]);
    }

    /**
     * Get Searchable Content Report Finance Data with Pagination.
     *
     * @param string $keyword
     * @param int $perPage
     * @return Paginator Array of ContentReportFinance Collection
     */
    public function searchContentReportFinances($keyword, $perPage): Paginator
    {
        $perPage = isset($perPage) ? intval($perPage) : 10;
        return ContentReportFinance::where('title', 'like', '%' . $keyword . '%')
            ->orderBy('created_at', 'desc')
            ->paginate($perPage);
    }

    /**
     * Create New Content Report Finance.
     *
     * @param array $data
     * @return ContentReportFinance
     */
    public function create(array $data): ContentReportFinance
    {
        if (!empty($data['file'])) {
            $data['file'] = UploadHelper::upload('file', $data['file'], substr(md5(uniqid(mt_rand(), false), false), 0, 20) . time(), 'documents/report/finance');
        }
        return ContentReportFinance::create($data);
    }

    /**
     * Delete Content Report Finance.
     *
     * @param string $id
     * @return boolean true if deleted otherwise false
     */
    public function delete(string $id): bool
    {
        $reportFinance = ContentReportFinance::find($id);
        if (empty($reportFinance)) {
            return false;
        }

        $reportFinance->delete($reportFinance);
        return true;
    }

    /**
     * Get Content Report Finance Detail By ID.
     *
     * @param string $id
     * @return ContentReportFinance|null
     */
    public function getByID(string $id): ContentReportFinance|null
    {
        return ContentReportFinance::find($id);
    }

    /**
     * Update Content Report Finance By ID.
     *
     * @param string $id
     * @param array $data
     * @return ContentReportFinance|null
     */
    public function update(string $id, array $data): ContentReportFinance|null
    {
        $reportFinance = ContentReportFinance::find($id);

        if (is_null($reportFinance)) {
            return null;
        }

        if (!empty($data['file'])) {
            if (!is_null($reportFinance->file)) {
                UploadHelper::deleteFile('documents/report/finance/' . $reportFinance->file);
            }

            $data['file'] = UploadHelper::upload('file', $data['file'], substr(md5(uniqid(mt_rand(), false), false), 0, 20) . time(), 'documents/report/finance');
        }

        // If everything is OK, then update.
        $reportFinance->update($data);

        // Finally return the updated user.
        return $this->getByID($reportFinance->id);
    }
}
