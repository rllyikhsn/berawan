<?php

namespace App\Content\Requests;

use App\Base\Requests\FormRequest;

class ContentAnnouncementRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'title'          => 'required|max:255',
            'short_content'  => 'required',
            'image'          => 'nullable|image|mimes:jpg,png,jpeg,gif,svg|max:2048',
            'start_at'       => 'nullable|date_format:Y-m-d H:i',
            'end_at'         => 'nullable|date_format:Y-m-d H:i|after:start_at',
            'is_draft'       => 'boolean',
            'is_published'   => 'boolean',
            'published_at'   => 'nullable|date_format:Y-m-d H:i',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     * Custom validation message
     */
    public function messages(): array
    {
        return [
            'title.required'          => 'Please give title for content announcement',
            'title.max'               => 'Please give title maximum of 255 characters',
            'short_content.required'  => 'Please give short content for content announcement',
            'image.image'             => 'Please upload an image file',
            'image.mimes'             => 'Please upload a valid image file type (jpg,png,jpeg,gif,svg)',
            'image.max'               => 'Please upload an image file no larger than 2MB',
            'start_at.date_format'    => 'Please give valid format for start date (YYYY-MM-DD HH:mm)',
            'end_at.date_format'      => 'Please give valid format for end date (YYYY-MM-DD HH:mm)',
            'end_at.after'            => 'End date must be after start date',
            'published_at.date_format'=> 'Please give valid format for published date (YYYY-MM-DD HH:mm)',
        ];
    }
}