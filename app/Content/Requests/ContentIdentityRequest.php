<?php

namespace App\Content\Requests;

use App\Base\Requests\FormRequest;

class ContentIdentityRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'nama_instansi'          => 'nullable|max:255',
            'alamat_office'          => 'nullable|max:255',
            'alamat_warehouse'       => 'nullable|max:255',
            'alamat_workshop'        => 'nullable|max:255',
            'no_telepon_office'      => 'nullable|max:255',
            'no_telepon_warehouse'   => 'nullable|max:255',
            'no_telepon_workshop'    => 'nullable|max:255',
            'no_whatsapp'            => 'nullable|max:255',
            'email_office'           => 'nullable|max:255',
            'email_warehouse'        => 'nullable|max:255',
            'email_workshop'         => 'nullable|max:255',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     * Custom validation message
     */
    public function messages(): array
    {
        return [
            'nama_instansi.max'               => 'Please give nama_instansi maximum of 255 characters',
            'alamat_office.max'               => 'Please give alamat_office maximum of 255 characters',
            'alamat_warehouse.max'            => 'Please give alamat_warehouse maximum of 255 characters',
            'alamat_workshop.max'             => 'Please give alamat_workshop maximum of 255 characters',
            'no_telepon_office.max'           => 'Please give no_telepon_office maximum of 255 characters',
            'no_telepon_warehouse.max'        => 'Please give no_telepon_warehouse maximum of 255 characters',
            'no_telepon_workshop.max'         => 'Please give no_telepon_workshop maximum of 255 characters',
            'no_whatsapp.max'                 => 'Please give no_whatsapp maximum of 255 characters',
            'email_office.max'                => 'Please give email_office maximum of 255 characters',
            'email_warehouse.max'             => 'Please give email_warehouse maximum of 255 characters',
            'email_workshop.max'              => 'Please give email_workshop maximum of 255 characters',
        ];
    }
}
