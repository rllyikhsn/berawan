<?php

namespace App\Content\Requests;

use App\Base\Requests\FormRequest;

class ContentLogoIdentityRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'favicon_image'       => 'nullable|max:255',
            'logo_image'          => 'nullable|max:255',
            'logo_front_image'    => 'nullable|max:255',
            'about_image'         => 'nullable|max:255',
            'about_content'       => 'nullable|max:255',
            'footer_image'        => 'nullable|max:255',
            'footer_content'      => 'nullable|max:255',
            'service'             => 'nullable|max:255',
            'service_image'       => 'nullable|max:255',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     * Custom validation message
     */
    public function messages(): array
    {
        return [];
    }
}
