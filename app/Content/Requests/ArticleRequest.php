<?php

namespace App\Content\Requests;

use App\Base\Requests\FormRequest;

class ArticleRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'title'                 => 'required|max:255',
            'slug'                  => 'max:255',
            'short_content'         => 'required|max:255',
            'long_content'          => 'nullable',
            'pinned_image'          => 'nullable|max:255',
            'thumbnail_image'       => 'nullable|max:255',
            'article_categories'    => 'array',
            'article_tags'          => 'array',
            'article_images'        => 'array',
            'is_draft'              => 'boolean',
            'is_published'          => 'boolean',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     * Custom validation message
     */
    public function messages(): array
    {
        return [
            'title.required'            => 'Please give title for article',
            'title.max'                 => 'Please give title maximum of 255 characters',
            'slug.max'                  => 'Please give slug maximum of 255 characters',
            'short_content.required'    => 'Please give short_content for article',
            'short_content.max'         => 'Please give short_content maximum of 255 characters',
            'long_content.required'     => 'Please give long_content for article',
        ];
    }
}
