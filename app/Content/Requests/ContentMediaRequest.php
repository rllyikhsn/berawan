<?php

namespace App\Content\Requests;

use App\Base\Requests\FormRequest;

class ContentContactRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'link_facebook'             => 'nullable|max:255',
            'link_whatsapp'             => 'nullable|max:255',
            'link_whatsapp_template'    => 'nullable|max:255',
            'link_twitter'              => 'nullable|max:255',
            'link_youtube'              => 'nullable|max:255',
            'link_linktr'               => 'nullable|max:255',
            'link_instagram'            => 'nullable|max:255',
            'link_linkedin'             => 'nullable|max:255',
            'link_gmaps'                => 'nullable|max:255',
            'deeplink_playstore'        => 'nullable|max:255',
            'deeplink_appstore'         => 'nullable|max:255',
            'link_brocure'              => 'nullable|max:255',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     * Custom validation message
     */
    public function messages(): array
    {
        return [
            'link_facebook.max'                 => 'Please give contact_person_name_1 maximum of 255 characters',
            'link_whatsapp.max'                 => 'Please give contact_person_phone_1 maximum of 255 characters',
            'link_whatsapp_template.max'        => 'Please give contact_person_whatsapp_1 maximum of 255 characters',
            'link_twitter.max'                  => 'Please give contact_person_email_1 maximum of 255 characters',
            'link_youtube.max'                  => 'Please give contact_person_name_2 maximum of 255 characters',
            'link_linktr.max'                   => 'Please give contact_person_phone_2 maximum of 255 characters',
            'link_instagram.max'                => 'Please give contact_person_whatsapp_2 maximum of 255 characters',
            'link_linkedin.max'                 => 'Please give contact_person_email_2 maximum of 255 characters',
            'link_gmaps.max'                    => 'Please give contact_person_name_3 maximum of 255 characters',
            'deeplink_playstore.max'            => 'Please give contact_person_phone_3 maximum of 255 characters',
            'deeplink_appstore.max'             => 'Please give contact_person_whatsapp_3 maximum of 255 characters',
            'link_brocure.max'                  => 'Please give contact_person_email_3 maximum of 255 characters',
        ];
    }
}
