<?php

namespace App\Content\Requests;

use App\Base\Requests\FormRequest;

class ContentWorkshopRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name'           => 'required|max:255',
            'alamat'         => 'required',
            'no_telepon'     => 'required',
            'is_draft'       => 'boolean',
            'is_published'   => 'boolean',
            'published_at'   => 'nullable|date_format:Y-m-d H:i',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     * Custom validation message
     */
    public function messages(): array
    {
        return [
            'name.required'           => 'Please give name',
            'name.max'                => 'Please give name maximum of 255 characters',
            'alamat.required'         => 'Please give alamat',
            'no_telepon.required'     => 'Please give no_telepon',
        ];
    }
}
