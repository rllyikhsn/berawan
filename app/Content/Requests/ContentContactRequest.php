<?php

namespace App\Content\Requests;

use App\Base\Requests\FormRequest;

class ContentContactRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'contact_person_name_1'       => 'nullable|max:255',
            'contact_person_phone_1'      => 'nullable|max:255',
            'contact_person_whatsapp_1'   => 'nullable|max:255',
            'contact_person_email_1'      => 'nullable|max:255',
            'contact_person_name_2'       => 'nullable|max:255',
            'contact_person_phone_2'      => 'nullable|max:255',
            'contact_person_whatsapp_2'   => 'nullable|max:255',
            'contact_person_email_2'      => 'nullable|max:255',
            'contact_person_name_3'       => 'nullable|max:255',
            'contact_person_phone_3'      => 'nullable|max:255',
            'contact_person_whatsapp_3'   => 'nullable|max:255',
            'contact_person_email_3'      => 'nullable|max:255',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     * Custom validation message
     */
    public function messages(): array
    {
        return [
            'contact_person_name_1.max'                => 'Please give contact_person_name_1 maximum of 255 characters',
            'contact_person_phone_1.max'               => 'Please give contact_person_phone_1 maximum of 255 characters',
            'contact_person_whatsapp_1.max'            => 'Please give contact_person_whatsapp_1 maximum of 255 characters',
            'contact_person_email_1.max'               => 'Please give contact_person_email_1 maximum of 255 characters',
            'contact_person_name_2.max'                => 'Please give contact_person_name_2 maximum of 255 characters',
            'contact_person_phone_2.max'               => 'Please give contact_person_phone_2 maximum of 255 characters',
            'contact_person_whatsapp_2.max'            => 'Please give contact_person_whatsapp_2 maximum of 255 characters',
            'contact_person_email_2.max'               => 'Please give contact_person_email_2 maximum of 255 characters',
            'contact_person_name_3.max'                => 'Please give contact_person_name_3 maximum of 255 characters',
            'contact_person_phone_3.max'               => 'Please give contact_person_phone_3 maximum of 255 characters',
            'contact_person_whatsapp_3.max'            => 'Please give contact_person_whatsapp_3 maximum of 255 characters',
            'contact_person_email_3.max'               => 'Please give contact_person_email_3 maximum of 255 characters',
        ];
    }
}
