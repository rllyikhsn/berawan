<?php

namespace App\Content\Requests;

use App\Base\Requests\FormRequest;

class ContentClientRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name'                     => 'required|max:255',
            'pinned_image'             => 'nullable|max:255',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     * Custom validation message
     */
    public function messages(): array
    {
        return [
            'name.required'             => 'Please give a name',
            'name.max'                  => 'Please give a name maximum of 255 characters',
        ];
    }
}
