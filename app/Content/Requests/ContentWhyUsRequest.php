<?php

namespace App\Content\Requests;

use App\Base\Requests\FormRequest;

class ContentWhyUsRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name_reviewer'     => 'required|max:255',
            'title'             => 'required|max:255',
            'short_content'     => 'required|max:255',
            'stars'             => 'required',
            'job'               => 'required|max:255',
            'avatar_image'      => 'nullable',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     * Custom validation message
     */
    public function messages(): array
    {
        return [
            'name_reviewer.required'    => 'Please give name_reviewer',
            'name_reviewer.max'         => 'Please give name_reviewer maximum of 255 characters',
            'title.required'            => 'Please give title',
            'title.max'                 => 'Please give title maximum of 255 characters',
            'short_content.required'    => 'Please give short_content',
            'short_content.max'         => 'Please give short_content maximum of 255 characters',
            'stars.required'            => 'Please give stars',
            'job.required'              => 'Please give job',
            'job.max'                   => 'Please give job maximum of 255 characters',
        ];
    }
}
