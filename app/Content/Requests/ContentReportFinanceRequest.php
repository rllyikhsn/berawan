<?php

namespace App\Content\Requests;

use App\Base\Requests\FormRequest;

class ContentReportFinanceRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'title'         => 'required|max:255',
            'file'          => 'required|file|mimes:pdf',
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * @return array
     * Custom validation message
     */
    public function messages(): array
    {
        return [
            'title.required'          => 'Please give title for content report finance',
            'title.max'               => 'Please give title maximum of 255 characters',
        ];
    }
}
