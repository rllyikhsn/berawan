<?php

namespace App\Frontend\Controllers;

use App\Base\Controllers\Controller;
use App\Content\Services\ArticleServices;

use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Article Repository class.
     *
     * @var ArticleServices
     */
    public $contentArticleServices;

    public function __construct(ArticleServices $contentArticleServices)
    {
        $this->contentArticleServices = $contentArticleServices;
    }

    public function index(Request $request)
    {
        $search = $request->input('search', '');
        $perPage = 2;
        $page = 1;
        $data = $this->contentArticleServices->searchArticle($search, $perPage, $page);

        $isNext = $page < $data->lastPage() ? true : false;
        $nextPage = $page + 1;
        $nextUrl = "/informations/blogs/page/" . $page + 1 . (!empty($search) ? "?search=" . $search : "");
        $isPrev = $page > 1 && $data->lastPage() != 1 ? true : false;
        $prevUrl = "/informations/blogs/page/" . $page - 1 . (!empty($search) ? "?search=" . $search : "");

        return view('frontend.pages.blog.blog', compact('data', 'isNext', 'nextUrl', 'isPrev', 'prevUrl'));
    }

    public function page($page, Request $request)
    {
        $search = $request->input('search', '');
        $perPage = 2;
        $data = $this->contentArticleServices->searchArticle($search, $perPage, $page);

        $isNext = $page < $data->lastPage() ? true : false;
        $nextUrl = "/informations/blogs/page/" . $page + 1 . (!empty($search) ? "?search=" . $search : "");
        $isPrev = $page > 1 && $data->lastPage() != 1 ? true : false;
        $prevUrl = "/informations/blogs/page/" . $page - 1 . (!empty($search) ? "?search=" . $search : "");

        return view('frontend.pages.blog.blog', compact('data', 'isNext', 'nextUrl', 'isPrev', 'prevUrl'));
    }
}
