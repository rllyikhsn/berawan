<?php

namespace App\Frontend\Controllers;

use App\Base\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        return view('frontend.pages.home');
    }
}
