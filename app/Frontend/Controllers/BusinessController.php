<?php

namespace App\Frontend\Controllers;

use App\Base\Controllers\Controller;

class BusinessController extends Controller
{
    public function digital()
    {
        return view('frontend.pages.business.digital');
    }

    public function procurement()
    {
        return view('frontend.pages.business.procurement');
    }

    public function merchant()
    {
        return view('frontend.pages.business.merchant');
    }
}
