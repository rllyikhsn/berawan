<?php

namespace App\Frontend\Controllers;

use App\Base\Controllers\Controller;

class ContactController extends Controller
{
    public function business()
    {
        return view('frontend.pages.contacts.business');
    }
}
