<?php

namespace App\Frontend\Controllers;

use App\Base\Controllers\Controller;

use Illuminate\Http\Request;

class PortfolioController extends Controller
{
    public function index(Request $request)
    {
        $search = $request->input('search', '');
        $perPage = 2;
        $page = 1;
        $data = null;

        $isNext = $page < $data->lastPage() ? true : false;
        $nextPage = $page + 1;
        $nextUrl = "/portfolio/page/" . $page + 1 . (!empty($search) ? "?search=" . $search : "");
        $isPrev = $page > 1 && $data->lastPage() != 1 ? true : false;
        $prevUrl = "/blog/page/" . $page - 1 . (!empty($search) ? "?search=" . $search : "");

        return view('frontend.pages.blog.blog', compact('data', 'isNext', 'nextUrl', 'isPrev', 'prevUrl'));
    }

    public function page($page, Request $request)
    {
        $search = $request->input('search', '');
        $perPage = 2;
        $data = $this->contentArticleServices->searchArticle($search, $perPage, $page);

        $isNext = $page < $data->lastPage() ? true : false;
        $nextUrl = "/blog/page/" . $page + 1 . (!empty($search) ? "?search=" . $search : "");
        $isPrev = $page > 1 && $data->lastPage() != 1 ? true : false;
        $prevUrl = "/blog/page/" . $page - 1 . (!empty($search) ? "?search=" . $search : "");

        return view('frontend.pages.blog.blog', compact('data', 'isNext', 'nextUrl', 'isPrev', 'prevUrl'));
    }
}
