<?php

namespace App\Frontend\Controllers;

use App\Base\Controllers\Controller;

class AboutUsController extends Controller
{
    public function index()
    {
        return view('frontend.pages.about-us');
    }

    public function privacy()
    {
        return view('frontend.pages.privacy');
    }

    public function terms()
    {
        return view('frontend.pages.terms');
    }
}
