<?php

namespace App\Frontend\Providers;

use Illuminate\Support\ServiceProvider;

class FrontendServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        // Register the FrontendRepository class
        // $this->app->bind(
        //     \App\Frontend\Repositories\FrontendRepository::class
        // );
        
        // // Register the FrontendService class
        // $this->app->bind(
        //     \App\Frontend\Services\FrontendService::class
        // );
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
