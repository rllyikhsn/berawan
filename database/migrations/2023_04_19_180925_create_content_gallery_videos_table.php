<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('content_gallery_videos', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->uuid('content_gallery_id')->nullable();
            $table->foreign('content_gallery_id')->references('id')->on('content_galleries')->onDelete('cascade');
            $table->string('title')->nullable();
            $table->string('permalink')->nullable();
            $table->boolean('is_external')->default(true);
            $table->string('video')->nullable();
            $table->timestamps();
            $table->uuid('created_by')->nullable();
            $table->uuid('updated_by')->nullable();
            $table->uuid('deleted_by')->nullable();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('deleted_by')->references('id')->on('users')->onDelete('cascade');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('content_gallery_videos');
    }
};
