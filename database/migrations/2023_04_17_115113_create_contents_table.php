<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->string('nama_instansi');
            $table->string('alamat_office')->nullable();
            $table->string('alamat_warehouse')->nullable();
            $table->string('alamat_workshop')->nullable();
            $table->string('no_telepon_office')->nullable();
            $table->string('no_telepon_warehouse')->nullable();
            $table->string('no_telepon_workshop')->nullable();
            $table->string('no_whatsapp')->nullable();
            $table->string('email_office')->nullable();
            $table->string('email_warehouse')->nullable();
            $table->string('email_workshop')->nullable();
            $table->string('contact_person_name_1')->nullable();
            $table->string('contact_person_phone_1')->nullable();
            $table->string('contact_person_whatsapp_1')->nullable();
            $table->string('contact_person_email_1')->nullable();
            $table->string('contact_person_name_2')->nullable();
            $table->string('contact_person_phone_2')->nullable();
            $table->string('contact_person_whatsapp_2')->nullable();
            $table->string('contact_person_email_2')->nullable();
            $table->string('contact_person_name_3')->nullable();
            $table->string('contact_person_phone_3')->nullable();
            $table->string('contact_person_whatsapp_3')->nullable();
            $table->string('contact_person_email_3')->nullable();
            $table->string('favicon_image')->nullable();
            $table->string('logo_image')->nullable();
            $table->string('logo_front_image')->nullable();
            $table->string('about_image')->nullable();
            $table->text('about_content')->nullable();
            $table->string('footer_image')->nullable();
            $table->text('footer_content')->nullable();
            $table->string('service')->nullable();
            $table->string('service_image')->nullable();
            $table->string('link_facebook')->nullable();
            $table->string('link_whatsapp')->nullable();
            $table->string('link_whatsapp_template')->nullable();
            $table->string('link_twitter')->nullable();
            $table->string('link_youtube')->nullable();
            $table->string('link_linktr')->nullable();
            $table->string('link_instagram')->nullable();
            $table->string('link_linkedin')->nullable();
            $table->string('link_gmaps')->nullable();
            $table->string('deeplink_playstore')->nullable();
            $table->string('deeplink_appstore')->nullable();
            $table->string('link_brocure')->nullable();
            $table->string('copyright')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contents');
    }
};
