<?php

namespace Database\Seeders;

use App\Content\Models\Content;
use Database\Factories\Content\ContentFactory;
use Illuminate\Database\Seeder;
use Database\Factories\User\UserFactory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class ContentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contents')->delete();
        $data = [
            'id' => '9b6f35e0-fe41-4da3-aeed-82ce6d24556c',
            'nama_instansi' => 'Solid Surface',
            'alamat_office' => 'Jl bersama',
            'alamat_warehouse' => 'Jl bersama',
            'alamat_workshop' => 'Jl bersama',
            'no_telepon_office' => '0822298765855',
            'no_telepon_warehouse' => '0822298765855',
            'no_telepon_workshop' => '0822298765855',
            'no_whatsapp' => '0822298765855',
            'email_office' => 'office@gmail.com',
            'email_warehouse' => 'office@gmail.com',
            'email_workshop' => 'office@gmail.com',
            'contact_person_name_1' => 'office@gmail.com',
            'contact_person_phone_1' => 'office@gmail.com',
            'contact_person_whatsapp_1' => 'office@gmail.com',
            'contact_person_email_1' => 'office@gmail.com',
            'copyright' => 'office@gmail.com',
        ];
        Content::create($data);

        // ContentFactory::new()->count(1)->create();
    }
}
