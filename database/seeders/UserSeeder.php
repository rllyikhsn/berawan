<?php

namespace Database\Seeders;

use App\User\Models\User;
use Illuminate\Database\Seeder;
use Database\Factories\User\UserFactory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        $data = [
            'name' => 'Maniruzzaman Akash',
            'username' => 'maniruza',
            'phone_number' => '082298765432',
            'email' => 'admin@example.com',
            'email_verified_at' => now(),
            'password' => Hash::make('123456')
        ];
        User::create($data);

        // Testing Dummy User
        UserFactory::new()->count(100)->create();
    }
}
