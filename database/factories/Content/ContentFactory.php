<?php

namespace Database\Factories\Content;

use App\Content\Models\Content;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class ContentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Content::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'nama_instansi' => $this->faker->name,
            'alamat_office' => $this->faker->name,
            'alamat_warehouse' => $this->faker->name,
            'alamat_workshop' => $this->faker->name,
            'no_telepon_office' => $this->faker->name,
            'no_telepon_warehouse' => $this->faker->name,
            'no_telepon_workshop' => $this->faker->name,
            'no_whatsapp' => $this->faker->name,
            'email_office' => $this->faker->unique()->safeEmail,
            'email_warehouse' => $this->faker->unique()->safeEmail,
            'email_workshop' => $this->faker->unique()->safeEmail,
            'contact_person_name_1' => $this->faker->name,
            'contact_person_phone_1' => $this->faker->name,
            'contact_person_whatsapp_1' => $this->faker->name,
            'contact_person_email_1' => $this->faker->name,
            'copyright' => $this->faker->name,
        ];
    }
}
